﻿using CE.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Recharge.Admin.SysRoleMamage
{
    public partial class frmUpdateRole : BasePageClass
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

            }
        }

        [WebMethod]
        public static JsonResult<Model.SYS_Role> GetRoleInfoByID(long roleID)
        {
            return new JsonResult<Model.SYS_Role>(new BLL.SYS_Role().GetModel(roleID));
        }
        [WebMethod]
        public static JsonResult<bool> UpdateRoleInfoById(Model.SYS_Role model)
        {
            if (new BLL.SYS_Role().Update(model))
            {
                AddOperationLog($"【修改角色】ID:{model.ID},{Newtonsoft.Json.JsonConvert.SerializeObject(model)}");
                return new JsonResult<bool>(true, "修改成功");
            }
            else
            {
                return new JsonResult<bool>(false, "修改失败");
            }
        }
    }
}