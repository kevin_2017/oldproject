﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmAssingAuth.aspx.cs" Inherits="Recharge.Admin.SysRoleMamage.frmAssingAuth" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <!-- Bootstrap 3.3.4 -->
    <link href="/Content/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="/Content/dist/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="/Content/dist/css/ionicons.min.css" rel="stylesheet" type="text/css" />

    <!-- Theme style -->
    <link href="/Content/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins 
         folder instead of downloading all of them to reduce the load. -->
    <link href="/Content/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
    <script src="/Content/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="/Content/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

    <!-- SlimScroll -->
    <script src="/Content/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src='/Content/plugins/fastclick/fastclick.min.js'></script>
    <!-- AdminLTE App -->
    <script src="/Content/dist/js/app.min.js" type="text/javascript"></script>
    <script src="/Content/dist/js/demo.js" type="text/javascript"></script>

    <script src="/Content/layer/layer.js"></script>
    <script src="/Content/jquery.json.min.js"></script>
    <script>
        function getQueryString(name) {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
            var r = window.location.search.substr(1).match(reg);
            if (r != null) return unescape(r[2]);
            return null;
        };
        function operateOneMenu(box) {
            var id = box.id;
            var boxList = document.getElementsByName("linkList");
            var btnlist = document.getElementsByName("btnIdList");
            for (var i = 0; i < boxList.length; i++) {
                if (boxList[i].id.split("_")[0] == id) {
                    boxList[i].checked = box.checked;
                    //选中子菜单下面的所有按钮
                    if (btnlist.length > 0) {
                        for (var j = 0; j < btnlist.length; j++) {
                            if (btnlist[j].id.split("_")[0] == boxList[i].id.split("_")[1]) {
                                btnlist[j].checked = boxList[i].checked;
                            }
                        }
                    }

                }
            }
        };
        function operateTwoMenu(box) {
            var boxList = document.getElementsByName("linkList");
            var btnlist = document.getElementsByName("btnIdList");
            var flage = false;

            for (var i = 0; i < boxList.length; i++) {
                if (boxList[i].id.split("_")[0] == box.id.split("_")[0]) {

                    if (boxList[i].id.split("_").length == 2 && boxList[i].checked == true) {

                        if (btnlist.length > 0) {
                            for (var j = 0; j < btnlist.length; j++) {
                                if (btnlist[j].id.split("_")[0] == box.id.split("_")[1]) {
                                    btnlist[j].checked = box.checked;
                                }
                            }
                        }
                        flage = true;
                    } else {
                        if (btnlist.length > 0) {
                            for (var j = 0; j < btnlist.length; j++) {
                                if (btnlist[j].id.split("_")[0] == box.id.split("_")[1]) {
                                    btnlist[j].checked = box.checked;
                                }
                            }
                        }

                    }

                }
            }
            var pbox = document.getElementById(box.id.split("_")[0]);
            pbox.checked = flage;
        };

        function operationBtnMenu(box) {
            var boxList = document.getElementsByName("linkList");
            var btnlist = document.getElementsByName("btnIdList");

            for (var j = 0; j < btnlist.length; j++) {
                if (btnlist[j].checked == true) {


                    for (var i = 0; i < boxList.length; i++) {
                        if (box.id.split("_")[0] == boxList[i].id.split("_")[1]) {
                            var parentTow = document.getElementById(boxList[i].id);
                            parentTow.checked = true;
                            var pbox = document.getElementById(parentTow.id.split("_")[0]);
                            pbox.checked = true;
                        }
                    }
                }
            }
        };



     

        function submitForm() {

            var hiddenRoleID = document.getElementById("<%=hfAuthoity.ClientID %>");
        
            var hfAuthBtnID = document.getElementById("<%=hfAuthBtn.ClientID %>");
            var boxList = document.getElementsByName("linkList");

            var btnlist = document.getElementsByName("btnIdList");
            var btnRightList = "";
            var rightList = "";
            for (var i = 0; i < boxList.length; i++) {
                if (boxList[i].checked == true) {
                    rightList += boxList[i].value + "_";
                }
            }
          
            hiddenRoleID.value = rightList;
            for (var i = 0; i < btnlist.length; i++) {
                if (btnlist[i].checked == true) {
                    btnRightList += btnlist[i].id + ",";
                }
            }

            hfAuthBtnID.value = btnRightList;
        }

        function chk(checkbox) {
            var item = document.getElementsByTagName("input");
            for (i = 0; i < item.length; i++) {
                if (item[i].type == "checkbox") {
                    item[i].checked = checkbox.checked;
                }
            }
        }

        function doPostBackCheck() {
            var o = window.event.srcElement;
            if (o.tagName == "INPUT" && o.type == "checkbox") {
                __doPostBack("", "");
            }
        }

    </script>
</head>
<body class="skin-blue">

    <form id="form1" runat="server">
        <section class="content">
            <div class="row">
                <div class="box box-info">
                    <div class="box-header">
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered table-striped mb-none" id="datatable-tabletools">
                            <thead>
                                <tr>
                                    <th>
                                        <asp:CheckBox ID="chkAll" runat="server" onclick="chk(this)" Text="序号" CssClass="checkall" />
                                    </th>
                                    <th>权限名
                                    </th>
                                    <th>状态
                                    </th>
                                    <th>按钮
                                    </th>
                                </tr>
                            </thead>

                            <tbody>
                                <asp:Literal runat="server" ID="ltRoleInfo"></asp:Literal>
                            </tbody>
                        </table>
                    </div>
                    <div class="box-footer">
                        <asp:HiddenField runat="server" ID="hfAuthoity" />
                        <asp:HiddenField runat="server" ID="hfAuthBtn" /> 
                        <asp:Button runat="server" ID="btnSave" Text="保存" CssClass="btn btn-primary" OnClick="btnSave_Click"
                                            OnClientClick="submitForm()" /> 
                    </div>
                </div>
            </div>
        </section>
    </form>
</body>
</html>
