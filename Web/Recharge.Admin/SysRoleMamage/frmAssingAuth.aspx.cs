﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Recharge.Admin.SysRoleMamage
{
    public partial class frmAssingAuth : BasePageClass
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitPage();
            }
        }
        BLL.SYS_AuthorityInfo AuthIBLL = new BLL.SYS_AuthorityInfo();
        BLL.SYS_AuthButton AuthBBLL = new BLL.SYS_AuthButton();
        BLL.SYS_Role_Authority RABLL = new BLL.SYS_Role_Authority();
        BLL.SYS_Role_Auth_AuthButton RAABBLL = new BLL.SYS_Role_Auth_AuthButton();
        public void InitPage()
        {
            DataTable alldt = new DataTable();
            Model.StrWhere swh = new Model.StrWhere
            {
                IsWhereExist = true,
                strWhere = new System.Text.StringBuilder("ParentID=0 and IsDelete=0")
            };
            alldt = AuthIBLL.GetDataSet(swh).Tables[0];
            DataTable roleDt = new DataTable();
            swh = new Model.StrWhere();
            swh.IsWhereExist = true;
            swh.strWhere = new System.Text.StringBuilder(" RoleID=" + Request.QueryString["id"].ToString());
            roleDt = RABLL.GetDataSet(swh).Tables[0];
            DataTable sondt = new DataTable();

            string Html = "";

            //顶级菜单页面
            for (int i = 0; i < alldt.Rows.Count; i++)
            {
                if (roleDt.Select("AuthorityID=" + alldt.Rows[i]["ID"].ToString()).Length > 0)
                {
                    Html += "<tr><td><input type=\"checkbox\" name=\"linkList\" id=\"" + alldt.Rows[i]["ID"] +
                            "\" checked=\"checked\" class=\"checker\"  onclick=\"operateOneMenu(this)\"  value=\"" +
                            alldt.Rows[i]["ID"] + "\" />" + (i + 1).ToString() + "</td><td>" +
                            alldt.Rows[i]["TypeName"].ToString() + "</td><td>" +
                            string.Concat(alldt.Rows[i]["ISEnable"].ToString() == "True"
                                ? "<font class=\"good\">正常</font>"
                                : "<font style=\"color:red\">停用</font>") + "</td><td>" +
                            GetButton(alldt.Rows[i]["ID"].ToString()) + "</td></tr>";
                }
                else
                {
                    Html += "<tr><td><input class=\"checker\" type=\"checkbox\" name=\"linkList\" id=\"" +
                            alldt.Rows[i]["ID"] + "\"  onclick=\"operateOneMenu(this)\"  value=\"" +
                            alldt.Rows[i]["ID"] + "\" />" + (i + 1).ToString() + "</td><td>" +
                            alldt.Rows[i]["TypeName"].ToString() + "</td><td>" +
                            string.Concat(alldt.Rows[i]["ISEnable"].ToString() == "True"
                                ? "<font class=\"good\">正常</font>"
                                : "<font style=\"color:red\">停用</font>") + "</td><td>" +
                            GetButton(alldt.Rows[i]["ID"].ToString()) + "</td></tr>";
                }
                //子级菜单

                swh = new Model.StrWhere();
                swh.IsWhereExist = true;
                swh.strWhere = new System.Text.StringBuilder("ParentID=" + alldt.Rows[i]["ID"].ToString());
                sondt = AuthIBLL.GetDataSet(swh).Tables[0];
                if (sondt.Rows.Count > 0)
                {
                    for (int j = 0; j < sondt.Rows.Count; j++)
                    {
                        if (roleDt.Select("AuthorityID=" + sondt.Rows[j]["ID"].ToString()).Length > 0)
                        {
                            Html += "<tr><td><input class=\"checker\" type=\"checkbox\" name=\"linkList\" id=\"" +
                                    alldt.Rows[i]["ID"] + "_" + sondt.Rows[j]["ID"] +
                                    "\" checked=\"checked\"  onclick=\"operateTwoMenu(this)\"   value=\"" +
                                    sondt.Rows[j]["ID"] +
                                    "\" /></td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
                                    sondt.Rows[j]["TypeName"].ToString() + "</td><td>" +
                                    string.Concat(sondt.Rows[j]["ISEnable"].ToString() == "True"
                                        ? "<font class=\"good\">正常</font>"
                                        : "<font style=\"color:red\">停用</font>") + "</td><td>" +
                                    GetButton(sondt.Rows[j]["ID"].ToString()) + "</td></tr>";
                        }
                        else
                        {
                            Html += "<tr><td><input class=\"checker\" type=\"checkbox\" name=\"linkList\" id=\"" +
                                    alldt.Rows[i]["ID"] + "_" + sondt.Rows[j]["ID"] +
                                    "\"  onclick=\"operateTwoMenu(this)\"   value=\"" + sondt.Rows[j]["ID"] +
                                    "\" /></td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
                                    sondt.Rows[j]["TypeName"].ToString() + "</td><td>" +
                                    string.Concat(sondt.Rows[j]["ISEnable"].ToString() == "True"
                                        ? "<font class=\"good\">正常</font>"
                                        : "<font style=\"color:red\">停用</font>") + "</td><td>" +
                                    GetButton(sondt.Rows[j]["ID"].ToString()) + "</td></tr>";
                        }
                    }
                }
            }

            this.ltRoleInfo.Text = Html;
        }


        /// <summary>
        /// 获取当前的权限的所有按钮
        /// </summary>
        /// <param name="authID"></param>
        /// <returns></returns>
        public string GetButton(string authID)
        {
            string btnStr = "";
            DataTable dt = new DataTable();
            Model.StrWhere swh = new Model.StrWhere();
            swh.IsWhereExist = true;
            swh.strWhere = new System.Text.StringBuilder($"AuthID ={authID} and IsEnable=1" );
            dt = AuthBBLL.GetDataSet(swh).Tables[0];
            if (dt.Rows.Count > 0)
            {
                int rows = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    ++rows;
                    DataSet ds = new DataSet();
                    ds = RAABBLL.GetRolesButton(" and  ABID=" + dr["ID"].ToString() + " and RoleID=" + Request.QueryString["id"].ToString());
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        btnStr += "<input type=\"checkbox\" class=\"checker\" name=\"btnIdList\" id=\"" + string.Concat(dr["AuthID"].ToString(), "_", dr["ID"].ToString()) + "\" checked=\"checked\" onclick=\"operationBtnMenu(this)\"  value=\"" + dr["ID"].ToString() + "\" />" + dr["DisplayName"].ToString() + "&nbsp;&nbsp;&nbsp;&nbsp;";
                    }
                    else
                    {
                        btnStr += "<input type=\"checkbox\" class=\"checker\" name=\"btnIdList\" id=\"" + string.Concat(dr["AuthID"].ToString(), "_", dr["ID"].ToString()) + "\" onclick=\"operationBtnMenu(this)\"  value=\"" + dr["ID"].ToString() + "\" />" + dr["DisplayName"].ToString() + "&nbsp;&nbsp;&nbsp;&nbsp;";
                    }
                    if (rows == 6)
                    {
                        btnStr += "<br/>";
                    }
                }
            }
            return btnStr;
        }



        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {


                string[] authList = hfAuthoity.Value.Split(new string[] { "_" }, StringSplitOptions.RemoveEmptyEntries);
                string[] btnList = hfAuthBtn.Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                if (authList.Length == 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "", "  layer.alert('请选择要分配的权限！',{ icon: 2 });", true);

                }
                else
                {
                    RABLL.DeleteByRoleID(int.Parse(Request.QueryString["id"].ToString()));
                    foreach (string auth in authList)
                    {
                        Model.SYS_Role_Authority model = new Model.SYS_Role_Authority();

                        model.AuthorityID = long.Parse(auth);
                        model.RoleID = int.Parse(Request.QueryString["id"].ToString());
                        RABLL.Add(model);
                    }
                    if (btnList.Length > 0)
                    {

                        foreach (string btnID in btnList)
                        {
                            string[] IDS = btnID.Split(new string[] { "_" }, StringSplitOptions.RemoveEmptyEntries);
                            if (IDS.Length == 2)
                            {
                                Model.AddRole_AuthButton abModel = new Model.AddRole_AuthButton();

                                abModel.ABID = long.Parse(IDS[1].ToString());
                                abModel.Aid = long.Parse(IDS[0].ToString());
                                abModel.RID = int.Parse(Request.QueryString["id"].ToString());

                                RAABBLL.Add(abModel);

                            }
                            continue;

                        }

                    }

                }

                Model.SYS_OperatLog SOModel = new Model.SYS_OperatLog();
                SOModel.UserID = SystemUserInfo.ID;
                SOModel.OperateIP = HttpContext.Current.Request.UserHostAddress == "::1" ? "127.0.0.1" : HttpContext.Current.Request.UserHostAddress;
                SOModel.OperateDescriptions = "【修改角色权限】角色ID:" + Request.QueryString["id"].ToString();

                new BLL.SYS_OperatLog().Add(SOModel);



                InitPage();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "", "  layer.alert('分配成功！',{icon:1});", true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "", " layer.alert( '分配失败！',{ icon: 2 });", true);

            }


        }
    }
}