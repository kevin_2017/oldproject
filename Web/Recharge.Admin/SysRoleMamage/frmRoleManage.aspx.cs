﻿using CE.Utility;
using Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Recharge.Admin.SysRoleMamage
{
    public partial class frmRoleManage : BasePageClass
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                AspNetPager1.CurrentPageIndex = 1;
                InitPage();
            }
        }
        public void InitPage()
        {

            PageAttribute page = new PageAttribute();
            page.TableName = "SYS_Role";
            page.Columns = @" ID, RoleName, Remark, ISSystem";
            page.StrOrder = " ID asc";
            page.WhereCondition = string.Concat(" and isdelete=0 ", StrWhere);

            page.PageIndex = AspNetPager1.CurrentPageIndex - 1;
            page.PageSize = AspNetPager1.PageSize;
            DataTable dt = new BLL.SYS_Role().GetListByPage(page);
            AspNetPager1.RecordCount = page.TotalRowCount.Value;
            repList.DataSource = dt;
            repList.DataBind();
        }
        protected void AspNetPager1_PageChanging(object src, Wuqi.Webdiyer.PageChangingEventArgs e)
        {
            AspNetPager1.CurrentPageIndex = e.NewPageIndex;
            InitPage();
        }
        [WebMethod]
        public static JsonResult<bool> GetUserRole(string path, string btnName)
        {
            if (GetButtonRole(path, btnName))
            {
                return new JsonResult<bool>(true, "拥有此权限！");
            }
            else
            {
                return new JsonResult<bool>(false, "对不起，您没有此权限！");
            }
        }
    }
}