﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="frmRoleManage.aspx.cs" Inherits="Recharge.Admin.SysRoleMamage.frmRoleManage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/Content/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
    <script>
        var urlPath = window.location.pathname;//

        function UpdateRoleInfo(roleId, isSystem) {
            if (isSystem == 1) {
                layer.alert('对不起，系统角色不能进行修改！', { icon: 2 }, function () { layer.closeAll(); });
            } else {
                $.ajax({
                    type: "POST",
                    url: "frmRoleManage.aspx/GetUserRole",
                    async: false,
                    data: $.toJSON({ path: urlPath, btnName: 'btnUpdate' }),
                    contentType: "application/json;charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        var index = layer.load(1, {
                            shade: [0.8, '#fff'], //0.1透明度的白色背景 
                        });
                    },
                    success: function (data) {
                        if (data.d.Success) {
                            layer.open({
                                type: 2,
                                title: "修改角色信息",
                                shadeClose: false,
                                shade: 0.8,
                                area: ['800px', '400px'],
                                content: "frmUpdateRole.aspx?id=" + roleId,
                                close: function (index) {
                                    window.location.reload();
                                },
                                cancel: function (index) {
                                    window.location.reload();
                                }
                            });
                        } else {
                            layer.alert('对不起，您没有此权限！', { icon: 2 }, function () { layer.closeAll(); });
                        }

                    },
                    error: function (err) {
                        layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                    }
                });
            }
        }

        function AssignRoleAuth(roleId, roleName) {  
            $.ajax({
                type: "POST",
                url: "frmRoleManage.aspx/GetUserRole",
                async: false,
                data: $.toJSON({ path: urlPath, btnName: 'btnAssign' }),
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    var index = layer.load(1, {
                        shade: [0.8, '#fff'], //0.1透明度的白色背景 
                    });
                },
                success: function (data) {
                    if (data.d) {
                        layer.open({
                            type: 2,
                            title: "修改【 " + roleName + " 】权限",
                            shadeClose: false,
                            shade: 0.8,
                            area: [($(window).width() - 50) + 'px', ($(window).height() - 50) + 'px'],
                            content: "frmAssingAuth.aspx?id=" + roleId,
                            close: function (index) {
                                window.location.reload();
                            },
                            cancel: function (index) {
                                window.location.reload();
                            }
                        });
                    } else {
                        layer.alert('对不起，您没有此权限！', { icon: 2 }, function () { layer.closeAll(); });
                    }

                },
                error: function (err) {
                    layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                }
            });

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="col-xs-12">
        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                    </div>

                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>序号</th>
                                    <th>角色名</th>
                                    <th>是否为系统角色</th>
                                    <th>操作</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater runat="server" ID="repList">
                                    <ItemTemplate>
                                        <tr>
                                            <td><%#Eval("ID") %></td>
                                            <td><%#Eval("RoleName") %></td>
                                            <td><%# Convert.ToInt32( Eval("ISSystem"))==1?"<span class=\"btn btn-success\" disabled=\"disabled\">系统角色</span>":"<span class=\"btn btn-info \" disabled=\"disabled\">一般角色</span>" %></td>
                                            <td>
                                                <a class="btn btn-success" onclick='UpdateRoleInfo(<%#Eval("ID")%>,<%# Convert.ToInt32( Eval("ISSystem")) %>)' href="javascript:void(0)">编辑</a>
                                                <a class="btn btn-info" onclick='AssignRoleAuth(<%#Eval("ID")%>,"<%#Eval("RoleName") %>")' href="javascript:void(0)">权限管理</a>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>

                        </table>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-5">
                        <div class="dataTables_info" id="example2_info1" role="status" aria-live="polite">总计 <%=AspNetPager1.RecordCount %>条记录，每页显示<%=AspNetPager1.PageSize %>条 </div>
                    </div>
                    <div class="col-sm-7">
                        <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate1">
                            <webdiyer:AspNetPager runat="server" ID="AspNetPager1" Width="100%" PageSize="16" FirstPageText="首页" LastPageText="末页" PrevPageText="上一页" NextPageText="下一页" PagingButtonsClass="paginate_button" CssClass="pagination" LayoutType="Ul" CurrentPageButtonClass="active" OnPageChanging="AspNetPager1_PageChanging" AlwaysShow="true" FirstLastButtonsClass="paginate_button" PagingButtonLayoutType="UnorderedList"></webdiyer:AspNetPager>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</asp:Content>
