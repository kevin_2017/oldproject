﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmUpdateRole.aspx.cs" Inherits="Recharge.Admin.SysRoleMamage.frmUpdateRole" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <!-- Bootstrap 3.3.4 -->
    <link href="/Content/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="/Content/dist/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="/Content/dist/css/ionicons.min.css" rel="stylesheet" type="text/css" />

    <!-- Theme style -->
    <link href="/Content/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins 
         folder instead of downloading all of them to reduce the load. -->
    <link href="/Content/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
    <script src="/Content/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="/Content/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

    <!-- SlimScroll -->
    <script src="/Content/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src='/Content/plugins/fastclick/fastclick.min.js'></script>
    <!-- AdminLTE App -->
    <script src="/Content/dist/js/app.min.js" type="text/javascript"></script>
    <script src="/Content/dist/js/demo.js" type="text/javascript"></script>

    <script src="/Content/layer/layer.js"></script>
    <script src="/Content/jquery.json.min.js"></script>
    <script>
        var model = {

        };
        function getQueryString(name) {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
            var r = window.location.search.substr(1).match(reg);
            if (r != null) return unescape(r[2]);
            return null;
        }

        $(function () {
            $.ajax({
                type: "POST",
                url: "frmUpdateRole.aspx/GetRoleInfoByID",
                async: false,
                data: $.toJSON({ roleID: getQueryString("id") }),
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    var index = layer.load(1, {
                        shade: [0.8, '#fff'] //0.1透明度的白色背景 
                    });
                },
                success: function (data) {
                    if (data.d.Rows.length > 0) {
                        model = data.d.Rows[0];
                        if (model != null) {
                            $("#txtRoleName").val(model.RoleName);
                            $("#txtRemark").val(model.Remark);
                            $("#selectISSystem option[value='" + (model.ISSystem == false ? 0 : 1) + "']").attr("selected", "selected");
                            layer.closeAll();
                        } else {
                            layer.alert('对不起，获取角色信息失败！', { icon: 2 }, function () { layer.closeAll(); });
                        }
                    }
                },
                error: function (err) {
                    layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                }
            });
        });

        function UpdateUserInfo() {
            model.ISSystem = $("#selectISSystem").val() == "1" ? true : false;
            model.RoleName = $("#txtRoleName").val();
            model.Remark = $("#txtRemark").val();
            // console.log(model);
            if (model.RoleName == "") {
                layer.alert('请输入角色名！', { icon: 2, title: '错误信息' }, function () { layer.closeAll(); $("#txtRoleName").focus(); });
            } else {
                $.ajax({
                    type: "POST",
                    url: "frmUpdateRole.aspx/UpdateRoleInfoById",
                    async: false,
                    data: $.toJSON({ model: model }),
                    contentType: "application/json;charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        var index = layer.load(1, {
                            shade: [0.8, '#fff'] //0.1透明度的白色背景 
                        });
                    },
                    success: function (data) {
                        if (data.d.Success) {
                            layer.alert('修改角色成功！', { icon: 1 }, function () { layer.closeAll(); });
                        } else {
                            layer.alert('对不起，获取角色信息失败！', { icon: 2 }, function () { layer.closeAll(); });
                        }
                    },
                    error: function (err) {
                        layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                    }
                });
            }
        }
    </script>
</head>
<body class="skin-blue">
    <section class="content">
        <div class="row">
            <div class="box box-info">
                <div class="box-header">
                </div>
                <div class="box-body">
                    <div class="input-group">
                        <span class="input-group-addon">角色名</span>
                        <input type="text" class="form-control" placeholder="角色名" id="txtRoleName" />
                    </div>
                    <br />
                    <div class="input-group">
                        <span class="input-group-addon">备&nbsp;&nbsp;&nbsp;&nbsp;注</span>
                        <input type="text" class="form-control" placeholder="角色名" id="txtRemark" />
                    </div>
                    <br />
                    <div class="input-group">
                        <span class="input-group-addon">角色类型</span>
                        <select id="selectISSystem" class="form-control">
                            <option value="0">一般角色</option>
                            <option value="1">系统角色</option>

                        </select>
                    </div>

                </div>
                <div class="box-footer">
                    <input type="button" class="btn btn-primary" value="保存" onclick="UpdateUserInfo()" />
                </div>
            </div>
        </div>
    </section>


</body>
</html>
