﻿using CE.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Recharge.Admin.SysRoleMamage
{
    public partial class frmAddRole : BasePageClass
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static JsonResult<long> AddRole(string roleName, string remark, int isSystem)
        {
            Model.SYS_Role model = new Model.SYS_Role();
            model.RoleName = roleName;
            model.Remark = remark;
            model.ISSystem = isSystem == 1 ? true : false;
            AddOperationLog($"【添加系统角色】角色名：" + roleName + ", 是否为系统角色：" + isSystem);
            return new JsonResult<long>(new BLL.SYS_Role().Add(model));
        }
    }
}