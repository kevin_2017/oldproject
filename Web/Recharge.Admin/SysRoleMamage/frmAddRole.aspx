﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="frmAddRole.aspx.cs" Inherits="Recharge.Admin.SysRoleMamage.frmAddRole" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        function SaveInfo() {
            var roleName = $("#txtRoleName").val();
            var remark = $("#txtRemark").val();
            var isSystem = $("#selectISSystem").val();

            if (roleName == "") {
                layer.alert('请输入角色名！', { icon: 2, title: '错误信息' }, function () { layer.closeAll(); $("#txtRoleName").focus(); });
            } else {
                $.ajax({
                    type: "POST",
                    url: "frmAddRole.aspx/AddRole",
                    async: false,
                    data: $.toJSON({ roleName: roleName, remark: remark, isSystem: isSystem }),
                    contentType: "application/json;charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        var index = layer.load(1, {
                            shade: [0.8, '#fff'], //0.1透明度的白色背景 
                        });
                    },
                    success: function (data) {
                        if (data.d.Success) {


                            layer.alert('添加角色成功！', { icon: 1 }, function () { layer.closeAll(); });
                        } else {
                            layer.alert('对不起，添加用户信息失败！', { icon: 2 }, function () { layer.closeAll(); });
                        }

                    },
                    error: function (err) {
                        layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                    }
                });
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="col-md-6">
        <div class="box box-info">
            <div class="box-header">
            </div>
            <div class="box-body">
                <div class="input-group">
                    <span class="input-group-addon">角色名</span>
                    <input type="text" class="form-control" placeholder="角色名" id="txtRoleName" />
                </div>
                <br />
                <div class="input-group">
                    <span class="input-group-addon">备注</span>
                    <input type="text" class="form-control" placeholder="备注" id="txtRemark" />
                </div>

                <br />
                <div class="input-group">
                    <span class="input-group-addon">角色类型</span>
                    <select id="selectISSystem" class="form-control">
                        <option value="0">一般角色</option>
                        <option value="1">系统角色</option>
                    </select>
                </div>

            </div>
            <div class="box-footer">
                <input type="button" class="btn btn-primary" value="保存" onclick="SaveInfo()" />
            </div>
        </div>
    </div>
</asp:Content>
