﻿using CE.Utility;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Recharge.Admin.SuppManage
{
    public partial class frmUpdateSupplier : BasePageClass
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static JsonResult<Model.SUP_SupplierManage> GetSupplierInfoByID(long ID)
        {
            return new JsonResult<Model.SUP_SupplierManage>(new BLL.SUP_SupplierManage().GetModel(ID));
        }


        [WebMethod]
        public static JsonResult<bool> UpdateSupplierInfoByID(Model.SUP_SupplierManage model)
        {
            AddOperationLog($"【修改供应商信息】ID:{model.ID} 详细：{JsonConvert.SerializeObject(model)}");
            return new JsonResult<bool>(new BLL.SUP_SupplierManage().Update(model));
        }
    }
}