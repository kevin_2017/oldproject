﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmUpdSuppSort.aspx.cs" Inherits="Recharge.Admin.SuppManage.frmUpdSuppSort" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>修改供应商充值顺序</title>
    <!-- Bootstrap 3.3.4 -->
    <link href="/Content/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="/Content/dist/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="/Content/dist/css/ionicons.min.css" rel="stylesheet" type="text/css" />

    <!-- Theme style -->
    <link href="/Content/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins 
    folder instead of downloading all of them to reduce the load. -->
    <link href="/Content/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
    <script src="/Content/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="/Content/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

    <!-- SlimScroll -->
    <script src="/Content/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src='/Content/plugins/fastclick/fastclick.min.js'></script>
    <!-- AdminLTE App -->
    <script src="/Content/dist/js/app.min.js" type="text/javascript"></script>
    <script src="/Content/dist/js/demo.js" type="text/javascript"></script>

    <script src="/Content/layer/layer.js"></script>
    <script src="/Content/jquery.json.min.js"></script>
    <script>


        function submitForm() {
            var hiddenProvince = document.getElementById("<%=hfSuppSortInfo.ClientID %>");
            var boxList = document.getElementsByName("txtSort");
            var rightList = "";
            for (var i = 0; i < boxList.length; i++) {
               var suplierId= boxList[i].id.split('_')[1];
                if (boxList[i].value != "") {
                    rightList +=(suplierId+"_"+boxList[i].value) + ",";
                }
            }
            hiddenProvince.value = rightList;
          //  console.log(rightList);
        }

        function doPostBackCheck() {
            var o = window.event.srcElement;
            if (o.tagName == "INPUT" && o.type == "checkbox") {
                __doPostBack("", "");
            }
        }
    </script>
</head>
<body class="skin-blue">

    <form id="form1" runat="server">
        <section class="content">
            <div class="row">
                <div class="box box-info">
                    <div class="box-header">
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered table-striped mb-none" id="datatable-tabletools">
                            <thead>
                                <tr>
                                    <th>运营商</th>
                                    <th>
                                        <asp:DropDownList runat="server" ID="ddlIspType" class="form-control" placeholder="商品类型" aria-controls="datatable-default" Width="150px" OnSelectedIndexChanged="ddlIspType_SelectedIndexChanged" AutoPostBack="True">
                                            <asp:ListItem Text="中国联通" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="中国移动" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="中国电信" Value="3"></asp:ListItem>
                                        </asp:DropDownList>
                                    </th>
                                </tr>
                                <tr>
                                    <th>接口供应商
                                    </th>
                                    <th>充值顺序
                                    </th>
                                </tr>
                            </thead>

                            <tbody>
                                <asp:Literal runat="server" ID="ltSuppSortInfo"></asp:Literal>
                            </tbody>
                        </table>
                    </div>
                    <div class="box-footer">
                        <asp:HiddenField runat="server" ID="hfSuppSortInfo" />
                        <asp:Button runat="server" ID="btnSave" Text="保存" CssClass="btn btn-primary" OnClick="btnSave_Click"
                            OnClientClick="submitForm()" />
                    </div>
                </div>
            </div>
        </section>
    </form>
</body>
</html>
