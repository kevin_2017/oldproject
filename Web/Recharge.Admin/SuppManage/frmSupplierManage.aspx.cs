﻿using CE.Utility;
using Common;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNet4.Utilities;

namespace Recharge.Admin.SuppManage
{
    public partial class frmSupplierManage : BasePageClass
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                Model.StrWhere str = new Model.StrWhere();
                str.IsWhereExist = false;
                SetDropDownList.BindDropDownList(ddlSupplier, new BLL.SUP_SupplierManage().GetDataSet(str).Tables[0], "TrueName", "ID");

                AspNetPager1.CurrentPageIndex = 1;
                InitPage();
            }
        }

        public void InitPage()
        {

            PageAttribute page = new PageAttribute();
            page.TableName = "SUP_SupplierManage";
            page.Columns = @" ID,UserName,PassWord,TrueName,Status,UserType,RechargeNum,NotRechargeNum,Balance,Contactor,ContactPhone,Address,IsCloseProduct,GateWay,RechargePayDomain,RechargeSearchUrl,RechargeNoticeUrl,FlowPayDomain,FlowSearchUrl,FlowNoticeUrl,IsDelete,RechargeCount,FlowCount,Sort,ApiCode";
            page.StrOrder = " Sort, ID asc";
            page.WhereCondition = string.Concat("", StrWhere);
            page.PageIndex = AspNetPager1.CurrentPageIndex - 1;
            page.PageSize = AspNetPager1.PageSize;
            DataTable dt = new BLL.SUP_SupplierManage().GetListByPage(page);
            AspNetPager1.RecordCount = page.TotalRowCount ?? 0;
            repList.DataSource = dt;
            repList.DataBind();
        }


        protected void AspNetPager1_PageChanging(object src, Wuqi.Webdiyer.PageChangingEventArgs e)
        {
            AspNetPager1.CurrentPageIndex = e.NewPageIndex;
            InitPage();
        }


        public string GetBalance(string apiCode)
        {
            try
            {

                var suppInfo = new BLL.SUP_SupplierManage().GetModel(apiCode);
                var address = ConfigurationManager.AppSettings["OrderQueryAddress"];
                var http = new HttpHelper();
                var item = GetHttpItem($"{suppInfo.GateWay}/{apiCode}/QueryBalance");
                var result = http.GetHtml(item);

                return result.Html;
            }
            catch (Exception ex)
            {

                return "0.00";
            }
        }

        #region 得到Http对象
        /// <summary>
        /// 得到Http对象
        /// </summary>
        /// <param name="url">访问的地址</param>
        /// <returns></returns>
        private static HttpItem GetHttpItem(string url)
        {
            var item = new HttpItem()
            {
                URL = url,//URL     必需项
                Encoding = Encoding.UTF8,//编码格式（utf-8,gb2312,gbk）     可选项 默认类会自动识别
                Method = "GET",//URL     可选项 默认为Get
                Timeout = 100000,//连接超时时间     可选项默认为100000
                ReadWriteTimeout = 30000,//写入Post数据超时时间     可选项默认为30000
                IsToLower = false,//得到的HTML代码是否转成小写     可选项默认转小写
                ContentType = "application/x-www-form-urlencoded",//返回类型    可选项有默认值
                Expect100Continue = false,
                PostDataType = PostDataType.Byte,
                PostEncoding = Encoding.UTF8
            };
            return item;
        }
        #endregion

        #region 生成Http参数列表
        /// <summary>
        /// 生成Http参数列表
        /// </summary>
        /// <param name="nc"></param>
        /// <returns></returns>
        private static string GetParams(NameValueCollection nc)
        {
            if (null == nc || nc.Count == 0)
            {
                return "";
            }
            var sb = new StringBuilder();
            foreach (string key in nc.Keys)
            {
                if (null != key)
                {
                    sb.AppendFormat("{0}={1}&", key, nc[key]);
                }
            }
            return sb.ToString(0, sb.Length - 1);
        }
        #endregion

        protected void btnSearch_Click(object obj, EventArgs e)
        {
            if (ddlSupplier.SelectedItem.Value.Trim() != "-1")
            {
                StrWhere = " and ID =" + ddlSupplier.SelectedItem.Value.Trim();
            }
            else
            {
                StrWhere = "";
            }

            AspNetPager1.CurrentPageIndex = 1;
            InitPage();
        }

        [WebMethod]
        public static JsonResult<bool> DelSupplierInfo(long id, string uRL, string buttonName, int operType)
        {
            if (GetButtonRole(uRL, buttonName))
            {
                Model.SUP_SupplierManage model = new Model.SUP_SupplierManage();
                model = new BLL.SUP_SupplierManage().GetModel(id);
                if (operType == 0)
                {
                    model.IsDelete = 1;

                    AddOperationLog($"【删除供应商】供应商ID:{id}");
                }
                else
                {
                    model.IsDelete = 0;

                    AddOperationLog($"【恢复供应商】供应商ID:{id}");
                }
                return new JsonResult<bool>(new BLL.SUP_SupplierManage().Update(model));
            }
            else
            {
                return new JsonResult<bool>(false, "对不起，您无此操作权限");
            }
        }

        [WebMethod]
        public static JsonResult<bool> GetOperateBtnRole(string uRL, string buttonName)
        {
            if (GetButtonRole(uRL, buttonName))
            {

                return new JsonResult<bool>(true);
            }
            else
            {
                return new JsonResult<bool>(false, "对不起，您无此操作权限");
            }
        }

    }
}