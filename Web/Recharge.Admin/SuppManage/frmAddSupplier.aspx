﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="frmAddSupplier.aspx.cs" Inherits="Recharge.Admin.SuppManage.frmAddSupplier" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="/Content/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
    <script>
        var objJson = {};
        function SaveInfo() {
            objJson.ID = $("#txtID").val();
            objJson.ApiCode = $("#txtApiCode").val();
            objJson.TrueName = $("#txtTrueName").val();
            objJson.NotRechargeNum = $("#txtNoRechargeNum").val();
            objJson.Contactor = "";
            objJson.RechargeNum = $("#txtRechargeNum").val();
            objJson.Status = $("#selectStatus").val();
            objJson.UserType = $("#selectUserType").val();
            objJson.RechargeCount = $("#txtRechargeCount").val();
            objJson.FlowCount = $("#txtFlowCount").val();
            objJson.RechargeNoticeUrl = $("#txtRechargeNoticeUrl").val();
            objJson.TelPhonePayDomain = $("#txtTelPhonePayDomain").val();
            objJson.RechargePayDomain = $("#txtRechargePayDomain").val();
            objJson.RechargeSearchUrl = $("#txtRechargeSearchUrl").val();
            objJson.FlowNoticeUrl = $("#txtFlowNoticeUrl").val();
            objJson.FlowPayDomain = $("#txtFlowPayDomain").val();
            objJson.FlowSearchUrl = $("#txtFlowSearchUrl").val();
            objJson.Address = $("#txtAddress").val();
            objJson.IsDelete = 0;
            objJson.IsCloseProduct = 0;
            objJson.Balance = 0;
            objJson.Sort = $("#txtSort").val();
            objJson.GateWay = $("#txtGateWay").val();


            console.log(objJson);
            if (objJson.ID == "") {
                layer.alert('请输入供应商ID！', { icon: 2, title: '错误信息' }, function () { layer.closeAll(); $("#txtID").focus(); });
                return;
            } if (objJson.ApiCode == "") {
                layer.alert('请输入供应商编码！', { icon: 2, title: '错误信息' }, function () { layer.closeAll(); $("#txtApiCode").focus(); });
                return;
            }
            if (objJson.TrueName == "") {
                layer.alert('请输入供应商名！', { icon: 2, title: '错误信息' }, function () { layer.closeAll(); $("#txtTrueName").focus(); });
                return;
            }
            $.ajax({
                type: "POST",
                url: "frmAddSupplier.aspx/AddSuplier",
                async: false,
                data: $.toJSON({ model: objJson }),
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    var index = layer.load(1, {
                        shade: [0.8, '#fff'], //0.1透明度的白色背景 
                    });
                },
                success: function (data) {
                    if (data.d.Data) {

                        layer.alert('增加供应商信息成功！', { icon: 1 }, function () { layer.closeAll(); });


                    } else {
                        layer.alert(data.d.Message == "" ? '对不起，增加供应商信息失败！' : data.d.Message, { icon: 2 }, function () { layer.closeAll(); });
                    }

                },
                error: function (err) {
                    layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                }
            });

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="box box-info">
        <div class="box-header">
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="box box-info">
                        <div class="box-header">
                            基础信息
                        </div>
                        <div class="box-body">
                            <table class="table table-bordered">
                                <tr>
                                    <td>供应商ID</td>
                                    <td>
                                        <input type="text" class="form-control" placeholder="供应商ID" id="txtID" /></td>
                                    <td>供应商编码</td>
                                    <td>
                                        <input type="text" class="form-control" placeholder="供应商编码" id="txtApiCode" /></td>
                                </tr>
                                <tr>
                                    <td>供应商名称</td>
                                    <td>
                                        <input type="text" class="form-control" placeholder="供应商名称" id="txtTrueName" /></td>
                                    <td>充值顺序</td>
                                    <td>
                                        <input type="text" class="form-control" placeholder="充值顺序" id="txtSort" /></td>
                                </tr>
                                <tr>
                                    <td>供&nbsp;应&nbsp;商&nbsp;状&nbsp;态</td>
                                    <td>
                                        <select id="selectStatus" class="form-control">
                                            <option value="1001">开通</option>
                                            <option value="1002">暂停</option>
                                            <option value="1003">停用</option>
                                            <option value="1004">暂停获取</option>
                                        </select></td>
                                    <td>供&nbsp;应&nbsp;商&nbsp;类&nbsp;型</td>
                                    <td>
                                        <select id="selectUserType" class="form-control">
                                            <option value="1001">接口供应商</option>
                                            <option value="1002">普通供应商</option>
                                        </select></td>
                                </tr>
                                <tr>
                                    <td>话费充值笔数</td>
                                    <td>
                                        <input type="text" class="form-control" placeholder="话费充值笔数" id="txtRechargeCount" value="0" /></td>
                                    <td>流量充值笔数</td>
                                    <td>
                                        <input type="text" class="form-control" placeholder="每日充值最大订单数" id="txtFlowCount" value="0" /></td>
                                </tr>
                                <tr>
                                    <td>可充值号段</td>
                                    <td>
                                        <input type="text" class="form-control" placeholder="多个用英文逗号(',')分隔" id="txtRechargeNum" /></td>
                                    <td>不可充号段</td>
                                    <td>
                                        <input type="text" class="form-control" placeholder="多个用英文逗号(',')分隔" id="txtNoRechargeNum" /></td>
                                </tr>
                                <tr>
                                    <td>请求网关地址</td>
                                    <td>
                                        <input type="text" class="form-control" placeholder="请求网关地址" id="txtGateWay" /></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">

                    <div class="input-group">
                        <span class="input-group-addon">座机充值地址</span>
                        <input type="text" class="form-control" placeholder="充值支付地址" id="txtTelPhonePayDomain" />
                    </div>
                    <br />
                    <div class="input-group">
                        <span class="input-group-addon">手机充值地址</span>
                        <input type="text" class="form-control" placeholder="充值支付地址" id="txtRechargePayDomain" />
                    </div>
                    <br />
                    <div class="input-group">
                        <span class="input-group-addon">话费查询地址</span>
                        <input type="text" class="form-control" placeholder="充值查询地址" id="txtRechargeSearchUrl" />
                    </div>
                    <br />
                    <div class="input-group">
                        <span class="input-group-addon">话费通知地址</span>
                        <input type="text" class="form-control" placeholder="充值通知地址" id="txtRechargeNoticeUrl" />
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="input-group">
                        <span class="input-group-addon">余额查询地址</span>
                        <input type="text" class="form-control" placeholder="余额查询地址" id="txtAddress" />
                    </div>
                    <br />

                    <div class="input-group">
                        <span class="input-group-addon">流量充值地址</span>
                        <input type="text" class="form-control" placeholder="流量充值地址" id="txtFlowPayDomain" />
                    </div>
                    <br />
                    <div class="input-group">
                        <span class="input-group-addon">流量查询地址</span>
                        <input type="text" class="form-control" placeholder="流量查询地址" id="txtFlowSearchUrl" />
                    </div>
                    <br />
                    <div class="input-group">
                        <span class="input-group-addon">流量通知地址</span>
                        <input type="text" class="form-control" placeholder="流量通知地址" id="txtFlowNoticeUrl" />
                    </div>


                </div>
            </div>
        </div>
        <div class="box-footer">
            <input type="button" class="btn btn-primary" value="保存" onclick="SaveInfo()" />
        </div>
    </div>

</asp:Content>
