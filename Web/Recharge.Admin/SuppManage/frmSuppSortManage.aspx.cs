﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using CE.Utility;
using Common;

namespace Recharge.Admin.SuppManage
{
    public partial class frmSuppSortManage : BasePageClass
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                AspNetPager1.CurrentPageIndex = 1;
                InitPage();
            }
        }

        public void InitPage()
        {
            PageAttribute page = new PageAttribute();

            page.TableName = "V8_ISP_Supplier_SortManage";
            page.Columns = @"id,ISPType,ISPName,SupplierId,SupplierName,CreateTime,Sort";
            page.StrOrder = "ISPType , Sort asc";
            page.WhereCondition = string.Concat(" and 1=1 ", StrWhere);
            page.PageIndex = AspNetPager1.CurrentPageIndex - 1;
            page.PageSize = AspNetPager1.PageSize;

            DataTable dt = new BLL.V8_Product_Manage().GetListByPage(page);
            AspNetPager1.RecordCount = page.TotalRowCount.Value;
            repList.DataSource = dt;
            repList.DataBind();
        }

        protected void AspNetPager1_PageChanging(object src, Wuqi.Webdiyer.PageChangingEventArgs e)
        {
            AspNetPager1.CurrentPageIndex = e.NewPageIndex;
            InitPage();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (ddlIspType.SelectedItem.Value.Trim() != "-1")
            {
                StrWhere = " and ISPType =" + ddlIspType.SelectedItem.Value.Trim();
            }
            else
            {
                StrWhere = "";
            }
            AspNetPager1.CurrentPageIndex = 1;
            InitPage();
        }


        [WebMethod]
        public static JsonResult<bool> GetUserBtnRole(string url, string btnName)
        {
            if (!GetButtonRole(url, btnName))
                return new JsonResult<bool>(false, "对不起，无权操作");
            else
                return new JsonResult<bool>(true, "操作成功");

        }
    }
}