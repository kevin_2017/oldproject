﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="frmSupplierManage.aspx.cs" Inherits="Recharge.Admin.SuppManage.frmSupplierManage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/Content/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
    <script>
        var urlPath = window.location.pathname;
        function UpdateSupplierIfo(id) {
            $.ajax({
                type: "POST",
                url: "frmSupplierManage.aspx/GetOperateBtnRole",
                async: false,
                data: $.toJSON({ uRL: urlPath, buttonName: 'btnUpdate' }),
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    var index = layer.load(1, {
                        shade: [0.8, '#fff'], //0.1透明度的白色背景 
                    });
                },
                success: function (data) {
                    if (data.d.Data) {
                        layer.open({
                            type: 2,
                            title: "修改供应商信息",
                            shadeClose: false,
                            shade: 0.8,
                            area: [($(window).width() - 50) + 'px', ($(window).height() - 50) + 'px'],
                            content: "frmUpdateSupplier.aspx?id=" + id,
                            close: function (index) {
                                window.location.reload();
                            },
                            cancel: function (index) {
                                window.location.reload();
                            }
                        });
                    } else {
                        layer.alert('对不起，您没有此权限！', { icon: 2 }, function () { layer.closeAll(); });
                    }

                },
                error: function (err) {
                    layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                }
            });
        }

        function DeleteSupplierIfo(id, trueName, isDelete) {
            var mess = "";
            if (isDelete == true) { mess = "恢复"; }
            else { mess = "删除"; }
            layer.confirm("确定要" + mess + "此用户？",
                { title: mess + '用户', btn: ['确定', '取消'], closeBtn: 0, shadeClose: true, icon: 3 },
                function() {
                    $.ajax({
                        type: "POST",
                        url: "frmSupplierManage.aspx/DelSupplierInfo",
                        async: false,
                        data: $.toJSON({ id: id, uRL: urlPath, buttonName: 'btnUpdate', operType: isDelete }),
                        contentType: "application/json;charset=utf-8",
                        dataType: "json",
                        beforeSend: function() {
                            var index = layer.load(1,
                                {
                                    shade: [0.8, '#fff'], //0.1透明度的白色背景 
                                });
                        },
                        success: function(data) {
                            if (data.d.Success) {
                                layer.alert('操作成功！',
                                    { icon: 1 },
                                    function() {
                                        layer.closeAll();
                                        window.location.reload();
                                    });
                            } else {
                                layer.alert('对不起，操作失败！', { icon: 2 }, function() { layer.closeAll(); });
                            }

                        },
                        error: function(err) {
                            layer.alert(err.responseText, { icon: 2 }, function() { layer.closeAll(); });
                        }
                    });
                },
                function() {
                    layer.closeAll();
                });


        }

        function InterfaceManage(id, supplierName) {
            $.ajax({
                type: "POST",
                url: "frmSupplierManage.aspx/GetOperateBtnRole",
                async: false,
                data: $.toJSON({ uRL: urlPath, buttonName: 'btnConfig' }),
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    var index = layer.load(1, {
                        shade: [0.8, '#fff'], //0.1透明度的白色背景 
                    });
                },
                success: function (data) {
                    if (data.d.Data) {

                        layer.open({
                            type: 2,
                            title: supplierName + "接口管理",
                            shadeClose: false,
                            shade: 0.8,
                            area: [($(window).width() - 100) + 'px', ($(window).height() - 50) + 'px'],
                            content: "frmConfig.aspx?id=" + id,
                            close: function (index) {
                                window.location.reload();
                            },
                            cancel: function (index) {
                                window.location.reload();
                            }
                        });
                    } else {
                        layer.alert('对不起，您没有此权限！', { icon: 2 }, function () { layer.closeAll(); });
                    }

                },
                error: function (err) {
                    layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                }
            });
        }

        function RechargeAreaManage(id, supplierName) {
            $.ajax({
                type: "POST",
                url: "frmSupplierManage.aspx/GetOperateBtnRole",
                async: false,
                data: $.toJSON({ uRL: urlPath, buttonName: 'btnRechargeArea' }),
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                beforeSend: function() {
                    var index = layer.load(1,
                        {
                            shade: [0.8, '#fff'] //0.1透明度的白色背景 
                        });
                },
                success: function(data) {
                    if (data.d.Data) {

                        layer.open({
                            type: 2,
                            title: supplierName + "充值区域管理",
                            shadeClose: false,
                            shade: 0.8,
                            area: [ ($(window).width() - 50) + 'px', ($(window).height() - 50) + 'px'],
                            content: "frmSupplierRechargeArea.aspx?id=" + id,
                            close: function(index) {
                                window.location.reload();
                            },
                            cancel: function(index) {
                                window.location.reload();
                            }
                        });
                    } else {
                        layer.alert('对不起，您没有此权限！', { icon: 2 }, function() { layer.closeAll(); });
                    }

                },
                error: function(err) {
                    layer.alert(err.responseText, { icon: 2 }, function() { layer.closeAll(); });
                }
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="dataTables_length" id="example1_length">
                            <label>查询：</label>
                            <label>
                                <asp:DropDownList runat="server" ID="ddlSupplier" class="form-control" placeholder="供应商管理名" aria-controls="datatable-default" Width="180px" />

                            </label>

                            <label>
                                <asp:Button runat="server" ID="btnSearch" Text="查询" CssClass="btn btn-success" OnClick="btnSearch_Click" /></label>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>编码</th>
                                    <th>供应商</th>
                                    <th>当前状态</th>
                                   <%-- <th>供应商类型</th>--%>
                                    <th>余额</th>
                                    <th>请求网关</th>
                                    <th>通知地址</th>
                                    <th>充值查询地址</th>
                                    <th>余额查询地址</th>
                                    <th>操作</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater runat="server" ID="repList">
                                    <ItemTemplate>
                                        <tr>
                                            <td style="text-align: center; vertical-align: middle"><%#Eval("ID")%></td>
                                            <td style="text-align: center; vertical-align: middle"><%#Eval("ApiCode") %></td>
                                            <td style="text-align: center; vertical-align: middle"><%#Eval("TrueName") %></td>

                                            <td style="text-align: center; vertical-align: middle"><%# Convert.ToInt32(Eval("IsDelete"))==1?"<span class=\"btn btn-danger btn-xs\" disabled=\"disabled\">删除</span>": Convert.ToInt32( Eval("Status"))==1001?"<span class=\"btn btn-success btn-xs\" disabled=\"disabled\">开通</span>":(Convert.ToInt32( Eval("Status"))==1002?"<span class=\"btn btn-info btn-xs\" disabled=\"disabled\">暂停</span>":(Convert.ToInt32( Eval("Status"))==1003?"<span class=\"btn btn-warning btn-xs\" disabled=\"disabled\">停用</span>":"<span class=\"btn btn-danger btn-xs\" disabled=\"disabled\">暂停获取</span>")) %></td>
                                           <%-- <td style="text-align: center; vertical-align: middle"><%#Eval("UserType")==DBNull.Value?"": Convert.ToInt32( Eval("UserType"))==1001?"<span class=\"btn btn-success btn-xs\" disabled=\"disabled\">接口用户</span>":"<span class=\"btn btn-info btn-xs\" disabled=\"disabled\">普通用户</span>" %></td>--%>
                                            <td><%#GetBalance(Eval("ApiCode").ToString()) %></td>
                                            <td><%# Eval("GateWay") %></td>
                                            <td><%# Eval("RechargeNoticeUrl") %></td>
                                            <td><%# Eval("RechargeSearchUrl") %></td>
                                            <td><%# Eval("Address") %></td>
                                            <td>
                                                <a class="btn btn-success  btn-xs" onclick='UpdateSupplierIfo(<%#Eval("ID")%>)' href="javascript:void(0)">编辑</a>
                                                <a class="btn btn-info  btn-xs" onclick='DeleteSupplierIfo(<%#Eval("ID")%>,"<%#Eval("TrueName") %>","<%#Eval("IsDelete") %>")' href="javascript:void(0)"><%# Convert.ToBoolean(Eval("IsDelete")) ? "恢复" : "删除" %></a>
                                                <a class="btn btn-warning  btn-xs" onclick='InterfaceManage(<%#Eval("ID")%>,"<%#Eval("TrueName") %>")' href="javascript:void(0)">接口管理</a>
                                                <a class="btn btn-primary  btn-xs" onclick='RechargeAreaManage(<%#Eval("ID")%>,"<%#Eval("TrueName") %>")' href="javascript:void(0)">可充区域管理</a>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>

                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-5">
                        <div class="dataTables_info" id="example2_info1" role="status" aria-live="polite">总计 <%=AspNetPager1.RecordCount %>条记录，每页显示<%=AspNetPager1.PageSize %>条 </div>
                    </div>

                    <div class="col-sm-7">
                        <div class="dataTables_paginate" id="example1_paginate">
                            <webdiyer:AspNetPager ShowPageIndexBox="Never" ID="AspNetPager1" runat="server" Width="100%" PageSize="16" PrevPageText="上一页" NextPageText="下一页" AlwaysShow="true" OnPageChanging="AspNetPager1_PageChanging" ShowFirstLast="False" PagingButtonsClass="paginate_button" CssClass="pagination" LayoutType="Ul" PagingButtonLayoutType="UnorderedList" PagingButtonSpacing="0" CurrentPageButtonClass="active" PrevNextButtonsClass="paginate_button previous">
                            </webdiyer:AspNetPager>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</asp:Content>
