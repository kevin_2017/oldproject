﻿<%@ Page Title="供应商充值顺序管理" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="frmSuppSortManage.aspx.cs" Inherits="Recharge.Admin.SuppManage.frmSuppSortManage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/Content/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
    <script>
        var urlPath = window.location.pathname;
        function UpdSort() {
            $.ajax({
                type: "POST",
                url: "frmSuppSortManage.aspx/GetUserBtnRole",
                async: false,
                data: $.toJSON({ url: urlPath, btnName: 'btnUpdSort' }),
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                beforeSenF: function () {
                    var index = layer.load(1, {
                        shade: [0.8, '#fff'], //0.1透明度的白色背景 
                    });
                },
                success: function (data) {
                    if (data.d.Data) {
                        layer.open({
                            type: 2,
                            title: "充值顺序管理",
                            shadeClose: false,
                            shade: 0.8,
                            area: [ '500px', ($(window).height() - 50) + 'px'],
                            content: "frmUpdSuppSort.aspx",
                            close: function (index) {
                                window.location.reload();
                            },
                            cancel: function (index) {
                                window.location.reload();
                            }
                        });
                    } else {
                        layer.alert('对不起，您没有此权限！', { icon: 2 }, function () { layer.closeAll(); });
                    }

                },
                error: function (err) {
                    layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                }
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="dataTables_length" id="example1_length">
                            <label>运营商类型：</label>
                            <label>
                                <asp:DropDownList runat="server" ID="ddlIspType" class="form-control" placeholder="商品类型" aria-controls="datatable-default" Width="150px">
                                    <asp:ListItem Text="请选择" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="中国联通" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="中国移动" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="中国电信" Value="3"></asp:ListItem>
                                </asp:DropDownList>
                            </label>
                            <label>商品类型：</label>
                            <label>
                                <asp:Button runat="server" ID="btnSearch" Text="查询" CssClass="btn btn-success" OnClick="btnSearch_Click" />
                            </label>
                            <label>
                                <input type="button" id="btnAdd" value="更新顺序" class="btn btn-danger" onclick="UpdSort()" />
                            </label>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>运营商</th>
                                    <th>供应商</th>
                                    <th>顺序</th>
                                    <th>操作时间</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater runat="server" ID="repList">
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <%#Eval("ID") %>
                                            </td>
                                            <td><%#Convert.ToInt32( Eval("ISPType"))==1?"中国联通":(Convert.ToInt32( Eval("ISPType"))==2?"中国移动":(Convert.ToInt32( Eval("ISPType"))==3?"中国电信":"其他")) %></td>
                                            <td><%#Eval("SupplierName") %></td>
                                            <td><%#Eval("Sort") %></td>
                                            <td><%#Eval("CreateTime") %></td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>

                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-5">
                        <div class="dataTables_info" id="example2_info1" role="status" aria-live="polite">总计 <%=AspNetPager1.RecordCount %>条记录，每页显示<%=AspNetPager1.PageSize %>条 </div>
                    </div>

                    <div class="col-sm-7">
                        <div class="dataTables_paginate" id="example1_paginate">
                            <webdiyer:AspNetPager ShowPageIndexBox="Never" ID="AspNetPager1" runat="server" Width="100%" PageSize="16" PrevPageText="上一页" NextPageText="下一页" AlwaysShow="true" OnPageChanging="AspNetPager1_PageChanging" ShowFirstLast="False" PagingButtonsClass="paginate_button" CssClass="pagination" LayoutType="Ul" PagingButtonLayoutType="UnorderedList" PagingButtonSpacing="0" CurrentPageButtonClass="active" PrevNextButtonsClass="paginate_button previous">
                            </webdiyer:AspNetPager>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</asp:Content>
