﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmSupplierRechargeArea.aspx.cs" Inherits="Recharge.Admin.SuppManage.frmSupplierRechargeArea" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>供应商可充区域管理</title>
    <!-- Bootstrap 3.3.4 -->
    <link href="/Content/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="/Content/dist/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="/Content/dist/css/ionicons.min.css" rel="stylesheet" type="text/css" />

    <!-- Theme style -->
    <link href="/Content/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins 
    folder instead of downloading all of them to reduce the load. -->
    <link href="/Content/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
    <script src="/Content/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="/Content/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

    <!-- SlimScroll -->
    <script src="/Content/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src='/Content/plugins/fastclick/fastclick.min.js'></script>
    <!-- AdminLTE App -->
    <script src="/Content/dist/js/app.min.js" type="text/javascript"></script>
    <script src="/Content/dist/js/demo.js" type="text/javascript"></script>

    <script src="/Content/layer/layer.js"></script>
    <script src="/Content/jquery.json.min.js"></script>
    <script type="text/javascript">
        function doPostBackCheck() {
            var o = window.event.srcElement;
            if (o.tagName == "INPUT" && o.type == "checkbox") {
                __doPostBack("", "");
            }
        }

        function SaveInfo() {
            var provinceIds = "";

            var boxList = document.getElementsByName("inline-checkboxProvince");
            for (var i = 0; i < boxList.length; i++) {
                if (boxList[i].checked == true) {
                    provinceIds += boxList[i].value + ",";
                }
            }
            $("#<%=hfProvince.ClientID %>").val(provinceIds);

            var cityIds = "";
            var cityList = document.getElementsByName("inline-checkboxCity");
            for (var i = 0; i < cityList.length; i++) {
                if (cityList[i].checked == true) {
                    cityIds += cityList[i].value + ",";
                }
            }
            $("#<%=hfCity.ClientID %>").val(cityIds);
            
        }
    </script>
</head>
<body class="skin-blue">
    <form runat="server" id="form1">
        <section class="content">
            <div class="row">
                <div class="box box-info">
                    <div class="box-header">
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-12">

                                <div class="input-group">
                                    <span class="input-group-addon">运&nbsp;&nbsp;营&nbsp;&nbsp;商</span>
                                    <asp:DropDownList runat="server" ID="ddlIspType" class="form-control" placeholder="商品类型" aria-controls="datatable-default" AutoPostBack="True" OnSelectedIndexChanged="ddlIspType_SelectedIndexChanged">
                                        <asp:ListItem Text="中国联通" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="中国移动" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="中国电信" Value="3"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>

                                            <th>可充省份</th>
                                            <th>可充城市</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater runat="server" ID="repList">
                                            <ItemTemplate>
                                                <tr>
                                                    <td class="form-inline"><%# GetProvince(Eval("ID"),Eval("ProvinceName")) %> </td>
                                                    <td class="form-inline"><%# GetCityInfo(Eval("ID")) %></td>

                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <asp:HiddenField runat="server" ID="hfProvince" />
                        <asp:HiddenField runat="server" ID="hfCity" />
                        <asp:Button runat="server" ID="btnSave" Text="保存" CssClass="btn btn-primary" OnClick="btnSave_Click"
                            OnClientClick="SaveInfo()" />

                    </div>
                </div>
            </div>
        </section>

    </form>
</body>
</html>
