﻿using CE.Utility;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Recharge.Admin.SuppManage
{
    public partial class frmAddSupplier : BasePageClass
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        public static JsonResult<bool> AddSuplier(Model.SUP_SupplierManage model)
        {
            Model.StrWhere str = new Model.StrWhere
            {
                IsWhereExist = true,
                strWhere = new System.Text.StringBuilder($" id={model.ID} or ApiCode='{model.ApiCode}'")
            };
            if (new BLL.SUP_SupplierManage().Exists(str))
                return new JsonResult<bool>(false, "该供应商ID或者编码已经存在");

            model.RechargeCount = model.RechargeCount == 0 ? 999999999 : model.RechargeCount;

            model.FlowCount = model.FlowCount == 0 ? 999999999 : model.FlowCount;
            AddOperationLog($"【添加供应商】供应商名：{model.TrueName},{JsonConvert.SerializeObject(model)}");
            return new JsonResult<bool>(new BLL.SUP_SupplierManage().Add(model) > 0);

        }
    }
}