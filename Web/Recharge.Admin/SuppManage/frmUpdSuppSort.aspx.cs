﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Model;

namespace Recharge.Admin.SuppManage
{
    public partial class frmUpdSuppSort : BasePageClass
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitPage(this.ddlIspType.SelectedItem.Value);
            }
        }


        public void InitPage(string ispType)
        {
            DataTable dtall = new BLL.SUP_SupplierManage().GetDataSet(new StrWhere() { IsWhereExist = false, strWhere = new System.Text.StringBuilder($"") }).Tables[0];
            var dtisp = new BLL.V8_ISP_Supplier_SortManage().GetModelList(new StrWhere() { IsWhereExist = true, strWhere = new System.Text.StringBuilder($"ISPType={ispType}") });
            string innerHtml = "";
            foreach (DataRow dr in dtall.Rows)
            {
                var model = dtisp.FirstOrDefault(x => x.SupplierID == Convert.ToInt64(dr["ID"].ToString()));
                if (model != null)
                {
                    innerHtml += "<tr><td>" +
                                 $"{dr["TrueName"].ToString()}" + "</td><td> <input type=\"text\" class=\"form-control\" placeholder=\"充值顺序\" id=\"txtSort_" + dr["ID"] + "\" value=\"" + model.Sort + "\" name=\"txtSort\" /></td></tr>";
                }
                else
                {
                    innerHtml += "<tr><td>" +
                                 dr["TrueName"].ToString() + "</td><td> <input type=\"text\" class=\"form-control\" placeholder=\"充值顺序\" id=\"txtSort_" + dr["ID"] + "\" name=\"txtSort\"/></td></tr>";
                }
            }

            ltSuppSortInfo.Text = innerHtml;
        }

        protected void ddlIspType_SelectedIndexChanged(object sender, EventArgs e)
        {
            InitPage(this.ddlIspType.SelectedValue);

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            string[] suppList = hfSuppSortInfo.Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            if (suppList.Length == 0)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "", "  layer.alert('请录入充值顺序！',{ icon: 2 });", true);
            }
            var list = new BLL.SUP_SupplierManage().GetModelList(new StrWhere() { IsWhereExist = false, strWhere = new System.Text.StringBuilder($"") });

            foreach (var supp in suppList)
            {
                string[] provs = supp.Split(new string[] { "_" }, StringSplitOptions.RemoveEmptyEntries);

                var suppInfo = list.FirstOrDefault(x => x.ID == Convert.ToInt64(provs[0]));

                V8_ISP_Supplier_SortManage model = new BLL.V8_ISP_Supplier_SortManage().GetModel(new StrWhere() { IsWhereExist = true, strWhere = new StringBuilder($"ISPType={this.ddlIspType.SelectedItem.Value} and SupplierId={provs[0]}") });
                if (model == null)
                {
                    model = new V8_ISP_Supplier_SortManage
                    {
                        SupplierID = suppInfo.ID,
                        SupplierName = suppInfo.TrueName,
                        ISPType = Convert.ToInt32(ddlIspType.SelectedItem.Value),
                        ISPName = ddlIspType.SelectedItem.Text,
                        Sort = Convert.ToInt32(provs[1])
                    };
                    new BLL.V8_ISP_Supplier_SortManage().Add(model);
                }
                else
                {
                    model.Sort = Convert.ToInt32(provs[1]);
                    new BLL.V8_ISP_Supplier_SortManage().Update(model);
                }

            }
            InitPage(this.ddlIspType.SelectedValue);
            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "", "  layer.alert('分配成功！',{icon:1});", true);
        }
    }
}