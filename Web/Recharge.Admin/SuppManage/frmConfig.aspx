﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmConfig.aspx.cs" Inherits="Recharge.Admin.SuppManage.frmConfig" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <!-- Bootstrap 3.3.4 -->
    <link href="/Content/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="/Content/dist/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="/Content/dist/css/ionicons.min.css" rel="stylesheet" type="text/css" />

    <!-- Theme style -->
    <link href="/Content/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins 
         folder instead of downloading all of them to reduce the load. -->
    <link href="/Content/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
    <script src="/Content/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="/Content/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

    <!-- SlimScroll -->
    <script src="/Content/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src='/Content/plugins/fastclick/fastclick.min.js'></script>
    <!-- AdminLTE App -->
    <script src="/Content/dist/js/app.min.js" type="text/javascript"></script>
    <script src="/Content/dist/js/demo.js" type="text/javascript"></script>

    <script src="/Content/layer/layer.js"></script>
    <script src="/Content/jquery.json.min.js"></script>
    <script>
        var list = [];
        var supplierID = "";
        function getQueryString(name) {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
            var r = window.location.search.substr(1).match(reg);
            if (r != null) return unescape(r[2]);
            return null;
        }
        $(function () {
            var innerhtml = "";
            $("#divHtml").html(innerhtml);
            supplierID = getQueryString("id");
            $.ajax({
                type: "POST",
                url: "frmConfig.aspx/GetSupplierConfigByID",
                async: false,
                data: "{'id':" + getQueryString("id") + "}",
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    var index = layer.load(1, {
                        shade: [0.8, '#fff'], //0.1透明度的白色背景 
                    });
                },
                success: function (data) {
                    var objJson = data.d;

                    if (objJson != null) {
                        $.each(data.d.Rows[0], function (i, item) {
                            innerhtml += "<tr><td>" + item["ConfigName"] + "(" + item["Remark"] + ")" + "</td><td><input type='text' id='" + item["ConfigName"] + "' value='" + item["ConfigValue"] + "'class=\"form-control\" style='width:100%' remark='" + item["Remark"] + "'/></td></tr>";
                        })
                        $("#divHtml").html(innerhtml);
                        layer.closeAll();
                    } else {
                        layer.alert('对不起，获取信息失败！', { icon: 2 }, function () { layer.closeAll(); });
                    }
                },
                error: function (err) {
                    layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                }
            });
        });

        function SaveInfo() {
            $("input[type='text']").each(function () {
                list.push({ SupplierID: supplierID, ConfigName: $(this).attr("id"), ConfigValue: $(this).val(), Remark: $(this).attr("remark") });
            });
            //AddConfigInfo
            $.ajax({
                type: "POST",
                url: "frmConfig.aspx/AddConfigInfo",
                async: false,
                data: $.toJSON({ list: list }),
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    var index = layer.load(1, {
                        shade: [0.8, '#fff'], //0.1透明度的白色背景 
                    });
                },
                success: function (data) {
                    if (data.d.Data) {
                        layer.alert('修改供应商接口信息成功！', { icon: 1 }, function () { layer.closeAll(); });
                    } else {
                        layer.alert('对不起，修改供应商接口信息失败！', { icon: 2 }, function () { layer.closeAll(); });
                    }

                },
                error: function (err) {
                    layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                }
            });
            //  console.log(list);
        }
    </script>
</head>
<body class="skin-blue">
    <section class="content">
        <div class="row">
            <div class="box box-info">
                <div class="box-header">
                </div>
                <div class="box-body">

                    <div class="col-sm-12">
                        <table   class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>名称</th>
                                    <th>值</th>
                                </tr>
                            </thead>
                            <tbody id="divHtml">
                            </tbody>

                        </table>
                    </div>

                </div>
                <div class="box-footer">
                    <input type="button" class="btn btn-primary" value="保存" onclick="SaveInfo()" />
                </div>
            </div>
        </div>
    </section>


</body>
</html>

