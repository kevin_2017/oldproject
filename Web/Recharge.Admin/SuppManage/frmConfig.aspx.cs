﻿using CE.Utility;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Recharge.Admin.SuppManage
{
    public partial class frmConfig : BasePageClass
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static JsonResult<List<Model.SUP_SupplierConfig>> GetSupplierConfigByID(long id)
        {
            Model.StrWhere str = new Model.StrWhere();
            str.IsWhereExist = true;
            str.strWhere.Append($" SupplierID ={id} ");

            var config = new BLL.SUP_SupplierConfig().GetModelList(str);


            return new JsonResult<List<Model.SUP_SupplierConfig>>(config);
        }

        [WebMethod]
        public static JsonResult<bool> AddConfigInfo(List<Model.SUP_SupplierConfig> list)
        {

            AddOperationLog($"【修改供应商接口信息】：{JsonConvert.SerializeObject(list)}");

            return new JsonResult<bool>(new BLL.SUP_SupplierConfig().SaveSupplierConfig(list));
        }

    }
}