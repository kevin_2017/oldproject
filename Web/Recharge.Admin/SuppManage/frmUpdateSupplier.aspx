﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmUpdateSupplier.aspx.cs" Inherits="Recharge.Admin.SuppManage.frmUpdateSupplier" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <!-- Bootstrap 3.3.4 -->
    <link href="/Content/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="/Content/dist/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="/Content/dist/css/ionicons.min.css" rel="stylesheet" type="text/css" />

    <!-- Theme style -->
    <link href="/Content/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins 
         folder instead of downloading all of them to reduce the load. -->
    <link href="/Content/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
    <script src="/Content/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="/Content/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

    <!-- SlimScroll -->
    <script src="/Content/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src='/Content/plugins/fastclick/fastclick.min.js'></script>
    <!-- AdminLTE App -->
    <script src="/Content/dist/js/app.min.js" type="text/javascript"></script>
    <script src="/Content/dist/js/demo.js" type="text/javascript"></script>

    <script src="/Content/layer/layer.js"></script>
    <script src="/Content/jquery.json.min.js"></script>
    <script>
        var objJson = {};

        function getQueryString(name) {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
            var r = window.location.search.substr(1).match(reg);
            if (r != null) return unescape(r[2]);
            return null;
        };

        $(function () {
            $.ajax({
                type: "POST",
                url: "frmUpdateSupplier.aspx/GetSupplierInfoByID",
                async: false,
                data: $.toJSON({ ID: getQueryString("id") }),
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    var index = layer.load(1, {
                        shade: [0.8, '#fff'], //0.1透明度的白色背景 
                    });
                },
                success: function (data) {
                    if (data.d.Data != null) {
                        objJson = data.d.Data;
                        $("#txtID").val(objJson.ID);
                        $("#txtApiCode").val(objJson.ApiCode);
                        $("#txtTrueName").val(objJson.TrueName);
                        $("#txtNoRechargeNum").val(objJson.NotRechargeNum);
                        $("#txtRechargeNum").val(objJson.RechargeNum);
                        $("#selectStatus option[value='" + objJson.Status + "']").attr("selected", "selected");
                        $("#selectUserType option[value='" + objJson.UserType + "']").attr("selected", "selected");
                        $("#txtRechargeCount").val(objJson.RechargeCount);
                        $("#txtFlowCount").val(objJson.FlowCount);
                        $("#txtTelPhonePayDomain").val(objJson.TelPhonePayDomain);
                        $("#txtRechargeNoticeUrl").val(objJson.RechargeNoticeUrl);
                        $("#txtRechargePayDomain").val(objJson.RechargePayDomain);
                        $("#txtRechargeSearchUrl").val(objJson.RechargeSearchUrl);
                        $("#txtFlowNoticeUrl").val(objJson.FlowNoticeUrl);
                        $("#txtFlowPayDomain").val(objJson.FlowPayDomain);
                        $("#txtFlowSearchUrl").val(objJson.FlowSearchUrl);
                        $("#txtGateWay").val(objJson.GateWay);

                        $("#txtAddress").val(objJson.Address);
                        $("#txtSort").val(objJson.Sort);
                        layer.closeAll();
                    } else {
                        layer.alert('对不起，获取信息失败！', { icon: 2 }, function () { layer.closeAll(); });
                    }
                },
                error: function (err) {
                    layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                }
            });
        });

        function SaveInfo() {

            objJson.Sort = $("#txtSort").val();
            objJson.Status = $("#selectStatus").val();
            objJson.UserType = $("#selectUserType").val();
            objJson.NotRechargeNum = $("#txtNoRechargeNum").val();
            objJson.RechargeNum = $("#txtRechargeNum").val();
            objJson.RechargeCount = $("#txtRechargeCount").val();
            objJson.FlowCount = $("#txtFlowCount").val();
            objJson.GateWay = $("#txtGateWay").val();

            objJson.TelPhonePayDomain = $("#txtTelPhonePayDomain").val();
            objJson.RechargeNoticeUrl = $("#txtRechargeNoticeUrl").val();
            objJson.RechargePayDomain = $("#txtRechargePayDomain").val();
            objJson.RechargeSearchUrl = $("#txtRechargeSearchUrl").val();
            objJson.FlowNoticeUrl = $("#txtFlowNoticeUrl").val();
            objJson.FlowPayDomain = $("#txtFlowPayDomain").val();
            objJson.FlowSearchUrl = $("#txtFlowSearchUrl").val();
            objJson.Address = $("#txtAddress").val();
            $.ajax({
                type: "POST",
                url: "frmUpdateSupplier.aspx/UpdateSupplierInfoByID",
                async: false,
                data: $.toJSON({ model: objJson }),
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    var index = layer.load(1, {
                        shade: [0.8, '#fff'], //0.1透明度的白色背景 
                    });
                },
                success: function (data) {
                    if (data.d.Data) {

                        layer.alert('修改供应商信息成功！', { icon: 1 }, function () { layer.closeAll(); });


                    } else {
                        layer.alert('对不起，修改供应商信息失败！', { icon: 2 }, function () { layer.closeAll(); });
                    }

                },
                error: function (err) {
                    layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                }
            });
        };

    </script>
</head>
<body class="skin-blue">
    <div class="box box-info">
        <div class="box-header">
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="box box-info">
                        <div class="box-header">
                            基础信息
                        </div>
                        <div class="box-body">
                            <table class="table table-bordered">
                                <tr>
                                    <td>供应商ID</td>
                                    <td>
                                        <input type="text" class="form-control" placeholder="供应商ID" id="txtID" readonly="readonly" /></td>
                                    <td>供应商编码</td>
                                    <td>
                                        <input type="text" class="form-control" placeholder="供应商编码" id="txtApiCode" readonly="readonly"/></td>
                                </tr>
                                <tr>
                                    <td>供应商名称</td>
                                    <td>
                                        <input type="text" class="form-control" placeholder="供应商名称" id="txtTrueName" readonly="readonly"/></td>
                                    <td>充值顺序</td>
                                    <td>
                                        <input type="text" class="form-control" placeholder="充值顺序" id="txtSort" /></td>
                                </tr>
                                <tr>
                                    <td>供&nbsp;应&nbsp;商&nbsp;状&nbsp;态</td>
                                    <td>
                                        <select id="selectStatus" class="form-control">
                                            <option value="1001">开通</option>
                                            <option value="1002">暂停</option>
                                            <option value="1003">停用</option>
                                            <option value="1004">暂停获取</option>
                                        </select></td>
                                    <td>供&nbsp;应&nbsp;商&nbsp;类&nbsp;型</td>
                                    <td>
                                        <select id="selectUserType" class="form-control">
                                            <option value="1001">接口供应商</option>
                                            <option value="1002">普通供应商</option>
                                        </select></td>
                                </tr>
                                <tr>
                                    <td>话费充值笔数</td>
                                    <td>
                                        <input type="text" class="form-control" placeholder="话费充值笔数" id="txtRechargeCount" value="0" /></td>
                                    <td>流量充值笔数</td>
                                    <td>
                                        <input type="text" class="form-control" placeholder="每日充值最大订单数" id="txtFlowCount" value="0" /></td>
                                </tr>
                                <tr>
                                    <td>可充值号段</td>
                                    <td>
                                        <input type="text" class="form-control" placeholder="多个用英文逗号(',')分隔" id="txtRechargeNum" /></td>
                                    <td>不可充号段</td>
                                    <td>
                                        <input type="text" class="form-control" placeholder="多个用英文逗号(',')分隔" id="txtNoRechargeNum" /></td>
                                </tr>
                                <tr>
                                    <td>请求网关地址</td>
                                    <td>
                                        <input type="text" class="form-control" placeholder="请求网关地址" id="txtGateWay" /></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">

                    <div class="input-group">
                        <span class="input-group-addon">座机充值地址</span>
                        <input type="text" class="form-control" placeholder="充值支付地址" id="txtTelPhonePayDomain" />
                    </div>
                    <br />
                    <div class="input-group">
                        <span class="input-group-addon">手机充值地址</span>
                        <input type="text" class="form-control" placeholder="充值支付地址" id="txtRechargePayDomain" />
                    </div>
                    <br />
                    <div class="input-group">
                        <span class="input-group-addon">话费查询地址</span>
                        <input type="text" class="form-control" placeholder="充值查询地址" id="txtRechargeSearchUrl" />
                    </div>
                    <br />
                    <div class="input-group">
                        <span class="input-group-addon">话费通知地址</span>
                        <input type="text" class="form-control" placeholder="充值通知地址" id="txtRechargeNoticeUrl" />
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="input-group">
                        <span class="input-group-addon">余额查询地址</span>
                        <input type="text" class="form-control" placeholder="余额查询地址" id="txtAddress" />
                    </div>
                    <br />

                    <div class="input-group">
                        <span class="input-group-addon">流量充值地址</span>
                        <input type="text" class="form-control" placeholder="流量充值地址" id="txtFlowPayDomain" />
                    </div>
                    <br />
                    <div class="input-group">
                        <span class="input-group-addon">流量查询地址</span>
                        <input type="text" class="form-control" placeholder="流量查询地址" id="txtFlowSearchUrl" />
                    </div>
                    <br />
                    <div class="input-group">
                        <span class="input-group-addon">流量通知地址</span>
                        <input type="text" class="form-control" placeholder="流量通知地址" id="txtFlowNoticeUrl" />
                    </div>


                </div>
            </div>
        </div>
        <div class="box-footer">
            <input type="button" class="btn btn-primary" value="保存" onclick="SaveInfo()" />
        </div>
    </div>



</body>
</html>
