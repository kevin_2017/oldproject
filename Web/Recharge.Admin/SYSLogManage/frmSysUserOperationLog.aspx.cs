﻿using Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Recharge.Admin.SYSLogManage
{
    public partial class frmSysUserOperationLog : BasePageClass
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                AspNetPager1.CurrentPageIndex = 1;
                InitPage(); 
                Model.StrWhere str = new Model.StrWhere();
                str.IsWhereExist = false;
                SetDropDownList.BindDropDownList(ddlUserInfo, new BLL.SYS_UserInfo().GetDataSet(str).Tables[0], "UserName", "ID");
            }
        }

        public void InitPage()
        {

            PageAttribute page = new PageAttribute()
            {
                TableName = "VW_SystemUserOperationLog",
                Columns = @" UserID, UserName,OperateDescriptions, OperateIP, OperateTime",
                StrOrder = " OperateTime desc",
                WhereCondition = StrWhere,
                PageSize = AspNetPager1.PageSize,

                PageIndex = AspNetPager1.CurrentPageIndex - 1
            };
            DataTable dt = new BLL.SYS_UserInfo().GetListByPage(page);
            AspNetPager1.RecordCount = page.TotalRowCount.Value;
            repList.DataSource = dt;
            repList.DataBind();


        }


        protected void AspNetPager1_PageChanging(object src, Wuqi.Webdiyer.PageChangingEventArgs e)
        {
            AspNetPager1.CurrentPageIndex = e.NewPageIndex;
            InitPage();
        }


        protected void btnSearch_Click(object obj, EventArgs e)
        {
            StrWhere = "";
            if (this.ddlUserInfo.SelectedItem.Value.Trim() != "-1")
            {
                StrWhere += " and UserID  =" + this.ddlUserInfo.SelectedItem.Value.Trim();
            }
            if (this.txtBeginTime.Value.Trim() != "")
            {
                StrWhere += " and datediff(day, OperateTime,'" + this.txtBeginTime.Value.Trim() + "')=0";
            }

            AspNetPager1.CurrentPageIndex = 1;
            InitPage();
        }

    }
}