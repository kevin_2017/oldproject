﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common;

namespace Recharge.Admin.SYSLogManage
{
    public partial class frmSYSUserLoginLog : BasePageClass
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitPage(1);
                Model.StrWhere str = new Model.StrWhere { IsWhereExist = false };
                SetDropDownList.BindDropDownList(ddlUserInfo, new BLL.SYS_UserInfo().GetDataSet(str).Tables[0], "UserName", "ID");
            }
        }
        public void InitPage(int pages)
        {
            PageAttribute page = new PageAttribute
            {
                TableName = "VW_SysUserLoginLog",
                Columns = @" UserID,UserName ,LoginTime, LoginIP,LoginIpAttr",
                StrOrder = " LoginTime desc",
                WhereCondition = StrWhere,
                PageSize = AspNetPager1.PageSize,
                PageIndex = pages - 1
            };
            DataTable dt = new BLL.SYS_UserInfo().GetListByPage(page);
            AspNetPager1.RecordCount = page.TotalRowCount ?? 0;
            AspNetPager1.CurrentPageIndex = pages;
            repList.DataSource = dt;
            repList.DataBind();


        }

        protected void AspNetPager1_PageChanging(object src, Wuqi.Webdiyer.PageChangingEventArgs e)
        {
            AspNetPager1.CurrentPageIndex = e.NewPageIndex;
            InitPage(AspNetPager1.CurrentPageIndex);
        }
        protected void btnSearch_Click(object obj, EventArgs e)
        {
            StrWhere = "";
            if (this.ddlUserInfo.SelectedItem.Value.Trim() != "-1")
            {
                StrWhere += " and UserID  =" + this.ddlUserInfo.SelectedItem.Value.Trim();
            }
            if (this.txtBeginTime.Value.Trim() != "")
            {
                StrWhere += " and datediff(day, LoginTime,'" + this.txtBeginTime.Value.Trim() + "')=0";
            }

            AspNetPager1.CurrentPageIndex = 1;
            InitPage(1);
        }
    }
}