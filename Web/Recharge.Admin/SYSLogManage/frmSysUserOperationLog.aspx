﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="frmSysUserOperationLog.aspx.cs" Inherits="Recharge.Admin.SYSLogManage.frmSysUserOperationLog" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/Content/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
    <script src="/Content/My97DatePicker/lang/zh-cn.js"></script>
    <link href="/Content/My97DatePicker/skin/default/datepicker.css" rel="stylesheet" />
    <script src="/Content/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="dataTables_length" id="example1_length">
                            <label>查询：</label>
                            <label>
                                <asp:DropDownList runat="server" ID="ddlUserInfo" CssClass="form-control" Width="150px"></asp:DropDownList>
                            </label>
                            <label>操作日期</label>
                            <label>
                                <input type="text" id="txtBeginTime" style="width: 185px; border-color: #CCCCCC;" runat="server" placeholder="操作日期" onfocus="WdatePicker({twoer:'whyGreen',maxDate:'%y-%M-%d'})" class=" form-control" />
                            </label>
                            <label>
                                <asp:Button runat="server" ID="btnSearch" Text="查询" CssClass="btn btn-success" OnClick="btnSearch_Click" /></label>
                        </div>
                    </div>
                    <%--<div class="col-sm-6">
                        <div id="example1_filter" class="dataTables_filter">
                            <label>Search:<input type="search" class="form-control input-sm" placeholder="" aria-controls="example1"></label>
                        </div>
                    </div>--%>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>序号</th>
                                    <th>用户名</th>
                                    <th>操作IP</th>
                                    <th>操作时间</th>
                                    <th>操作描述</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater runat="server" ID="repList">
                                    <ItemTemplate>
                                        <tr>
                                            <td><%#Eval("Rownumber") %></td>
                                            <td><%#Eval("UserName") %></td>
                                            <td><%#Eval("OperateIP") %> </td>
                                            <td><%#Eval("OperateTime") %> </td>
                                            <td><%#GetCut(Convert.ToString( Eval("OperateDescriptions")),200) %> </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>

                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-5">
                        <div class="dataTables_info" id="example2_info1" role="status" aria-live="polite">总计 <%=AspNetPager1.RecordCount %>条记录，每页显示<%=AspNetPager1.PageSize %>条 </div>
                    </div>

                    <div class="col-sm-7">
                        <div class="dataTables_paginate" id="example1_paginate">
                            <webdiyer:AspNetPager ShowPageIndexBox="Never" ID="AspNetPager1" runat="server" Width="100%" PageSize="16" PrevPageText="上一页" NextPageText="下一页" AlwaysShow="true" OnPageChanging="AspNetPager1_PageChanging" ShowFirstLast="False" PagingButtonsClass="paginate_button" CssClass="pagination" LayoutType="Ul" PagingButtonLayoutType="UnorderedList" PagingButtonSpacing="0" CurrentPageButtonClass="active" PrevNextButtonsClass="paginate_button previous">
                            </webdiyer:AspNetPager>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</asp:Content>
