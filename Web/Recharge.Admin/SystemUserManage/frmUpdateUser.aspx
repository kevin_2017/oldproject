﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmUpdateUser.aspx.cs" Inherits="Recharge.Admin.SystemUserManage.frmUpdateUser" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!-- Bootstrap 3.3.4 -->
    <link href="/Content/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="/Content/dist/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="/Content/dist/css/ionicons.min.css" rel="stylesheet" type="text/css" />

    <!-- Theme style -->
    <link href="/Content/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins 
         folder instead of downloading all of them to reduce the load. -->
    <link href="/Content/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
    <script src="/Content/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="/Content/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

    <!-- SlimScroll -->
    <script src="/Content/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src='/Content/plugins/fastclick/fastclick.min.js'></script>
    <!-- AdminLTE App -->
    <script src="/Content/dist/js/app.min.js" type="text/javascript"></script>
    <script src="/Content/dist/js/demo.js" type="text/javascript"></script>

    <script src="/Content/layer/layer.js"></script>
    <script src="/Content/jquery.json.min.js"></script>
    <script>
        var model = {

        };

        function getQueryString(name) {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
            var r = window.location.search.substr(1).match(reg);
            if (r != null) return unescape(r[2]);
            return null;
        };

        $(function () {
            $.ajax({
                type: "POST",
                url: "frmUpdateUser.aspx/GetRole",
                async: false,
                data: {},
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    var index = layer.load(1, {
                        shade: [0.8, '#fff'] //0.1透明度的白色背景 
                    });
                },
                success: function (data) {
                    if (data.d.Data.length > 0) {
                        var rolehtml = "";
                        $.each(data.d.Data, function (i, ite) {
                            rolehtml += "<option value=\"" + ite["ID"] + "\">" + ite["RoleName"] + "</option>";
                        });
                        $("#selectRole").html(rolehtml);
                    }
                },
                error: function (err) {
                    layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                }
            });
            //获取用户信息
            $.ajax({
                type: "POST",
                url: "frmUpdateUser.aspx/GetUserInfo",
                async: false,
                data: $.toJSON({ userID: getQueryString("id") }),
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    var index = layer.load(1, {
                        shade: [0.8, '#fff'] //0.1透明度的白色背景 
                    });
                },
                success: function (data) {
                    if (data.d.Rows.length > 0) {
                        model = data.d.Rows[0];
                        if (model != null) {
                            $("#txtUserName").val(model.UserName);
                            $("#txtTrueName").val(model.TrueName);
                            $("#selectSex option[value='" + (model.Sex) + "']").attr("selected", "selected");
                            $("#selectStatus option[value='" + (model.Status) + "']").attr("selected", "selected");
                            layer.closeAll();
                        } else {
                            layer.alert('对不起，获取用户信息失败！', { icon: 2 }, function () { layer.closeAll(); });
                        }
                    }
                },
                error: function (err) {
                    layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                }
            });
            //

            $.ajax({
                type: "POST",
                url: "frmUpdateUser.aspx/GetUserRole",
                async: false,
                data: $.toJSON({ userID: getQueryString("id") }),
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    var index = layer.load(1, {
                        shade: [0.8, '#fff'] //0.1透明度的白色背景 
                    });
                },
                success: function (data) {
                    if (data.d.Rows.length > 0) {

                        $("#selectRole option[value='" + (data.d.Data.RoleID) + "']").attr("selected", "selected");
                        layer.closeAll();

                    } else {
                        layer.alert('对不起，获取用户信息失败！', { icon: 2 }, function () { layer.closeAll(); });
                    }
                },
                error: function (err) {
                    layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                }
            });

        });

        function UpdateUserInfo() {
            model.UserName = $("#txtUserName").val();
            model.TrueName = $("#txtTrueName").val();
            model.Sex = parseInt($("#selectSex").val());
            model.Status = parseInt($("#selectStatus").val());
            var userRoldId = $("#selectRole").val();
            if (model.TrueName == "") {
                layer.alert('请输入昵称！', { icon: 2 }, function () { layer.closeAll(); $("#txtTrueName").focus(); });
            } else {
                $.ajax({
                    type: "POST",
                    url: "frmUpdateUser.aspx/UpdateUserInfo",
                    async: false,
                    data: $.toJSON({ userId: model.ID, trueName: model.TrueName, sex: model.Sex, status: model.Status, roldId: userRoldId }),
                    contentType: "application/json;charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        var index = layer.load(1, {
                            shade: [0.8, '#fff'], //0.1透明度的白色背景 
                        });
                    },
                    success: function (data) {
                        if (data.d.Success) {
                            layer.alert('修改用户信息及角色成功！', { icon: 1 }, function () { layer.closeAll(); });
                        } else {
                            layer.alert('对不起，修改用户信息失败！', { icon: 2 }, function () { layer.closeAll(); });
                        }
                    },
                    error: function (err) {
                        layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                    }
                });
            }
        }
    </script>
</head>
<body class="skin-blue">
    <section class="content">
        <div class="row">
            <div class="box box-info">
                <div class="box-header">
                </div>
                <div class="box-body">
                    <div class="input-group">
                        <span class="input-group-addon">用户名</span>
                        <input type="text" class="form-control" placeholder="用户名" id="txtUserName" readonly />
                    </div>
                    <br />
                    <div class="input-group">
                        <span class="input-group-addon">昵称</span>
                        <input type="text" class="form-control" placeholder="昵称" id="txtTrueName" />
                    </div>
                    <br />
                    <div class="input-group">
                        <span class="input-group-addon">角色</span>
                        <select id="selectRole" class="form-control">
                            <%--<option value="0">一般角色</option>
                            <option value="1">系统角色</option>--%>
                        </select>
                    </div>
                    <br />
                    <div class="input-group">
                        <span class="input-group-addon">性别</span>
                        <select id="selectSex" class="form-control">
                            <option value="0">保密</option>
                            <option value="1">男</option>
                            <option value="2">女</option>
                        </select>
                    </div>
                    <br />
                    <div class="input-group">
                        <span class="input-group-addon">当前状态</span>
                        <select id="selectStatus" class="form-control">
                            <option value="0">禁用</option>
                            <option value="1">正常</option>
                            <option value="2">锁定</option>
                            <option value="3">离职</option>
                        </select>
                    </div>
                </div>
                <div class="box-footer">
                    <input type="button" class="btn btn-primary" value="保存" onclick="UpdateUserInfo()" />
                </div>
            </div>
        </div>
    </section>
</body>
</html>
