﻿using CE.Utility;
using Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Recharge.Admin.SystemUserManage
{
    public partial class frmUserList : BasePageClass
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitPage(1);
            }
        }
        public void InitPage(int pageIndex)
        {
            AspNetPager1.CurrentPageIndex = pageIndex;
            PageAttribute page = new PageAttribute
            {
                TableName = "vw_SystemUserInfo",
                Columns =
                    @"  RoleName,  UserID,  RoleID, UserName, TrueName, PWD, Sex, Status, LastLoginIP,  LastLoginTime,  LoginIP,  LoginTime, LoginCounts, ISDelete",
                StrOrder = " UserID asc",
                WhereCondition = $" and 1=1 {StrWhere}",
                PageIndex = pageIndex - 1,
                PageSize = AspNetPager1.PageSize
            };

            DataTable dt = new BLL.SYS_UserInfo().GetListByPage(page);
            //UserPager.RecordCount = page.TotalRowCount ?? 0;
            //UserPager.PageIndex = pageIndex;
            AspNetPager1.RecordCount = page.TotalRowCount.Value;
            repList.DataSource = dt;
            repList.DataBind();
        }

        protected void AspNetPager1_PageChanging(object src, Wuqi.Webdiyer.PageChangingEventArgs e)
        {
            AspNetPager1.CurrentPageIndex = e.NewPageIndex;
            InitPage(AspNetPager1.CurrentPageIndex);
        }

        protected void btnSearch_Click(object obj, EventArgs e)
        {
            if (txtUserName.Value.Trim() != "")
            {
                StrWhere = " and UserName like '%" + this.txtUserName.Value.Trim() + "%'";
            }
            else
            {
                StrWhere = "";
            }
            InitPage(1);
        }

        [WebMethod]
        public static JsonResult<bool> UpdateUserInfo(long userID, string path)
        {
            if (GetButtonRole(path, "btnDelete"))
            {
                if (new BLL.SYS_UserInfo().UpdateUserISDelete(userID, 1))
                {
                    AddOperationLog($"【删除用户】Id：{userID}");
                    return new JsonResult<bool>(true, "删除成功！");
                }
                else
                {
                    return new JsonResult<bool>(false, "对不起，删除失败！");
                }
            }
            else
            {
                return new JsonResult<bool>(false, "对不起，您没有此权限！");
            }
        }


        [WebMethod]
        public static JsonResult<bool> ResetUserInfoPassword(long userID, string password, string path)
        {
            if (GetButtonRole(path, "btnReset"))
            {
                if (new BLL.SYS_UserInfo().UpdatePassword(userID, password))
                {
                    AddOperationLog($"【重置用户密码】用户Id：{userID}");
                    return new JsonResult<bool>(true, "重置成功！");
                }
                else
                {
                    return new JsonResult<bool>(false, "对不起，重置失败！");
                }
            }
            else
            {
                return new JsonResult<bool>(false, "对不起，您没有此权限！");
            }
        }
    }
}