﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="frmUserList.aspx.cs" Inherits="Recharge.Admin.SystemUserManage.frmUserList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <!-- DATA TABLES -->
    <link href="/Content/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
    <!-- DATA TABES SCRIPT -->
    <%-- <script src="/Content/plugins/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="/Content/plugins/datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>--%>
    <script>
        var urlPath = window.location.pathname;

        function UpdateUserIfo(userId) {
            if (userId == 1001) {
                layer.alert("超管不能进行信息更改");
                return;
            }
            layer.open({
                type: 2,
                title: "修改用户信息",
                shadeClose: false,
                shade: 0.8,
                area: ['800px', '500px'],
                content: "frmUpdateUser.aspx?id=" + userId,
                close: function (index) {
                    window.location.reload();
                },
                cancel: function (index) {
                    window.location.reload();
                }
            });
        }

        function DeleteUserIfo(userId, userName) {
            if (userId == 1001) {
                layer.alert("超管不能被删除");
                return;
            }
            layer.confirm("确定要删除此用户？", { title: '删除用户', btn: ['确定', '取消'], closeBtn: 0, shadeClose: true, icon: 3 },
                function () {
                    /*删除*/
                    $.ajax({
                        type: "POST",
                        url: "frmUserList.aspx/UpdateUserInfo",
                        async: false,
                        data: $.toJSON({ userID: userId, path: urlPath }),
                        contentType: "application/json;charset=utf-8",
                        dataType: "json",
                        beforeSend: function () {
                            var index = layer.load(1, {
                                shade: [0.8, '#fff'], //0.1透明度的白色背景 
                            });
                        },
                        success: function (data) {
                            if (data.d.Success) {
                                layer.alert('删除成功！', { icon: 1 }, function () { layer.closeAll(); window.location.reload(); });
                            } else {
                                layer.alert('对不起，修改失败！', { icon: 2 }, function () { layer.closeAll(); });
                            }
                        },
                        error: function (err) {
                            layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                        }
                    });
                    /*End删除*/

                }, function () {
                    layer.closeAll();
                })
        }

        function ResetUserIfo(userId, userName) {

            if (userId == 1001) {
                layer.alert("超管不允许密码重置");
                return;
            }
            var body = "<div>" +
                "新密码： <input id='txtNewPassword' style='width: 140px;' type='text'  maxlength='16' value='' />" +
                "</div>";
            var newpassowrd = "";
            layer.alert(body, {
                skin: '', title: '修改‘' + userName + '’密码'
            }, function () {
                newpassowrd = $("#txtNewPassword").val();
                if (newpassowrd == "") {
                    alert("请输入正确的密码！");
                    $("#txtNewPassword").focus();
                    //layer.alert('请输入正确的密码！', { icon: 2 }, function (index) { layer.close(index); });
                    return;
                }
                layer.closeAll();
                layer.confirm("确定要重置用户密码？", { title: '重置用户密码', btn: ['确定', '取消'], closeBtn: 0, shadeClose: true, icon: 3 }, function () {
                    $.ajax({
                        type: "POST",
                        url: "frmUserList.aspx/ResetUserInfoPassword",
                        async: false,
                        //long productID, int iSPrint, int operType
                        data: $.toJSON({ userID: userId, password: newpassowrd, path: urlPath }),
                        contentType: "application/json;charset=utf-8",
                        dataType: "json",
                        beforeSend: function () {
                            var index = layer.load(1, {
                                shade: [0.8, '#fff'], //0.1透明度的白色背景 
                            });
                        },
                        success: function (data) {
                            if (data.d.Success) {
                                layer.alert("密码修改成功！", { icon: 1 }, function () {
                                    window.location.reload();
                                    layer.closeAll();
                                });
                            } else {
                                layer.alert("修改失败！", { icon: 1 }, function () {
                                    layer.closeAll();
                                });
                            }
                        },
                        error: function (err) {
                            layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                        }
                    });
                }, function () {
                    layer.closeAll();
                })
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="col-xs-12">
        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="dataTables_length" id="example1_length">
                            <label>查询：</label>
                            <label>
                                <input runat="server" id="txtUserName" class="form-control" placeholder="用户名" aria-controls="datatable-default" /></label>
                            <label>
                                <asp:Button runat="server" ID="btnSearch" Text="查询" CssClass="btn btn-success" OnClick="btnSearch_Click" /></label>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>昵称</th>
                                    <th>用户名</th>
                                    <th>系统角色</th>
                                    <th>当前状态</th>
                                    <th>登录次数</th>
                                    <th>登录IP</th>
                                    <th>登录时间</th>
                                    <th>操作</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater runat="server" ID="repList">
                                    <ItemTemplate>
                                        <tr>
                                            <td><%#Eval("UserID") %></td>
                                            <td><%#Eval("TrueName") %></td>
                                            <td><%#Eval("UserName") %></td>
                                            <td><%#Eval("RoleName") %></td>
                                            <td><%#Convert.ToInt32(Eval("Isdelete"))==0?(  Convert.ToInt32( Eval("Status"))==1?"<span class=\"btn btn-success btn-xs\" disabled=\"disabled\">正常</span>":(Convert.ToInt32( Eval("Status"))==0?"<span class=\"btn btn-info btn-xs\" disabled=\"disabled\">禁用</span>":(Convert.ToInt32( Eval("Status"))==2?"<span class=\"btn btn-warning btn-xs\" disabled=\"disabled\">锁定</span>":"<span class=\"btn btn-danger btn-xs\" disabled=\"disabled\">离职</span>"))):"<span class=\"btn btn-info btn-xs\" disabled=\"disabled\">删除</span>" %></td>
                                            <td style="text-align: center; vertical-align: middle"><%#Eval("LoginCounts") %></td>
                                            <td>当前：<%#Eval("LoginIP") %><br />
                                                上次：<%#Eval("LastLoginIP") %></td>

                                            <td>当前：<%#Eval("LoginTime") %><br />
                                                上次：<%#Eval("LastLoginTime") %></td>
                                            <td>
                                                <a class="btn btn-success" onclick='UpdateUserIfo(<%#Eval("UserID")%>)' href="javascript:void(0)">编辑</a>
                                                <a class="btn btn-info" onclick='DeleteUserIfo(<%#Eval("UserID")%>,"<%#Eval("UserName") %>")' href="javascript:void(0)">删除</a>
                                                <a class="btn btn-warning" onclick='ResetUserIfo(<%#Eval("UserID")%>,"<%#Eval("UserName") %>")' href="javascript:void(0)">密码重置</a></td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>

                        </table>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-5">
                        <div class="dataTables_info" id="example2_info1" role="status" aria-live="polite">总计 <%=AspNetPager1.RecordCount %>条记录，每页显示<%=AspNetPager1.PageSize %>条 </div>
                    </div>
                    <div class="col-sm-7">
                        <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate1">
                            <webdiyer:AspNetPager runat="server" ID="AspNetPager1" Width="100%" PageSize="16" FirstPageText="首页" LastPageText="末页" PrevPageText="上一页" NextPageText="下一页" PagingButtonsClass="paginate_button" CssClass="pagination" LayoutType="Ul" CurrentPageButtonClass="active" OnPageChanging="AspNetPager1_PageChanging" AlwaysShow="true" FirstLastButtonsClass="paginate_button" PagingButtonLayoutType="UnorderedList"></webdiyer:AspNetPager>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->

</asp:Content>
