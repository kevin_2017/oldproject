﻿using CE.Utility;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Recharge.Admin.SystemUserManage
{
    public partial class frmAddUser : BasePageClass
    {
        protected void Page_Load(object sender, EventArgs e)
        {



        }

        [WebMethod]
        public static JsonResult<List<Model.SYS_Role>> GetRole()
        {
            Model.StrWhere str = new Model.StrWhere();
            str.IsWhereExist = false;
            return new JsonResult<List<Model.SYS_Role>>(new BLL.SYS_Role().GetModelList(str));
        }
        [WebMethod]
        public static JsonResult<bool> AddUserInfo(string userName, string trueName, int sex, int status, string password, int roleID)
        {
            Model.SYS_UserInfo model = new Model.SYS_UserInfo()
            {
                UserName = userName,
                TrueName = trueName,
                Sex = sex,
                Status = status,
                PWD = password,
                ISDelete = 0,
                ISLocking = 0
            };
            var result = new BLL.SYS_UserInfo().Add(model);
            Model.SYS_Role_User userRoleModel = new Model.SYS_Role_User()
            {
                UserID = result,
                RoleID = roleID
            };
            new BLL.SYS_Role_User().Add(userRoleModel);

            AddOperationLog($"【添加用户】用户名：{userName},{JsonConvert.SerializeObject(model)}");
            return new JsonResult<bool>(result > 0);
        }
    }
}