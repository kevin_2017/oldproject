﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="frmAddUser.aspx.cs" Inherits="Recharge.Admin.SystemUserManage.frmAddUser" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        $(function () {
            $.ajax({
                type: "POST",
                url: "frmUpdateUser.aspx/GetRole",
                async: false,
                data: {},
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    var index = layer.load(1, {
                        shade: [0.8, '#fff'] //0.1透明度的白色背景 
                    });
                },
                success: function (data) {
                    if (data.d.Data.length > 0) {
                        var rolehtml = "";
                        $.each(data.d.Data, function (i, ite) {
                            rolehtml += "<option value=\"" + ite["ID"] + "\">" + ite["RoleName"] + "</option>";
                        });
                        $("#selectRole").html(rolehtml);
                        layer.closeAll();
                    }
                },
                error: function (err) {
                    layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                }
            });
        });
        function UpdateUserInfo() {
            var model = {};
            model.UserName = $("#txtUserName").val();
            model.Password = $("#txtPassword").val();
            model.TrueName = $("#txtTrueName").val();
            model.Sex = parseInt($("#selectSex").val());
            model.Status = parseInt($("#selectStatus").val());
            var userRoldId = $("#selectRole").val();
            if (model.UserName == "") {
                layer.alert('请输入用户名！', { icon: 2, title: '错误信息' }, function () { layer.closeAll(); $("#txtUserName").focus(); });
            } else if (model.Password == "") {
                layer.alert('请输入密码！', { icon: 2, title: '错误信息' }, function () { layer.closeAll(); $("#txtPassword").focus(); });
            } else if (model.TrueName == "") {
                layer.alert('请输入真实姓名！', { icon: 2, title: '错误信息' }, function () { layer.closeAll(); $("#txtTrueName").focus(); });
            } else {
                $.ajax({
                    type: "POST",
                    url: "frmAddUser.aspx/AddUserInfo",
                    async: false,
                    data: $.toJSON({ userName: model.UserName, trueName: model.TrueName, sex: model.Sex, status: model.Status, password: model.Password, roleID: userRoldId }),
                    contentType: "application/json;charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        var index = layer.load(1, {
                            shade: [0.8, '#fff'], //0.1透明度的白色背景 
                        });
                    },
                    success: function (data) {
                        if (data.d.Success) {
                            layer.alert('添加成功！', { icon: 1 }, function () { layer.closeAll(); });

                        } else {
                            layer.alert('对不起，添加用户信息失败！', { icon: 2 }, function () { layer.closeAll(); });
                        }

                    },
                    error: function (err) {
                        layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                    }
                });
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="box box-info">
        <div class="box-header">
        </div>
        <div class="box-body">
            <div class="input-group">
                <span class="input-group-addon">用户名</span>
                <input type="text" class="form-control" placeholder="用户名" id="txtUserName" />
            </div>
            <br />
            <div class="input-group">
                <span class="input-group-addon">密码</span>
                <input type="text" class="form-control" placeholder="密码" id="txtPassword" />
            </div>
            <br />
            <div class="input-group">
                <span class="input-group-addon">昵称</span>
                <input type="text" class="form-control" placeholder="昵称" id="txtTrueName" />
            </div>
            <br />
            <div class="input-group">
                <span class="input-group-addon">角色</span>
                <select id="selectRole" class="form-control">
                    <%--<option value="0">一般角色</option>
                            <option value="1">系统角色</option>--%>
                </select>
            </div>
            <br />
            <div class="input-group">
                <span class="input-group-addon">性别</span>
                <select id="selectSex" class="form-control">
                    <option value="0">保密</option>
                    <option value="1">男</option>
                    <option value="2">女</option>
                </select>
            </div>
            <br />
            <div class="input-group">
                <span class="input-group-addon">状态</span>
                <select id="selectStatus" class="form-control">
                    <option value="0">禁用</option>
                    <option value="1">正常</option>
                    <option value="2">锁定</option>
                    <option value="3">离职</option>
                </select>
            </div>
        </div>
        <div class="box-footer">
            <input type="button" class="btn btn-primary" value="保存" onclick="UpdateUserInfo()" />
        </div>
    </div>
</asp:Content>
