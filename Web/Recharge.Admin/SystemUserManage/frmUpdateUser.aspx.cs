﻿using CE.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Recharge.Admin.SystemUserManage
{
    public partial class frmUpdateUser : BasePageClass
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        public static JsonResult<Model.SYS_UserInfo> GetUserInfo(long userID)
        {
            return new JsonResult<Model.SYS_UserInfo>(new BLL.SYS_UserInfo().GetModel(userID));
        }

        [WebMethod]
        public static JsonResult<bool> UpdateUserInfo(long userId, string trueName, int sex, int status, int roldId)
        {
            Model.SYS_UserInfo userModel = new BLL.SYS_UserInfo().GetModel(userId);
            userModel.TrueName = trueName;
            userModel.Sex = sex;
            userModel.Status = status;
            Model.SYS_Role_User roleModel = new Model.SYS_Role_User();

            Model.StrWhere str = new Model.StrWhere()
            {
                IsWhereExist = true,
                strWhere = new System.Text.StringBuilder("userid=" + userModel.ID)
            };
            roleModel = new BLL.SYS_Role_User().GetModel(str);
            roleModel.RoleID = roldId;
            new BLL.SYS_Role_User().Update(roleModel);

            var result = new BLL.SYS_UserInfo().UpdateBaseInfo(userModel);

            AddOperationLog($"【修改用户信息】用户名：{userModel.UserName},真实姓名：{userModel.TrueName}");
            return new JsonResult<bool>(result);
        }


        [WebMethod]
        public static JsonResult<List<Model.SYS_Role>> GetRole()
        {
            Model.StrWhere str = new Model.StrWhere();
            str.IsWhereExist = false;
            return new JsonResult<List<Model.SYS_Role>>(new BLL.SYS_Role().GetModelList(str));
        }


        [WebMethod]
        public static JsonResult<Model.SYS_Role_User> GetUserRole(long userID)
        {
            Model.StrWhere str = new Model.StrWhere();
            str.IsWhereExist = true;
            str.strWhere = new System.Text.StringBuilder(" userid=" + userID);
            return new JsonResult<Model.SYS_Role_User>(new BLL.SYS_Role_User().GetModel(str));
        }


    }
}