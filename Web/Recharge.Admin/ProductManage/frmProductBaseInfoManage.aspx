﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="frmProductBaseInfoManage.aspx.cs" Inherits="Recharge.Admin.ProductManage.frmProductBaseInfoManage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/Content/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
    <script>

        var urlPath = window.location.pathname;

        function DeleteProductInfo(id, productName, IsDelete) {
            var msg = "";
            if (IsDelete == 1)
                msg = "恢复";
            else
                msg = "删除";
            layer.confirm("确定要" + msg + "商品 " + productName + "？",
                { title: msg + '商品', btn: ['确定', '取消'], closeBtn: 0, icon: 3 },
                function () {

                    /*删除*/
                    $.ajax({
                        type: "POST",
                        url: "frmProductBaseInfoManage.aspx/DelProductInfo",
                        async: false,
                        data: $.toJSON({ ID: id, url: urlPath, btnName: 'btnDelete', operType: IsDelete }),
                        contentType: "application/json;charset=utf-8",
                        dataType: "json",
                        beforeSend: function () {
                            var index = layer.load(1,
                                {
                                    shade: [0.8, '#fff'], //0.1透明度的白色背景 
                                });
                        },
                        success: function (data) {
                            if (data.d.Data) {
                                layer.alert(msg + '成功！',
                                    { icon: 1 },
                                    function () {
                                        layer.closeAll();
                                        window.location.reload();
                                    });
                            } else {
                                layer.alert('对不起，' + msg + '失败！', { icon: 2 }, function () { layer.closeAll(); });
                            }

                        },
                        error: function (err) {
                            layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                        }
                    });
                    /*End删除*/

                },
                function () {
                    layer.closeAll();
                });



        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="dataTables_length" id="example1_length">
                            <label>运营商类型：</label>
                            <label>
                                <asp:DropDownList runat="server" ID="ddlIspType" class="form-control" placeholder="商品类型" aria-controls="datatable-default" Width="150px">
                                    <asp:ListItem Text="请选择" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="中国联通" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="中国移动" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="中国电信" Value="3"></asp:ListItem>
                                </asp:DropDownList></label>
                            <label>商品类型：</label>
                            <label>
                                <asp:DropDownList runat="server" ID="ddlProductType" class="form-control" placeholder="商品类型" aria-controls="datatable-default" Width="150px">
                                </asp:DropDownList></label>
                            &nbsp;<label>当前状态：</label>
                            <label>
                                <asp:DropDownList runat="server" ID="ddlStatus" class="form-control">
                                    <asp:ListItem Value="-1">--请选择--</asp:ListItem>
                                    <asp:ListItem Value="1">正常上架</asp:ListItem>
                                </asp:DropDownList></label>

                            <label>
                                <asp:TextBox runat="server" ID="txtProductName" class="form-control" placeholder="商品名称" aria-controls="datatable-default" Width="150px" /></label>
                            <label>
                                <asp:TextBox runat="server" ID="txtDenomination" class="form-control" placeholder="面额" aria-controls="datatable-default" Width="150px" /></label>

                            <label>
                                <asp:Button runat="server" ID="btnSearch" Text="查询" CssClass="btn btn-success" OnClick="btnSearch_Click" /></label>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>商品ID</th>
                                    <th>商品名</th>
                                    <th>商品类型</th>
                                    <th>当前状态</th>
                                    <th>面值</th>
                                    <th>运营商类型</th>
                                    <th>充值对象</th>
                                    <th>操作时间</th>
                                    <th>操作人</th>
                                    <th>操作</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater runat="server" ID="repList">
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <%#Eval("ID") %>

                                            </td>
                                            <td>系统：<%#Eval("ID") %>
                                            </td>
                                            <td><%#Eval("ProductName") %></td>
                                            <td><%#Eval("ProductTypeName") %></td>
                                            <td><%# Convert.ToInt32( Eval("IsDelete"))==0? Convert.ToInt32( Eval("Status"))==1?"正常上架":(Convert.ToInt32( Eval("Status"))==2?"商城单独上架":(Convert.ToInt32( Eval("Status"))==3?"商城接口上架":(Convert.ToInt32( Eval("Status"))==4?"商城会员上架":(Convert.ToInt32( Eval("Status"))==5?"接口单独上架":(Convert.ToInt32( Eval("Status"))==6?"接口会员上架":(Convert.ToInt32( Eval("Status"))==7?"会员单独上架":"全部下架")))))):"已删除" %></td>

                                            <td><%#Eval("denomination") %></td>
                                            <td><%#Convert.ToInt32( Eval("ISPType"))==1?"中国联通":(Convert.ToInt32( Eval("ISPType"))==2?"中国移动":(Convert.ToInt32( Eval("ISPType"))==3?"中国电信":"其他")) %></td>
                                            <td><%#Eval("RechargeRange")==DBNull.Value ?"": Convert.ToInt32(Eval("RechargeRange") )==1?"固定":"自定义"%></td>
                                            <td><%#Eval("AddTime") %></td>
                                            <td><%#Eval("Updator") %></td>
                                            <td>
                                                <a class="btn btn-info btn-xs" onclick='DeleteProductInfo(<%#Eval("ID")%>,"<%#Eval("ProductName") %>",<%#Eval("IsDelete") %>)' href="javascript:void(0)"><%# Convert.ToInt32(Eval("IsDelete"))==1?"恢复":"删除" %></a>

                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>

                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-5">
                        <div class="dataTables_info" id="example2_info1" role="status" aria-live="polite">总计 <%=AspNetPager1.RecordCount %>条记录，每页显示<%=AspNetPager1.PageSize %>条 </div>
                    </div>

                    <div class="col-sm-7">
                        <div class="dataTables_paginate" id="example1_paginate">
                            <webdiyer:AspNetPager ShowPageIndexBox="Never" ID="AspNetPager1" runat="server" Width="100%" PageSize="16" PrevPageText="上一页" NextPageText="下一页" AlwaysShow="true" OnPageChanging="AspNetPager1_PageChanging" ShowFirstLast="False" PagingButtonsClass="paginate_button" CssClass="pagination" LayoutType="Ul" PagingButtonLayoutType="UnorderedList" PagingButtonSpacing="0" CurrentPageButtonClass="active" PrevNextButtonsClass="paginate_button previous">
                            </webdiyer:AspNetPager>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</asp:Content>
