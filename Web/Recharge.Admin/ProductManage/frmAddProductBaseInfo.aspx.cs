﻿using CE.Utility;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Recharge.Admin.ProductManage
{
    public partial class frmAddProductBaseInfo : BasePageClass
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static JsonResult<List<Model.V8_Product_Type>> GetAllProductType()
        {
            Model.StrWhere str = new Model.StrWhere { IsWhereExist = false };
            return new JsonResult<List<Model.V8_Product_Type>>(new BLL.V8_Product_Type().GetModelList(str));
        }

        [WebMethod]
        public static JsonResult<bool> AddProductInfo(Model.V8_Product_Manage model)
        {

            Model.StrWhere str = new Model.StrWhere
            {
                IsWhereExist = true,
                strWhere = new StringBuilder($" ISPType={model.ISPType} and ProductTypeID={model.ProductTypeID} and PhoneNumType={model.PhoneNumType} and ProductName like '{model.ProductName}'")
            };
            var result = new BLL.V8_Product_Manage().GetCount(str);
            if (result > 0)
            {
                return new JsonResult<bool>(false, new List<string>() { "该商品已存在" });
            }
            AddOperationLog($"【增加商品基础信息】商品名：{model.ProductName},{JsonConvert.SerializeObject(model)}");
            return new JsonResult<bool>(new BLL.V8_Product_Manage().Add(model) > 0);
        }
    }
}