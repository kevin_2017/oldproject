﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="frmAddProductBaseInfo.aspx.cs" Inherits="Recharge.Admin.ProductManage.frmAddProductBaseInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        var baseInfo = { ISPType: 1, ProductTypeID: 1003, PhoneNumType: 1, RechargeRange: 1, ProductTypeName: '', ProductName: '', denomination: '', ISPName: '', Status: '' };

        $(function () {
            var innerhtml = "";
            $.ajax({
                type: "POST",
                url: "frmAddProductBaseInfo.aspx/GetAllProductType",
                async: false,
                data: {},
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    var index = layer.load(1, {
                        shade: [0.8, '#fff'], //0.1透明度的白色背景 
                    });
                },
                success: function (data) {
                    if (data.d.Data.length > 0) {

                        $.each(data.d.Data, function (i, item) {
                            innerhtml += "  <option value=\"" + item["ID"] + "\">" + item["TypeName"] + "</option>";
                        });
                        $("#ddlProductType").html(innerhtml);
                        $("#selectISPType").val(baseInfo.ISPType);//1、联通 2、移动 3、电信

                        $("#ddlProductType").val(baseInfo.ProductTypeID);

                        $("#selectPhoneNumType").val(baseInfo.PhoneNumType);
                        layer.closeAll();
                    } else {
                        layer.alert("获取商品类型失败", { icon: 2 }, function () { layer.closeAll(); });
                    }

                },
                error: function (err) {
                    layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                }
            });
        });
        function SaveInfo() {
            baseInfo.ProductTypeID = $("#ddlProductType").val();
            baseInfo.ProductTypeName = $("#ddlProductType").find("option:selected").text();
            baseInfo.ProductName = $("#txtProductName").val();
            baseInfo.denomination = $("#txtdenomination").val();
            baseInfo.Status = $("#selectStatus").val();;
            baseInfo.ISPType = $("#selectISPType").val();
            baseInfo.ISPName = $("#selectISPType").find("option:selected").text();
            baseInfo.PhoneNumType = $("#selectPhoneNumType").val();

            if (baseInfo.ProductName == "") {
                layer.alert('商品名称不为能空！', { icon: 2 }, function () { layer.closeAll(); });
                return;
            };
            if (baseInfo.denomination == "") {
                layer.alert('商品面值不为能空！', { icon: 2 }, function () { layer.closeAll(); });
                return;
            };
            $.ajax({
                type: "POST",
                url: "frmAddProductBaseInfo.aspx/AddProductInfo",
                async: false,
                data: $.toJSON({ model: baseInfo }),
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    var index = layer.load(1,
                        {
                            shade: [0.8, '#fff'] //0.1透明度的白色背景 
                        });
                },
                success: function (data) {
                    if (data.d.Data) {

                        layer.alert('添加商品成功！', { icon: 1 }, function () { layer.closeAll(); });
                    } else {
                        layer.alert(data.d.Message, { icon: 2 }, function () { layer.closeAll(); });
                    }
                },
                error: function (err) {
                    layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                }
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="box box-info">
        <div class="box-header">
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-sm-6">
                    <div class="input-group">
                        <span class="input-group-addon">运营商名</span>
                        <select id="selectISPType" class="form-control">
                            <option value="1">中国联通</option>
                            <option value="2">中国移动</option>
                            <option value="3">中国电信</option>
                        </select>
                    </div>
                    <br />
                    <div class="input-group">
                        <span class="input-group-addon">商品类型</span>
                        <select id="ddlProductType" class="form-control">
                        </select>
                    </div>
                    <br />
                    <div class="input-group">
                        <span class="input-group-addon">号码类型</span>
                        <select id="selectPhoneNumType" class="form-control">
                            <option value="1">手机</option>
                            <option value="2">固话</option>
                        </select>
                    </div>
                    <br />
                    <div class="input-group">
                        <span class="input-group-addon">商品名称</span>
                        <input type="text" id="txtProductName" class="form-control" placeholder="商品名称，此项填写后不能修改" />
                    </div>
                    <br />
                    <div class="input-group">
                        <span class="input-group-addon">商品面值</span>
                        <input type="text" id="txtdenomination" class="form-control" placeholder="商品面值，此项填写后不能修改" />
                    </div>
                    <br />
                    <div class="input-group">
                        <span class="input-group-addon">当前状态</span>
                        <select id="selectStatus" class="form-control">
                            <option value="1">正常上架</option>
                            <option value="2">全部下架</option>

                        </select>
                    </div>


                </div>
            </div>
            <div class="box-footer">
                <input type="button" class="btn btn-primary" value="保存" onclick="SaveInfo()" />
            </div>
        </div>
    </div>
</asp:Content>
