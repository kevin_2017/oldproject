﻿using CE.Utility;
using Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Recharge.Admin.ProductManage
{
    public partial class frmProductBaseInfoManage : BasePageClass
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                Model.StrWhere str = new Model.StrWhere();
                str.IsWhereExist = false;

                SetDropDownList.BindDropDownList(ddlProductType, new BLL.V8_Product_Type().GetDataSet(str).Tables[0], "TypeName", "ID");

                AspNetPager1.CurrentPageIndex = 1;

                InitPage();

            }
        }

        public void InitPage()
        {
            PageAttribute page = new PageAttribute();
            page.TableName = "V8_Product_Manage";
            page.Columns = @"id,AddTime,RechargeRange,CreateTime,Creator,UpdateTime,Updator,IsDelete,ProductTypeID,ProductTypeName,ProductName,denomination,Status,ISPType,ISPName,PhoneNumType";
            page.StrOrder = "Id , denomination asc";
            page.WhereCondition = string.Concat(" and 1=1 ", StrWhere);
            page.PageIndex = AspNetPager1.CurrentPageIndex - 1;
            page.PageSize = AspNetPager1.PageSize;
            DataTable dt = new BLL.V8_Product_Manage().GetListByPage(page);
            AspNetPager1.RecordCount = page.TotalRowCount.Value;
            repList.DataSource = dt;
            repList.DataBind();
        }






        protected void AspNetPager1_PageChanging(object src, Wuqi.Webdiyer.PageChangingEventArgs e)
        {
            AspNetPager1.CurrentPageIndex = e.NewPageIndex;
            InitPage();
        }



        protected void btnSearch_Click(object obj, EventArgs e)
        {
            StrWhere = "";

            if (ddlIspType.SelectedItem.Value != "0")
            {
                StrWhere += " and ISPType =" + ddlIspType.SelectedItem.Value.Trim();

            }
            if (ddlProductType.SelectedItem.Value.Trim() != "-1")
            {
                StrWhere += " and ProductTypeID =" + ddlProductType.SelectedItem.Value.Trim();
            }

            if (txtProductName.Text.Trim() != "")
            {
                StrWhere += " and (ProductName  like '%" + txtProductName.Text.Trim() + "%' or ID like '%" + txtProductName.Text.Trim() + "%')";
            }
            if (ddlStatus.SelectedItem.Value != "-1")
            {
                StrWhere += " and Status =" + ddlStatus.SelectedItem.Value.Trim();
            }
            if (txtDenomination.Text.Trim() != "")
            {
                StrWhere += " and Denomination like'" + txtDenomination.Text.Trim()+"'";
            }
            AspNetPager1.CurrentPageIndex = 1;
            InitPage();
        }


        [WebMethod]
        public static JsonResult<bool> DelProductInfo(long ID, string url, string btnName, int operType)
        {
            if (GetButtonRole(url, btnName))
            {
                if (operType == 0)
                {
                    AddOperationLog($"【删除基础商品】商品ID:{ID}");
                    return new JsonResult<bool>(new BLL.V8_Product_Manage().ExecuteSql("update V8_Product_Manage set IsDelete=1 where id=" + ID) > 0);
                }
                else
                {
                    AddOperationLog($"【恢复基础商品】商品ID:{ID}");
                    return new JsonResult<bool>(new BLL.V8_Product_Manage().ExecuteSql("update V8_Product_Manage set IsDelete=0 where id=" + ID) > 0);
                }
            }
            else
            {
                return new JsonResult<bool>(false, "对不起，您没有权限！");

            }
        }
    }
}