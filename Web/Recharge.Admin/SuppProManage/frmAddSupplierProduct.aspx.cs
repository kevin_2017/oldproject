﻿using CE.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Recharge.Admin.SuppProManage
{
    public partial class frmAddSupplierProduct : BasePageClass
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        public static JsonResult<List<Model.V8_Province>> GetAllProvince()
        {
            Model.StrWhere str = new Model.StrWhere() { IsWhereExist = false };
            return new JsonResult<List<Model.V8_Province>>(new BLL.V8_Province().GetModelList(str));
        }
        [WebMethod]
        public static JsonResult<List<Model.SUP_SupplierManage>> GetAllSupplier()
        {
            Model.StrWhere str = new Model.StrWhere() { IsWhereExist = false };
            return new JsonResult<List<Model.SUP_SupplierManage>>(new BLL.SUP_SupplierManage().GetModelList(str));
        }


        [WebMethod]
        public static JsonResult<List<Model.V8_Product_Manage>> GetAllProductBaseInfo(int productTypeId, int ispType, int phoneNumType)
        {
            var strSql = new StringBuilder(" 1=1 ");
            Model.StrWhere str = new Model.StrWhere() { IsWhereExist = true };
            if (productTypeId > 0)
                strSql.Append($" and ProductTypeID={productTypeId}");
            if (ispType > 0)
                strSql.Append($" and ISPType={ispType}");
            if (phoneNumType > 0)
                strSql.Append($" and PhoneNumType={phoneNumType}");
            str.strWhere = strSql;

            return new JsonResult<List<Model.V8_Product_Manage>>(new BLL.V8_Product_Manage().GetModelList(str));
        }

        [WebMethod]
        public static JsonResult<bool> CreateProductInfo(Model.V8_Product_Supplier model,  List<ProductList> productList)
        {
            long result = 0;
            int haveCounts = 0;
            foreach (var product in productList)
            {
                 
                    Model.V8_Product_Supplier prod = new Model.V8_Product_Supplier();
                    prod = model; 
                    prod.ProductID = product.ProductID;
                    prod.ProductName = product.ProductName;
                    prod.Creator = (HttpContext.Current.Session["SystemUserLoginInfo"] as Model.SystemUserInfo).UserName;

                    Model.StrWhere str = new Model.StrWhere()
                    {
                        IsWhereExist = true,
                        strWhere = new StringBuilder($" SupplierID={model.SupplierID} and ProductTypeId={model.ProductTypeId} and ProductID={product.ProductID}")
                    };
                    if (new BLL.V8_Product_Supplier().Exists(str))
                    {
                        haveCounts++;
                        continue;
                    }
                    else
                        result += new BLL.V8_Product_Supplier().Add(prod) > 0 ? 1 : 0;
               

            }
            return new JsonResult<bool>(result > 0, haveCounts > 0 ? $"已经存在{haveCounts}条记录" : "操作成功");
        }

    }
}