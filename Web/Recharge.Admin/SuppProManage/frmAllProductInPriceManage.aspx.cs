﻿using CE.Utility;
using Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Recharge.Admin.SuppProManage
{
    public partial class frmAllProductInPriceManage : BasePageClass
    {
        protected void Page_Load(object sender, EventArgs e)
        {


            if (!IsPostBack)
            {
                Model.StrWhere str = new Model.StrWhere();
                str.IsWhereExist = false;

                SetDropDownList.BindDropDownList(ddlProductType, new BLL.V8_Product_Type().GetDataSet(str).Tables[0], "TypeName", "ID");

                SetDropDownList.BindDropDownList(ddlSupplier, new BLL.SUP_SupplierManage().GetDataSet(str).Tables[0], "TrueName", "ID");
                SetDropDownList.BindDropDownList(ddlPrvince, new BLL.V8_Province().GetDataSet(str).Tables[0], "ProvinceName", "ID");
                AspNetPager1.CurrentPageIndex = 1;

                InitPage();

            }
        }

        public void InitPage()
        {
            PageAttribute page = new PageAttribute();
            page.TableName = "V8_Product_Supplier";
            page.Columns = @"id,SupplierID,SupplierName,ProductTypeId,ProductTypeName,ProductID,ProductName,ISPType,ISPName,Inprice,Sort,IsClosed,IsDelete,ArriveMethod,SupplierProductCode,ProvinceIDs,FlowType,CreateTime,Creator,UpdateTime,Updator";
            page.StrOrder = "Id  asc";
            page.WhereCondition = string.Concat(" and 1=1 ", StrWhere);
            page.PageIndex = AspNetPager1.CurrentPageIndex - 1;
            page.PageSize = AspNetPager1.PageSize;
            DataTable dt = new BLL.V8_Product_Manage().GetListByPage(page);
            AspNetPager1.RecordCount = page.TotalRowCount.Value;
            repList.DataSource = dt;
            repList.DataBind();
        }


        [WebMethod]
        public static JsonResult<bool> GetUserBtnRole(string url, string btnName)
        {
            if (!GetButtonRole(url, btnName))
                return new JsonResult<bool>(false, "对不起，无权操作");
            else
                return new JsonResult<bool>(true, "操作成功");

        }



        protected void AspNetPager1_PageChanging(object src, Wuqi.Webdiyer.PageChangingEventArgs e)
        {
            AspNetPager1.CurrentPageIndex = e.NewPageIndex;
            InitPage();
        }



        protected void btnSearch_Click(object obj, EventArgs e)
        {
            StrWhere = "";

            if (ddlSupplier.SelectedItem.Value != "-1")
            {
                StrWhere += " and SupplierID =" + ddlSupplier.SelectedItem.Value.Trim();

            }
            if(ddlPrvince.SelectedItem.Value!="-1")
                StrWhere += " and ProvinceID =" + ddlPrvince.SelectedItem.Value.Trim();

            if (ddlIspType.SelectedItem.Value != "0")
            {
                StrWhere += " and ISPType =" + ddlIspType.SelectedItem.Value.Trim();

            }
            if (ddlProductType.SelectedItem.Value.Trim() != "-1")
            {
                StrWhere += " and ProductTypeID =" + ddlProductType.SelectedItem.Value.Trim();
            }

            if (txtProductName.Text.Trim() != "")
            {
                StrWhere += " and (ProductName  like '%" + txtProductName.Text.Trim() + "%' or ID like '%" + txtProductName.Text.Trim() + "%')";
            }
            if (ddlStatus.SelectedItem.Value != "-1")
            {
                StrWhere += " and IsClosed =" + (ddlStatus.SelectedItem.Value.Trim() == "1" ? 0 : 1);
            }
            if (txtInPrice.Text.Trim() != "")
            {
                StrWhere += " and Inprice =" + txtInPrice.Text.Trim();
            }
            AspNetPager1.CurrentPageIndex = 1;
            InitPage();
        }

    }
}