﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmUpdateSupplierProvince.aspx.cs" Inherits="Recharge.Admin.SuppProManage.frmUpdateSupplierProvince" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
 <!-- Bootstrap 3.3.4 -->
    <link href="/Content/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="/Content/dist/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="/Content/dist/css/ionicons.min.css" rel="stylesheet" type="text/css" />

    <!-- Theme style -->
    <link href="/Content/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins 
    folder instead of downloading all of them to reduce the load. -->
    <link href="/Content/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
    <script src="/Content/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="/Content/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

    <!-- SlimScroll -->
    <script src="/Content/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src='/Content/plugins/fastclick/fastclick.min.js'></script>
    <!-- AdminLTE App -->
    <script src="/Content/dist/js/app.min.js" type="text/javascript"></script>
    <script src="/Content/dist/js/demo.js" type="text/javascript"></script>

    <script src="/Content/layer/layer.js"></script>
    <script src="/Content/jquery.json.min.js"></script>
    <script>
        function getQueryString(name) {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
            var r = window.location.search.substr(1).match(reg);
            if (r != null) return unescape(r[2]);
            return null;
        };

        function operateOneMenu(box) {
            var id = box.id;
            var boxList = document.getElementsByName("linkList");
            for (var i = 0; i < boxList.length; i++) {
                if (boxList[i].id.split("_")[0] == id) {
                    boxList[i].checked = box.checked;
                }
            }
        };
        
        function submitForm() {
            var hiddenProvince = document.getElementById("<%=hfProvince.ClientID %>");
            var boxList = document.getElementsByName("linkList");
            var rightList = "";
            for (var i = 0; i < boxList.length; i++) {
                if (boxList[i].checked == true) {
                    rightList += boxList[i].value + ",";
                }
            }
            hiddenProvince.value = rightList;
            // console.log(rightList);
        }

        function chk(checkbox) {
            var item = document.getElementsByTagName("input");
            for (i = 0; i < item.length; i++) {
                if (item[i].type == "checkbox") {
                    item[i].checked = checkbox.checked;
                }
            }
        }

        function doPostBackCheck() {
            var o = window.event.srcElement;
            if (o.tagName == "INPUT" && o.type == "checkbox") {
                __doPostBack("", "");
            }
        }

    </script>
</head>
<body class="skin-blue">

    <form id="form1" runat="server">
        <section class="content">
            <div class="row">
                <div class="box box-info">
                    <div class="box-header">
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered table-striped mb-none" id="datatable-tabletools">
                            <thead>
                                <tr>
                                    <th>
                                        <asp:CheckBox ID="chkAll" runat="server" onclick="chk(this)" Text="ID" CssClass="checkall" />
                                    </th>

                                    <th>可充省份
                                    </th>

                                </tr>
                            </thead>

                            <tbody>
                                <asp:Literal runat="server" ID="ltProvinceInfo"></asp:Literal>
                            </tbody>
                        </table>
                    </div>
                    <div class="box-footer">
                        <asp:HiddenField runat="server" ID="hfProvince" />
                        <asp:Button runat="server" ID="btnSave" Text="保存" CssClass="btn btn-primary" OnClick="btnSave_Click"
                            OnClientClick="submitForm()" />
                    </div>
                </div>
            </div>
        </section>
    </form>
</body>
</html>