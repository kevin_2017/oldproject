﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.DBUtility;
using Model;

namespace Recharge.Admin.SuppProManage
{
    public partial class frmUpdateSupplierProvince : BasePageClass
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitPage();
            }
        }

        public void InitPage()
        {
            DataTable allProv = new BLL.V8_Province().GetDataSet(new StrWhere() { IsWhereExist = false }).Tables[0];

            Model.V8_Product_Supplier productSupplier = new BLL.V8_Product_Supplier().GetModel(new StrWhere() { IsWhereExist = true, strWhere = new StringBuilder($" id={Request.QueryString["id"]}") });
            string innerHtml = "";
            foreach (DataRow dr in allProv.Rows)
            {
                if (productSupplier.ProvinceIds != null && productSupplier.ProvinceIds.Contains(dr["ID"].ToString()))
                {
                    innerHtml += "<tr><td><input type=\"checkbox\" name=\"linkList\" id=\"" + dr["ID"] +
                            "\" checked=\"checked\" class=\"checker\"  onclick=\"operateOneMenu(this)\"  value=\"" +
                            dr["ID"] + "\" />" + dr["ID"] + "</td><td>" +
                            dr["ProvinceName"].ToString() + "</td></tr>";
                }
                else
                {
                    innerHtml += "<tr><td><input type=\"checkbox\" name=\"linkList\" id=\"" + dr["ID"] +
                                 "\"  class=\"checker\"  onclick=\"operateOneMenu(this)\"  value=\"" +
                                 dr["ID"] + "\" />" + dr["ID"] + "</td><td>" +
                                 dr["ProvinceName"].ToString() + "</td></tr>";
                }
            }

            ltProvinceInfo.Text = innerHtml;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string provinceIds = hfProvince.Value;
            provinceIds = provinceIds.Substring(0, provinceIds.Length - 1);
            if (provinceIds.Length < 3)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "", "  layer.alert('请选择可充省份！',{ icon: 2 });", true);
            }

            DBHelper.ExecuteSql($"update V8_Product_Supplier set ProvinceIds='{provinceIds}' where id={Request.QueryString["id"]}");

            InitPage();
            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "", "  layer.alert('分配成功！',{icon:1});", true);
        }
    }

}