﻿using CE.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Recharge.Admin.SuppProManage
{
    public partial class frmSettingProductInPrice : BasePageClass
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static JsonResult<List<Model.V8_Province>> GetAllProvince()
        {
            Model.StrWhere str = new Model.StrWhere() { IsWhereExist = false };
            return new JsonResult<List<Model.V8_Province>>(new BLL.V8_Province().GetModelList(str));
        }
        [WebMethod]
        public static JsonResult<List<Model.SUP_SupplierManage>> GetAllSupplier()
        {
            Model.StrWhere str = new Model.StrWhere() { IsWhereExist = false };
            return new JsonResult<List<Model.SUP_SupplierManage>>(new BLL.SUP_SupplierManage().GetModelList(str));
        }


        [WebMethod]
        public static JsonResult<List<Model.V8_Product_Manage>> GetAllProductBaseInfo(int productTypeId, int ispType, int phoneNumType)
        {
            var strSql = new StringBuilder(" 1=1 ");
            Model.StrWhere str = new Model.StrWhere() { IsWhereExist = true };
            if (productTypeId > 0)
                strSql.Append($" and ProductTypeID={productTypeId}");
            if (ispType > 0)
                strSql.Append($" and ISPType={ispType}");
            if (phoneNumType > 0)
                strSql.Append($" and PhoneNumType={phoneNumType}");
            str.strWhere = strSql;

            return new JsonResult<List<Model.V8_Product_Manage>>(new BLL.V8_Product_Manage().GetModelList(str));
        }

        [WebMethod]
        public static JsonResult<bool> CreateProductInfo(Model.V8_Product_Supplier model, List<ProvinceList> obj, List<ProductList> productList)
        {
            int haveCounts = 0;
            foreach (var product in productList)
            {
                foreach (var prov in obj)
                {
                    var str = new StringBuilder($"update V8_Product_Supplier set Inprice={model.Inprice} where  SupplierID={model.SupplierID} and ProductTypeId={model.ProductTypeId} and ProductID={product.ProductID} and ProvinceID={prov.ProvinceID}");
                    if (new BLL.V8_Product_Supplier().ExecuteSql(str.ToString()) > 0)
                        haveCounts++;
                }

            }
            return new JsonResult<bool>(haveCounts > 0, "操作成功");
        }

    }

    public class ProvinceList
    {

        public long ProvinceID { get; set; }
        public string ProvinceName { get; set; }
    }

    public class ProductList
    {
        public long ProductID { get; set; }
        public string ProductName { get; set; }
    }
}