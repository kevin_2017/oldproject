﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Recharge.Admin
{
    public class BasePageClass : System.Web.UI.Page, IHttpHandler
    {

        /// <summary>
        /// 初始化的时候执行的操作
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreInit(EventArgs e)
        {
            if (!IsLogin || HttpContext.Current.Session["SystemUserLoginInfo"] == null)
            {
                Response.Write("<script>window.top.location.href='/frmlogin.aspx'</script>");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(EventArgs e)
        {
            if (SystemUserInfo != null && IsLogin && HttpContext.Current.Session["SystemUserLoginInfo"] != null)
            {
                base.OnLoad(e);
            }
            else
            {
                //Response.Redirect("/frmLogin.aspx", false);
                Response.Write("<script>window.top.location.href='/frmlogin.aspx'</script>");
            }
        }


        /// <summary>
        /// 当前页面的权限按钮
        /// </summary>
        public static bool GetButtonRole(string uRL, string buttonName)
        {
            Model.SystemUserInfo userModel = new Model.SystemUserInfo();
            if (HttpContext.Current.Session["SystemUserLoginInfo"] != null)
                userModel = HttpContext.Current.Session["SystemUserLoginInfo"] as Model.SystemUserInfo;

            BLL.SYS_AuthButton Bll = new BLL.SYS_AuthButton();
            List<Model.GetSysUserAuthButton> list = new List<Model.GetSysUserAuthButton>();
            list = Bll.GetModelList(userModel.ID, uRL.Trim());

            Model.GetSysUserAuthButton model = new Model.GetSysUserAuthButton();
            model = list.Find(delegate (Model.GetSysUserAuthButton p) { return p.ButtonName == buttonName; });
            if (model != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 当前页面的权限按钮集合
        /// </summary>
        public string GetButtonList(string uRL, string enetiyid)
        {
            string result = "";
            BLL.SYS_AuthButton Bll = new BLL.SYS_AuthButton();
            List<Model.GetSysUserAuthButton> list = new List<Model.GetSysUserAuthButton>();
            list = Bll.GetModelList(SystemUserInfo.ID, uRL.Trim());

            foreach (Model.GetSysUserAuthButton item in list)
            {
                if (item.ButtonName != "Add")
                {
                    result += url(item.ButtonName, enetiyid, item.DisplayName);
                }
            }
            return result;
        }
        public string url(string btnName, string ID, string DisplayName)
        {
            string url = "";
            switch (btnName)
            {
                case "Delete":
                    //url = "<a href='#' onclick='javascript:OpenURL(\"删除\",\"Delete.aspx?entityid=<%#Eval(" + ID + ") %>\")'>删除</a>";
                    url = "<a href='javascript:void(0);' onclick='javascript:DeleteURL(\"Delete.aspx?entityid=" + ID + "\")'>" + DisplayName + "</a>";
                    break;
                default:
                    url = "<a href='javascript:void(0);' onclick='javascript:OpenURL(\"" + DisplayName + "\",\"" + btnName + ".aspx?entityid=" + ID + "\")'>" + DisplayName + "</a>";
                    break;

            }
            return url;
        }
        public virtual string StrWhere
        {
            get
            {
                if (ViewState["strWhere"] == null)
                {
                    return "";
                }
                else
                    return ViewState["strWhere"].ToString();
            }
            set { ViewState["strWhere"] = value; }
        }
        /// <summary>
        /// true 登录， false 未登录 
        /// </summary>
        public bool IsLogin
        {
            get
            {
                if (HttpContext.Current.Session["SystemUserLoginInfo"] != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }


            }
        }
        /// <summary>
        /// 根据票证获取登录信息
        /// </summary>
        public static Model.SystemUserInfo SystemUserInfo
        {
            get
            {
                Model.SystemUserInfo model = new Model.SystemUserInfo();
                if (HttpContext.Current.Session["SystemUserLoginInfo"] != null)
                    model = HttpContext.Current.Session["SystemUserLoginInfo"] as Model.SystemUserInfo;
                return model;
            }
        }

        /// <summary>
        /// 添加操作日志
        /// </summary>
        /// <param name="Descriptions"></param>
        public static void AddOperationLog(string Descriptions)
        {
            var userModel = HttpContext.Current.Session["SystemUserLoginInfo"] as Model.SystemUserInfo;

            Model.SYS_OperatLog model = new Model.SYS_OperatLog();
            model.UserID = userModel.ID;
            model.OperateIP = HttpContext.Current.Request.UserHostAddress == "::1" ? "127.0.0.1" : HttpContext.Current.Request.UserHostAddress;
            model.OperateDescriptions = Descriptions;
            BLL.SYS_OperatLog bll = new BLL.SYS_OperatLog();
            bll.Add(model);
        }


        #region  SetDropDownList or SetRadioButtonList

        public void SetDropDownListValue(System.Web.UI.WebControls.DropDownList ddl, string dropdownlistSelectValue)
        {
            ddl.SelectedIndex = 0;
            for (int i = 0; i < ddl.Items.Count; i++)
            {
                if (ddl.Items[i].Value == dropdownlistSelectValue)
                {
                    ddl.SelectedIndex = i;
                    break;
                }
            }
        }
        public void SetDropDownListValueByText(System.Web.UI.WebControls.DropDownList ddl, string dropdownlistSelectText)
        {
            ddl.SelectedIndex = 0;
            for (int i = 0; i < ddl.Items.Count; i++)
            {
                if (ddl.Items[i].Text == dropdownlistSelectText)
                {
                    ddl.SelectedIndex = i;
                    break;
                }
            }
        }
        public void SetRadioButtonList(System.Web.UI.WebControls.RadioButtonList ddl, string radioButtonListSelectValue)
        {
            ddl.SelectedIndex = 0;
            for (int i = 0; i < ddl.Items.Count; i++)
            {
                if (ddl.Items[i].Value == radioButtonListSelectValue)
                {
                    ddl.SelectedIndex = i;
                    break;
                }
            }
        }
        #endregion


        public long UserRoleID
        {
            get
            {
                if (SystemUserInfo == null)
                {
                    return 0;
                }
                else
                    return Convert.ToInt64(SystemUserInfo.RoleID);
            }
        }

        #region 截取成一定长度的文字

        /// <summary>
        ///截取成一定长度的文字
        /// </summary>
        /// <param name="Htmlstring"></param>
        /// <param name="intLen"></param>
        /// <returns></returns>
        public static string GetCut(string Htmlstring, int intLen)
        {
            //删除脚本
            Htmlstring = Regex.Replace(Htmlstring, @"<script[^>]*?>.*?</script>", "", RegexOptions.IgnoreCase);
            //删除HTML
            Htmlstring = Regex.Replace(Htmlstring, @"<(.[^>]*)>", "", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"([\r\n])[\s]+", "", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"-->", "", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"<!--.*", "", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(quot|#34);", "\"", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(amp|#38);", "&", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(lt|#60);", "<", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(gt|#62);", ">", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(nbsp|#160);", " ", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(iexcl|#161);", "\xa1", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(cent|#162);", "\xa2", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(pound|#163);", "\xa3", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(copy|#169);", "\xa9", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&#(\d+);", "", RegexOptions.IgnoreCase);
            Htmlstring.Replace("<", "");
            Htmlstring.Replace(">", "");
            Htmlstring.Replace("\r\n", "");
            Htmlstring = HttpContext.Current.Server.HtmlEncode(Htmlstring).Trim();
            if (Htmlstring.Length > intLen)
            {
                return Htmlstring = Htmlstring.Substring(0, intLen) + "……";
            }
            else
            {
                return Htmlstring;
            }

        }

        #endregion


        #region
        bool IHttpHandler.IsReusable
        {
            get { throw new NotImplementedException(); }
        }

        void IHttpHandler.ProcessRequest(HttpContext context)
        {
            throw new NotImplementedException();
        }
        #endregion

        protected void ResponseJs(string name, string News)
        {
            this.ClientScript.RegisterStartupScript(this.GetType(), name, "<script>" + News + "</script>");
        }
        protected void ErrorMessage(string News, string t = "")
        {
            this.ClientScript.RegisterStartupScript(this.GetType(), "", "<script>" + t + "layer.alert('" + News + "！', {icon: 2}); </script>");
        }
        protected void SuccessMessage(string News, string t = "")
        {
            this.ClientScript.RegisterStartupScript(this.GetType(), "", "<script>" + t + "layer.alert('" + News + "！', {icon: 1}); </script>");
        }
    }
}