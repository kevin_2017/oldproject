﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Recharge.Admin.Models
{
    public class ChkQueryresp
    {
        public bool Success { get; set; }

        public string Code { get; set; }

        public string Message { get; set; }

        public string CurrentStatus { get; set; }

        public object Data { get; set; }
    }
}