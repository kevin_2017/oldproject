﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.DBUtility;
using Model;

namespace Recharge.Admin
{
    public partial class Main : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (HttpContext.Current.Session["SystemUserLoginInfo"] == null)
                {
                    Response.Redirect("/frmLogin.aspx");
                }
                else
                {
                    var model = (Model.SystemUserInfo)HttpContext.Current.Session["SystemUserLoginInfo"];
                    
                    spanUserName.InnerHtml = model.UserName;
                    spanUserName1.InnerHtml = model.UserName;
                    pUserName.InnerHtml = model.UserName;

                    GetMenu(model.RoleID);


                    var dataset = DBHelper.Query(" select  IspType,count(1) counts from V8_OrderManage where CONVERT(varchar(10),CreateTime,120)=CONVERT(varchar(10),GETDATE(),120)  group by IspType");
                    if (dataset.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in dataset.Tables[0].Rows)
                        {
                            if (dr["IspType"].ToString() == "1")
                                ltLianTong.Text = dr["counts"].ToString();
                            if (dr["IspType"].ToString() == "2")
                                ltYiDong.Text = dr["counts"].ToString();
                            if (dr["IspType"].ToString() == "3")
                                ltDianXin.Text = dr["counts"].ToString();
                        }

                    }

                }
            }

        }



        public void GetMenu(long roleID)
        {
            var menuHtml = "<ul class=\"sidebar-menu\">";
            menuHtml += "<li class=\"header\">MAIN NAVIGATION</li>";
            #region 加载菜单

            DataTable dt = new DataTable();
            dt = new BLL.SYS_AuthorityInfo().GetAuthorityinfoByRoleID(roleID).Tables[0];


            DataView dv = new DataView(dt);
            dv.RowFilter = "ParentID=0";
            foreach (DataRowView drv in dv)
            {
                string url = drv["Url"].ToString().Trim();
                DataView sonDv = new DataView(dt);
                sonDv.RowFilter = "parentID=" + drv["ID"].ToString();
                if (sonDv.Count > 0)
                {
                    menuHtml += "<li class=\"treeview\">" +
                        "<a href=\"javascript:void(0)\">" +
                        "<i class=\"" + drv["LogImg"] + "\"></i> <span>" + drv["TypeName"] + "</span> " +
                        "<i class=\"fa fa-angle-left pull-right\"></i>" +
                        "</a>";
                    menuHtml += "<ul class=\"treeview-menu\">";
                    foreach (DataRowView sonDrv in sonDv)
                    {
                        menuHtml += "<li>" +
                            "<a href=\"" + sonDrv["Url"] + "\" title=\"" + sonDrv["TypeName"] + "\" >" +
                            "<i class=\"fa fa-circle-o\"></i>" + sonDrv["TypeName"] + "</a>" +
                            "</li>";
                    }
                    menuHtml += "</ul></li>";
                }
                else
                {
                    if (url.Equals("#"))
                    {
                        menuHtml += "<li>" +
                            "<a href=\"javascript:void(0)\"> " +
                            "<i class=\"" + drv["LogImg"] + "\"></i>" +
                            "<span>" + drv["TypeName"] + "</span>" +
                            "</a></li>";
                    }
                    else
                    {
                        menuHtml += "<li>" +
                            "<a href=\"" + url + "\"> " +
                            "<i class=\"" + drv["LogImg"] + "\"></i>" +
                            "<span>" + drv["TypeName"] + "</span>" +
                            "</a></li>";
                    }
                }
            }
            #endregion
            menuHtml += "</ul>";
            ltAuthorityInfo.Text = menuHtml;
        }


        protected void LoginOut(object sender, EventArgs e)
        {
            Session.Clear();
            Response.Redirect("/frmlogin.aspx#loginout");
        }
    }
}