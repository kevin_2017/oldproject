﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="frmSettingProductSalePrice.aspx.cs" Inherits="Recharge.Admin.BranchProManage.frmSettingProductSalePrice" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        var allBaseInfo = [];

        function fanxuan(obj) {
            if ($("#" + obj).attr("checked")) {
                $("#" + obj).removeAttr("checked");
            }
            else {
                $("#" + obj).attr("checked", "checked");
            }
        }
        $(function () {

            //加载代理
            $.ajax({
                type: "POST",
                url: "frmSettingProductSalePrice.aspx/GetAllBranch",
                async: false,
                data: {},
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    var index = layer.load(1, {
                        shade: [0.8, '#fff'], //0.1透明度的白色背景 
                    });
                },
                success: function (data) {
                    if (data.d.Data.length > 0) {
                        var innerhtml = "";
                        $.each(data.d.Data, function (i, item) {
                            innerhtml += "  <option value=\"" + item["ID"] + "\">" + item["BranchName"] + "</option>";
                        });
                        $("#selectBranch").html(innerhtml);


                        layer.closeAll();
                    } else {
                        layer.alert("获取代理失败", { icon: 2 }, function () { layer.closeAll(); });
                    }

                },
                error: function (err) {
                    layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                }
            });
            InitProductBaseInfo(0, 0, 0);
            ChangeBaseInfo();

        });


        function checkAllprodctBaseInfo() {
            if ($("#prodctBaseInfo-checkboxall").attr("checked") == undefined) {
                $("#prodctBaseInfo-checkboxall").attr("checked", "checked");
                $("input[name='inline-checkboxprodctBaseInfo']").attr("checked", "checked");
            } else {
                $("#prodctBaseInfo-checkboxall").removeAttr("checked");
                $("input[name='inline-checkboxprodctBaseInfo']").removeAttr("checked");
            }
        }

        function InitProductBaseInfo(productTypeId, ispType, phoneNumType) {

            $.ajax({
                type: "POST",
                url: "frmSettingProductSalePrice.aspx/GetAllProductBaseInfo",
                async: false,
                data: $.toJSON({ productTypeId: productTypeId, ispType: ispType, phoneNumType: phoneNumType }),
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    var index = layer.load(1, {
                        shade: [0.8, '#fff'], //0.1透明度的白色背景 
                    });
                },
                success: function (data) {
                    if (data.d.Data.length > 0) {
                        allBaseInfo = data.d.Data;
                        var innerHtml = " <div class=\"checkbox  checkbox-inline\"><label style=\"color: red\"><input type=\"checkbox\"  onclick=\"checkAllprodctBaseInfo()\"   id=\"prodctBaseInfo-checkboxall\" name=\"inline-allcheckboxprodctBaseInfo\" value=\"1000\"/>全选</label></div>";

                        $.each(data.d.Data, function (i, item) {
                            innerHtml += "<div class=\"checkbox  checkbox-inline\"><label><input type=\"checkbox\" onclick=\"fanxuan('inline-checkbox" + item["ID"] + "')\" id=\"inline-checkbox" + item["ID"] + "\" name=\"inline-checkboxprodctBaseInfo\" value=\"" + item["ID"] + "\" />" + item["ProductName"] + "</label></div>";
                        })

                        $("#divProdctBaseInfo").html(innerHtml);

                        layer.closeAll();
                    } else {
                        layer.alert("获取商品失败", { icon: 2 }, function () { layer.closeAll(); });
                    }

                },
                error: function (err) {
                    layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                }
            });
        }

        function ChangeBaseInfo() {
            var ispType = $("#selectISPType").val();
            var productTypeId = $("#ddlProductType").val();
            var phoneNumType = $("#selectPhoneNumType").val();
            InitProductBaseInfo(productTypeId, ispType, phoneNumType);
        }

        function SaveInfo() {

            var productList = [];
            $("[name='inline-checkboxprodctBaseInfo'][checked]").each(function () {
                var id = $(this).val();
                $.each(allBaseInfo, function (i, item) {
                    if (item.ID == id) {
                        productList.push({ ProductID: item.ID, ProductName: item.ProductName });
                    }
                });
            });
            if ($("#txtInPrice").val() == "") {
                layer.alert("请先录入售价！", { icon: 2 }, function () { $("#txtInPrice").focus(); layer.closeAll(); });
                return;
            }
            if (productList.length == 0) {
                layer.alert("请先选择商品信息！", { icon: 2 }, function () { layer.closeAll(); });
                return;
            }


            $.ajax({
                type: "POST",
                url: "frmSettingProductSalePrice.aspx/CreateProductInfo",
                async: false,
                data: $.toJSON({ model: { Inprice: $("#txtInPrice").val(), BranchID: $("#selectBranch").val(), BranchName: $("#selectBranch").find("option:selected").text(), ProductTypeId: $("#ddlProductType").val(), ProductTypeName: $("#ddlProductType").find("option:selected").text(), ISPType: $("#selectISPType").val(), ISPName: $("#selectISPType").find("option:selected").text() }, productList: productList }),
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    var index = layer.load(1, {
                        shade: [0.8, '#fff'], //0.1透明度的白色背景 
                    });
                },
                success: function (data) {
                    if (data.d.Data) {
                        layer.alert("修改商品售价成功", { icon: 1 }, function () { layer.closeAll(); });
                    } else {
                        layer.alert("修改商品售价失败", { icon: 2 }, function () { layer.closeAll(); });
                    }

                },
                error: function (err) {
                    layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                }
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="box box-info">
        <div class="box-header">
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-sm-10">
                    <div class="input-group">
                        <span class="input-group-addon">运营商名</span>
                        <select id="selectISPType" class="form-control" onchange="ChangeBaseInfo()">
                            <option value="1">中国联通</option>
                            <option value="2">中国移动</option>
                            <option value="3">中国电信</option>
                        </select>
                    </div>
                    <br />
                    <div class="input-group">
                        <span class="input-group-addon">商品类型</span>
                        <select id="ddlProductType" class="form-control" onchange="ChangeBaseInfo()">
                            <option value="1001">手机话费</option>
                            <option value="1002">座机话费</option>
                            <option value="1003">流量充值</option>
                        </select>
                    </div>
                    <br />
                    <div class="input-group">
                        <span class="input-group-addon">号码类型</span>
                        <select id="selectPhoneNumType" class="form-control" onchange="ChangeBaseInfo()">
                            <option value="1">手机</option>
                            <option value="2">固话</option>
                        </select>
                    </div>
                    <br />
                    <div class="input-group">
                        <span class="input-group-addon">当前状态</span>
                        <select id="selectStatus" class="form-control">
                            <option value="1">正常上架</option>
                            <option value="2">全部下架</option>
                        </select>
                    </div>
                    <br />
                    <div class="input-group">
                        <span class="input-group-addon">供&nbsp;应&nbsp;商</span>
                        <select id="selectBranch" class="form-control">
                        </select>
                    </div>
                    <br />
                    <div class="input-group">
                        <span class="input-group-addon">商品售价</span>
                        <input type="text" id="txtInPrice" class="form-control" placeholder="商品进价" />
                    </div>
                    <br />
                    <div class="input-group">
                        <span class="input-group-addon">商品信息</span>
                        <div id="divProdctBaseInfo" style="border: 1px solid #d2d6de">
                            &nbsp;
                        </div>
                    </div>

                </div>
            </div>

        </div>
        <div class="box-footer">
            <input type="button" class="btn btn-primary" value="保存" onclick="SaveInfo()" />
        </div>
    </div>
</asp:Content>
