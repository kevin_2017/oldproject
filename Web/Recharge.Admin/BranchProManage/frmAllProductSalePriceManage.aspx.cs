﻿using CE.Utility;
using Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Recharge.Admin.BranchProManage
{
    public partial class frmAllProductSalePriceManage : BasePageClass
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Model.StrWhere str = new Model.StrWhere();
                str.IsWhereExist = false;

                SetDropDownList.BindDropDownList(ddlProductType, new BLL.V8_Product_Type().GetDataSet(str).Tables[0], "TypeName", "ID");

                SetDropDownList.BindDropDownList(ddlBranch, new BLL.MS_BranchManage().GetDataSet(str).Tables[0], "BranchName", "ID");
                AspNetPager1.CurrentPageIndex = 1;

                InitPage();

            }
        }

        public void InitPage()
        {
            PageAttribute page = new PageAttribute();
            page.TableName = "V8_Product_Branch";
            page.Columns = @"id,Inprice,Sort,IsClosed,IsDelete,ArriveMethod,FlowType,CreateTime,BranchID,Creator,UpdateTime,Updator,BranchName,ProductTypeId,ProductTypeName,ProductID,ProductName,ISPType,ISPName,MinValue,MaxValue";
            page.StrOrder = "Id  asc";
            page.WhereCondition = string.Concat(" ", StrWhere);
            page.PageIndex = AspNetPager1.CurrentPageIndex - 1;
            page.PageSize = AspNetPager1.PageSize;
            DataTable dt = new BLL.V8_Product_Branch().GetListByPage(page);
            AspNetPager1.RecordCount = page.TotalRowCount.Value;
            repList.DataSource = dt;
            repList.DataBind();
        }


        [WebMethod]
        public static JsonResult<bool> GetUserBtnRole(string url, string btnName)
        {
            if (!GetButtonRole(url, btnName))
                return new JsonResult<bool>(false, "对不起，无权操作");
            else
                return new JsonResult<bool>(true, "操作成功");

        }



        protected void AspNetPager1_PageChanging(object src, Wuqi.Webdiyer.PageChangingEventArgs e)
        {
            AspNetPager1.CurrentPageIndex = e.NewPageIndex;
            InitPage();
        }



        protected void btnSearch_Click(object obj, EventArgs e)
        {
            StrWhere = "";

            if (ddlBranch.SelectedItem.Value != "-1")
            {
                StrWhere += " and BranchID =" + ddlBranch.SelectedItem.Value.Trim();

            }
             
            if (ddlIspType.SelectedItem.Value != "0")
            {
                StrWhere += " and ISPType =" + ddlIspType.SelectedItem.Value.Trim();

            }
            if (ddlProductType.SelectedItem.Value.Trim() != "-1")
            {
                StrWhere += " and ProductTypeID =" + ddlProductType.SelectedItem.Value.Trim();
            }

            if (txtProductName.Text.Trim() != "")
            {
                StrWhere += " and (ProductName  like '%" + txtProductName.Text.Trim() + "%' or ID like '%" + txtProductName.Text.Trim() + "%')";
            }
            if (ddlStatus.SelectedItem.Value != "-1")
            {
                StrWhere += " and IsClosed =" + (ddlStatus.SelectedItem.Value.Trim() == "1" ? 0 : 1);
            }
             
            AspNetPager1.CurrentPageIndex = 1;
            InitPage();
        }

    }
}