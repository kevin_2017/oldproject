﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using CE.Utility;
using Common;
using Model;

namespace Recharge.Admin.BranchProManage
{
    public partial class frmProductRebateRule : BasePageClass
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Model.StrWhere str = new Model.StrWhere { IsWhereExist = false };

                SetDropDownList.BindDropDownList(ddlProductType, new BLL.V8_Product_Type().GetDataSet(str).Tables[0], "TypeName", "ID");

                SetDropDownList.BindDropDownList(ddlBranch, new BLL.MS_BranchManage().GetDataSet(str).Tables[0], "BranchName", "ID");
                AspNetPager1.CurrentPageIndex = 1;

                InitPage();

            }
        }

        public void InitPage()
        {
            PageAttribute page = new PageAttribute
            {
                TableName = "MS_Branch_RebateRule",
                Columns = @"id,BranchId,BranchName,ProductTypeId,ProductTypeName,IspType,IspName,RebateMethod,Rebate,MinMoney,MaxMoney,Sort,Is_Delete,CreateTime,Creator,UpdateTime,Updator,ProvinceIds,dbo.GetProvinceNames(ProvinceIds) ProvinceNames",
                StrOrder = "branchId asc,IspType, sort ",
                WhereCondition = string.Concat(" and 1=1 ", StrWhere),
                PageIndex = AspNetPager1.CurrentPageIndex - 1,
                PageSize = AspNetPager1.PageSize
            };
            DataTable dt = new BLL.V8_Product_Branch().GetListByPage(page);
            AspNetPager1.RecordCount = page.TotalRowCount.Value;
            repList.DataSource = dt;
            repList.DataBind();
        }

        protected void AspNetPager1_PageChanging(object src, Wuqi.Webdiyer.PageChangingEventArgs e)
        {
            AspNetPager1.CurrentPageIndex = e.NewPageIndex;
            InitPage();
        }

        [WebMethod]
        public static JsonResult<bool> GetUserBtnRole(string url, string btnName)
        {
            if (!GetButtonRole(url, btnName))
                return new JsonResult<bool>(false, "对不起，无权操作");
            else
                return new JsonResult<bool>(true, "操作成功");
        }



        protected void btnSearch_Click(object obj, EventArgs e)
        {
            StrWhere = "";

            if (ddlBranch.SelectedItem.Value != "-1")
            {
                StrWhere += " and BranchID =" + ddlBranch.SelectedItem.Value.Trim();
            }

            if (ddlIspType.SelectedItem.Value != "0")
            {
                StrWhere += " and ISPType =" + ddlIspType.SelectedItem.Value.Trim();
            }
            if (ddlProductType.SelectedItem.Value.Trim() != "-1")
            {
                StrWhere += " and ProductTypeID =" + ddlProductType.SelectedItem.Value.Trim();
            }

            AspNetPager1.CurrentPageIndex = 1;
            InitPage();
        }

    }
}