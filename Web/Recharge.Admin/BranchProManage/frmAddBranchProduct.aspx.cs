﻿using CE.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Model;
using Newtonsoft.Json;

namespace Recharge.Admin.BranchProManage
{
    public partial class frmAddBranchProduct : BasePageClass
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static JsonResult<List<Model.MS_BranchManage>> GetAllBranch()
        {
            Model.StrWhere str = new Model.StrWhere() { IsWhereExist = false };
            return new JsonResult<List<Model.MS_BranchManage>>(new BLL.MS_BranchManage().GetModelList(str));
        }


        [WebMethod]
        public static JsonResult<List<Model.V8_Product_Manage>> GetAllProductBaseInfo(int productTypeId, int ispType, int phoneNumType)
        {
            var strSql = new StringBuilder(" 1=1 ");
            Model.StrWhere str = new Model.StrWhere() { IsWhereExist = true };
            if (productTypeId > 0)
                strSql.Append($" and ProductTypeID={productTypeId}");
            if (ispType > 0)
                strSql.Append($" and ISPType={ispType}");
            if (phoneNumType > 0)
                strSql.Append($" and PhoneNumType={phoneNumType}");
            str.strWhere = strSql;

            return new JsonResult<List<Model.V8_Product_Manage>>(new BLL.V8_Product_Manage().GetModelList(str));
        }

        [WebMethod]
        public static JsonResult<bool> CreateProductInfo(Model.V8_Product_Branch model)
        {
            long id = 0;
            Model.V8_Product_Branch prod = new Model.V8_Product_Branch();
            prod = model;
            prod.Creator = (HttpContext.Current.Session["SystemUserLoginInfo"] as Model.SystemUserInfo).UserName;
            Model.StrWhere str = new Model.StrWhere()
            {
                IsWhereExist = true,
                strWhere = new StringBuilder($" BranchID={model.BranchID} and ProductTypeId={model.ProductTypeId} and ProductID={model.ProductID} ")
            };

            var oldModel = new BLL.V8_Product_Branch().GetModel(str);
            if (oldModel == null)
            {
                id = new BLL.V8_Product_Branch().Add(prod);

                AddOperationLog($"【增加代理商品】 代理信息：{prod.BranchID}，{prod.BranchName},商品信息{JsonConvert.SerializeObject(prod)}");
                return new JsonResult<bool>(true, "操作成功");
            }
            else
            {
                return new JsonResult<bool>(false, "该类型商品已存在");
            }
        }

        [WebMethod]
        public static JsonResult<List<Model.V8_Province>> GetAllProvince()
        {
            return new JsonResult<List<V8_Province>>(new BLL.V8_Province().GetModelList(new StrWhere() { IsWhereExist = false }));
        }

    }
}