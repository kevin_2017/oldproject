﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using CE.Utility;
using Common;
using Model;
using Newtonsoft.Json;

namespace Recharge.Admin.BranchProManage
{
    public partial class frmAddRebateRule : BasePageClass
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitProvinces();
            }
        }

        [WebMethod]
        public static JsonResult<List<Model.MS_BranchManage>> GetAllBranch()
        {
            Model.StrWhere str = new Model.StrWhere() { IsWhereExist = false };
            return new JsonResult<List<Model.MS_BranchManage>>(new BLL.MS_BranchManage().GetModelList(str));
        }

        public void InitProvinces()
        {
            var id = Request.QueryString["id"].ToString();

            DataTable dtAll = new BLL.V8_Province().GetDataSet(new StrWhere() { IsWhereExist = false }).Tables[0];
            string innerHtml = "";
            var rebateModel = new BLL.MS_Branch_RebateRule().GetModel(Convert.ToInt64(id));

            foreach (DataRow dr in dtAll.Rows)
            {
                if (rebateModel != null && rebateModel.ProvinceIds.Contains(dr["ID"].ToString()))
                {
                    innerHtml += "<div class=\"checkbox  checkbox-inline\"><label><input type=\"checkbox\"   id=\"inline-checkbox" +
                                 dr["ID"] +
                                 "\" name=\"inline-checkboxprovince\"  checked=\"checked\"   value=\"" +
                                 dr["ID"] +
                                 "\" />" +
                                 dr["ProvinceName"] +
                                 "</label></div>"; ;
                }
                else
                {
                    innerHtml += "<div class=\"checkbox  checkbox-inline\"><label><input type=\"checkbox\"  id=\"inline-checkbox" +
                                 dr["ID"] +
                                 "\" name=\"inline-checkboxprovince\" value=\"" +
                                 dr["ID"] +
                                 "\" />" +
                                 dr["ProvinceName"] +
                                 "</label></div>"; ;
                }

            }

            divProvince.InnerHtml = innerHtml;
            //   if(rebateModel.ProvinceIds.Length>4)

        }


        [WebMethod]
        public static JsonResult<Model.MS_Branch_RebateRule> GetModel(long id)
        {
            return new JsonResult<MS_Branch_RebateRule>(new BLL.MS_Branch_RebateRule().GetModel(id));
        }

        [WebMethod]
        public static JsonResult<bool> OperateModel(Model.MS_Branch_RebateRule model)
        {
            AddOperationLog($"【操作代理返利规则】代理名：{model.BranchName},{JsonConvert.SerializeObject(model)}");

            model.ProvinceIds = model.ProvinceIds.Substring(0, model.ProvinceIds.Length - 1);
            if (model.ID > 0)
            {
                model.Updator = SystemUserInfo.UserName;

                return new JsonResult<bool>(new BLL.MS_Branch_RebateRule().Update(model));
            }
            else
            {
                model.Creator = SystemUserInfo.UserName;

                return new JsonResult<bool>(new BLL.MS_Branch_RebateRule().Add(model) > 0);
            }
        }

    }
}