﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmUpdBranchProduct.aspx.cs" Inherits="Recharge.Admin.BranchProManage.frmUpdBranchProduct" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <!-- Bootstrap 3.3.4 -->
    <link href="/Content/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="/Content/dist/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="/Content/dist/css/ionicons.min.css" rel="stylesheet" type="text/css" />

    <!-- Theme style -->
    <link href="/Content/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins 
         folder instead of downloading all of them to reduce the load. -->
    <link href="/Content/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
    <script src="/Content/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="/Content/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

    <!-- SlimScroll -->
    <script src="/Content/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src='/Content/plugins/fastclick/fastclick.min.js'></script>
    <!-- AdminLTE App -->
    <script src="/Content/dist/js/app.min.js" type="text/javascript"></script>
    <script src="/Content/dist/js/demo.js" type="text/javascript"></script>

    <script src="/Content/layer/layer.js"></script>
    <script src="/Content/jquery.json.min.js"></script>
    <script> 
        var isZiDingyi = false;
        var allProvince = [];
        var allProductInfos = [];
        function getQueryString(name) {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
            var r = window.location.search.substr(1).match(reg);
            if (r != null) return unescape(r[2]);
            return null;
        };
        var prodInfo = {};

        function fanxuan(obj) {

            if ($(obj).attr("checked")) {
                $(obj).removeAttr("checked");
            }
            else {
                $(obj).attr("checked", "checked");
            }
        }
        $(function () {

            $("#txtMinValue").val("0");
            $("#txtMaxValue").val("0");
            $("#selectIsMultiple").val("0");

            //加载代理
            $.ajax({
                type: "POST",
                url: "frmAddBranchProduct.aspx/GetAllBranch",
                async: false,
                data: {},
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    var index = layer.load(1, {
                        shade: [0.8, '#fff'], //0.1透明度的白色背景 
                    });
                },
                success: function (data) {
                    if (data.d.Data.length > 0) {
                        // var innerhtml = "";
                        $.each(data.d.Data, function (i, item) {
                            //  innerhtml += "  <option value=\"" + item["ID"] + "\">" + item["BranchName"] + "</option>";
                            $("#selectBranch").append("<option value='" + item["ID"] + "'>" + item["BranchName"] + "</option>");
                        });
                        //  $("#selectBranch").html(innerhtml);


                        layer.closeAll();
                    } else {
                        layer.alert("获取代理信息失败", { icon: 2 }, function () { layer.closeAll(); });
                    }

                },
                error: function (err) {
                    layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                }
            });

            //加载省份
            $.ajax({
                type: "POST",
                url: "frmAddBranchProduct.aspx/GetAllProvince",
                async: false,
                data: {},
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    var index = layer.load(1, {
                        shade: [0.8, '#fff'] //0.1透明度的白色背景 
                    });
                },
                success: function (data) {
                    if (data.d.Data.length > 0) {
                        var innerHtml = "";
                        allProvince = data.d.Data;

                        $.each(data.d.Data, function (i, item) {

                            innerHtml +=
                                "<div class=\"checkbox  checkbox-inline\"><label><input type=\"checkbox\" onclick=\"fanxuan(this)\" id=\"inline-checkbox" +
                                item["ID"] +
                                "\" name=\"inline-checkboxprovince\" value=\"" +
                                item["ID"] +
                                "\" />" +
                                item["ProvinceName"] +
                                "</label></div>";

                        });
                        $("#divProvince").html(innerHtml);


                        layer.closeAll();
                    } else {
                        layer.alert("获取充值省份范围失败", { icon: 2 }, function () { layer.closeAll(); });
                    }

                },
                error: function (err) {
                    layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                }
            });

            ChangeBaseInfo();

            $("#selectProductInfo").change(function () {
                var text = $("#selectProductInfo").find("option:selected").text();
                if (text == "自定义") {
                    isZiDingyi = true;
                    $("#txtMinValue").removeAttr("disabled");
                    $("#txtMaxValue").removeAttr("disabled");
                    $("#selectIsMultiple").removeAttr("disabled");
                } else {
                    isZiDingyi = false;
                    $("#txtMinValue").attr("disabled", "disabled");
                    $("#txtMaxValue").attr("disabled", "disabled");
                    $("#selectIsMultiple").attr("disabled", "disabled");


                }
            });

            $.ajax({
                type: "POST",
                url: "frmUpdBranchProduct.aspx/GetModel",
                async: false,
                data: $.toJSON({ id: getQueryString("id") }),
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    var index = layer.load(1, {
                        shade: [0.8, '#fff'] //0.1透明度的白色背景 
                    });
                },
                success: function (data) {
                    if (data.d.Success) {
                        console.log(data.d.Data);
                        prodInfo = data.d.Data;

                        $("#selectFlowType").val(data.d.Data.ArriveMethod);
                        $("#selectBranch").val(data.d.Data.BranchID);

                        $("#ddlProductType").val(data.d.Data.ProductTypeId);
                        $("#selectISPType").val(data.d.Data.ISPType);
                        $("#txtMinValue").val(data.d.Data.MinValue);
                        $("#txtMaxValue").val(data.d.Data.MaxValue);
                        $("#selectIsMultiple").val(data.d.Data.IsMultiple);
                        var phoneNumType = 1;
                        if (data.d.Data.ProductTypeId == 1002)
                            phoneNumType = 2;

                        InitProductBaseInfo(data.d.Data.ProductTypeId, data.d.Data.ISPType, phoneNumType);

                        $("#selectProductInfo").val(data.d.Data.ProductID);

                        var text = $("#selectProductInfo").find("option:selected").text();
                        if (text == "自定义") {
                            isZiDingyi = true;
                            $("#txtMinValue").removeAttr("disabled");
                            $("#txtMaxValue").removeAttr("disabled");
                            $("#selectIsMultiple").removeAttr("disabled");
                        } else {
                            isZiDingyi = false;
                            $("#txtMinValue").attr("disabled", "disabled");
                            $("#txtMaxValue").attr("disabled", "disabled");
                            $("#selectIsMultiple").attr("disabled", "disabled");


                        }

                        layer.closeAll();
                    } else {
                        layer.alert("获取代理商品信息失败", { icon: 2 }, function () { layer.closeAll(); });
                    }

                },
                error: function (err) {
                    layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                }
            });
        });

        function InitProductBaseInfo(productTypeId, ispType, phoneNumType) {
            $("#selectProductInfo").empty();
            $.ajax({
                type: "POST",
                url: "frmAddBranchProduct.aspx/GetAllProductBaseInfo",
                async: false,
                data: $.toJSON({ productTypeId: productTypeId, ispType: ispType, phoneNumType: phoneNumType }),
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    var index = layer.load(1, {
                        shade: [0.8, '#fff'] //0.1透明度的白色背景 
                    });
                },
                success: function (data) {
                    if (data.d.Data.length > 0) {
                        allProductInfos = data.d.Data;
                        $.each(data.d.Data,
                            function (i, item) {
                                $("#selectProductInfo").append("<option  value='" + item["ID"] + "'>" + item["ProductName"] + "</option>");
                            });
                        layer.closeAll();
                    } else {
                        layer.alert("获取商品失败", { icon: 2 }, function () { layer.closeAll(); });
                    }
                },
                error: function (err) {
                    layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                }
            });
        }

        function ChangeBaseInfo() {
            var ispType = $("#selectISPType").val();
            var productTypeId = $("#ddlProductType").val();
            var phoneNumType = $("#selectPhoneNumType").val();
            InitProductBaseInfo(productTypeId, ispType, phoneNumType);
        }

        function SaveInfo() {

            if (isZiDingyi) {
                if ($("#txtMinValue").val() == "") {
                    layer.alert("请录入最小充值金额！", { icon: 2 }, function () { $("#txtMinValue").focus(); layer.closeAll(); });
                    return;
                }
                if ($("#txtMaxValue").val() == "") {
                    layer.alert("请录入最大充值金额！", { icon: 2 }, function () { $("#txtMaxValue").focus(); layer.closeAll(); });
                    return;
                }
            }

            $.ajax({
                type: "POST",
                url: "frmUpdBranchProduct.aspx/UpdateProductInfo",
                async: false,
                data: $.toJSON({ model: { ArriveMethod: $("#selectFlowType").val(), BranchID: $("#selectBranch").val(), BranchName: $("#selectBranch").find("option:selected").text(), ProductID: $("#selectProductInfo").val(), ProductName: $("#selectProductInfo").find("option:selected").text(), ProductTypeId: $("#ddlProductType").val(), ProductTypeName: $("#ddlProductType").find("option:selected").text(), ISPType: $("#selectISPType").val(), ISPName: $("#selectISPType").find("option:selected").text(), MinValue: $("#txtMinValue").val(), MaxValue: $("#txtMaxValue").val(), IsMultiple: $("#selectIsMultiple").val(), ID: prodInfo.ID } }),
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    var index = layer.load(1, {
                        shade: [0.8, '#fff'] //0.1透明度的白色背景 
                    });
                },
                success: function (data) {
                    if (data.d.Data) {
                        layer.alert("代理商品修改成功", { icon: 1 }, function () { layer.closeAll(); });
                    } else {
                        layer.alert("代理商品修改失败   " + data.d.Message, { icon: 2 }, function () { layer.closeAll(); });
                    }
                },
                error: function (err) {
                    layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                }
            });
        }
    </script>
</head>
<body class="skin-blue">
    <section class="content">
        <div class="row">

            <div class="box box-info">
                <div class="box-header">
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon">代&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;理</span>
                                <select id="selectBranch" class="form-control">
                                </select>
                            </div>

                            <br />
                            <div class="input-group">
                                <span class="input-group-addon">运营商名</span>
                                <select id="selectISPType" class="form-control" onchange="ChangeBaseInfo()">
                                    <option value="1">中国联通</option>
                                    <option value="2">中国移动</option>
                                    <option value="3">中国电信</option>
                                </select>
                            </div>
                            <br />
                            <div class="input-group">
                                <span class="input-group-addon">商品类型</span>
                                <select id="ddlProductType" class="form-control" onchange="ChangeBaseInfo()">
                                    <option value="1001">手机话费</option>
                                    <option value="1002">座机话费</option>
                                    <option value="1003">流量充值</option>
                                </select>
                            </div>
                            <br />
                            <div class="input-group">
                                <span class="input-group-addon">号码类型</span>
                                <select id="selectPhoneNumType" class="form-control" onchange="ChangeBaseInfo()">
                                    <option value="1">手机</option>
                                    <option value="2">固话</option>
                                </select>
                            </div>
                            <br />
                            <div class="input-group">
                                <span class="input-group-addon">当前状态</span>
                                <select id="selectStatus" class="form-control">
                                    <option value="1">正常上架</option>
                                    <option value="2">全部下架</option>
                                </select>
                            </div>
                            <br />
                            <div class="input-group">
                                <span class="input-group-addon">到账方式</span>
                                <select id="selectFlowType" class="form-control">
                                    <option value="0">即时到账</option>
                                    <option value="1">正月到账</option>
                                </select>
                            </div>
                            <br />
                            <div class="input-group">
                                <span class="input-group-addon">商品信息</span>
                                <select id="selectProductInfo" class="form-control">
                                </select>
                            </div>
                            <br />
                            <div class="input-group">
                                <span class="input-group-addon">充值最小值</span>
                                <input type="number" id="txtMinValue" class="form-control" placeholder="充值最小值" min="0" disabled="disabled" />
                            </div>
                            <br />
                            <div class="input-group">
                                <span class="input-group-addon">充值最大值</span>
                                <input type="number" id="txtMaxValue" class="form-control" placeholder="充值最大值" min="0" disabled="disabled" />
                            </div>
                            <br />
                            <div class="input-group">
                                <span class="input-group-addon">充值金额是否10的倍数</span>
                                <select id="selectIsMultiple" class="form-control" disabled="disabled">
                                    <option value="0">否</option>
                                    <option value="1">是</option>
                                </select>
                            </div>

                        </div>
                    </div>

                </div>
                <div class="box-footer">
                    <input type="button" class="btn btn-primary" value="保存" onclick="SaveInfo()" />
                </div>
            </div>
        </div>
    </section>
</body>
</html>
