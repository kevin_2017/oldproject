﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmAddRebateRule.aspx.cs" Inherits="Recharge.Admin.BranchProManage.frmAddRebateRule" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <!-- Bootstrap 3.3.4 -->
    <link href="/Content/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="/Content/dist/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="/Content/dist/css/ionicons.min.css" rel="stylesheet" type="text/css" />

    <!-- Theme style -->
    <link href="/Content/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins 
    folder instead of downloading all of them to reduce the load. -->
    <link href="/Content/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
    <script src="/Content/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="/Content/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

    <!-- SlimScroll -->
    <script src="/Content/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src='/Content/plugins/fastclick/fastclick.min.js'></script>
    <!-- AdminLTE App -->
    <script src="/Content/dist/js/app.min.js" type="text/javascript"></script>
    <script src="/Content/dist/js/demo.js" type="text/javascript"></script>

    <script src="/Content/layer/layer.js"></script>
    <script src="/Content/jquery.json.min.js"></script>
    <script>
        var ruleId = 0;
        var ruleInfo = { ID: 0, BranchId: 0, BranchName: "", ProductTypeId: 0, ProductTypeName: "", IspType: 0, IspName: "", RebateMethod: 0, Rebate: 0, MinMoney: 0, MaxMoney: 0, Sort: 0 };

        function getQueryString(name) {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
            var r = window.location.search.substr(1).match(reg);
            if (r != null) return unescape(r[2]);
            return null;
        };

        $(function () {
            $("#btnNew").attr("disabled", "disabled");
            //加载
            $.ajax({
                type: "POST",
                url: "frmAddRebateRule.aspx/GetAllBranch",
                async: false,
                data: {},
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    var index = layer.load(1,
                        {
                            shade: [0.8, '#fff'] //0.1透明度的白色背景 
                        });
                },
                success: function (data) {
                    if (data.d.Data.length > 0) {
                        var innerhtml = "";
                        $.each(data.d.Data,
                            function (i, item) {
                                innerhtml += "  <option value=\"" +
                                    item["ID"] +
                                    "\">" +
                                    item["BranchName"] +
                                    "</option>";
                            });
                        $("#selectBranch").html(innerhtml);


                        layer.closeAll();
                    } else {
                        layer.alert("获取代理信息失败", { icon: 2 }, function () { layer.closeAll(); });
                    }

                },
                error: function (err) {
                    layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                }
            });
            ruleInfo.BranchId = $("#selectBranch").val();
            ruleInfo.BranchName = $("#selectBranch").find("option:selected").text();
            ruleInfo.IspType = $("#selectISPType").val();
            ruleInfo.IspName = $("#selectISPType").find("option:selected").text();
            ruleInfo.ProductTypeId = $("#ddlProductType").val();
            ruleInfo.ProductTypeName = $("#ddlProductType").find("option:selected").text();
            ruleInfo.RebateMethod = $("#ddlRebateMethod").val();
            var id = getQueryString("id");
            if (id > 0) {
                QueryRule(id);
            }
        });

        function QueryRule(id) {
            $("#btnNew").removeAttr("disabled");
            ruleInfo.ID = id;
            //GetModel
            $.ajax({
                type: "POST",
                url: "frmAddRebateRule.aspx/GetModel",
                async: false,
                data: $.toJSON({ id: id }),
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    var index = layer.load(1, {
                        shade: [0.8, '#fff'] //0.1透明度的白色背景 
                    });
                },
                success: function (data) {
                    if (data.d.Rows.length > 0) {
                        ruleInfo.ID = data.d.Data.ID;
                        ruleInfo.BranchId = data.d.Data.BranchID;
                        ruleInfo.BranchName = data.d.Data.BranchName;
                        ruleInfo.IspType = data.d.Data.IspType;
                        ruleInfo.IspName = data.d.Data.IspName;
                        ruleInfo.ProductTypeId = data.d.Data.ProductTypeId;
                        ruleInfo.ProductTypeName = data.d.Data.ProductTypeName;
                        ruleInfo.RebateMethod = data.d.Data.RebateMethod;
                        ruleInfo.Rebate = data.d.Data.Rebate;
                        ruleInfo.MinMoney = data.d.Data.MinMoney;
                        ruleInfo.MaxMoney = data.d.Data.MaxMoney;
                        ruleInfo.Sort = data.d.Data.Sort;
                        ruleInfo.ProvinceIds = data.d.Data.ProvinceIds;

                        $("#txtRebate").val(ruleInfo.Rebate);
                        $("#txtMinMoney").val(ruleInfo.MinMoney);
                        $("#txtMaxMoney").val(ruleInfo.MaxMoney);
                        $("#txtSort").val(ruleInfo.Sort);
                        $("#selectBranch").val(ruleInfo.BranchId);
                        $("#selectISPType").val(ruleInfo.IspType);
                        $("#ddlProductType").val(ruleInfo.ProductTypeId);
                        $("#ddlRebateMethod").val(ruleInfo.RebateMethod);
                        layer.closeAll();
                    } else {
                        layer.alert("获取规则失败", { icon: 2 }, function () { layer.closeAll(); });
                    }

                },
                error: function (err) {
                    layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                }
            });
        };

        function addNew() {
            ruleInfo.ID = 0;
            $("#btnNew").attr("disabled", "disabled");
        }



        function SaveInfo() {
            var rebate = $("#txtRebate").val();
            var minMoney = $("#txtMinMoney").val();
            var maxMoney = $("#txtMaxMoney").val();
            var sort = $("#txtSort").val();

            if (rebate == "") {
                layer.alert("请输入返利值！", { icon: 2 }, function () { layer.closeAll(); });
                return;
            }
            if (minMoney == "") {
                layer.alert("请输入充值最小金额！", { icon: 2 }, function () { layer.closeAll(); });
                return;
            }
            if (maxMoney == "") {
                layer.alert("请输入充值最大金额！", { icon: 2 }, function () { layer.closeAll(); });
                return;
            }
            if (sort == "") {
                layer.alert("请输入优先级值！", { icon: 2 }, function () { layer.closeAll(); });
                return;
            }
            ruleInfo.ProvinceIds = "";
            var boxList = document.getElementsByName("inline-checkboxprovince");
            for (var i = 0; i < boxList.length; i++) {
                if (boxList[i].checked == true) {
                    ruleInfo.ProvinceIds += boxList[i].value + ",";
                }
            }

            if (ruleInfo.ProvinceIds == "") {
                layer.alert("请选择返利省份！", { icon: 2 }, function () { layer.closeAll(); });
                return;
            }  


            ruleInfo.BranchId = $("#selectBranch").val();
            ruleInfo.BranchName = $("#selectBranch").find("option:selected").text();
            ruleInfo.IspType = $("#selectISPType").val();
            ruleInfo.IspName = $("#selectISPType").find("option:selected").text();
            ruleInfo.ProductTypeId = $("#ddlProductType").val();
            ruleInfo.ProductTypeName = $("#ddlProductType").find("option:selected").text();
            ruleInfo.RebateMethod = $("#ddlRebateMethod").val();
            ruleInfo.Rebate = rebate;
            ruleInfo.MinMoney = minMoney;
            ruleInfo.MaxMoney = maxMoney;
            ruleInfo.Sort = sort;
            //OperateModel
            $.ajax({
                type: "POST",
                url: "frmAddRebateRule.aspx/OperateModel",
                async: false,
                data: $.toJSON({ model: ruleInfo }),
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    var index = layer.load(1,
                        {
                            shade: [0.8, '#fff'] //0.1透明度的白色背景 
                        });
                },
                success: function (data) {
                    if (data.d.Data) {

                        layer.alert('操作返利规则成功！', { icon: 1 }, function () { layer.closeAll(); });
                    } else {
                        layer.alert(data.d.Message, { icon: 2 }, function () { layer.closeAll(); });
                    }
                },
                error: function (err) {
                    layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                }
            });
        }
    </script>
</head>
<body class="skin-blue">
    <form runat="server" id="form1">
        <section class="content">
            <div class="row">
                <div class="box box-info">
                    <div class="box-header">
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-10">

                                <div class="input-group">
                                    <span class="input-group-addon">代&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;理</span>
                                    <select id="selectBranch" class="form-control">
                                    </select>
                                </div>
                                <br />
                                <div class="input-group">
                                    <span class="input-group-addon">运&nbsp;&nbsp;营&nbsp;&nbsp;商</span>
                                    <select id="selectISPType" class="form-control">
                                        <option value="1">中国联通</option>
                                        <option value="2">中国移动</option>
                                        <option value="3">中国电信</option>
                                    </select>
                                </div>
                                <br />
                                <div class="input-group">
                                    <span class="input-group-addon">商品类型</span>
                                    <select id="ddlProductType" class="form-control">
                                        <option value="1001">手机话费</option>
                                        <option value="1002">座机话费</option>
                                        <option value="1003">流量充值</option>
                                    </select>
                                </div>
                                <br />
                                <div class="input-group">
                                    <span class="input-group-addon">返利方式</span>
                                    <select id="ddlRebateMethod" class="form-control">
                                        <option value="1">固定金额</option>
                                        <option value="2">比例</option>
                                    </select>
                                </div>
                                <br />
                                <div class="input-group">
                                    <span class="input-group-addon">返&nbsp;&nbsp;利&nbsp;&nbsp;值</span>
                                    <input type="number" class="form-control" placeholder="返利值" id="txtRebate" min="0" />
                                </div>
                                <br />
                                <div class="input-group">
                                    <span class="input-group-addon">充值最小值</span>
                                    <input type="number" class="form-control" placeholder="最小值" id="txtMinMoney" min="0" />
                                </div>
                                <br />
                                <div class="input-group">
                                    <span class="input-group-addon">充值最大值</span>
                                    <input type="number" class="form-control" placeholder="最大值" id="txtMaxMoney" min="0" />
                                </div>
                                <br />
                                <div class="input-group">
                                    <span class="input-group-addon">优先级</span>
                                    <input type="number" class="form-control" placeholder="优先级" id="txtSort" min="0" />
                                </div>
                                <br />
                                <div class="input-group">
                                    <span class="input-group-addon">返利省份</span>
                                    <div id="divProvince" style="border: 1px solid #d2d6de" runat="server" class="form-inline">
                                    </div>
                                </div>
                            </div>

                        </div>


                    </div>
                    <div class="box-footer">
                        <input type="button" class="btn btn-primary" value="保存" onclick="SaveInfo()" />

                        <input type="button" id="btnNew" value="清空" class="btn btn-success" onclick="addNew()" />
                    </div>
                </div>
            </div>
        </section>
    </form>

</body>
</html>
