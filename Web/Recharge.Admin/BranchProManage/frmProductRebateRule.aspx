﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="frmProductRebateRule.aspx.cs" Inherits="Recharge.Admin.BranchProManage.frmProductRebateRule" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/Content/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
    <script>
        var urlPath = window.location.pathname;
        function AddRebateRule(id) {
            $.ajax({
                type: "POST",
                url: "frmProductRebateRule.aspx/GetUserBtnRole",
                async: false,
                data: $.toJSON({ url: urlPath, btnName: 'btnAddRebateRule' }),
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                beforeSenF: function () {
                    var index = layer.load(1, {
                        shade: [0.8, '#fff'], //0.1透明度的白色背景 
                    });
                },
                success: function (data) {
                    if (data.d.Data) {
                        layer.open({
                            type: 2,
                            title: "代理返利设置",
                            shadeClose: false,
                            shade: 0.8,
                            area: [($(window).width() - 50) + 'px', ($(window).height() - 50) + 'px'],
                            content: "frmAddRebateRule.aspx?id=" + id,
                            close: function (index) {
                                window.location.reload();
                            },
                            cancel: function (index) {
                                window.location.reload();
                            }
                        });
                    } else {
                        layer.alert('对不起，您没有此权限！', { icon: 2 }, function () { layer.closeAll(); });
                    }

                },
                error: function (err) {
                    layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                }
            });
        };



    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="dataTables_length" id="example1_length">
                            <label>代理：</label>
                            <label>
                                <asp:DropDownList runat="server" ID="ddlBranch" class="form-control" placeholder="代理" aria-controls="datatable-default" Width="150px">
                                </asp:DropDownList>

                            </label>
                            <label>运营商类型：</label>
                            <label>
                                <asp:DropDownList runat="server" ID="ddlIspType" class="form-control" placeholder="商品类型" aria-controls="datatable-default" Width="150px">
                                    <asp:ListItem Text="请选择" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="中国联通" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="中国移动" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="中国电信" Value="3"></asp:ListItem>
                                </asp:DropDownList>
                            </label>
                            <label>商品类型：</label>
                            <label>
                                <asp:DropDownList runat="server" ID="ddlProductType" class="form-control" placeholder="商品类型" aria-controls="datatable-default" Width="150px">
                                </asp:DropDownList>
                            </label>
                            <label>
                                <asp:TextBox runat="server" ID="txtProductName" class="form-control" placeholder="商品名称" aria-controls="datatable-default" Width="150px" />

                            </label>

                            <label>
                                <asp:Button runat="server" ID="btnSearch" Text="查询" CssClass="btn btn-success" OnClick="btnSearch_Click" /></label>

                            <label>
                                <input type="button" id="btnAdd" value="返利规则管理" class="btn btn-danger" onclick="AddRebateRule(0)" /></label>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>编号</th>
                                    <th>代理商</th>
                                    <th>商品类型</th>
                                    <th>运营商</th>
                                    <th>返利方式</th>
                                    <th>充值区间</th>
                                    <th>当前状态</th>
                                    <th>优先顺序</th>
                                    <th>返利范围</th>
                                    <th>操作</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater runat="server" ID="repList">
                                    <ItemTemplate>
                                        <tr>
                                            <td><%#Eval("ID") %> </td>
                                            <td><%#Eval("BranchName") %> </td>
                                            <td><%#Eval("ProductTypeName") %> </td>
                                            <td><%#Eval("ISPName") %></td>
                                            <td><%#Convert.ToInt32(Eval("RebateMethod"))==1?"金额":"比例"%>(<%# Eval("Rebate") %>)</td>
                                            <td><%#Eval("MinMoney") %>-<%#Eval("MaxMoney") %></td>
                                            <td><%#Convert.ToInt32( Eval("Is_delete"))==0?"正常":"删除" %></td>
                                            <td><%#Eval("Sort") %></td>
                                            <td><%#Eval("ProvinceNames") %></td>
                                            <td><a class="btn btn-info btn-xs" onclick='AddRebateRule(<%#Eval("ID")%>)' href="javascript:void(0)">修改</a></td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>

                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-5">
                        <div class="dataTables_info" id="example2_info1" role="status" aria-live="polite">总计 <%=AspNetPager1.RecordCount %>条记录，每页显示<%=AspNetPager1.PageSize %>条 </div>
                    </div>

                    <div class="col-sm-7">
                        <div class="dataTables_paginate" id="example1_paginate">
                            <webdiyer:AspNetPager ShowPageIndexBox="Never" ID="AspNetPager1" runat="server" Width="100%" PageSize="16" PrevPageText="上一页" NextPageText="下一页" AlwaysShow="true" OnPageChanging="AspNetPager1_PageChanging" ShowFirstLast="False" PagingButtonsClass="paginate_button" CssClass="pagination" LayoutType="Ul" PagingButtonLayoutType="UnorderedList" PagingButtonSpacing="0" CurrentPageButtonClass="active" PrevNextButtonsClass="paginate_button previous">
                            </webdiyer:AspNetPager>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</asp:Content>
