﻿using CE.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Recharge.Admin.BranchProManage
{
    public partial class frmSettingProductSalePrice : BasePageClass
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static JsonResult<List<Model.MS_BranchManage>> GetAllBranch()
        {
            Model.StrWhere str = new Model.StrWhere() { IsWhereExist = true, strWhere = new StringBuilder(" isdelete=0") };
            return new JsonResult<List<Model.MS_BranchManage>>(new BLL.MS_BranchManage().GetModelList(str));
        }



        [WebMethod]
        public static JsonResult<List<Model.V8_Product_Manage>> GetAllProductBaseInfo(int productTypeId, int ispType, int phoneNumType)
        {
            var strSql = new StringBuilder(" 1=1 ");
            Model.StrWhere str = new Model.StrWhere() { IsWhereExist = true };
            if (productTypeId > 0)
                strSql.Append($" and ProductTypeID={productTypeId}");
            if (ispType > 0)
                strSql.Append($" and ISPType={ispType}");
            if (phoneNumType > 0)
                strSql.Append($" and PhoneNumType={phoneNumType}");
            str.strWhere = strSql;

            return new JsonResult<List<Model.V8_Product_Manage>>(new BLL.V8_Product_Manage().GetModelList(str));
        }

        [WebMethod]
        public static JsonResult<bool> CreateProductInfo(Model.V8_Product_Branch model, List<ProductList> productList)
        {
            int haveCounts = 0;
            foreach (var product in productList)
            {

                var str = new StringBuilder($"update V8_Product_Branch set Inprice={model.Inprice} where  BranchID={model.BranchID} and ProductTypeId={model.ProductTypeId} and ProductID={product.ProductID} ");
                if (new BLL.V8_Product_Branch().ExecuteSql(str.ToString()) > 0)
                    haveCounts++;


            }
            return new JsonResult<bool>(haveCounts > 0, "操作成功");
        }

    }

    public class ProductList
    {
        public long ProductID { get; set; }
        public string ProductName { get; set; }
    }
}