﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using CE.Utility;
using Model;

namespace Recharge.Admin.BranchProManage
{
    public partial class frmUpdBranchProduct : BasePageClass
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        [WebMethod]
        public static JsonResult<Model.V8_Product_Branch> GetModel(long id)
        {

            return new JsonResult<V8_Product_Branch>(new BLL.V8_Product_Branch().GetModel(id));
        }

        [WebMethod]
        public static JsonResult<bool> UpdateProductInfo(Model.V8_Product_Branch model)
        {
            if (model.ID == null || model.ID == 0)
            {
                return new JsonResult<bool>(false, "ID错误");
            }

            model.Updator = (HttpContext.Current.Session["SystemUserLoginInfo"] as Model.SystemUserInfo).UserName;
            model.UpdateTime = DateTime.Now;
            var result = new BLL.V8_Product_Branch().Update(model);
            return new JsonResult<bool>(result, result ? "修改成功" : "修改失败");

        }

    }
}