﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.DBUtility;
using Model;

namespace Recharge.Admin.BranchProManage
{
    public partial class frmUpdateBranchProvince : BasePageClass
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitPage();
            }
        }


        private void InitPage()
        {


            var dsAllProvince = new BLL.V8_Province().GetDataSet(new Model.StrWhere() { IsWhereExist = false });
            repList.DataSource = dsAllProvince;
            repList.DataBind();

        }


        public string GetProvince(object obj, object provinceName)
        { 

            var id = Request.QueryString["id"].ToString();
             
            var provCounts = new BLL.V8_Branch_RechargeArea().GetCount(new Model.StrWhere()
            {
                IsWhereExist = true,
                strWhere = new StringBuilder($" BranchProductId={id} and ProvinceId={obj} and cityId=0")
            });
            string innerHtml = "";
            if (provCounts > 0)
            {
                innerHtml +=
                    "<div class=\"checkbox  checkbox-inline\"><label><input type=\"checkbox\"  id=\"" +
                    obj +
                    "\" name=\"inline-checkboxProvince\" checked=\"checked\"   value=\"" +
                    obj +
                    "\" />" +
                    provinceName +
                    "</label></div>";

            }
            else
            {
                innerHtml +=
                    "<div class=\"checkbox  checkbox-inline\"><label><input type=\"checkbox\"   id=\"" +
                    obj +
                    "\" name=\"inline-checkboxProvince\" value=\"" +
                    obj +
                    "\" />" +
                    provinceName +
                    "</label></div>";

            }

            return innerHtml;
        }

        public string GetCityInfo(object obj)
        {
            var id = Request.QueryString["id"].ToString(); 
            string innerHtml = "";
            var allCity = new BLL.V8_City().GetDataSet(new Model.StrWhere() { IsWhereExist = true, strWhere = new StringBuilder($"ProvinceID={obj}") }).Tables[0];

            foreach (DataRow cityRow in allCity.Rows)
            {
                var cityCount = new BLL.V8_Branch_RechargeArea().GetCount(new Model.StrWhere()
                {
                    IsWhereExist = true,
                    strWhere = new StringBuilder($" BranchProductId={id} and   ProvinceId={obj} and cityId={cityRow["ID"]}")
                });

                if (cityCount > 0)
                {
                    innerHtml +=
                        "<div class=\"checkbox  checkbox-inline\"><label><input type=\"checkbox\"   id=\"" +
                       cityRow["ID"] +
                        "\" name=\"inline-checkboxCity\" checked=\"checked\"  value=\"" +
                        cityRow["ID"] +
                        "\" />" +
                        cityRow["CityName"].ToString() +
                        "</label></div>";
                }
                else
                {
                    innerHtml +=
                        "<div class=\"checkbox  checkbox-inline\"><label><input type=\"checkbox\"   id=\"" + cityRow["ID"] +
                        "\" name=\"inline-checkboxCity\" value=\"" +
                        cityRow["ID"] +
                        "\" />" +
                        cityRow["CityName"].ToString() +
                        "</label></div>";
                }
            }

            return innerHtml;
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            var branchProid = Request.QueryString["id"].ToString();
            var branchPro = new BLL.V8_Product_Branch().GetModel(Convert.ToInt64(branchProid));
            var branchInfo = new BLL.MS_BranchManage().GetModel(branchPro.BranchID);
            

            string provinceIds = hfProvince.Value;
            string cityIds = hfCity.Value;
            if (provinceIds.Length < 3 && cityIds.Length < 3)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "", "  layer.alert('请选择可充省份！',{ icon: 2 });", true);
            }

            DBHelper.ExecuteSql($"delete V8_Branch_RechargeArea where BranchProductId={branchProid} ");

            if (provinceIds.Length > 3)
            {
                provinceIds = provinceIds.Substring(0, provinceIds.Length - 1);

                DBHelper.ExecuteSql($"update V8_Product_Branch set ProvinceIds='{provinceIds}' where id={Request.QueryString["id"]}");

                foreach (var id in provinceIds.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    var provinceInfo = new BLL.V8_Province().GetModel(Convert.ToInt64(id));
                    new BLL.V8_Branch_RechargeArea().Add(new Model.V8_Branch_RechargeArea()
                    {
                        IspType = branchPro.ISPType,
                        IspName = branchPro.ISPName,
                        ProvinceId = (int)provinceInfo.ID,
                        ProvinceName = provinceInfo.ProvinceName,
                        BranchCode = branchInfo.BranchCode,
                        BranchId = branchInfo.ID,
                        BranchProductId = Convert.ToInt64(branchProid)

                    });
                }
            }

            if (cityIds.Length > 3)
            {
                cityIds = cityIds.Substring(0, cityIds.Length - 1);
                foreach (var id in cityIds.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    var cityInfo = new BLL.V8_City().GetModel(Convert.ToInt64(id));
                    var provinceInfo = new BLL.V8_Province().GetModel(cityInfo.ProvinceID);
                    new BLL.V8_Branch_RechargeArea().Add(new Model.V8_Branch_RechargeArea()
                    {
                        IspType = branchPro.ISPType,
                        IspName = branchPro.ISPName,
                        ProvinceId = (int)provinceInfo.ID,
                        ProvinceName = provinceInfo.ProvinceName,
                        BranchCode = branchInfo.BranchCode,
                        BranchId = branchInfo.ID,
                        BranchProductId = Convert.ToInt64(branchProid),
                        CityName = cityInfo.CityName,
                        CityId = Convert.ToInt32(id)

                    });
                }
            }


            AddOperationLog($"{branchInfo.BranchName}重新分配充值区域省份：{provinceIds},城市：{cityIds}");
            InitPage();
            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "", "  layer.alert('分配成功！',{icon:1});", true); 
           
        }
    }
}