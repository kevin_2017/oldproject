﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Recharge.Admin.Content
{
    public partial class PageControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
        private int _TotalRowCount;
        private int _pageSize;
        private int _previousPageCount = 5; // 当前页之前可以显示的最多条目数，大于此条目的将被隐藏
        private int _afterPageCount = 4; // 当前页之后可以显示的最多条目数，大于此条目的将被隐藏

        public int RecordCount
        {
            get { return _TotalRowCount; }
            set { _TotalRowCount = value; }
        }

        public int PageSize
        {
            get
            {
                return _pageSize == 0 ? int.Parse(ViewState["pagesize"].ToString()) : _pageSize;
            }
            set
            {
                _pageSize = value;
                ViewState["pagesize"] = value;
            }
        }

        public int PageIndex
        {
            get { return Convert.ToInt32(ViewState["xsCurrentPage"] ?? 1); }
            set { ViewState["xsCurrentPage"] = value; }
        }

        protected int PageCount
        {
            get
            {
                int tpage = 0;
                if (PageSize > 0)
                {
                    tpage = (RecordCount % PageSize == 0 ? RecordCount / PageSize : RecordCount / PageSize + 1);
                }
                return tpage;
            }
        }

        protected int PreviousPageCount
        {
            get
            {
                if (PageIndex > _previousPageCount)
                    return _previousPageCount;
                else
                {
                    if (PageIndex > 0)
                        return PageIndex - 1;
                    else
                    {
                        return 0;
                    }
                }
            }


        }

        protected int AfterPageCount
        {
            get
            {
                if (_afterPageCount <= (PageCount - PageIndex))
                    return _afterPageCount;
                else
                {
                    if (PageCount == PageIndex)
                        return 0;
                    else
                    {
                        return PageCount - PageIndex;
                    }
                }
            }
        }



        protected void btnPrevious_Click(object sender, EventArgs e)
        {
            PageIndex -= 1;
        }
        protected void lbtnNext_Click(object sender, EventArgs e)
        {
            PageIndex += 1;
        }
    }
}