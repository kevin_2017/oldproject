﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PageControl.ascx.cs" Inherits="Recharge.Admin.Content.PageControl" %>
<div class="row">
    <div class="col-sm-5">
        <div class="dataTables_info" id="example1_info" role="status" aria-live="polite">总计<%=PageCount %>页，<%=RecordCount %>条记录</div>
    </div>
    <div class="col-sm-7">
        <div class="dataTables_paginate" id="example1_paginate">
            <ul class="pagination">
                <% if (PageIndex == 1)
                    {
                %>
                <li class="paginate_button previous disabled"><a href="#" aria-controls="example1" data-dt-idx="0" tabindex="0">上一页</a></li>
                <%
                    }
                    else
                    {
                %>
                <li class="paginate_button previous">
                    <asp:LinkButton runat="server" ID="btnPrevious" OnClick="btnPrevious_Click" Text="上一页"></asp:LinkButton></li>
                <%
                    }
                    for (int i = PageIndex; i < PageCount && i < 5 + PageIndex; i++)
                    {
                        if (PageIndex == i)
                        {
                %>
                <li class="paginate_button active"><a href="#" aria-controls="example1" data-dt-idx="<%=i %>" tabindex="0"><%=i  %></a></li>
                <%
                    }
                    else
                    {
                %>
                <li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="<%=i+1 %>" tabindex="0"><%=i+1 %></a></li>
                <%
                        }
                    }


                    if (PageCount > PageIndex + 1 && PageCount - PageIndex > 5)
                    {
                %>
                <li class="paginate_button next">
                    <asp:LinkButton runat="server" ID="lbtnNext" OnClick="lbtnNext_Click" Text="下一页"></asp:LinkButton>
                </li>
                <%
                    }
                    else
                    {
                %>
                <li class="paginate_button next disabled"><a href="#" aria-controls="example1" data-dt-idx="<%=PageIndex %>" tabindex="0">下一页</a></li>

                <%
                    } %>
            </ul>
        </div>
    </div>
</div>

