﻿using CE.Utility;
using Common;
using Common.DBUtility;
using DotNet4.Utilities;
using Model;
using Newtonsoft.Json;
using Recharge.Admin.Models;
using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web.Services;

namespace Recharge.Admin.OrderManage
{
    public partial class frmRechargeOrderManage : BasePageClass
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.txtCreateTime.Value = DateTime.Now.ToString("yyyy-MM-dd");
                this.txtEndTime.Value = DateTime.Now.ToString("yyyy-MM-dd");
                StrWhere = "";
                if (txtCreateTime.Value.Trim() != "")
                {
                    StrWhere += " and (CreateTime  between '" + $"{txtCreateTime.Value.Trim()} 00:00:00" + "' and '" + $"{txtCreateTime.Value.Trim()} 23:59:59" + "')";
                }
                // Model.StrWhere str = new Model.StrWhere { IsWhereExist = false };

                // SetDropDownList.BindDropDownList(ddlProductType, new BLL.V8_Product_Type().GetDataSet(str).Tables[0], "TypeName", "ID");
                SetDropDownList.BindDropDownList(ddlBranch, new BLL.MS_BranchManage().GetDataSet(new StrWhere() { IsWhereExist = false }).Tables[0], "BranchName", "ID");
                SetDropDownList.BindDropDownList(ddlSupplierInfo, new BLL.SUP_SupplierManage().GetDataSet(new StrWhere() { IsWhereExist = false }).Tables[0], "TrueName", "ID");
                AspNetPager1.CurrentPageIndex = 1;
                InitPage();

            }
        }

        public void InitPage()
        {
            PageAttribute page = new PageAttribute
            {
                TableName = "vw_orderManage",
                Columns =
                    @"id,BranchName,RechargeNo,AttributionID,BranchOrderNo,CurentStatus,Counts,UnitPrice,TotalPrice,RechargeMoney,DiscountMethod,OrderNo,ProductSalePrice,CreateTime,OperationTime,CallBackTime,Operator,Descriptions,TimeLimit,SysSerialNum,BranchSerialNum,IsBuyBack,ProductID,IsDelete,ClientIp,NotifyUrl,ExtendParam,NoticeCount,NotifyStatus,ProductName,IspType,IspName,PhoneType,ComFrom,BranchID,CurrentRequestId,SupplierId, TrueName,ProvinceId,ProvinceName,CityName",
                StrOrder = "Id desc",
                WhereCondition = string.Concat(" and 1=1 ", StrWhere),
                PageIndex = AspNetPager1.CurrentPageIndex - 1,
                PageSize = AspNetPager1.PageSize
            };
            DataTable dt = new BLL.V8_OrderManage().GetListByPage(page);
            AspNetPager1.RecordCount = page.TotalRowCount.Value;
            repList.DataSource = dt;
            repList.DataBind();
            GetTotalInfo();
        }


        private void GetTotalInfo()
        {
            var sqlStr =
                $"select sum(1) totalCounts, sum(case when  curentstatus=1 then 1 else 0 end ) SuccessCount,sum(case when  curentstatus=1 then (totalprice)else 0 end )SuccessMoney ,sum(case when  curentstatus=1 then (totalprice-ProductSalePrice)else 0 end ) RebateMoney,sum(case when  curentstatus=5 then 1 else 0  end ) ErrorCount,sum(case when  curentstatus=5 then totalprice else 0 end ) ErrorMoney,sum(case when  curentstatus=3 then 1 else 0 end ) RechargeingCount,sum(case when  curentstatus=3 then  totalprice else 0 end ) RechargeingMoney from vw_orderManage ";
            if (StrWhere != "")
                sqlStr = $"{sqlStr} where 1=1 {StrWhere}";
            DataTable dt = DBHelper.Query(sqlStr).Tables[0];
            var html = "";
            if (dt.Rows.Count == 1)
            {
                DataRow dr = dt.Rows[0];

                html = $"总充值记录数：{(string.IsNullOrEmpty(dr["totalCounts"].ToString()) ? "0" : dr["totalCounts"].ToString())}，成功记录数：<span style=\"color:red\">{(string.IsNullOrEmpty(dr["SuccessCount"].ToString()) ? "0" : dr["SuccessCount"].ToString())}</span>，成功金额：<span style=\"color:red\">{(string.IsNullOrEmpty(dr["SuccessMoney"].ToString()) ? "0" : dr["SuccessMoney"].ToString())}</span>，返利金额：<span style=\"color:red\">{(string.IsNullOrEmpty(dr["RebateMoney"].ToString()) ? "0" : dr["RebateMoney"].ToString())}</span>，失败记录数：{(string.IsNullOrEmpty(dr["ErrorCount"].ToString()) ? "0" : dr["ErrorCount"].ToString())}，失败金额：{(string.IsNullOrEmpty(dr["ErrorMoney"].ToString()) ? "0" : dr["ErrorMoney"].ToString())}，正在充值数：{(string.IsNullOrEmpty(dr["RechargeingCount"].ToString()) ? "0" : dr["RechargeingCount"].ToString())}，正在充值金额：{(string.IsNullOrEmpty(dr["RechargeingMoney"].ToString()) ? "0" : dr["RechargeingMoney"].ToString())}";
            }

            divTotal.InnerHtml = html;
        }


        protected void AspNetPager1_PageChanging(object src, Wuqi.Webdiyer.PageChangingEventArgs e)
        {
            AspNetPager1.CurrentPageIndex = e.NewPageIndex;
            InitPage();
        }



        protected void btnSearch_Click(object obj, EventArgs e)
        {
            StrWhere = "";
            if (ddlBranch.SelectedItem.Value != "-1")
            {
                StrWhere += " and BranchID =" + ddlBranch.SelectedItem.Value.Trim();

            }
            if (ddlSupplierInfo.SelectedItem.Value != "-1")
            {
                StrWhere += " and SupplierId =" + ddlSupplierInfo.SelectedItem.Value.Trim();

            }
            if (ddlIspType.SelectedItem.Value != "-1")
            {
                StrWhere += " and ISPType =" + ddlIspType.SelectedItem.Value.Trim();

            }
            //if (ddlProductType.SelectedItem.Value.Trim() != "-1")
            //{
            //    StrWhere += " and ProductTypeID =" + ddlProductType.SelectedItem.Value.Trim();
            //}
            if (ddlCurentStatus.SelectedItem.Value != "-1")
            {
                StrWhere += " and CurentStatus =" + ddlCurentStatus.SelectedItem.Value.Trim();
            }
            if (txtOrderNo.Text.Trim() != "")
            {
                StrWhere += " and  OrderNo='" + txtOrderNo.Text.Trim() + "' ";
            }

            if (txtBranchNo.Text.Trim() != "")
            {
                StrWhere += " and BranchOrderNo ='" + txtBranchNo.Text.Trim() + "'";
            }
            if (txtRechargeNo.Text.Trim() != "")
            {
                StrWhere += " and RechargeNo ='" + txtRechargeNo.Text.Trim() + "'";
            }
            if (txtCreateTime.Value.Trim() != "" && string.IsNullOrEmpty(txtEndTime.Value.Trim()))
            {
                StrWhere += " and (CreateTime  between '" + $"{txtCreateTime.Value.Trim()} 00:00:00" + "' and '" + $"{txtCreateTime.Value.Trim()} 23:59:59" + "')";
            }
            else
            if (txtEndTime.Value.Trim() != "" && string.IsNullOrEmpty(txtCreateTime.Value.Trim()))
            {
                StrWhere += " and (CreateTime  between '" + $"{txtEndTime.Value.Trim()} 00:00:00" + "' and '" + $"{txtEndTime.Value.Trim()} 23:59:59" + "')";
            }
            else
            if (txtEndTime.Value.Trim() != "" && !string.IsNullOrEmpty(txtCreateTime.Value.Trim()))
            {
                StrWhere += " and (CreateTime  between '" + $"{txtCreateTime.Value.Trim()} 00:00:00" + "' and '" + $"{txtEndTime.Value.Trim()} 23:59:59" + "')";
            }
            AspNetPager1.CurrentPageIndex = 1;
            InitPage();
        }

        /// <summary>
        /// 查询订单状态
        /// </summary>
        /// <param name="orderNo"></param>
        /// <returns></returns>
        [WebMethod]
        public static JsonResult<ChkQueryresp> Queryorder(string orderNo)
        {
            ChkQueryresp resp;
            try
            {
                AddOperationLog($"【查询订单状态】 订单号：{orderNo}");

                //var address = ConfigurationManager.AppSettings["OrderQueryAddress"];
                var orderInfo = new BLL.V8_OrderManage().GetModel(orderNo);
                var requestInfo = new BLL.V8_OrderRequestRecs().GetModel(orderInfo.CurrentRequestId);
                var suppModel = new BLL.SUP_SupplierManage().GetModel(requestInfo.SupplierId);

                var http = new HttpHelper();
                var item = GetHttpItem($"{suppModel.GateWay}/{suppModel.ApiCode}/QueryOrder");
                item.URL += $"?orderNo={orderNo}";
                var result = http.GetHtml(item);

                resp = JsonConvert.DeserializeObject<ChkQueryresp>(result.Html);
                return new JsonResult<ChkQueryresp>(resp);
            }
            catch (Exception ex)
            {
                resp = new ChkQueryresp
                {
                    Success = false,
                    Message = "查询失败"
                };
                return new JsonResult<ChkQueryresp>(resp);

            }
        }

        /// <summary>
        /// 手动进行充值
        /// </summary>
        /// <param name="orderNo"></param>
        /// <returns></returns>
        [WebMethod]
        public static JsonResult<bool> DoRecharge(string orderNo)
        {
            AddOperationLog($"【手动执行充值请求】 订单号：{orderNo}");
            var orderModel = new BLL.V8_OrderManage().GetModel(orderNo);
            var msmqName = ConfigurationManager.AppSettings["RechargeOrderQueueName"];
            var msmqHelper = new MsmqHelper(msmqName);
            msmqHelper.Send<string>(orderModel.OrderNo);

            return new JsonResult<bool>(true, "请求成功");
        }

        /// <summary>
        /// 手动通知
        /// </summary>
        /// <param name="orderNo"></param>
        /// <returns></returns>
        [WebMethod]
        public static JsonResult<bool> NotifyBranch(string orderNo)
        {
            AddOperationLog($"【手动通知下游充值结果】 订单号：{orderNo}");
            var orderModel = new BLL.V8_OrderManage().GetModel(orderNo);
            var msmqName = ConfigurationManager.AppSettings["RechargeNotifyQueueName"];
            var msmqHelper = new MsmqHelper(msmqName);
            msmqHelper.Send<string>(orderNo);

            return new JsonResult<bool>(true, "请求成功");
        }


        public string GetSearchStatus(object obj)
        {
            return $" style='display:{((Convert.ToInt32(obj) == 2 || Convert.ToInt32(obj) == 3) ? "display" : "none")}'";
        }
        public string GetNotifyStatus(object obj, object obj1)
        {
            return $" style='display:{(((Convert.ToInt32(obj) == 1 || Convert.ToInt32(obj) == 4 || Convert.ToInt32(obj) == 5) && Convert.ToInt32(obj1) == 0) ? "" : "none")}'";
        }


        protected void btnExport_Click(object obj, EventArgs e)
        {
            string fileName = "";
            if (ddlBranch.SelectedItem.Value != "-1")
            {
                fileName += ddlBranch.SelectedItem.Text.Trim() + "_";
                StrWhere += " and BranchID =" + ddlBranch.SelectedItem.Value.Trim();
            }
            if (ddlSupplierInfo.SelectedItem.Value != "-1")
            {
                fileName += ddlSupplierInfo.SelectedItem.Text.Trim() + "_";
                StrWhere += " and SupplierId =" + ddlSupplierInfo.SelectedItem.Value.Trim();
            }
            if (ddlIspType.SelectedItem.Value != "-1")
            {
                fileName += ddlIspType.SelectedItem.Text + "_";
                StrWhere += " and ISPType =" + ddlIspType.SelectedItem.Value.Trim();
            }
            if (ddlCurentStatus.SelectedItem.Value != "-1")
            {
                fileName += ddlCurentStatus.SelectedItem.Text + "_";
                StrWhere += " and CurentStatus =" + ddlCurentStatus.SelectedItem.Value.Trim();
            }
            if (txtOrderNo.Text.Trim() != "")
            {
                fileName += txtOrderNo.Text.Trim() + "_";
                StrWhere += " and  OrderNo='" + txtOrderNo.Text.Trim() + "' ";
            }

            if (txtBranchNo.Text.Trim() != "")
            {
                fileName += txtBranchNo.Text.Trim() + "_";
                StrWhere += " and BranchOrderNo ='" + txtBranchNo.Text.Trim() + "'";
            }
            if (txtRechargeNo.Text.Trim() != "")
            {
                fileName += txtRechargeNo.Text.Trim() + "_";
                StrWhere += " and RechargeNo ='" + txtRechargeNo.Text.Trim() + "'";
            }
            if (txtCreateTime.Value.Trim() != "" && string.IsNullOrEmpty(txtEndTime.Value.Trim()))
            {
                StrWhere += " and (CreateTime  between '" + $"{txtCreateTime.Value.Trim()} 00:00:00" + "' and '" + $"{txtCreateTime.Value.Trim()} 23:59:59" + "')";
            }
            else
             if (txtEndTime.Value.Trim() != "" && string.IsNullOrEmpty(txtCreateTime.Value.Trim()))
            {
                StrWhere += " and (CreateTime  between '" + $"{txtEndTime.Value.Trim()} 00:00:00" + "' and '" + $"{txtEndTime.Value.Trim()} 23:59:59" + "')";
            }
            else
             if (txtEndTime.Value.Trim() != "" && !string.IsNullOrEmpty(txtCreateTime.Value.Trim()))
            {
                StrWhere += " and (CreateTime  between '" + $"{txtCreateTime.Value.Trim()} 00:00:00" + "' and '" + $"{txtEndTime.Value.Trim()} 23:59:59" + "')";
            }

            var TableName = "vw_orderManage";
            var Columns = @" OrderNo 订单号,BranchName 代理名,BranchOrderNo 代理订单号,RechargeNo 充值号码 ,ProductName 商品名称,IspName 运营商,case when PhoneType=1 then '手机' else '座机' end  号码类型 ,TrueName 接口,TotalPrice 充值金额,case when CurentStatus=1 then '充值成功' when CurentStatus=5 then '充值失败' end  充值状态,CreateTime 充值时间,CallBackTime 通知时间";

            var sqlStr = $" select {Columns} from {TableName} where 1=1 {StrWhere}";
            DataSet ds = DBHelper.Query(sqlStr);
            fileName = string.IsNullOrEmpty(fileName)
                ? DateTime.Now.ToString("yyyyMMddHHmmss")
                : fileName.Substring(0, fileName.Length - 1);
            NPOIHelper.ExportExcel(ds.Tables[0], fileName);
        }


        #region 得到Http对象
        /// <summary>
        /// 得到Http对象
        /// </summary>
        /// <param name="url">访问的地址</param>
        /// <returns></returns>
        private static HttpItem GetHttpItem(string url)
        {
            var item = new HttpItem()
            {
                URL = url,//URL     必需项
                Encoding = Encoding.UTF8,//编码格式（utf-8,gb2312,gbk）     可选项 默认类会自动识别
                Method = "GET",//URL     可选项 默认为Get
                Timeout = 100000,//连接超时时间     可选项默认为100000
                ReadWriteTimeout = 30000,//写入Post数据超时时间     可选项默认为30000
                IsToLower = false,//得到的HTML代码是否转成小写     可选项默认转小写
                ContentType = "application/x-www-form-urlencoded",//返回类型    可选项有默认值
                Expect100Continue = false,
                PostDataType = PostDataType.Byte,
                PostEncoding = Encoding.UTF8
            };
            return item;
        }
        #endregion

        #region 生成Http参数列表
        /// <summary>
        /// 生成Http参数列表
        /// </summary>
        /// <param name="nc"></param>
        /// <returns></returns>
        private static string GetParams(NameValueCollection nc)
        {
            if (null == nc || nc.Count == 0)
            {
                return "";
            }
            var sb = new StringBuilder();
            foreach (string key in nc.Keys)
            {
                if (null != key)
                {
                    sb.AppendFormat("{0}={1}&", key, nc[key]);
                }
            }
            return sb.ToString(0, sb.Length - 1);
        }
        #endregion



    }
}