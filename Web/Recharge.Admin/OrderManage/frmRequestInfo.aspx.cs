﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Model;

namespace Recharge.Admin.OrderManage
{
    public partial class frmRequestInfo : BasePageClass
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitPage();
            }
        }


        public void InitPage()
        {
            var orderInfo = new BLL.V8_OrderManage().GetModel(Request.QueryString["id"]);
            if (orderInfo != null)
            {
                lblBranchName.Text = orderInfo.BranchName;
                lblBranchOrderNo.Text = orderInfo.BranchOrderNo;
                lblOrderNo.Text = orderInfo.OrderNo;
                lblRechargeNo.Text = orderInfo.RechargeNo;
                lblIspName.Text = orderInfo.IspName;
                lblProductName.Text = orderInfo.ProductName; 
                lblNotifyUrl.Text = orderInfo.NotifyUrl;
                lblNotifyStatus.Text = orderInfo.NotifyStatus == 0 ? "未通知" : (orderInfo.NotifyStatus == 1 ? "通知成功" : (orderInfo.NotifyStatus == 2 ? "通知失败" : "通知中"));//（0、未通知，1、通知成功，2、通知失败，3、通知中）
                lblExtendParam.Text = orderInfo.ExtendParam;
                lblTotalPrice.Text = orderInfo.TotalPrice.ToString("f4");
                lblCreateTime.Text = orderInfo.CreateTime.ToString("yyyy-MM-dd HH:mm:ss:fff");
                lblRechargeMoney.Text = orderInfo.RechargeMoney.ToString("f4");
                lblCallBackTime.Text = orderInfo.CallBackTime == DateTime.MinValue ? "" : orderInfo.CallBackTime.ToString("yyyy-MM-dd HH:mm:ss:fff");
                lblCurentStatus.Text = orderInfo.CurentStatus == 1 ? "<span style=\"color:red\">成功</span>" : (orderInfo.CurentStatus == 2 ? "等待充值" : (orderInfo.CurentStatus == 3 ? "充值中" : (orderInfo.CurentStatus == 4 ? "问题订单" : "<span style=\"color:green\">失败</span>")));//1、充值成功，2、等待充值，3、充值中，4、问题订单，5、充值失败
            }

            DataSet ds = new BLL.V8_OrderRequestRecs().GetDataSet("vw_OrderRechargeRequestRecs", new StrWhere() { IsWhereExist = true, strWhere = new StringBuilder($" OrderNo='{Request.QueryString["id"]}' order by ID") });
            repList.DataSource = ds.Tables[0];
            repList.DataBind();

            DataSet dsnotify = new BLL.V8_OrderNotifyRecs().GetDataSet(new StrWhere() { IsWhereExist = true, strWhere = new StringBuilder($"OrderNo='{Request.QueryString["id"]}'") });
            repNotify.DataSource = dsnotify.Tables[0];
            repNotify.DataBind();
        }

        public string GetNotifyContents(string notifyStr)
        {
            var result = "";
            //  branchid：代理Id，branchorderno：代理订单号，extendparam：代理提交扩展参数，status,key

            string[] keys = notifyStr.Split(new[] { '&' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var item in keys)
            {
                string[] values = item.Split(new[] { '=' });
                if (item.Contains("branchid"))
                {
                    result = $"{result},代理ID:{values[1]}";
                }
                if (item.Contains("branchcode"))
                {
                    result = $"{result},代理编码:{values[1]}";
                }
                if (item.Contains("branchorderno"))
                {
                    result = $"{result},代理订单号:{values[1]}";
                }
                if (item.Contains("extendparam"))
                {
                    result = $"{result},充值扩展参数:{values[1]}";
                }
                if (item.Contains("status"))
                {
                    result = $"{result},充值状态:{(values[1]=="1"? "<span style=\"color:red\">成功</span>" : "<span style=\"color:green\">失败</span>")}";
                }
                //if (item.Contains("key"))
                //{
                //    result = $"{result},代理Key:{values[1]}";
                //}
            }

            return result.Substring(1);
        }
    }
}