﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="frmRechargeOrderManage.aspx.cs" Inherits="Recharge.Admin.OrderManage.frmRechargeOrderManage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/Content/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
    <script src="/Content/My97DatePicker/lang/zh-cn.js"></script>
    <link href="/Content/My97DatePicker/skin/default/datepicker.css" rel="stylesheet" />
    <script src="/Content/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
    <script>
        function QueryStatus(orderNo) {
            $.ajax({
                type: "POST",
                url: "frmRechargeOrderManage.aspx/Queryorder",
                async: false,
                data: $.toJSON({ orderNo: orderNo }),
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                beforeSenF: function () {
                    var index = layer.load(1, {
                        shade: [0.8, '#fff'] //0.1透明度的白色背景 
                    });
                },
                success: function (data) {
                    if (data.d.Data.Success) {
                        layer.alert(data.d.Data.Message, { icon: 1 }, function () { layer.closeAll(); window.location.reload(); });
                    } else {
                        layer.alert(data.d.Data.Message, { icon: 2 }, function () { layer.closeAll(); });
                    }
                },
                error: function (err) {
                    layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                }
            });
        };
        function DoRecharge(orderNo) {
            $.ajax({
                type: "POST",
                url: "frmRechargeOrderManage.aspx/DoRecharge",
                async: false,
                data: $.toJSON({ orderNo: orderNo }),
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                beforeSenF: function () {
                    var index = layer.load(1, {
                        shade: [0.8, '#fff'] //0.1透明度的白色背景 
                    });
                },
                success: function (data) {
                    if (data.d.Success) {
                        layer.alert(data.d.Message, { icon: 1 }, function () { layer.closeAll(); });
                    } else {
                        layer.alert(data.d.Message, { icon: 2 }, function () { layer.closeAll(); });
                    }
                },
                error: function (err) {
                    layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                }
            });
        }

        function openRequestInfo(orderNo) {
            layer.open({
                type: 2,
                title: orderNo + "充值详情",
                shadeClose: false,
                shade: 0.8,
                area: [($(window).width() - 100) + 'px', ($(window).height() - 50) + 'px'],
                content: "frmRequestInfo.aspx?id=" + orderNo,
                close: function (index) {

                },
                cancel: function (index) {

                }
            });
        }

        function NotifyBranch(orderNo) {
            $.ajax({
                type: "POST",
                url: "frmRechargeOrderManage.aspx/NotifyBranch",
                async: false,
                data: $.toJSON({ orderNo: orderNo }),
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                beforeSenF: function () {
                    var index = layer.load(1, {
                        shade: [0.8, '#fff'] //0.1透明度的白色背景 
                    });
                },
                success: function (data) {
                    if (data.d.Success) {
                        layer.alert(data.d.Message, { icon: 1 }, function () { layer.closeAll(); });
                    } else {
                        layer.alert(data.d.Message, { icon: 2 }, function () { layer.closeAll(); });
                    }

                },
                error: function (err) {
                    layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                }
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager runat="server" ID="smMain">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="upMain">
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExport" runat="server" />
        </Triggers>
        <ContentTemplate>
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="dataTables_length" id="example1_length">
                                    <label>接口：</label>
                                    <label>
                                        <asp:DropDownList runat="server" ID="ddlSupplierInfo" class="form-control" placeholder="接口" aria-controls="datatable-default" Width="150px">
                                        </asp:DropDownList></label>
                                    &nbsp;
                                    <label>代理：</label>
                                    <label>
                                        <asp:DropDownList runat="server" ID="ddlBranch" class="form-control" placeholder="代理" aria-controls="datatable-default" Width="150px">
                                        </asp:DropDownList></label>
                                    &nbsp;
                            <label>运营商类型：</label>
                                    <label>
                                        <asp:DropDownList runat="server" ID="ddlIspType" class="form-control" placeholder="商品类型" aria-controls="datatable-default" Width="150px">
                                            <asp:ListItem Text="请选择" Value="-1"></asp:ListItem>
                                            <asp:ListItem Text="中国联通" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="中国移动" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="中国电信" Value="3"></asp:ListItem>
                                        </asp:DropDownList></label>
                                    <%--  &nbsp;
                            <label>商品类型：</label>
                            <label>
                                <asp:DropDownList runat="server" ID="ddlProductType" class="form-control" placeholder="商品类型" aria-controls="datatable-default" Width="150px">
                                </asp:DropDownList></label>--%>
                            &nbsp;<label>当前状态：</label>
                                    <label>
                                        <%--1、充值成功，2、等待充值，3、充值中，4、问题订单，5、充值失败--%>
                                        <asp:DropDownList runat="server" ID="ddlCurentStatus" class="form-control" Width="150px">
                                            <asp:ListItem Value="-1">--请选择--</asp:ListItem>
                                            <asp:ListItem Value="1">充值成功</asp:ListItem>
                                            <asp:ListItem Value="2">等待充值</asp:ListItem>
                                            <asp:ListItem Value="3">充值中</asp:ListItem>
                                            <asp:ListItem Value="4">问题订单</asp:ListItem>
                                            <asp:ListItem Value="5">充值失败</asp:ListItem>
                                        </asp:DropDownList>
                                    </label>
                                    &nbsp;
                            <label>开始日期</label>
                                    <label>
                                        <input type="text" id="txtCreateTime" style="width: 185px; border-color: #CCCCCC;" runat="server" placeholder="开始日期" onfocus="WdatePicker({twoer:'whyGreen',maxDate:'%y-%M-%d'})" class=" form-control" />
                                    </label>
                                    &nbsp;
                                    <label>结束日期</label>
                                    <label>
                                        <input type="text" id="txtEndTime" style="width: 185px; border-color: #CCCCCC;" runat="server" placeholder="结束日期" onfocus="WdatePicker({twoer:'whyGreen',maxDate:'%y-%M-%d'})" class=" form-control" />
                                    </label>
                                    &nbsp;
                            <label>
                                <asp:TextBox runat="server" ID="txtOrderNo" class="form-control" placeholder="订单号" aria-controls="datatable-default" Width="170px" />
                            </label>
                                    &nbsp;
                            <label>
                                <asp:TextBox runat="server" ID="txtBranchNo" class="form-control" placeholder="代理订单号" aria-controls="datatable-default" Width="170px" />
                            </label>
                                    &nbsp;
                            <label>
                                <asp:TextBox runat="server" ID="txtRechargeNo" class="form-control" placeholder="充值号码" aria-controls="datatable-default" Width="150px" />
                            </label>
                                    &nbsp;
                            <label>
                                <asp:Button runat="server" ID="btnSearch" Text="查询" CssClass="btn btn-success" OnClick="btnSearch_Click" /></label>
                                    &nbsp;
                                    <label>
                                        <asp:Button runat="server" ID="btnExport" Text="导出数据" CssClass="btn btn-success" OnClick="btnExport_Click" /></label>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>订单号</th>
                                            <th>代理</th>
                                            <th>代理订单</th>
                                            <th>充值号码</th>
                                            <th>当前状态</th>
                                            <th>商品</th>
                                            <th>运营商</th>
                                            <th>号码归属地</th>
                                            <th>请求金额</th>
                                            <th>返利</th>
                                            <th>请求时间</th>
                                            <th>交易时间</th>
                                            <th>转发状态</th>
                                            <th>当前请求运营商</th>
                                            <%--<th>操作时间</th>
                                    <th>操作人</th>--%>
                                            <th>操作</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater runat="server" ID="repList">
                                            <ItemTemplate>
                                                <tr>
                                                    <td><a href="javascript:void(0)" onclick='openRequestInfo("<%# Eval("OrderNo") %>")'><%# Eval("OrderNo") %></a>
                                                    </td>
                                                    <td><a href="javascript:void(0)" title="<%# Eval("BranchName") %>"><%# GetCut(Eval("BranchName").ToString(),8) %></a></td>
                                                    <td><%# Eval("BranchOrderNo") %></td>
                                                    <td><%# Eval("RechargeNo") %></td>
                                                    <td>
                                                        <%# Convert.ToInt32( Eval("CurentStatus"))==1?"<span style=\"color:red\">充值成功</span>":(Convert.ToInt32( Eval("CurentStatus"))==2?"等待充值":(Convert.ToInt32( Eval("CurentStatus"))==3?"充值中":(Convert.ToInt32( Eval("CurentStatus"))==4?"问题订单":(Convert.ToInt32( Eval("CurentStatus"))==5?"<span style=\"color:green\">充值失败</span>":"未充值")))) %>
                                                    </td>
                                                    <td><%# Eval("ProductName") %></td>
                                                    <td><%# Eval("IspName") %></td>
                                                    <td><%# Eval("ProvinceName") %><%# Eval("CityName") %></td>
                                                    <td><%# Eval("TotalPrice") %></td>
                                                    <td><%#(Convert.ToInt32(Eval("CurentStatus"))==1?(Convert.ToDecimal(Eval("TotalPrice"))-Convert.ToDecimal( Eval("ProductSalePrice"))):0) %></td>
                                                    <td><%# Eval("CreateTime") %></td>
                                                    <td><%# Eval("CallBackTime") %></td>
                                                    <td><%# Convert.ToInt32(Eval("NotifyStatus"))==0?"未通知":( Convert.ToInt32(Eval("NotifyStatus"))==1?"通知成功":( Convert.ToInt32(Eval("NotifyStatus"))==2?"通知失败":"通知中")) %>
                                                    </td>
                                                    <td>
                                                        <%# Convert.ToString(Eval("TrueName"))==""?Eval("Descriptions"):Eval("TrueName") %>
                                                    </td>
                                                    <td>
                                                        <a class="btn btn-success btn-xs" onclick='QueryStatus("<%# Eval("OrderNo") %>")' href="javascript:void(0)" <%#GetSearchStatus(Eval("CurentStatus"))%>>查询</a>

                                                        <a class="btn btn-info btn-xs" onclick='DoRecharge("<%# Eval("OrderNo") %>")' href="javascript:void(0)" style='display: <%#(Convert.ToInt32(Eval("CurentStatus"))==2)?"":"none" %>'>补充</a>

                                                        <a class="btn btn-info btn-xs" onclick='NotifyBranch("<%# Eval("OrderNo") %>")' <%#GetNotifyStatus(Eval("CurentStatus"),0)%> href="javascript:void(0)">通知</a>
                                                        <%--<%#GetNotifyStatus(Eval("CurentStatus"),Eval("NotifyStatus"))%>--%>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th colspan="15">
                                                <div class="dataTables_info" id="divTotal" runat="server"></div>
                                            </th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-5">
                                <div class="dataTables_info" id="example2_info1" role="status" aria-live="polite">总计<%=AspNetPager1.RecordCount %>条记录，每页显示<%=AspNetPager1.PageSize %>条 </div>
                            </div>

                            <div class="col-sm-7">
                                <div class="dataTables_paginate" id="example1_paginate">
                                    <webdiyer:AspNetPager ShowPageIndexBox="Never" ID="AspNetPager1" runat="server" Width="100%" PageSize="16" PrevPageText="上一页" NextPageText="下一页" AlwaysShow="true" OnPageChanging="AspNetPager1_PageChanging" ShowFirstLast="False" PagingButtonsClass="paginate_button" CssClass="pagination" LayoutType="Ul" PagingButtonLayoutType="UnorderedList" PagingButtonSpacing="0" CurrentPageButtonClass="active" PrevNextButtonsClass="paginate_button previous">
                                    </webdiyer:AspNetPager>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
