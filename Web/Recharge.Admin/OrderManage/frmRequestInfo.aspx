﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmRequestInfo.aspx.cs" Inherits="Recharge.Admin.OrderManage.frmRequestInfo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <!-- Bootstrap 3.3.4 -->
    <link href="/Content/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="/Content/dist/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="/Content/dist/css/ionicons.min.css" rel="stylesheet" type="text/css" />

    <!-- Theme style -->
    <link href="/Content/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins 
    folder instead of downloading all of them to reduce the load. -->
    <link href="/Content/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
    <script src="/Content/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="/Content/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

    <!-- SlimScroll -->
    <script src="/Content/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src='/Content/plugins/fastclick/fastclick.min.js'></script>
    <!-- AdminLTE App -->
    <script src="/Content/dist/js/app.min.js" type="text/javascript"></script>
    <script src="/Content/dist/js/demo.js" type="text/javascript"></script>

    <script src="/Content/layer/layer.js"></script>
    <script src="/Content/jquery.json.min.js"></script>
    <script>
        //function getQueryString(name) {
        //    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
        //    var r = window.location.search.substr(1).match(reg);
        //    if (r != null) return unescape(r[2]);
        //    return null;
        //};
    </script>
</head>
<body class="skin-blue">
    <section class="content">
        <div class="row">
            <div class="box box-info">
                <div class="box-header">
                    充值基础信息
                </div>
                <div class="box-body">
                    <table class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <td>订单号:</td>
                                <td>
                                    <asp:Label runat="server" ID="lblOrderNo"></asp:Label>
                                </td>
                                <td>代理订单号:</td>
                                <td>
                                    <asp:Label runat="server" ID="lblBranchOrderNo"></asp:Label>
                                </td>
                                <td>代理:</td>
                                <td>
                                    <asp:Label runat="server" ID="lblBranchName"></asp:Label>
                                </td>
                                <td>充值号码:</td>
                                <td>
                                    <asp:Label runat="server" ID="lblRechargeNo"></asp:Label>
                                </td>
                                <td>运营商:</td>
                                <td>
                                    <asp:Label runat="server" ID="lblIspName"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>商品:</td>
                                <td>
                                    <asp:Label runat="server" ID="lblProductName"></asp:Label>
                                </td>
                               
                                <td>通知地址:</td>
                                <td>
                                    <asp:Label runat="server" ID="lblNotifyUrl"></asp:Label>
                                </td>
                                <td>通知状态:</td>
                                <td>
                                    <asp:Label runat="server" ID="lblNotifyStatus"></asp:Label>
                                </td>
                                <td>扩展参数:</td>
                                <td>
                                    <asp:Label runat="server" ID="lblExtendParam"></asp:Label>
                                </td>
                                <td> </td>
                                <td>
                                    
                                </td>
                            </tr>
                            <tr>
                                <td>充值金额:</td>
                                <td>
                                    <asp:Label runat="server" ID="lblTotalPrice"></asp:Label>
                                </td>
                                <td>充值时间:</td>
                                <td>
                                    <asp:Label runat="server" ID="lblCreateTime"></asp:Label>
                                </td>
                                <td>实际充值金额:</td>
                                <td>
                                    <asp:Label runat="server" ID="lblRechargeMoney"></asp:Label>
                                </td>
                                <td>到账时间:</td>
                                <td>
                                    <asp:Label runat="server" ID="lblCallBackTime"></asp:Label>
                                </td>
                                <td>当前状态:</td>
                                <td>
                                    <span style="color: red">
                                        <asp:Label runat="server" ID="lblCurentStatus"></asp:Label></span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="box box-info">
                <div class="box-header">
                    充值请求详情
                </div>
                <div class="box-body">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>订单信息</th>
                                <th>充值内容</th>
                                <th>充值返回内容</th>
                                <th>上游通知内容</th>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Repeater runat="server" ID="repList">
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Eval("OrderNo") %>
                                            <br />
                                            供应商： <%# Eval("SupplierName") %>
                                            <br />
                                            充值请求时间 <%# Eval("RequestTime") %></td>
                                        <td>
                                            <textarea rows="5" cols="80" style="resize: none"><%# Eval("RequestContent") %></textarea>
                                        </td>

                                        <td>
                                            <textarea rows="5" cols="80" style="resize: none"><%# Eval("ResponseContent") %></textarea>
                                        </td>
                                        <td>
                                            <textarea rows="5" cols="80" style="resize: none"><%# Eval("NoticeContent") %></textarea>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="box box-info">
                <div class="box-header">
                    下游通知详情
                </div>
                <div class="box-body">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>通知时间</th>
                                <th>通知结果</th>
                                <th colspan="2">通知内容（branchid：代理Id，branchorderno：代理订单号，extendparam：代理提交扩展参数，status：返回充值状态：【0：<span style="color: green">:充值失败</span>，1：<span style="color: red">充值成功</span>）</th>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Repeater runat="server" ID="repNotify">
                                <ItemTemplate>
                                    <tr>
                                        <td>
                                            <%# Eval("NotifyTime") %> 
                                        </td>
                                        <td>
                                            <%# Convert.ToInt32(Eval("NotifyResult"))==1?"<span style=\"color:red\">通知成功</span>":"<span style=\"color:green\">通知失败</span>" %>
                                        </td>
                                        <td>
                                            <%# Eval("NotifyConent") %> 
                                        </td>
                                        <td>
                                            <%#GetNotifyContents(Eval("NotifyConent").ToString()) %>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>


</body>
</html>
