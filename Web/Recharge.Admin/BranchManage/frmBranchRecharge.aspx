﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="frmBranchRecharge.aspx.cs" Inherits="Recharge.Admin.BranchManage.frmBranchRecharge" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        function getQueryString(name) {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
            var r = window.location.search.substr(1).match(reg);
            if (r != null) return unescape(r[2]);
            return null;
        }
        var branchList = [];
        var branchInfo = {};
        $(function () {
            var innerhtml = "";
            $.ajax({
                type: "POST",
                url: "frmBranchRecharge.aspx/GetAllBranch",
                async: false,
                data: {},
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                beforeSenF: function () {
                    var index = layer.load(1, {
                        shade: [0.8, '#fff'], //0.1透明度的白色背景 
                    });
                },
                success: function (data) {
                    if (data.d.Data.length > 0) {
                        branchList = data.d.Data;
                        $.each(data.d.Data, function (i, item) {
                            innerhtml += "  <option value=\"" + item["ID"] + "\">" + item["BranchName"] + "</option>";
                        })
                        $("#ddlBranch").html(innerhtml);
                        if (getQueryString("id") == null)
                            $("#ddlBranch option[0]").attr("selected", "selected");
                        else
                            $("#ddlBranch option[value='" + getQueryString("id") + "']").attr("selected", "selected");
                        ChangeBranch();
                    }
                },
                error: function (err) {
                    layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                }
            });
        })
        function ChangeBranch() {
            var branchId = $("#ddlBranch").val();
            $.each(branchList, function (i, item) {
                if (item["ID"] == branchId) {
                    branchInfo = item;
                    $("#txtBalance").val(branchInfo.Balance);
                }
            })
        };
        function SaveInfo() {
            branchInfo.Balance = $("#txtBalance").val();
            var rechargeMoney = $("#txtRechargeBalance").val();
            //if (parseFloat(rechargeMoney) < 1) {
            //    layer.alert("最小充值金额为1", { icon: 2 }, function () { layer.closeAll(); });
            //    return;
            //}
            $.ajax({
                type: "POST",
                url: "frmBranchRecharge.aspx/SaveMoney",
                async: false,
                data: $.toJSON({ branchId: branchInfo.ID, money: rechargeMoney, changeMethod: $("#rdoChangeMethod").val() }),
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                beforeSenF: function () {
                    var index = layer.load(1, {
                        shade: [0.8, '#fff'], //0.1透明度的白色背景 
                    });
                },
                success: function (data) {
                    if (data.d.Data) {
                        layer.alert('代理商增加余额成功！', { icon: 1 }, function () { layer.closeAll(); });
                    } else {
                        layer.alert('对不起，代理商增加余额失败！', { icon: 2 }, function () { layer.closeAll(); });
                    }
                },
                error: function (err) {
                    layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                }
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="box box-info">
        <div class="box-header">
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-sm-6">
                    <div class="input-group">
                        <span class="input-group-addon">代理商名</span>
                        <select id="ddlBranch" class="form-control" onchange="ChangeBranch()"></select>
                    </div>
                    <br />
                    <div class="input-group">
                        <span class="input-group-addon">当前余额</span>
                        <input type="text" class="form-control" placeholder="当前余额" id="txtBalance" readonly />
                    </div>
                    <br />
                    <div class="input-group">
                        <span class="input-group-addon">充值金额</span>
                        <input type="number" class="form-control" placeholder="充值金额" id="txtRechargeBalance"  />
                    </div>

                    <br />
                    <div class="input-group">
                        <span class="input-group-addon">加款方式</span>
                        <select id="rdoChangeMethod" class="form-control">
                            <option value="现金">现金</option>
                            <option value="微信">微信支付</option>
                            <option value="pos机">pos机支付</option>
                        </select>
                    </div>

                </div>
            </div>
        </div>
        <div class="box-footer">
            <input type="button" class="btn btn-primary" value="保存" onclick="SaveInfo()" />
        </div>
    </div>
</asp:Content>
