﻿using CE.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Recharge.Admin.BranchManage
{
    public partial class frmBranchRecharge : BasePageClass
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static JsonResult<List<Model.MS_BranchManage>> GetAllBranch()
        {
            Model.StrWhere str = new Model.StrWhere();
            str.IsWhereExist = true;
            str.strWhere = new StringBuilder().Append("Status=1");
            return new JsonResult<List<Model.MS_BranchManage>>(new BLL.MS_BranchManage().GetModelList(str));
        }

        [WebMethod]
        public static JsonResult<bool> SaveMoney(long branchId, decimal money, string changeMethod)
        {
            AddOperationLog($"【代理增加余额】代理ID:{branchId},金额;{money}");

            return new JsonResult<bool>(new BLL.MS_BranchManage().Proc_BalanceChange(1, money, branchId, 0, changeMethod, "系统加款" + money + "元"));
        }
    }
}