﻿using CE.Utility;
using Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Recharge.Admin.BranchManage
{
    public partial class frmBranchManage : BasePageClass
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

                Model.StrWhere str = new Model.StrWhere();
                str.IsWhereExist = false;
                SetDropDownList.BindDropDownList(ddlBranch, new BLL.SUP_SupplierManage().GetDataSet(str).Tables[0], "TrueName", "ID");

                AspNetPager1.CurrentPageIndex = 1;
                InitPage();

            }
        }

        public void InitPage()
        {

            PageAttribute page = new PageAttribute();
            page.TableName = "MS_BranchManage";
            page.Columns = @"ID, BranchName, StoreKeeper, Phones, Address, Status, ClientCount, EmployeeCount, RechargeMOney, Balance, SettlementMethod, CreateTime, Level, ISSystem, ParentID,IsDelete,RechargeType,FrozenMoney,BranchCode ";
            page.StrOrder = " ID asc";
            page.WhereCondition = string.Concat(StrWhere);
            page.PageIndex = AspNetPager1.CurrentPageIndex - 1;
            page.PageSize = AspNetPager1.PageSize;
            DataTable dt = new BLL.MS_BranchManage().GetListByPage(page);
            AspNetPager1.RecordCount = page.TotalRowCount.Value;
            repList.DataSource = dt;
            repList.DataBind();
        }


        protected void AspNetPager1_PageChanging(object src, Wuqi.Webdiyer.PageChangingEventArgs e)
        {
            AspNetPager1.CurrentPageIndex = e.NewPageIndex;
            InitPage();
        }


        protected void btnSearch_Click(object obj, EventArgs e)
        {
            if (ddlBranch.SelectedItem.Value.Trim() != "-1")
            {
                StrWhere = " and ID =" + ddlBranch.SelectedItem.Value.Trim();
            }
            else
            {
                StrWhere = "";
            }

            AspNetPager1.CurrentPageIndex = 1;
            InitPage();
        }
        [WebMethod]
        public static JsonResult<bool> GetUserBtnRole(string url, string btnName)
        {
            if (!GetButtonRole(url, btnName))
                return new JsonResult<bool>(false, "对不起，无权操作");
            else
                return new JsonResult<bool>(true, "操作成功");

        }

        [WebMethod]
        public static JsonResult<bool> DelBranchInfo(long id, string url, string btnName, int operType)
        {
            if (!GetButtonRole(url, btnName))
                return new JsonResult<bool>(false, "对不起，无权操作");
            Model.MS_BranchManage model = new Model.MS_BranchManage();

            model = new BLL.MS_BranchManage().GetModel(id);
            if (operType == 1)
            {
                AddOperationLog($"【恢复代理】 代理ID：{id}");
                model.IsDelete = 0;
            }
            else
            {
                model.IsDelete = 1;
                AddOperationLog($"【删除代理】 代理ID：{id}");
            }

            return new JsonResult<bool>(new BLL.MS_BranchManage().Update(model));
        }
    }

}