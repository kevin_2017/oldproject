﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="frmBranchManage.aspx.cs" Inherits="Recharge.Admin.BranchManage.frmBranchManage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/Content/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
    <script>
        var urlPath = window.location.pathname;

        function UpdateBranchInfo(isSystem, id, branchName) {
            if (isSystem == 1) {
                layer.alert('对不起，系统级代理无法进行修改！', { icon: 2 }, function () { layer.closeAll(); });
            } else {
                $.ajax({
                    type: "POST",
                    url: "frmBranchManage.aspx/GetUserBtnRole",
                    async: false,
                    data: $.toJSON({ url: urlPath, btnName: 'btnUpdate' }),
                    contentType: "application/json;charset=utf-8",
                    dataType: "json",
                    beforeSenF: function () {
                        var index = layer.load(1, {
                            shade: [0.8, '#fff'], //0.1透明度的白色背景 
                        });
                    },
                    success: function (data) {
                        if (data.d.Data) {
                            layer.open({
                                type: 2,
                                title: "修改代理商  " + branchName + "  信息",
                                shadeClose: false,
                                shade: 0.8,
                                area: ['800px', '720px'],
                                content: "frmUpdateBranch.aspx?id=" + id,
                                close: function (index) {
                                    window.location.reload();
                                },
                                cancel: function (index) {
                                    window.location.reload();
                                }
                            });
                        } else {
                            layer.alert('对不起，您没有此权限！', { icon: 2 }, function () { layer.closeAll(); });
                        }

                    },
                    error: function (err) {
                        layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                    }
                });
            }
        }

        //删除或恢复代理
        function DeleteBranchInfo(isSystem, id, trueName, isDelete) {
            if (isSystem == 1) {
                layer.alert('对不起，系统级代理无法进行删除！', { icon: 2 }, function () { layer.closeAll(); });
            } else {
                var msg = "";
                if (isDelete == 1)
                    msg = "恢复";
                else
                    msg = "删除";
                layer.confirm("确定要" + msg + "理商 " + trueName + " ？", { title: msg + '代理商', btn: ['确定', '取消'], closeBtn: 0, icon: 3 }, function () {

                    /*删除*/
                    $.ajax({
                        type: "POST",
                        url: "frmBranchManage.aspx/DelBranchInfo",
                        async: false,
                        data: $.toJSON({ id: id, url: urlPath, btnName: 'btnDelete', operType: isDelete }),
                        contentType: "application/json;charset=utf-8",
                        dataType: "json",
                        beforeSenF: function () {
                            var index = layer.load(1, {
                                shade: [0.8, '#fff'], //0.1透明度的白色背景 
                            });
                        },
                        success: function (data) {
                            if (data.d.Data) {
                                layer.alert(msg + '成功！', { icon: 1 }, function () { layer.closeAll(); window.location.reload(); });
                            } else {
                                layer.alert('对不起，' + msg + '失败！{data.d.Message}', { icon: 2 }, function () { layer.closeAll(); });
                            }

                        },
                        error: function (err) {
                            layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                        }
                    });
                    /*End删除*/

                }, function () {
                    layer.closeAll();
                })


            }
        }

        function BranchUserManage(id, branchName) {
            $.ajax({
                type: "POST",

                url: "frmBranchManage.aspx/GetUserBtnRole",
                async: false,
                data: $.toJSON({ url: urlPath, btnName: 'btnUserManage' }),
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                beforeSenF: function () {
                    var index = layer.load(1, {
                        shade: [0.8, '#fff'], //0.1透明度的白色背景 
                    });
                },
                success: function (data) {
                    if (data.d.Data) {
                        layer.open({
                            type: 2,
                            title: "代理商  " + branchName + "  用户管理",
                            shadeClose: false,
                            shade: 0.8,
                            area: [($(window).width() - 100) + 'px', ($(window).height() - 100) + 'px'],
                            content: "frmBranchUserManage.aspx?id=" + id,
                            close: function (index) {
                                window.location.reload();
                            },
                            cancel: function (index) {
                                window.location.reload();
                            }
                        });
                    } else {
                        layer.alert('对不起，您没有此权限！', { icon: 2 }, function () { layer.closeAll(); });
                    }

                },
                error: function (err) {
                    layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                }
            });
        }

        function UpdateBranchPrivateKey(isSystem, id, branchName) {
            if (isSystem == 1) {
                layer.alert('对不起，系统级代理无法进行修改！', { icon: 2 }, function () { layer.closeAll(); });
            } else {
                $.ajax({
                    type: "POST",
                    url: "frmBranchManage.aspx/GetUserBtnRole",
                    async: false,
                    data: $.toJSON({ url: urlPath, btnName: 'btnUpdatePrivateKey' }),
                    contentType: "application/json;charset=utf-8",
                    dataType: "json",
                    beforeSenF: function () {
                        var index = layer.load(1, {
                            shade: [0.8, '#fff'], //0.1透明度的白色背景 
                        });
                    },
                    success: function (data) {
                        if (data.d.Data) {
                            layer.open({
                                type: 2,
                                title: "修改代理商  " + branchName + "  接口密钥",
                                shadeClose: false,
                                shade: 0.8,
                                area: ['500px', '350px'],
                                content: "frmPrivateKeyManage.aspx?id=" + id,
                                close: function (index) {
                                    window.location.reload();
                                },
                                cancel: function (index) {
                                    window.location.reload();
                                }
                            });
                        } else {
                            layer.alert('对不起，您没有此权限！', { icon: 2 }, function () { layer.closeAll(); });
                        }

                    },
                    error: function (err) {
                        layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                    }
                });
            }
        }
        function SaveRecharge(id, branchName) {
            $.ajax({
                type: "POST",

                url: "frmBranchManage.aspx/GetUserBtnRole",
                async: false,
                data: $.toJSON({ url: urlPath, btnName: 'btnSaveRecharge' }),
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                beforeSenF: function () {
                    var index = layer.load(1, {
                        shade: [0.8, '#fff'], //0.1透明度的白色背景 
                    });
                },
                success: function (data) {
                    if (data.d.Data) {
                        layer.open({
                            type: 2,
                            title: "代理商  " + branchName + "  充值接口管理",
                            shadeClose: false,
                            shade: 0.8,
                            area: [ '500px', ($(window).height() - 100) + 'px'],
                            content: "frmBranch_SuppManage.aspx?id=" + id,
                            close: function (index) {
                                window.location.reload();
                            },
                            cancel: function (index) {
                                window.location.reload();
                            }
                        });
                    } else {
                        layer.alert('对不起，您没有此权限！', { icon: 2 }, function () { layer.closeAll(); });
                    }

                },
                error: function (err) {
                    layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                }
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="dataTables_length" id="example1_length">
                            <label>查询：</label>
                            <label>
                                <asp:DropDownList runat="server" ID="ddlBranch" class="form-control" placeholder="代理商管理名" aria-controls="datatable-default" Width="180px" />
                            </label>

                            <label>
                                <asp:Button runat="server" ID="btnSearch" Text="查询" CssClass="btn btn-success" OnClick="btnSearch_Click" /></label>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>代理编码</th>
                                    <th>代理商名</th>
                                    <%--    <th>联系方式</th>--%>
                                    <th>当前状态</th>
                                    <th>用户数量</th>
                                    <th>电脑终端数</th>
                                    <th>余额</th>
                                    <th>冻结金额</th>
                                    <th>充值总金额</th>
                                    <th>结算方式</th>
                                    <th>代理类型</th>
                                    <th>充值方式</th>
                                    <th>操作</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater runat="server" ID="repList">
                                    <ItemTemplate>
                                        <tr>
                                            <td><%#Eval("ID") %></td>
                                            <td><%#Eval("BranchCode") %></td>
                                            <td><%#Eval("BranchName") %></td>
                                            <%-- <td>联  系  人  ：<%#Eval("StoreKeeper") %><br />
                                                联系电话：<%#Eval("Phones") %>
                                            </td>--%>

                                            <td><%#  Convert.ToInt32(Eval("IsDelete"))==0?( Convert.ToInt32( Eval("Status"))==1?"<span class=\"btn btn-success btn-xs\" disabled=\"disabled\">正常</span>":(Convert.ToInt32( Eval("Status"))==2?"<span class=\"btn btn-info btn-xs\" disabled=\"disabled\">暂停</span>":"<span class=\"btn btn-danger btn-xs\" disabled=\"disabled\">停店</span>")):"<span class=\"btn btn-danger btn-xs\" disabled=\"disabled\">删除</span>" %> </td>

                                            <td><%#Eval("EmployeeCount") %></td>
                                            <td><%#Eval("ClientCount") %></td>
                                            <td><%#Eval("Balance") %></td>
                                            <td><%#Eval("FrozenMoney") %></td>
                                            <td><%#Eval("RechargeMoney") %></td>
                                            <td><%# Convert.ToInt32(Eval("SettlementMethod"))==1?"普通":(Convert.ToInt32(Eval("SettlementMethod"))==2?"按日":(Convert.ToInt32(Eval("SettlementMethod"))==3?"签约":"年租")) %></td>
                                            <td><%# Convert.ToInt32( Eval("ISSystem"))==1?"<span class=\"btn btn-success btn-xs\" disabled=\"disabled\">系统代理</span>":"<span class=\"btn btn-danger btn-xs\" disabled=\"disabled\">普通代理</span>" %></td>
                                            <td><%# Convert.ToInt32( Eval("RechargeType"))==0?"<span class=\"btn btn-success btn-xs\" disabled=\"disabled\">系统轮循</span>":"<span class=\"btn btn-danger btn-xs\" disabled=\"disabled\">指定供应商</span>" %></td>

                                            <td>
                                                <a class="btn btn-success  btn-xs" onclick='UpdateBranchInfo(<%#Eval("ISSystem") %>,<%#Eval("ID")%>,"<%#Eval("BranchName") %>")' href="javascript:void(0)">编辑</a>
                                                <a class="btn btn-info  btn-xs" onclick='DeleteBranchInfo(<%#Eval("ISSystem") %>,<%#Eval("ID")%>,"<%#Eval("BranchName") %>",<%#Eval("IsDelete") %>)' href="javascript:void(0)"><%#Convert.ToInt32(Eval("IsDelete"))==0?"删除":"恢复" %></a>
                                                <a class="btn btn-warning  btn-xs" onclick='BranchUserManage(<%#Eval("ID")%>,"<%#Eval("BranchName") %>")' href="javascript:void(0)">用户管理</a>
                                                <%-- <a class="btn btn-primary  btn-xs" onclick='BranchClientManage(<%#Eval("ID")%>,"<%#Eval("BranchName") %>")' href="javascript:void(0)" style="display: none">终端管理</a>--%>
                                                <a class="btn btn-primary  btn-xs" href="frmBranchRecharge.aspx?id=<%#Eval("ID") %>">加款</a>
                                                <a class="btn btn-info  btn-xs" onclick='UpdateBranchPrivateKey(<%#Eval("ISSystem") %>,<%#Eval("ID")%>,"<%#Eval("BranchName") %>")' href="javascript:void(0)">密钥管理</a>

                                                <a class="btn btn-success btn-xs" onclick='SaveRecharge(<%# Eval("ID") %>,"<%#Eval("BranchName") %>")' href="javascript:void(0)" style='display: <%# Convert.ToInt32(Eval("RechargeType"))==1?"":"none" %>'>充值接口管理</a>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>

                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-5">
                        <div class="dataTables_info" id="example2_info1" role="status" aria-live="polite">总计 <%=AspNetPager1.RecordCount %>条记录，每页显示<%=AspNetPager1.PageSize %>条 </div>
                    </div>

                    <div class="col-sm-7">
                        <div class="dataTables_paginate" id="example1_paginate">
                            <webdiyer:AspNetPager ShowPageIndexBox="Never" ID="AspNetPager1" runat="server" Width="100%" PageSize="16" PrevPageText="上一页" NextPageText="下一页" AlwaysShow="true" OnPageChanging="AspNetPager1_PageChanging" ShowFirstLast="False" PagingButtonsClass="paginate_button" CssClass="pagination" LayoutType="Ul" PagingButtonLayoutType="UnorderedList" PagingButtonSpacing="0" CurrentPageButtonClass="active" PrevNextButtonsClass="paginate_button previous">
                            </webdiyer:AspNetPager>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</asp:Content>
