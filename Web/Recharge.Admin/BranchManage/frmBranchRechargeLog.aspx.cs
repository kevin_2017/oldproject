﻿using Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Recharge.Admin.BranchManage
{
    public partial class frmBranchRechargeLog : BasePageClass
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

                Model.StrWhere str = new Model.StrWhere();
                str.IsWhereExist = true;
                str.strWhere = new StringBuilder().Append("Status=1");
                SetDropDownList.BindDropDownList(ddlBranch, new BLL.MS_BranchManage().GetDataSet("MS_BranchManage", str).Tables[0], "BranchName", "ID");

                StrWhere = "";
                AspNetPager1.CurrentPageIndex = 1;
                InitPage();
            }
        }

        public void InitPage()
        {
            PageAttribute page = new PageAttribute
            {
                TableName = "vw_BranchRechargeLog",
                Columns =
                    @"ID, BranchName, Status, EmployeeCount, ClientCount, RechargeMOney, Balance, AccountType, ChangeType, ChangeMethod, BeforeValue, ChangeValue, AfterValue, Descriptions, CreateTime,BranchId",
                StrOrder = " ID desc",
                WhereCondition = (" and Status=1" + StrWhere),
                PageIndex = AspNetPager1.CurrentPageIndex - 1,
                PageSize = AspNetPager1.PageSize
            };
            DataTable dt = new BLL.V8_OrderManage().GetListByPage(page);
            AspNetPager1.RecordCount = page.TotalRowCount.Value;
            repList.DataSource = dt;
            repList.DataBind();
        }

        protected void AspNetPager1_PageChanging(object src, Wuqi.Webdiyer.PageChangingEventArgs e)
        {
            AspNetPager1.CurrentPageIndex = e.NewPageIndex;
            InitPage();
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            StrWhere = "";
            if (ddlBranch.SelectedItem.Value.Trim() != "-1")
            {
                StrWhere += " and BranchId =" + ddlBranch.SelectedItem.Value.Trim();
            }
            if (ddlChangeType.SelectedItem.Value.Trim() != "-1")
            {
                StrWhere += " and ChangeType='" + ddlChangeType.SelectedItem.Value.Trim() + "'";
            }
            if (txtStartTime.Text.Trim() != "")
            {
                StrWhere += " and   CreateTime  >= '" + txtStartTime.Text.Trim() + "' ";
            }
            if (txtEndTime.Text.Trim() != "")
            {
                StrWhere += " and  CreateTime  <= '" + txtEndTime.Text.Trim() + "' ";
            }
            AspNetPager1.CurrentPageIndex = 1;
            InitPage();
        }

    }
}