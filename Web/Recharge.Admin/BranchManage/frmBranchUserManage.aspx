﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmBranchUserManage.aspx.cs" Inherits="Recharge.Admin.BranchManage.frmBranchUserManage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <!-- Bootstrap 3.3.4 -->
    <link href="/Content/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="/Content/dist/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="/Content/dist/css/ionicons.min.css" rel="stylesheet" type="text/css" />

    <!-- Theme style -->
    <link href="/Content/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins 
         folder instead of downloading all of them to reduce the load. -->
    <link href="/Content/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
    <script src="/Content/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="/Content/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

    <!-- SlimScroll -->
    <script src="/Content/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src='/Content/plugins/fastclick/fastclick.min.js'></script>
    <!-- AdminLTE App -->
    <script src="/Content/dist/js/app.min.js" type="text/javascript"></script>
    <script src="/Content/dist/js/demo.js" type="text/javascript"></script>

    <script src="/Content/layer/layer.js"></script>
    <script src="/Content/jquery.json.min.js"></script>
    <script src="/Content/JSDataFormat.js"></script>
    <script>

        function getQueryString(name) {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
            var r = window.location.search.substr(1).match(reg);
            if (r != null) return unescape(r[2]);
            return null;
        }
        var userModel = {};
        var userList = [];

        $(function () {
            InitUserList();
        });
        function InitUserList()
        {
            var innerhtml = "";
            $("#divHtml").html(innerhtml);
            $("#btnClear").attr("disabled", "disabled");
            $.ajax({
                type: "POST",
                url: "frmBranchUserManage.aspx/InitAllUserInfo",
                async: false,
                data: $.toJSON({ branchID: getQueryString("id") }),
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                beforeSenF: function () {
                    var index = layer.load(1, {
                        shade: [0.8, '#fff'], //0.1透明度的白色背景 
                    });
                },
                success: function (data) {

                    if (data.d.Data.length > 0) {
                        userList = data.d.Data;
                        $.each(data.d.Data, function (i, item) {
                            innerhtml += "<tr><td>"+item["ID"]+"</td><td>" + item["UserName"] + "</td><td>" + (parseInt(item["UserRole"]) == 1 ? "管理员" : "普通员工") + "</td><td>" + (parseInt(item["Status"]) == 1 ? "正常" : (parseInt(item["Status"]) == 2 ? "锁定" : (parseInt(item["Status"]) == 3 ? "未激活" : "离职"))) + "</td><td>" + format(item["LoginTime"], "yyyy-MM-dd HH:mm:ss") + "</td><td>" + item["LoginIP"] + "</td><td>" + item["LoginCount"] + "</td><td><input type='button' value='修改' class='btn btn-info' onclick='InitUserInfo(" + item["ID"] + ")'></td></tr>";
                        })
                        $("#divHtml").html(innerhtml);
                        layer.closeAll();
                    } else {
                        layer.closeAll();
                    }

                },
                error: function (err) {
                    layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                }
            });}
        function InitUserInfo(id) {
            $.each(userList, function (i, item) {
                if (item.ID == id) {
                    userModel = item;
                    $("#ddlUserRole option[value='" + userModel.UserRole + "']").attr("selected", "selected");
                    $("#ddlStatus option[value='" + userModel.Status + "']").attr("selected", "selected");
                    $("#txtUserName").val(userModel.UserName);
                    $("#txtPassword").val(userModel.Password);
                    $("#txtPassword").attr("readonly", "readonly");
                    $("#btnClear").removeAttr("disabled");
                }
            });
        }

        function ClearInfo() {
            userModel = { ID: 0 };
            $("#txtPassword").removeAttr("readonly");
        }

        function SaveInfo() {
            var userName = $("#txtUserName").val();
            var Password = $("#txtPassword").val();
            var Status = $("#ddlStatus").val();
            var userRole = $("#ddlUserRole").val();

            if (userName == "") {
                layer.alert('请输入用户名！', { icon: 2, title: '错误信息' }, function () { layer.closeAll(); $("#txtUserName").focus() });
            } else if (Password == "") {
                layer.alert('请输入密码！', { icon: 2, title: '错误信息' }, function () { layer.closeAll(); $("#txtSPassword").focus() });
            } else {
                userModel.UserName = userName;
                userModel.Password = Password;
                userModel.Status = Status;
                userModel.BranchID = getQueryString("id");
                userModel.UserRole = userRole;

                userModel.LoginTime = "";
                userModel.LastLoginTime = "";
                $.ajax({
                    type: "POST",
                    url: "frmBranchUserManage.aspx/SaveBranchUserInfo",
                    async: false,
                    data: $.toJSON({ model: userModel }),
                    contentType: "application/json;charset=utf-8",
                    dataType: "json",
                    beforeSenF: function () {
                        var index = layer.load(1, {
                            shade: [0.8, '#fff'], //0.1透明度的白色背景 
                        });
                    },
                    success: function (data) {

                        if (data.d.Data) {

                            layer.alert("操作成功！", { icon: 1 }, function () { layer.closeAll(); InitUserList(); });
                        } else {
                            layer.alert("操作失败！", { icon: 1 }, function () { layer.closeAll(); InitUserList(); });
                        }

                    },
                    error: function (err) {
                        layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                    }
                });

            }
        }
    </script>
</head>
<body class="skin-blue">
    <section class="content">
        <div class="row">
            <div class="box box-info">
                <div class="box-header">
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-5">

                            <div class="input-group">
                                <span class="input-group-addon">用&nbsp;&nbsp;户&nbsp;&nbsp;名</span>
                                <input type="text" class="form-control" placeholder="用户名" id="txtUserName" />
                            </div>
                            <br />
                            <div class="input-group">
                                <span class="input-group-addon">密&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;码</span>
                                <input type="text" class="form-control" placeholder="密码" id="txtPassword" />
                            </div>
                            <br />
                            <div class="input-group">
                                <span class="input-group-addon">用户角色</span>
                                <select id="ddlUserRole" class="form-control">
                                    <option value="1">管理员</option>
                                    <option value="2">普通员工</option>
                                </select>
                            </div>
                            <br />
                            <div class="input-group">
                                <span class="input-group-addon">当前状态</span>
                                <select id="ddlStatus" class="form-control">
                                    <option value="1">正常</option>
                                    <option value="2">锁定</option>
                                    <option value="3">未激活</option>
                                    <option value="4">离职</option>
                                </select>
                            </div>

                        </div>
                        <div class="col-sm-7">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>用户名</th>
                                        <th>角色</th>
                                        <th>当前状态</th>
                                        <th>登录时间</th>
                                        <th>登录IP</th>
                                        <th>登录次数</th>
                                        <th>操作</th>
                                    </tr>
                                </thead>
                                <tbody id="divHtml">
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <input type="button" class="btn btn-primary" value="保存" onclick="SaveInfo()" />
                    <input type="button" class="btn btn-danger" id="btnClear" value="清空" onclick="ClearInfo()" disabled="disabled" />
                </div>
            </div>
        </div>
    </section>


</body>
</html>

