﻿using CE.Utility;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Recharge.Admin.BranchManage
{
    public partial class frmUpdateBranch : BasePageClass
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static JsonResult<Model.MS_BranchManage> GetModel(long id)
        {
            return new JsonResult<Model.MS_BranchManage>(new BLL.MS_BranchManage().GetModel(id));
        }

        [WebMethod]
        public static JsonResult<bool> UpdateBranchInfo(Model.MS_BranchManage model)
        {
            model.CreateTime = DateTime.Now;
            AddOperationLog($"【修改代理商】代理商名{model.BranchName},{JsonConvert.SerializeObject(model)}");
            return new JsonResult<bool>(new BLL.MS_BranchManage().Update(model));
        }
    }
}