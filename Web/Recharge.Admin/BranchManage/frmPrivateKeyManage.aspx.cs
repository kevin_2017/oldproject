﻿using CE.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using CE.Utility.Text;

namespace Recharge.Admin.BranchManage
{
    public partial class frmPrivateKeyManage : BasePageClass
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        public static JsonResult<Model.MS_BranchManage> GetBranchInfo(long ID)
        {
            return new JsonResult<Model.MS_BranchManage>(new BLL.MS_BranchManage().GetModel(ID));
        }

        [WebMethod]
        public static JsonResult<bool> UpdatePrivateKey(long ID)
        {
            AddOperationLog($"【修改代理接口密钥】代理ID:{ID}");

            return new JsonResult<bool>(
                new BLL.MS_BranchManage().UpdateBranchPrivateKeyByBranchID(ID, StringUtil.GetRandomString(32)));
        }
    }
}