﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmPrivateKeyManage.aspx.cs" Inherits="Recharge.Admin.BranchManage.frmPrivateKeyManage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <!-- Bootstrap 3.3.4 -->
    <link href="/Content/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="/Content/dist/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="/Content/dist/css/ionicons.min.css" rel="stylesheet" type="text/css" />

    <!-- Theme style -->
    <link href="/Content/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins 
         folder instead of downloading all of them to reduce the load. -->
    <link href="/Content/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
    <script src="/Content/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="/Content/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

    <!-- SlimScroll -->
    <script src="/Content/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src='/Content/plugins/fastclick/fastclick.min.js'></script>
    <!-- AdminLTE App -->
    <script src="/Content/dist/js/app.min.js" type="text/javascript"></script>
    <script src="/Content/dist/js/demo.js" type="text/javascript"></script>

    <script src="/Content/layer/layer.js"></script>
    <script src="/Content/jquery.json.min.js"></script>
    <script>
        function getQueryString(name) {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
            var r = window.location.search.substr(1).match(reg);
            if (r != null) return unescape(r[2]);
            return null;
        }
        var brnachInfo = {};

        $(function () {
            $.ajax({
                type: "POST",
                url: "frmPrivateKeyManage.aspx/GetBranchInfo",
                async: false,
                data: $.toJSON({ ID: getQueryString("id") }),
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                beforeSenF: function () {
                    var index = layer.load(1, {
                        shade: [0.8, '#fff'], //0.1透明度的白色背景 
                    });
                },
                success: function (data) {
                    if (data.d.Data != null) {
                        brnachInfo = data.d.Data;
                        $("#txtUserName").val(brnachInfo.ID);
                        $("#txtBranchCode").val(brnachInfo.BranchCode);
                        $("#txtPassword").val(brnachInfo.PrivateKey);
                        layer.closeAll();
                    } else {
                        layer.closeAll();
                    }

                },
                error: function (err) {
                    layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                }
            });
        });
        function SaveInfo() {

            $.ajax({
                type: "POST",
                url: "frmPrivateKeyManage.aspx/UpdatePrivateKey",
                async: false,
                data: $.toJSON({ ID: getQueryString("id") }),
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                beforeSenF: function () {
                    var index = layer.load(1, {
                        shade: [0.8, '#fff'], //0.1透明度的白色背景 
                    });
                },
                success: function (data) {

                    if (data.d.Data) {
                        layer.alert("修改成功！", { icon: 1 }, function () { layer.closeAll(); });
                    } else {
                        layer.closeAll();
                    }

                },
                error: function (err) {
                    layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                }
            });
        }
    </script>
</head>
<body class="skin-blue">
    <section class="content">
        <div class="row">
            <div class="box box-info">
                <div class="box-header">
                    <span style="color: red">"代理编码"及"密钥信息"提供给下游，新接口已不使用代理ID</span>
                </div>
                <div class="box-body">
                    <div class="input-group">
                        <span class="input-group-addon">代理ID</span>
                        <input type="text" class="form-control" placeholder="代理ID" id="txtUserName" readonly />
                    </div>
                    <br /> 
                    <div class="input-group">
                        <span class="input-group-addon">代理编码</span>
                        <input type="text" class="form-control" placeholder="代理编码" id="txtBranchCode" readonly />
                    </div>
                    <br />
                    <div class="input-group">
                        <span class="input-group-addon">密&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;钥</span>
                        <input type="text" class="form-control" placeholder="密钥" id="txtPassword"  />
                    </div>

                </div>
                <div class="box-footer">
                    <input type="button" class="btn btn-primary" value="保存" onclick="SaveInfo()" />
                </div>
            </div>
        </div>
    </section>


</body>
</html>

