﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmUpdateBranch.aspx.cs" Inherits="Recharge.Admin.BranchManage.frmUpdateBranch" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <!-- Bootstrap 3.3.4 -->
    <link href="/Content/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="/Content/dist/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="/Content/dist/css/ionicons.min.css" rel="stylesheet" type="text/css" />

    <!-- Theme style -->
    <link href="/Content/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins 
         folder instead of downloading all of them to reduce the load. -->
    <link href="/Content/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
    <script src="/Content/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="/Content/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

    <!-- SlimScroll -->
    <script src="/Content/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src='/Content/plugins/fastclick/fastclick.min.js'></script>
    <!-- AdminLTE App -->
    <script src="/Content/dist/js/app.min.js" type="text/javascript"></script>
    <script src="/Content/dist/js/demo.js" type="text/javascript"></script>

    <script src="/Content/layer/layer.js"></script>
    <script src="/Content/jquery.json.min.js"></script>
    <script>
        var objJson = {

        };
        function getQueryString(name) {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
            var r = window.location.search.substr(1).match(reg);
            if (r != null) return unescape(r[2]);
            return null;
        };
        $(function() {
            $.ajax({
                type: "POST",
                url: "frmUpdateBranch.aspx/GetModel",
                async: false,
                data: $.toJSON({ id: getQueryString("id") }),
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                beforeSenF: function() {
                    var index = layer.load(1,
                        {
                            shade: [0.8, '#fff'], //0.1透明度的白色背景 
                        });
                },
                success: function(data) {
                    objJson = data.d.Data;

                    if (objJson != null) {
                        $("#txtBranchName").val(objJson.BranchName);
                        $("#txtStoreKeeper").val(objJson.StoreKeeper);
                        $("#txtPhones").val(objJson.Phones);
                        $("#txtAddress").val(objJson.Address);
                        $("#txtClientCount").val(objJson.ClientCount);
                        $("#txtEmployeeCount").val(objJson.EmployeeCount);
                        $("#selectSettlementMethod option[value='" + objJson.SettlementMethod + "']")
                            .attr("selected", "selected");
                        $("#selectStatus option[value='" + objJson.Status + "']").attr("selected", "selected");
                        $("#selectISSystem option[value='" + objJson.ISSystem + "']").attr("selected", "selected");
                        $("#selectRechargeType option[value='" + objJson.RechargeType + "']")
                            .attr("selected", "selected");
                        layer.closeAll();
                    } else {
                        layer.alert('对不起，获取代理商失败！', { icon: 2 }, function() { layer.closeAll(); });
                    }

                },
                error: function(err) {
                    layer.alert(err.responseText, { icon: 2 }, function() { layer.closeAll(); });
                }
            });
        });
        function SaveInfo() {
            var storeKeeper = $("#txtStoreKeeper").val();
            var phones = $("#txtPhones").val();
            var address = $("#txtAddress").val();
            var clientCount = $("#txtClientCount").val();
            var employeeCount = $("#txtEmployeeCount").val();
            var settlementMethod = $("#selectSettlementMethod").val();
            var isSystem = $("#selectISSystem").val();
            var status = $("#selectStatus").val();
            var rechargeType = $("#selectRechargeType").val();
            if (storeKeeper == "") {
                layer.alert('请输入联系人！', { icon: 2, title: '错误信息' }, function () { layer.closeAll(); $("#txtUserName").focus(); });
            } else if (phones == "") {
                layer.alert('请输入联系方式！', { icon: 2, title: '错误信息' }, function () { layer.closeAll(); $("#txtPassWord").focus(); });
            } else {
                objJson.StoreKeeper = storeKeeper;
                objJson.Phones = phones;
                objJson.Address = address;
                objJson.ClientCount = clientCount;
                objJson.EmployeeCount = employeeCount;
                objJson.SettlementMethod = settlementMethod;
                objJson.ISSystem = isSystem;
                objJson.Status = status; 
                objJson.RechargeType = rechargeType;
                objJson.CreateTime = "1900-01-01";
                $.ajax({
                    type: "POST",
                    url: "frmUpdateBranch.aspx/UpdateBranchInfo",
                    async: false,
                    data: $.toJSON({ model: objJson }),
                    contentType: "application/json;charset=utf-8",
                    dataType: "json",
                    beforeSenF: function () {
                        var index = layer.load(1, {
                            shade: [0.8, '#fff'], //0.1透明度的白色背景 
                        });
                    },
                    success: function (data) {

                        if (data.d.Data) {

                            layer.alert('修改代理商成功！', { icon: 1 }, function () { layer.closeAll(); });


                        } else {
                            layer.alert('对不起，修改代理商失败！', { icon: 2 }, function () { layer.closeAll(); });
                        }

                    },
                    error: function (err) {
                        layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                    }
                });

            }
        };
    </script>
</head>
<body class="skin-blue">
    <section class="content">
        <div class="row">
            <div class="box box-info">
                <div class="box-header">
                </div>
                <div class="box-body">
                    <div class="input-group">
                        <span class="input-group-addon">代理商名</span>
                        <input type="text" class="form-control" placeholder="代理商名" id="txtBranchName" readonly />
                    </div>
                    <br />
                    <div class="input-group">
                        <span class="input-group-addon">联&nbsp;&nbsp;系&nbsp;&nbsp;人</span>
                        <input type="text" class="form-control" placeholder="联系人" id="txtStoreKeeper" />
                    </div>
                    <br />
                    <div class="input-group">
                        <span class="input-group-addon">联系方式</span>
                        <input type="text" class="form-control" placeholder="联系方式" id="txtPhones" />
                    </div>
                    <br />
                    <div class="input-group">
                        <span class="input-group-addon">联系地址</span>
                        <input type="text" class="form-control" placeholder="联系地址" id="txtAddress" />
                    </div>
                    <br />
                    <div class="input-group">
                        <span class="input-group-addon">客户端数</span>
                        <input type="text" class="form-control" placeholder="客户端数" id="txtClientCount" />
                    </div>
                    <br />
                    <div class="input-group">
                        <span class="input-group-addon">员&nbsp;&nbsp;工&nbsp;&nbsp;数</span>
                        <input type="text" class="form-control" placeholder="员工数" id="txtEmployeeCount" />
                    </div>

                    <br />
                    <div class="input-group">
                        <span class="input-group-addon">结算方式</span>
                        <select id="selectSettlementMethod" class="form-control">
                            <option value="1">普通</option>
                            <option value="2">按日</option>
                            <option value="3">签约</option>
                            <option value="2">年租</option>
                        </select>
                    </div>
                    <br />
                    <div class="input-group">
                        <span class="input-group-addon">代理类型</span>
                        <select id="selectISSystem" class="form-control">
                            <option value="0">普通代理商</option>
                            <option value="1">系统代理商</option>
                        </select>
                    </div>
                    <br />
                    <div class="input-group">
                        <span class="input-group-addon">当前状态</span>
                        <select id="selectStatus" class="form-control">
                            <option value="3">停店</option>
                            <option value="2">暂停</option>
                            <option value="1">正常</option>
                        </select>
                    </div>
                    <br />
                    <div class="input-group">
                        <span class="input-group-addon">充值方式</span>
                        <select id="selectRechargeType" class="form-control">
                            <option value="0">系统轮循</option>
                            <option value="1">指定供应商</option>
                             
                        </select>
                    </div>
                </div>
                <div class="box-footer">
                    <input type="button" class="btn btn-primary" value="保存" onclick="SaveInfo()" />
                </div>
            </div>
        </div>
    </section>


</body>
</html>

