﻿using CE.Utility;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Recharge.Admin.BranchManage
{
    public partial class frmBranchUserManage : BasePageClass
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        public static JsonResult<List<Model.MS_UserInfo>> InitAllUserInfo(long branchID)
        {
            Model.StrWhere str = new Model.StrWhere();
            str.IsWhereExist = true;
            str.strWhere = new System.Text.StringBuilder("   isdelete=0 and  BranchID=" + branchID);
            return new JsonResult<List<Model.MS_UserInfo>>(new BLL.MS_UserInfo().GetModelList(str));
        }

        [WebMethod]
        public static JsonResult<bool> SaveBranchUserInfo(Model.MS_UserInfo model)
        {
            if (model.ID == 0)
            {
                AddOperationLog($"【添加代理商用户】代理商用户:{model.UserName},{JsonConvert.SerializeObject(model)}");
                return new JsonResult<bool>(new BLL.MS_UserInfo().Add(model) > 0);
            }
            else
            {
                AddOperationLog($"【修改代理商用户】代理商用户:{model.UserName},{JsonConvert.SerializeObject(model)}");
                return new JsonResult<bool>(new BLL.MS_UserInfo().Update(model));
            }
        }
    }
}