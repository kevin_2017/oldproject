﻿using CE.Utility;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Model;

namespace Recharge.Admin.BranchManage
{
    public partial class frmAddBranch : BasePageClass
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static JsonResult<bool> AddBranchInfo(Model.MS_BranchManage model)
        {
            Model.StrWhere str = new StrWhere() { IsWhereExist = true, strWhere = new StringBuilder($" branchCode like '%{model.BranchCode.Trim()}%'") };

            var oldBranch = new BLL.MS_BranchManage().GetModel(str);
            if (oldBranch != null)
                return new JsonResult<bool>(false, "该代理编码已存在");
            AddOperationLog($"【添加代理商】 代理商名：{model.BranchName},{JsonConvert.SerializeObject(model)}");
            return new JsonResult<bool>(new BLL.MS_BranchManage().Add(model) > 0);
        }
    }
}