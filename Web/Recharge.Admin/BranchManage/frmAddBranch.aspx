﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="frmAddBranch.aspx.cs" Inherits="Recharge.Admin.BranchManage.frmAddBranch" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        var objJson = {

        };
        function SaveInfo() {
            var storeKeeper = $("#txtStoreKeeper").val();
            var phones = $("#txtPhones").val();
            var address = $("#txtAddress").val();
            var clientCount = $("#txtClientCount").val();
            var employeeCount = $("#txtEmployeeCount").val();
            var settlementMethod = $("#selectSettlementMethod").val();
            var isSystem = $("#selectISSystem").val();
            var status = $("#selectStatus").val();
            var rechargeType = $("#selectRechargeType").val();

          if ($("#txtBranchCode").val() == "") {
                layer.alert('请输入代理编码！', { icon: 2, title: '错误信息' }, function () { layer.closeAll(); $("#txtBranchCode").focus(); });
            } else   if ($("#txtBranchName").val() == "") {
                layer.alert('请输入代理商名称！', { icon: 2, title: '错误信息' }, function () { layer.closeAll(); $("#txtBranchName").focus(); });
            } else if (storeKeeper == "") {
                layer.alert('请输入联系人！', { icon: 2, title: '错误信息' }, function () { layer.closeAll(); $("#txtUserName").focus(); });
            } else if (phones == "") {
                layer.alert('请输入联系方式！', { icon: 2, title: '错误信息' }, function () { layer.closeAll(); $("#txtPassWord").focus(); });
          } else {
                objJson.BranchCode = $("#txtBranchCode").val();
                objJson.BranchName = $("#txtBranchName").val();
                objJson.StoreKeeper = storeKeeper;
                objJson.Phones = phones;
                objJson.Address = address;
                objJson.ClientCount = parseInt(clientCount);
                objJson.EmployeeCount = parseInt(employeeCount);
                objJson.SettlementMethod = parseInt(settlementMethod);
                objJson.ISSystem = parseInt(isSystem);
                objJson.Status = parseInt(status);
                objJson.Level = 1;
                objJson.ParentID = 0;
                objJson.RechargeType = rechargeType;
                $.ajax({
                    type: "POST",
                    url: "frmAddBranch.aspx/AddBranchInfo",
                    async: false,
                    data: $.toJSON({ model: objJson }),
                    contentType: "application/json;charset=utf-8",
                    dataType: "json",
                    beforeSenF: function () {
                        var index = layer.load(1, {
                            shade: [0.8, '#fff'], //0.1透明度的白色背景 
                        });
                    },
                    success: function (data) {

                        if (data.d.Data) {

                            layer.alert('增加代理商成功！', { icon: 1 }, function () { layer.closeAll(); });


                        } else {
                            layer.alert('对不起，增加代理商失败！', { icon: 2 }, function () { layer.closeAll(); });
                        }

                    },
                    error: function (err) {
                        layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                    }
                });

            }
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="box box-info">
        <div class="box-header">
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-sm-6"> <div class="input-group">
                        <span class="input-group-addon">代理编码</span>
                        <input type="text" class="form-control" placeholder="代理编码" id="txtBranchCode" />
                    </div>
                    <br />
                    <div class="input-group">
                        <span class="input-group-addon">代理商名</span>
                        <input type="text" class="form-control" placeholder="代理商名" id="txtBranchName" />
                    </div>
                    <br />
                    <div class="input-group">
                        <span class="input-group-addon">联&nbsp;&nbsp;系&nbsp;&nbsp;人</span>
                        <input type="text" class="form-control" placeholder="联系人" id="txtStoreKeeper" />
                    </div>
                    <br />
                    <div class="input-group">
                        <span class="input-group-addon">联系方式</span>
                        <input type="text" class="form-control" placeholder="联系方式" id="txtPhones" />
                    </div>
                    <br />
                    <div class="input-group">
                        <span class="input-group-addon">联系地址</span>
                        <input type="text" class="form-control" placeholder="联系地址" id="txtAddress" />
                    </div>
                    <br />
                    <div class="input-group">
                        <span class="input-group-addon">客户端数</span>
                        <input type="text" class="form-control" placeholder="客户端数" id="txtClientCount" value="0" />
                    </div>
                    <br />
                    <div class="input-group">
                        <span class="input-group-addon">员&nbsp;&nbsp;工&nbsp;&nbsp;数</span>
                        <input type="text" class="form-control" placeholder="员工数" id="txtEmployeeCount" value="0" />
                    </div>

                    <br />
                    <div class="input-group">
                        <span class="input-group-addon">结算方式</span>
                        <select id="selectSettlementMethod" class="form-control">
                            <option value="1">普通</option>
                            <option value="2">按日</option>
                            <option value="3">签约</option>
                            <option value="2">年租</option>
                        </select>
                    </div>
                    <br />
                    <div class="input-group">
                        <span class="input-group-addon">代理类型</span>
                        <select id="selectISSystem" class="form-control">
                            <option value="0">普通代理商</option>
                            <option value="1">系统代理商</option>
                        </select>
                    </div>
                    <br />
                    <div class="input-group">
                        <span class="input-group-addon">当前状态</span>
                        <select id="selectStatus" class="form-control">
                            <option value="3">停店</option>
                            <option value="2">暂停</option>
                            <option value="1">正常</option>
                        </select>
                    </div>
                    <br />
                    <div class="input-group">
                        <span class="input-group-addon">充值方式</span>
                        <select id="selectRechargeType" class="form-control">
                            <option value="0">系统轮循</option>
                            <option value="1">指定供应商</option>

                        </select>
                    </div>  
                </div>
                
            </div>
        </div>
        <div class="box-footer">
            <input type="button" class="btn btn-primary" value="保存" onclick="SaveInfo()" />
        </div>
    </div>
</asp:Content>
