﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Model;

namespace Recharge.Admin.BranchManage
{
    public partial class frmBranch_SuppManage : BasePageClass
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitPage();
            }
        }

        public void InitPage()
        {
            var allSupp = new BLL.SUP_SupplierManage().GetDataSet(new StrWhere() { IsWhereExist = false }).Tables[0];
            var branchSupp =
                new BLL.MS_Branch_Supplier().GetModelList(new StrWhere()
                {
                    IsWhereExist = true,
                    strWhere = new StringBuilder($" BranchId={Request.QueryString["id"]}")
                });
            var innerHtml = "";
            foreach (DataRow dr in allSupp.Rows)
            {
                var model = branchSupp.FirstOrDefault(x => x.SupplierID == Convert.ToInt64(dr["ID"]));
                if (model != null)
                {
                    innerHtml += "<tr><td><input type=\"checkbox\" name=\"linkList\" id=\"" + dr["ID"] +
                                 "\" checked=\"checked\" class=\"checker\"  onclick=\"operateOneMenu(this)\"  value=\"" +
                                 (dr["ID"] + "_" + dr["TrueName"]) + "\" />" + dr["ID"] + "</td><td>" +
                                 dr["TrueName"].ToString() + "</td><td>" + (Convert.ToInt32(dr["Status"].ToString()) == 1001 ? "开通" : (Convert.ToInt32(dr["Status"].ToString()) == 1002 ? "暂停" : (Convert.ToInt32(dr["Status"].ToString()) == 1003 ? "停用" : (Convert.ToInt32(dr["Status"].ToString()) == 1004 ? "暂停获取" : "未知")))) + "</td><td><input type=\"text\" id=\"txt" + dr["ID"] + "\" class=\"form-control\" value=\"" + (model?.Sort ?? 0) + "\" name=\"txtSort\"/></td></tr>";
                }
                else
                {
                    innerHtml += "<tr><td><input type=\"checkbox\" name=\"linkList\" id=\"" + dr["ID"] +
                                 "\"  class=\"checker\"  onclick=\"operateOneMenu(this)\"  value=\"" +
                                 (dr["ID"] + "_" + dr["TrueName"]) + "\" />" + dr["ID"] + "</td><td>" +
                                 dr["TrueName"].ToString() + "</td><td>" + (Convert.ToInt32(dr["Status"].ToString()) == 1001 ? "开通" : (Convert.ToInt32(dr["Status"].ToString()) == 1002 ? "暂停" : (Convert.ToInt32(dr["Status"].ToString()) == 1003 ? "停用" : (Convert.ToInt32(dr["Status"].ToString()) == 1004 ? "暂停获取" : "未知")))) + "</td><td><input type=\"text\" id=\"txt" + dr["ID"] + "\" class=\"form-control\" name=\"txtSort\"/></td></tr>";
                }
            }

            ltSuppInfo.Text = innerHtml;

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string[] provList = hfSuppId.Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            string[] sortList = hfSuppSort.Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            if (provList.Length == 0)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "", "  layer.alert('请选择接口供应商！',{ icon: 2 });", true);
            }
            var branchModel = new BLL.MS_BranchManage().GetModel(Convert.ToInt64(Request.QueryString["id"]));

            new BLL.MS_Branch_Supplier().Delete(new StrWhere()
            {
                IsWhereExist = true,
                strWhere = new StringBuilder($" BranchID={branchModel.ID} ")
            });
            foreach (var str in provList)
            {
                string[] provs = str.Split(new string[] { "_" }, StringSplitOptions.RemoveEmptyEntries);
                int sort = 0;
                foreach (var sortInfo in sortList)
                {
                    if (sortInfo.Contains(provs[0]))
                    {
                        var sortInfoArr = sortInfo.Split(new string[] { "_" }, StringSplitOptions.RemoveEmptyEntries);
                        if (sortInfoArr.Length == 2)
                            sort = Convert.ToInt32(sortInfo.Split(new string[] { "_" }, StringSplitOptions.RemoveEmptyEntries)[1]);
                    }
                }
                Model.MS_Branch_Supplier model = new MS_Branch_Supplier()
                {
                    BranchID = branchModel.ID,
                    BranchName = branchModel.BranchName,
                    SupplierID = Convert.ToInt64(provs[0]),
                    SupplierName = provs[1],
                    Sort = sort
                };
                new BLL.MS_Branch_Supplier().Add(model);
            }

            InitPage();
            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "", "  layer.alert('分配成功！',{icon:1});", true);
        }
    }
}