﻿using CE.Utility;
using System;
using System.Web;
using System.Web.Services;

namespace Recharge.Admin
{
    public partial class frmLogin : System.Web.UI.Page, IHttpHandler
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static JsonResult<bool> UserLogin(string userName, string password, string validateCode)
        {
            var code = HttpContext.Current.Session["validateCode"].ToString();
            if (code.ToLower().Equals(validateCode.ToLower()))
            {
                int errorcount = 0;

                Model.SystemUserInfo logininfo = new Model.SystemUserInfo();

                logininfo = new BLL.SYS_UserInfo().GetSystemUserInfo(userName, password, out errorcount);
                HttpContext.Current.Session["SystemUserLoginInfo"] = logininfo;
                var result = logininfo != null;
                return new JsonResult<bool>(result, result ? "登录成功" : "登录失败");
            }
            else
            {
                return new JsonResult<bool>(false, "验证码错误");
            }
        }
       

    }
}