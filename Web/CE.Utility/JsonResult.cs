﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace CE.Utility
{
    /// <summary>
    /// 通用的Json返回结果值对象
    /// </summary> 
    public class JsonResult<T>
    {
        /// <summary>
        /// 构造方法
        /// </summary>
        public JsonResult()
        {
            Success = true;
        }

        /// <summary>
        /// 构造方法
        /// </summary>
        /// <param name="result">结果值</param>
        public JsonResult(T result)
        {
            Success = true;
            Message = "操作成功";
            Rows = new T[1];
            Rows[0] = result;
            Data = result;
        }

        /// <summary>
        /// 构造方法
        /// </summary>
        /// <param name="result">结果值</param>
        /// <param name="successMsg"></param>
        public JsonResult(T result,string successMsg)
        {
            Success = true;
            Message = string.IsNullOrEmpty(successMsg) ? "操作成功" : successMsg;
            Rows = new T[1];
            Rows[0] = result;
            Data = result;
        }

        /// <summary>
        /// 构造方法
        /// </summary>
        /// <param name="result">结果值</param>
        /// <param name="successMsg"></param>
        public JsonResult(T result,IEnumerable<string> successMsg)
        {
            Success = true;
            var msgList = successMsg as IList<string> ?? successMsg.ToList();
            Message = string.IsNullOrEmpty(string.Join("<br/>", msgList.ToArray())) ?
                "操作成功" : string.Join("<br/>", msgList.ToArray());
            Rows = new T[1];
            Rows[0] = result;
            Data = result;
        }

        /// <summary>
        /// 构造方法
        /// </summary>
        /// <param name="result">结果值</param>
        public JsonResult(IEnumerable<T> result)
        {
            Success = true;
            Message = "操作成功";
            var list = result as IList<T> ?? result.ToList();
            Rows = list.ToArray();
            Total = list.Count;
        }

        /// <summary>
        /// 构造方法
        /// </summary>
        /// <param name="result">结果值</param>
        /// <param name="successMsg"></param>
        public JsonResult(IEnumerable<T> result,string successMsg)
        {
            Success = true;
            Message = string.IsNullOrEmpty(successMsg) ? "操作成功" : successMsg;
            var list = result as IList<T> ?? result.ToList();
            Rows = list.ToArray();
            Total = list.Count;
        }

        /// <summary>
        /// 构造方法
        /// </summary>
        /// <param name="result">结果值</param>
        /// <param name="successMsg"></param>
        public JsonResult(IEnumerable<T> result,IEnumerable<string> successMsg)
        {
            Success = true;
            var msgList = successMsg as IList<string> ?? successMsg.ToList();
            Message = string.IsNullOrEmpty(string.Join("<br/>", msgList.ToArray())) ?
                "操作成功" : string.Join("<br/>", msgList.ToArray());
            var list = result as IList<T> ?? result.ToList();
            Rows = list.ToArray();
            Total = list.Count;
        }


        /// <summary>
        /// 构造方法
        /// </summary>
        /// <param name="result">结果值</param>
        /// <param name="totalCount">全部结果数</param>
        public JsonResult(IEnumerable<T> result, int totalCount)
        {
            Success = true;
            Message = "操作成功";
            Rows = result.ToArray();
            Total = totalCount;
        }

        /// <summary>
        /// 构造方法
        /// </summary>
        /// <param name="result">结果值</param>
        /// <param name="totalCount">全部结果数</param>
        /// <param name="successMsg"></param>
        public JsonResult(IEnumerable<T> result, int totalCount,string successMsg)
        {
            Success = true;
            Message = string.IsNullOrEmpty(successMsg) ? "操作成功" : successMsg;
            Rows = result.ToArray();
            Total = totalCount;
        }

        /// <summary>
        /// 构造方法
        /// </summary>
        /// <param name="result">结果值</param>
        /// <param name="totalCount">全部结果数</param>
        /// <param name="successMsg"></param>
        public JsonResult(IEnumerable<T> result, int totalCount,IEnumerable<string> successMsg)
        {
            Success = true;
            var msgList = successMsg as IList<string> ?? successMsg.ToList();
            Message = string.IsNullOrEmpty(string.Join("<br/>", msgList.ToArray())) ?
                "操作成功" : string.Join("<br/>", msgList.ToArray());
            Rows = result.ToArray();
            Total = totalCount;
        }

        /// <summary>
        /// 构造方法
        /// </summary>
        /// <param name="error">例外对象</param>
        public JsonResult(Exception error)
        {
            Success = false;
            Message = "操作失败";
            ErrorType = error.GetType().FullName;
            ErrorMessage = error.Message;
            ErrorStackTrace = error.StackTrace;
        }

        /// <summary>
        /// 构造方法
        /// </summary>
        /// <param name="errType">错误类型</param>
        /// <param name="error">例外对象</param>
        public JsonResult(string errType,Exception error)
        {
            Success = false;
            Message = "操作失败";
            ErrorType = errType;
            ErrorMessage = error.Message;
            ErrorStackTrace = error.StackTrace;
        }

        public override string ToString()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this);
        }

        /// <summary>
        /// 执行结果是否成功
        /// </summary> 
        public bool Success
        {
            get;
            private set;
        }

        /// <summary>
        /// 行数
        /// </summary> 
        public int Total
        {
            get;
            private set;
        }

        /// <summary>
        /// 数据集
        /// </summary> 
        public T[] Rows
        {
            get;
            private set;
        }

        /// <summary>
        /// 数据集
        /// </summary> 
        public T Data
        {
            get;
            private set;
        }

        /// <summary>
        /// 错误类型
        /// </summary> 
        public string ErrorType
        {
            get;
            private set;
        }

        /// <summary>
        /// 错误信息
        /// </summary> 
        public string ErrorMessage { get; set; }

        /// <summary>
        /// 执行堆栈
        /// </summary> 
        public string ErrorStackTrace { get; set; }

        
        /// <summary>
        /// 成功信息
        /// </summary> 
        public string Message { get; set; }
    }
}