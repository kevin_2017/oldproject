﻿using System;
using System.Collections.Generic;
using System.Messaging;

namespace CE.Utility
{
    /// <summary>
    /// 消息队列帮助类
    /// </summary>
    public class MsmqHelper
    {
        #region 静态属性
        /// <summary>
        /// 单例对象
        /// </summary>
        private static MessageQueue instance;  //唯一单例
        private static Dictionary<string, MessageQueue> _instance = new Dictionary<string, MessageQueue>();   //一key一单例

        private static string _queueName = ""; //消息队列名称
        /// <summary>
        /// 进程辅助对象(只读, 用于锁)
        /// </summary>
        private static readonly object _object = new object();
        #endregion

        #region 实例属性
        private string Path { get; set; }
        #endregion

        #region 构造函数, 私有
        /// <summary>
        /// 构造方法, 私有,不能通过New实例化
        /// </summary>
        /// <param name="key">关键字</param>
        public MsmqHelper(string key)
        {
            _queueName = key;

            Path = string.Concat(@".\private$\", key);  //$@".\private$\{key}"
        }
        #endregion

        #region 静态方法
        /// <summary>
        /// 获取单例
        /// 唯一全局访问点(加锁 lock)
        /// </summary>
        /// <param name="key">关键字</param>
        /// <returns></returns>
        public static MessageQueue GetInstance(string key)
        {
            //先判断实例是否存在, 不存在再加锁处理
            if (!_instance.ContainsKey(key))
            {
                //在同一时刻加了锁的那部分程序只有一个线程可以进入
                lock (_object)
                {
                    //若实例不存在, 则New一个新实例, 否则返回已有实例
                    if (!_instance.ContainsKey(key))
                        _instance[key] = new MessageQueue($@".\private$\{key}");
                }
            }
            return _instance[key];
        }
        #endregion

        #region 实例方法
        /// <summary>
        /// 发送
        /// </summary>
        /// <typeparam name="T">泛型</typeparam>
        /// <param name="message">泛型对象</param>
        public void Send<T>(T message)
        {
            try
            {
                //创建队列
                // if (!MessageQueue.Exists(path))
                //  MessageQueue.Create(path); 
                var msg = new Message
                {
                    Formatter = new XmlMessageFormatter(new Type[] { typeof(T) }),
                    Body = message
                };
                //发送消息到队列中
                MessageQueueTransaction trans = null;
                if (GetInstance(_queueName).Transactional)
                    trans = new MessageQueueTransaction();
                trans?.Begin();
                GetInstance(_queueName).Send(msg);
                trans?.Commit();
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// 接收
        /// </summary>
        /// <typeparam name="T">泛型</typeparam>
        /// <returns>泛型</returns>
        public T Recieve<T>()
        {
            Message msg = new Message();
            try
            {
                MessageQueueTransaction trans = null;
                if (GetInstance(_queueName).Transactional)
                    trans = new MessageQueueTransaction();
                trans?.Begin();
                msg = GetInstance(_queueName).Receive(new TimeSpan(0, 0, 0, 3));
                trans?.Commit();
                msg.Formatter = new XmlMessageFormatter(new Type[] { typeof(T) });
            }
            catch (Exception ex)
            {
                throw ex;
            }

            if (msg == null)
                return default(T);
            else
                return (T)msg.Body;
        }
        #endregion
    }
}
