﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace CE.Utility.Text
{
    public static class StringUtil
    {
        public static string ConvertStringToHex(string asciiString)
        {
            string hex = "";
            foreach (char c in asciiString)
            {
                int value = Convert.ToInt32(c);
                // Convert the decimal value to a hexadecimal value in string form. 
                hex += String.Format("{0:X2}", value);
            }
            return hex;
        }
        #region 验证IP格式
        public static bool IsIP(string ip)
        {
            return Regex.IsMatch(ip, @"^((25[0-5]|2[0-4]\d|[01]?\d\d?)($|(?!\.$)\.)){4}$");
        }
        #endregion

        #region 验证邮箱格式
        public static bool IsEmail(string email)
        {
            var r = new Regex("^\\s*([A-Za-z0-9_-]+(\\.\\w+)*@(\\w+\\.)+\\w{2,5})\\s*$");
            return r.IsMatch(email);
        }
        #endregion

        #region 验证手机号码格式
        public static bool IsPhoneNum(string handset)
        {
            //var telRegex = new Regex("/^(0|86|17951)?(13[0-9]|15[012356789]|17[678]|18[0-9]|14[57])[0-9]{8}$/");
            return Regex.IsMatch(handset, @"^(0|86|17951)?(13[0-9]|15[012356789]|17[678]|18[0-9]|14[57])[0-9]{8}");
        }
        #endregion

        public static string ConvertHexToString(string HexValue)
        {
            string StrValue = "";
            HexValue = HexValue.Replace(" ", "");
            if (HexValue.Length <= 0) return "";
            byte[] vBytes = new byte[HexValue.Length / 2];
            for (int i = 0; i < HexValue.Length; i += 2)
                if (!byte.TryParse(HexValue.Substring(i, 2), NumberStyles.HexNumber, null, out vBytes[i / 2]))
                    vBytes[i / 2] = 0;
            StrValue = ASCIIEncoding.Default.GetString(vBytes);
            return StrValue;
        }

        /// <summary>
        /// 将任意类型转换为整型，当无法转换时返回 0 。
        /// </summary>
        /// <param name="id">object 类型。</param>
        /// <returns>返回转换结果。</returns>
        public static int GetInt(object id)
        {
            int newid;
            int.TryParse(GetString(id).Trim(), out newid);
            return newid;
        }

        /// <summary>
        /// 将任意类型转换为整型，当无法转换时返回 0 。
        /// </summary>
        /// <param name="id">object 类型。</param>
        /// <returns>返回转换结果。</returns>
        public static bool GetBool(object id)
        {
            bool newid;
            bool.TryParse(GetString(id).Trim(), out newid);
            return newid;
        }

        /// <summary>
        /// 将任意类型转换为整型，当无法转换时返回 0 。
        /// </summary>
        /// <param name="id">object 类型。</param>
        /// <returns>返回转换结果。</returns>
        public static decimal GetDecimal(object id)
        {
            decimal newid;
            decimal.TryParse(GetString(id).Trim(), out newid);
            return newid;
        }
        /// <summary>
        /// 将任意类型转换为字符串型，当无法转换时返回空字符串。
        /// </summary>
        /// <param name="str">object 类型。</param>
        /// <returns>返回转换结果。</returns>
        public static string GetString(object str)
        {
            if (str == null)
                return string.Empty;
            else
                return str.ToString();
        }

        #region 静态方法

        public static string OrderNo()
        {
            return $"{Timestamp()}-{GetRandomString(10)}";
        }

        public static string GetRandomString(int length)
        {
            StringBuilder randomStr = new StringBuilder();
            Random rd = new Random();
            string str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            for (int i = 0; i < length; i++)
            {
                randomStr.Append(str[rd.Next(str.Length)]);
            }
            return randomStr.ToString();
        }
        public static string GetRandom(int length)
        {
            StringBuilder randomStr = new StringBuilder();
            Random rd = new Random();
            string str = "0123456789";
            for (int i = 0; i < length; i++)
            {
                randomStr.Append(str[rd.Next(str.Length)]);
            }
            return randomStr.ToString();
        }

        public static string ToUrlEncode(this string input)
        {
            return HttpUtility.UrlEncode(input, Encoding.UTF8);
        }

        public static string ToUrlDecode(this string input)
        {
            return HttpUtility.UrlDecode(input, Encoding.UTF8);
        }

        /// <summary>
        /// 对字符串进行base64编码
        /// </summary>
        /// <param name="input">字符串</param>
        /// <returns>base64编码串</returns>
        public static string EncodeBase64String(this string input)
        {
            byte[] encbuff = System.Text.Encoding.UTF8.GetBytes(input);
            return Convert.ToBase64String(encbuff);
        }

        /// <summary>
        /// 对字符串进行反编码
        /// </summary>
        /// <param name="input">base64编码串</param>
        /// <returns>字符串</returns>
        public static string DecodeBase64String(this string input)
        {
            string str = input;
            int mod = input.Length % 4;
            if (mod > 0)
            {
                str = input.PadRight(input.Length + 4 - mod, '=');
            }
            byte[] decbuff = Convert.FromBase64String(str);
            return System.Text.Encoding.UTF8.GetString(decbuff);
        }

        /// <summary>
        /// 替换字符串(忽略大小写)
        /// </summary>
        /// <param name="input">要进行替换的内容</param>
        /// <param name="oldValue">旧字符串</param>
        /// <param name="newValue">新字符串</param>
        /// <returns>替换后的字符串</returns>
        public static string CaseInsensitiveReplace(this string input, string oldValue, string newValue)
        {
            Regex regEx = new Regex(oldValue, RegexOptions.IgnoreCase | RegexOptions.Multiline);
            return regEx.Replace(input, newValue);
        }

        /// <summary>
        /// 替换首次出现的字符串
        /// </summary>
        /// <param name="input">要进行替换的内容</param>
        /// <param name="oldValue">旧字符串</param>
        /// <param name="newValue">新字符串</param>
        /// <returns>替换后的字符串</returns>
        public static string ReplaceFirst(this string input, string oldValue, string newValue)
        {
            Regex regEx = new Regex(oldValue, RegexOptions.Multiline);
            return regEx.Replace(input, newValue, 1);
        }

        /// <summary>
        /// 替换最后一次出现的字符串
        /// </summary>
        /// <param name="input">要进行替换的内容</param>
        /// <param name="oldValue">旧字符串</param>
        /// <param name="newValue">新字符串</param>
        /// <returns>替换后的字符串</returns>
        public static string ReplaceLast(this string input, string oldValue, string newValue)
        {
            int index = input.LastIndexOf(oldValue);
            if (index < 0)
            {
                return input;
            }
            else
            {
                StringBuilder sb = new StringBuilder(input.Length - oldValue.Length + newValue.Length);
                sb.Append(input.Substring(0, index));
                sb.Append(newValue);
                sb.Append(input.Substring(index + oldValue.Length, input.Length - index - oldValue.Length));
                return sb.ToString();
            }
        }

        /// <summary>
        /// 根据词组过虑字符串(忽略大小写)
        /// </summary>
        /// <param name="input">要进行过虑的内容</param>
        /// <param name="filterWords">要过虑的词组</param>
        /// <returns>过虑后的字符串</returns>
        public static string FilterWords(this string input, params string[] filterWords)
        {
            return FilterWords(input, char.MinValue, filterWords);
        }

        /// <summary>
        /// 根据词组过虑字符串(忽略大小写)
        /// </summary>
        /// <param name="input">要进行过虑的内容</param>
        /// <param name="mask">字符掩码</param>
        /// <param name="filterWords">要过虑的词组</param>
        /// <returns>过虑后的字符串</returns>
        public static string FilterWords(this string input, char mask, params string[] filterWords)
        {
            string stringMask = mask == char.MinValue ? string.Empty : mask.ToString();
            string totalMask = stringMask;

            foreach (string s in filterWords)
            {
                Regex regEx = new Regex(s, RegexOptions.IgnoreCase | RegexOptions.Multiline);

                if (stringMask.Length > 0)
                {
                    for (int i = 1; i < s.Length; i++)
                        totalMask += stringMask;
                }

                input = regEx.Replace(input, totalMask);

                totalMask = stringMask;
            }

            return input;
        }

        /// <summary>
        /// MD5加密
        /// </summary>
        /// <param name="input">要进行加密的字符串</param>
        /// <returns>加密后的字符串</returns>
        public static string ToMD5(this string input)
        {
            MD5 md5Hasher = MD5.Create();
            byte[] data = md5Hasher.ComputeHash(Encoding.UTF8.GetBytes(input));
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }

        /// <summary>
        /// MD5加密
        /// </summary>
        /// <param name="input">要进行加密的字符串</param>
        /// <returns>加密后的字符串</returns>
        public static string ToPayMD5(this string input)
        {
            MD5 md5Hasher = MD5.Create();
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));
            return Convert.ToBase64String(data).ToLower();
        }

        /// <summary>
        /// 字符串反转
        /// </summary>
        /// <param name="input">要进行反转的字符串</param>
        /// <returns>反转后的字符串</returns>
        public static string Reverse(this string input)
        {
            char[] reverse = new char[input.Length];
            for (int i = 0, k = input.Length - 1; i < input.Length; i++, k--)
            {
                if (char.IsSurrogate(input[k]))
                {
                    reverse[i + 1] = input[k--];
                    reverse[i++] = input[k];
                }
                else
                {
                    reverse[i] = input[k];
                }
            }
            return new System.String(reverse);
        }

        /// <summary>
        /// 转成首字母大字形式
        /// </summary>
        /// <param name="input">要进行转换的字符串</param>
        /// <returns>转换后的字符串</returns>
        public static string SentenceCase(this string input)
        {
            if (input.Length < 1)
                return input;

            string sentence = input.ToLower();
            return sentence[0].ToString().ToUpper() + sentence.Substring(1);
        }

        /// <summary>
        /// 空格转换成&nbsp;
        /// </summary>
        /// <param name="input">要进行转换的字符串</param>
        /// <returns>转换后的字符串</returns>
        public static string SpaceToNbsp(this string input)
        {
            string space = "&nbsp;";
            return input.Replace(" ", space);
        }

        /// <summary>
        /// 去除"<" 和 ">" 符号之间的内容
        /// </summary>
        /// <param name="input">要进行处理的字符串</param>
        /// <returns>处理后的字符串</returns>
        public static string StripTags(this string input)
        {
            Regex stripTags = new Regex("<(.|\n)+?>");
            return stripTags.Replace(input, "");
        }
        /// <summary>
        /// 去除字符串中的HTML代码
        /// </summary>
        /// <param name="Htmlstring"></param>
        /// <returns></returns>
        public static string ToDelHTMLString(this string Htmlstring)
        {
            //删除脚本
            Htmlstring = System.Text.RegularExpressions.Regex.Replace(Htmlstring, @"<script[^>]*?>.*?</script>", "", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            //删除HTML
            Htmlstring = System.Text.RegularExpressions.Regex.Replace(Htmlstring, @"<(.[^>]*)>", "", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            Htmlstring = System.Text.RegularExpressions.Regex.Replace(Htmlstring, @"([\r\n])[\s]+", "", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            Htmlstring = System.Text.RegularExpressions.Regex.Replace(Htmlstring, @"-->", "", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            Htmlstring = System.Text.RegularExpressions.Regex.Replace(Htmlstring, @"<!--.*", "", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            Htmlstring = System.Text.RegularExpressions.Regex.Replace(Htmlstring, @"&(quot|#34);", "\"", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            Htmlstring = System.Text.RegularExpressions.Regex.Replace(Htmlstring, @"&(amp|#38);", "&", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            Htmlstring = System.Text.RegularExpressions.Regex.Replace(Htmlstring, @"&(lt|#60);", "<", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            Htmlstring = System.Text.RegularExpressions.Regex.Replace(Htmlstring, @"&(gt|#62);", ">", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            Htmlstring = System.Text.RegularExpressions.Regex.Replace(Htmlstring, @"&(nbsp|#160);", " ", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            Htmlstring = System.Text.RegularExpressions.Regex.Replace(Htmlstring, @"&(iexcl|#161);", "\xa1", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            Htmlstring = System.Text.RegularExpressions.Regex.Replace(Htmlstring, @"&(cent|#162);", "\xa2", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            Htmlstring = System.Text.RegularExpressions.Regex.Replace(Htmlstring, @"&(pound|#163);", "\xa3", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            Htmlstring = System.Text.RegularExpressions.Regex.Replace(Htmlstring, @"&(copy|#169);", "\xa9", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            Htmlstring = System.Text.RegularExpressions.Regex.Replace(Htmlstring, @"&#(\d+);", "", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            Htmlstring.Replace("<", "");
            Htmlstring.Replace(">", "");
            Htmlstring.Replace("\r\n", "");
            return Htmlstring;
        }

        public static string TitleCase(this string input)
        {
            return TitleCase(input, true);
        }

        public static string TitleCase(this string input, bool ignoreShortWords)
        {
            List<string> ignoreWords = null;
            if (ignoreShortWords)
            {
                //TODO: Add more ignore words?
                ignoreWords = new List<string>();
                ignoreWords.Add("a");
                ignoreWords.Add("is");
                ignoreWords.Add("was");
                ignoreWords.Add("the");
            }

            string[] tokens = input.Split(' ');
            StringBuilder sb = new StringBuilder(input.Length);
            foreach (string s in tokens)
            {
                if (ignoreShortWords == true
                    && s != tokens[0]
                    && ignoreWords.Contains(s.ToLower()))
                {
                    sb.Append(s + " ");
                }
                else
                {
                    sb.Append(s[0].ToString().ToUpper());
                    sb.Append(s.Substring(1).ToLower());
                    sb.Append(" ");
                }
            }

            return sb.ToString().Trim();
        }

        /// <summary>
        /// 去除字符串内的空白字符
        /// </summary>
        /// <param name="input">要进行处理的字符串</param>
        /// <returns>处理后的字符串</returns>
        public static string TrimIntraWords(this string input)
        {
            Regex regEx = new Regex(@"[\s]+");
            return regEx.Replace(input, " ");
        }

        /// <summary>
        /// 换行符转换成Html标签的换行符<br />
        /// </summary>
        /// <param name="input">要进行处理的字符串</param>
        /// <returns>处理后的字符串</returns>
        public static string NewLineToBreak(this string input)
        {
            Regex regEx = new Regex(@"[\n|\r]+");
            return regEx.Replace(input, "<br />");
        }

        public static string ProcessFormText2Html(this string input)
        {
            if (input == null) return "";
            return input.Replace("<", "&lt").Replace(">", "&gt").Replace("\n", "<br/>").Replace("'", "&#39;");
        }


        /// <summary>
        /// 插入换行符(不中断单词)
        /// </summary>
        /// <param name="input">要进行处理的字符串</param>
        /// <param name="charCount">每行字符数</param>
        /// <returns>处理后的字符串</returns>
        public static string WordWrap(this string input, int charCount)
        {
            return WordWrap(input, charCount, false, Environment.NewLine);
        }

        /// <summary>
        /// 插入换行符
        /// </summary>
        /// <param name="input">要进行处理的字符串</param>
        /// <param name="charCount">每行字符数</param>
        /// <param name="cutOff">如果为真，将在单词的中部断开</param>
        /// <returns>处理后的字符串</returns>
        public static string WordWrap(this string input, int charCount, bool cutOff)
        {
            return WordWrap(input, charCount, cutOff, Environment.NewLine);
        }

        /// <summary>
        /// 插入换行符
        /// </summary>
        /// <param name="input">要进行处理的字符串</param>
        /// <param name="charCount">每行字符数</param>
        /// <param name="cutOff">如果为真，将在单词的中部断开</param>
        /// <param name="breakText">插入的换行符号</param>
        /// <returns>处理后的字符串</returns>
        public static string WordWrap(this string input, int charCount, bool cutOff, string breakText)
        {
            StringBuilder sb = new StringBuilder(input.Length + 100);
            int counter = 0;

            if (cutOff)
            {
                while (counter < input.Length)
                {
                    if (input.Length > counter + charCount)
                    {
                        sb.Append(input.Substring(counter, charCount));
                        sb.Append(breakText);
                    }
                    else
                    {
                        sb.Append(input.Substring(counter));
                    }
                    counter += charCount;
                }
            }
            else
            {
                string[] strings = input.Split(' ');
                for (int i = 0; i < strings.Length; i++)
                {
                    counter += strings[i].Length + 1; // the added one is to represent the inclusion of the space.
                    if (i != 0 && counter > charCount)
                    {
                        sb.Append(breakText);
                        counter = 0;
                    }

                    sb.Append(strings[i] + ' ');
                }
            }
            return sb.ToString().TrimEnd(); // to get rid of the extra space at the end.
        }
        /// <summary>
        /// 将逗号分隔的字符串，每一个用单引号引起来
        /// </summary>
        /// <param name="s">要处理的字符串</param>
        /// <returns></returns>
        public static string ConvertSQLStringSplit(this string s)
        {
            string result = string.Empty;
            for (int i = 0; i <= s.Split(',').Length - 1; i++)
            {
                if (string.IsNullOrEmpty(s.Split(',')[i])) continue;
                if (string.IsNullOrEmpty(result))
                    result = "'" + s.Split(',')[i] + "'";
                else
                    result += ",'" + s.Split(',')[i] + "'";
            }
            return result;
        }
        /// <summary>
        /// JS单引号安全的字符串
        /// </summary>
        /// <param name="str">The STR.</param>
        /// <returns>The formated str.</returns>
        public static string ToJsSingleQuoteSafeString(this string str)
        {
            return str.Replace("'", "\\'");
        }

        /// <summary>
        /// JS双引号安全的字符串
        /// </summary>
        /// <param name="str">The STR.</param>
        /// <returns>The formated str.</returns>
        public static string ToJsDoubleQuoteSafeString(this string str)
        {
            return str.Replace("\"", "\\\"");
        }

        /// <summary>
        /// Toes the VBS quote safe string.
        /// </summary>
        /// <param name="str">The STR.</param>
        /// <returns>The formated str.</returns>
        public static string ToVbsQuoteSafeString(this string str)
        {
            return str.Replace("\"", "\"\"");
        }

        /// <summary>
        /// 防止SQL注入，过滤危险字符串.
        /// </summary>
        /// <param name="str">The STR.</param>
        /// <returns>The formated str.</returns>
        public static string ToSqlQuoteSafeString(this string input)
        {
            string s = input;
            //过滤关键字
            string StrKeyWord = @"select|insert|delete|from|count\(|drop table|update|truncate|asc\(|mid\(|char\(|xp_cmdshell|exec master|netlocalgroup administrators|:|net user|""|or|and";
            //过滤关键字符
            string StrRegex = @"[-|;|,|/|\(|\)|\[|\]|}|{|%|\@|*|!|'|=|<|>]";
            s = Regex.Replace(s, StrKeyWord, "", RegexOptions.IgnoreCase);
            s = Regex.Replace(s, StrRegex, "", RegexOptions.IgnoreCase);
            return s;
        }

        /// <summary>
        /// Texts to HTML.
        /// </summary>
        /// <param name="txtStr">The TXT STR.</param>
        /// <returns>The formated str.</returns>
        public static string TextToHtml(this string txtStr)
        {
            return txtStr.Replace(" ", "&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;").
                Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r", "").Replace("\n", "<br />");
        }

        /// <summary>
        /// HTML to Texts .
        /// </summary>
        /// <param name="txtStr">The Html STR.</param>
        /// <returns>The formated str.</returns>
        public static string HtmlToText(this string txtStr)
        {
            return txtStr.Replace("&nbsp;", " ").Replace("&nbsp;&nbsp;&nbsp;&nbsp;", "\t").
                Replace("&lt;", "<").Replace("&gt;", ">").Replace("<br />", "\r\n");
        }


        /// <summary>
        /// //截断字符串,并在字符串后加上... 
        /// </summary>
        public static string StringTruncat(this string input, int length)
        {
            char[] chArray = input.ToCharArray();
            string str = "";
            int num = 0;
            for (int i = 0; i < chArray.Length; i++)
            {
                if (Convert.ToUInt16(chArray[i]) > 0xff)
                {
                    str = str + chArray[i].ToString();
                    num += 2;
                }
                else
                {
                    str = str + chArray[i].ToString();
                    num++;
                }
                if (num > length)
                {
                    return str.Substring(0, i) + "...";
                }
            }
            return str;
        }
        /// <summary>
        /// 将任意类型转换为整型，当无法转换时返回 0 。
        /// </summary>
        /// <param name="id">object 类型。</param>
        /// <returns>返回转换结果。</returns>
        public static int ToInt(this string input)
        {
            int newid;
            int.TryParse(GetString(input).Trim(), out newid);
            return newid;
        }

        public static string ToDate(this string input)
        {
            if (String.IsNullOrEmpty(GetString(input))) return string.Empty;
            DateTime newdt;
            DateTime.TryParse(GetString(input), out newdt);
            return newdt.ToString("yyyy-MM-dd");
        }

        public static string ToDate(this string input, string format)
        {
            if (String.IsNullOrEmpty(GetString(input))) return string.Empty;
            DateTime newdt;
            DateTime.TryParse(GetString(input), out newdt);
            return newdt.ToString(format);
        }

        public static string ToTime(this string input)
        {
            if (String.IsNullOrEmpty(GetString(input))) return string.Empty;
            DateTime newdt;
            DateTime.TryParse(GetString(input), out newdt);
            return newdt.ToString("HH:mm:ss");
        }

        public static string ToDateTime(this string input, string format)
        {
            if (String.IsNullOrEmpty(GetString(input))) return string.Empty;
            DateTime newdt;
            DateTime.TryParse(GetString(input), out newdt);
            return newdt.ToString(format);
        }
        public static double ToDouble(this string input)
        {
            if (String.IsNullOrEmpty(GetString(input))) return 0;
            double newdouble;
            double.TryParse(GetString(input), out newdouble);
            return newdouble;
        }

        public static string UUID()
        {
            return Guid.NewGuid().ToString().ToUpper();
        }

        public static string Timestamp()
        {
           /* DateTime dt = DateTime.Now;
            DateTime dt1 = new DateTime(0x7b2, 1, 1, 0, 0, 0);
            TimeSpan ts = new TimeSpan();
            ts = (TimeSpan)(dt - dt1);
            double tt = ts.TotalMilliseconds;
            return tt.ToString("0");*/
            
            DateTime Epoch = new DateTime(1970, 1, 1);
            return (DateTime.UtcNow - Epoch).TotalMilliseconds.ToString("0");
        }

        #endregion
    }
}
