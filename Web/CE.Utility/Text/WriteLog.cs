﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Configuration;
using System.Web;
using Common.Helper;

namespace CE.Utility.Text
{
    /// <summary>
    /// 写日志
    /// </summary>
    public static class WriteLog
    {

        /// <summary>
        /// 日志
        /// </summary>
        /// <param name="fileName">文件名</param>
        /// <param name="title">标题</param>
        /// <param name="msg">内容</param>
        /// <param name="exception">异常</param>
        public static void Write(string title, string msg)
        {
            StreamWriter sw = null;
            FileStream fs = null;

            var suffix = ".log";
            try
            {
                string logDir = string.Concat(HttpContext.Current.Request.MapPath("~/Contents/log/"), DateTime.Now.ToString("yyyy-MM-dd")) + "\\";

                if (!Directory.Exists(logDir))
                    Directory.CreateDirectory(logDir); //创建路径
                string logPath = logDir + DateTime.Now.Date.ToString("yyyy-MM-dd") + suffix;
                string newFileName = "";
                string[] fileNames = Directory.GetFiles(logDir);
                if (fileNames.Length > 0)
                {
                    var maxLength = 1024 * 1024;

                    foreach (var fileName in fileNames)
                    {
                        if (new FileInfo(fileName).Length < maxLength)
                            newFileName = fileName;
                    }

                    if (string.IsNullOrEmpty(newFileName))
                    {
                        newFileName = logPath.Insert(logPath.IndexOf(suffix), "_" + (fileNames.Length + 1));
                    }
                }
                else
                {
                    newFileName = logPath;
                }
                fs = File.Exists(logPath) ? new FileStream(newFileName, FileMode.Append, FileAccess.Write) : new FileStream(logPath, FileMode.Create, FileAccess.Write);
                sw = new StreamWriter(fs);
                sw.WriteLine($"[{DateTime.Now:yyyy-MM-dd HH:mm:ss:fff}][{title}]    {msg}");
                sw.WriteLine(" ");
            }
            catch (Exception ex)
            {
            }
            finally
            {
                sw?.Close();
                fs?.Close();
            }

        }

        /// <summary>
        /// 日志
        /// </summary>
        /// <param name="msg"></param>      
        /// <param name="fileName">文件名字</param> 
        /// <param name="exception"></param>
        public static void WriteError(string fileName, string msg, Exception exception)
        {

            FileStream fs = null;
            StreamWriter sw = null;
            try
            {

                string mLogPath = string.Concat(HttpContext.Current.Request.MapPath("~/Contents/log/Error/"), DateTime.Now.ToString("yyyy-MM-dd"), "\\");
                if (!Directory.Exists(mLogPath))
                    Directory.CreateDirectory(mLogPath);

                string logPath = mLogPath + fileName + ".log";
                fs = File.Exists(logPath) ? new FileStream(logPath, FileMode.Append, FileAccess.Write) : new FileStream(logPath, FileMode.Create, FileAccess.Write);
                sw = new StreamWriter(fs);
                sw.WriteLine("[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff") + "]    " + msg);
                if (exception != null)
                {
                    sw.WriteLine($"[{DateTime.Now:yyyy-MM-dd HH:mm:ss:fff}][Exception:Data]    {exception.Data}");
                    sw.WriteLine($"[{DateTime.Now:yyyy-MM-dd HH:mm:ss:fff}][Exception:Message] {exception.Message}");
                    sw.WriteLine($"[{DateTime.Now:yyyy-MM-dd HH:mm:ss:fff}][Exception:Source]  {exception.Source}");
                    sw.WriteLine($"[{DateTime.Now:yyyy-MM-dd HH:mm:ss:fff}][Exception:StackTrace]  {exception.StackTrace}");
                    sw.WriteLine($"[{DateTime.Now:yyyy-MM-dd HH:mm:ss:fff}][Exception:InnerException]  {exception.InnerException}");
                    sw.WriteLine($"[{DateTime.Now:yyyy-MM-dd HH:mm:ss:fff}][Exception:TargetSite]  {exception.TargetSite}");
                }

            }
            catch (Exception ex)
            {
            }
            finally
            {
                sw?.Close();
                fs?.Close();
            }
        }
    }
}
