﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CE.Utility.Text
{
    public static class VerifyUtil
    {
        #region 验证是否为空
        /// <summary>
        /// 验证input 是否是数字
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsNullOrEmpty(string input)
        {
            return string.IsNullOrEmpty(input.Trim());
        }
        #endregion

        #region 验证是正浮点数

        /// <summary>
        /// 验证input 是否是正浮点数
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsFloatNumber(string input)
        {
            //模式字符串
            string pattern = @"^[1-9]\d*\.?\d*|0\.?\d*[1-9]\d*$";
            //验证
            return IsMatch(input, pattern);
        }

        #endregion


        #region 验证是否是数字

        /// <summary>
        /// 验证input 是否是数字
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsNumber(string input)
        {
            //var result = false;
            //var regex = new Regex("^[0-9]+$");
            //result = regex.IsMatch(input);
            //return result;
            //模式字符串
            string pattern = @"^[0-9]+[0-9]*$";
            //验证
            return IsMatch(input, pattern);
        }

        #endregion

        #region 验证IP地址是否合法
        /// <summary>
        /// 验证IP地址是否合法
        /// </summary>
        /// <param name="input">要验证的IP地址</param>        
        public static bool IsIP(string input)
        {
            //如果为空，认为验证合格
            if (IsNullOrEmpty(input))
            {
                return true;
            }
            //清除要验证字符串中的空格
            input = input.Trim();
            //模式字符串
            string pattern = @"^((2[0-4]\d|25[0-5]|[01]?\d\d?)\.){3}(2[0-4]\d|25[0-5]|[01]?\d\d?)$";

            //验证
            return IsMatch(input, pattern);
        }
        #endregion

        #region  验证EMail是否合法
        /// <summary>
        /// 验证EMail是否合法
        /// </summary>
        /// <param name="input">要验证的Email</param>
        public static bool IsEmail(string input)
        {
            //如果为空，认为验证不合格
            if (IsNullOrEmpty(input))
            {
                return false;
            }

            //清除要验证字符串中的空格
            input = input.Trim();
            //模式字符串
            string pattern = @"^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$";

            //验证
            return IsMatch(input, pattern);
        }
        #endregion

        #region 验证是否是日期(yyyy-MM-dd)

        /// <summary>
        ///验证是否是日期 是否是 yyyy-MM-dd格式
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsShortDate(string input)
        {
            //模式字符串
            string pattern = @"(([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3})-(((0[13578]|1[02])-(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)-(0[1-9]|[12][0-9]|30))|(02-(0[1-9]|[1][0-9]|2[0-8]))))|((([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00))-02-29)";
            //验证
            return IsMatch(input, pattern);
        }

        #endregion

        #region 验证是输入是否是数字和小写字母的组成
        /// <summary>
        /// 验证是输入是否是数字和小写字母的组成
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsString(string input)
        {
            //模式字符串
            string pattern = @"^[A-Za-z][A-Za-z|0-9]+$";
            //验证
            return IsMatch(input, pattern);
        }
        

        #endregion

        #region 检测客户的输入中是否有危险字符串
        /// <summary>
        /// 检测客户输入的字符串是否有效,并将原始字符串修改为有效字符串或空字符串。
        /// 当检测到客户的输入中有攻击性危险字符串,则返回false,有效返回true。
        /// </summary>
        /// <param name="input">要检测的字符串</param>
        public static bool IsValidInput(ref string input)
        {
            try
            {
                if (IsNullOrEmpty(input))
                {
                    //如果是空值,则跳出
                    return true;
                }
                else
                {
                    //替换单引号
                    input = input.Replace("'", "''").Trim();

                    //检测攻击性危险字符串
                    string testString = "and |or |exec |insert |select |delete |update |count |chr |mid |master |truncate |char |declare ";
                    string[] testArray = testString.Split('|');
                    foreach (string testStr in testArray)
                    {
                        if (input.ToLower().IndexOf(testStr, StringComparison.Ordinal) != -1)
                        {
                            //检测到攻击字符串,清空传入的值
                            input = "";
                            return false;
                        }
                    }

                    //未检测到攻击字符串
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region 验证

        /// <summary>
        /// 验证输入字符串是否与模式字符串匹配，匹配返回true
        /// </summary>
        /// <param name="input">输入字符串</param>
        /// <param name="pattern">模式字符串</param>        
        public static bool IsMatch(string input, string pattern)
        {
            return IsMatch(input, pattern, RegexOptions.IgnoreCase);
        }

        /// <summary>
        /// 验证输入字符串是否与模式字符串匹配，匹配返回true
        /// </summary>
        /// <param name="input">输入的字符串</param>
        /// <param name="pattern">模式字符串</param>
        /// <param name="options">筛选条件</param>
        public static bool IsMatch(string input, string pattern, RegexOptions options)
        {
            return Regex.IsMatch(input, pattern, options);
        }

        #endregion

    }
}
