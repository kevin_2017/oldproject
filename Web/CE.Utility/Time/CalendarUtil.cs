﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CE.Utility.Time
{
    public class CalendarUtil
    {
        private const string FORMAT = "yyyy-MM-dd";
        /// <summary>
        /// 今天
        /// </summary>
        /// <returns></returns>
        public static string NowDay()
        {
            return DateTime.Now.ToString(FORMAT);
        }
        /// <summary>
        /// 取当前月的第一天
        /// </summary>
        /// <returns></returns>
        public static string FirstDayOfMonth()
        {
            return FirstDayOfMonth(DateTime.Now);
        }
        /// <summary>
        /// 取得某月的第一天
        /// </summary>
        /// <param name="datetime">要取得月份第一天的时间</param>
        /// <returns></returns>
        public static string FirstDayOfMonth(DateTime datetime)
        {
            return datetime.AddDays(1 - datetime.Day).ToString(FORMAT);
        }

        /// <summary>
        /// 取当前月的最后一天
        /// </summary>
        /// <returns></returns>
        public static string LastDayOfMonth()
        {
            return LastDayOfMonth(DateTime.Now);
        }

        /// <summary>
        /// 取得某月的最后一天
        /// </summary>
        /// <param name="datetime">要取得月份最后一天的时间</param>
        /// <returns></returns>
        public static string LastDayOfMonth(DateTime datetime)
        {
            return datetime.AddDays(1 - datetime.Day).AddMonths(1)
                .AddDays(-1).ToString(FORMAT);
        }

        /// <summary>
        /// 取上个月第一天
        /// </summary>
        /// <returns></returns>
        public static string FirstDayOfPreviousMonth()
        {
            return FirstDayOfPreviousMonth(DateTime.Now);
        }

        /// <summary>
        /// 取得上个月第一天
        /// </summary>
        /// <param name="datetime">要取得上个月第一天的当前时间</param>
        /// <returns></returns>
        private static string FirstDayOfPreviousMonth(DateTime datetime)
        {
            return datetime.AddDays(1 - datetime.Day)
                .AddMonths(-1).ToString(FORMAT);
        }

        /// <summary>
        /// 取得上个月的最后一天
        /// </summary>
        /// <returns></returns>
        public static string LastDayOfPrdviousMonth()
        {
            return LastDayOfPrdviousMonth(DateTime.Now);
        }

        /// <summary>
        /// 取得上个月的最后一天
        /// </summary>
        /// <param name="datetime">要取得上个月最后一天的当前时间</param>
        /// <returns></returns>
        private static string LastDayOfPrdviousMonth(DateTime datetime)
        {
            return datetime.AddDays(1 - datetime.Day).AddDays(-1)
                .ToString(FORMAT);
        }
    }
}
