﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.Utility.Images
{
    /// <summary>
    /// 生成验证码的类
    /// </summary>
    public class ValidateCode
    {
        private int letterWidth = 28;
        private int letterHeight = 34;
        private int fontSize = 24;
        private char[] chars = "A2B3C4D5E6F7G8H9G2K3L4M5N6P7Q8R9S2T3U4V5W6X7Y8Z".ToCharArray();
        private string[] fonts = { "Courier New", "Arial", "Georgia" , "Tahoma", "Verdana","Comic sans MS", "Lucida console" };
        private const double PI = 3.1415926535897932384626433832795;
        private const double PI2 = 6.283185307179586476925286766559;

        public int LetterWidth
        {
            get
            {
                return letterWidth;
            }
            set
            {
                letterWidth = value;
            }
        }

        public int LetterHeight
        {
            get
            {
                return letterHeight;
            }
            set
            {
                letterHeight = value;
            }
        }

        public int FontSize
        {
            get
            {
                return fontSize;
            }
            set
            {
                fontSize = value;
            }
        }

        public byte[] CreateValidateGraphic(string checkCode)
        {
            int int_ImageWidth = checkCode.Length * letterWidth + 5;
            Random newRandom = new Random();
            Bitmap image = new Bitmap(int_ImageWidth, letterHeight);
            Graphics g = Graphics.FromImage(image);
            Random random = new Random();
            g.Clear(Color.White);

            for (int i = 0; i < 10; i++)
            {
                int x1 = random.Next(image.Width);
                int x2 = random.Next(image.Width);
                int y1 = random.Next(image.Height);
                int y2 = random.Next(image.Height);

                g.DrawLine(new Pen(Color.Silver), x1, y1, x2, y2);
            }

            for (int i = 0; i < 10; i++)
            {
                int x = random.Next(image.Width);
                int y = random.Next(image.Height);

                image.SetPixel(x, y, Color.FromArgb(random.Next()));
            }

            int findex;
            for (int int_index = 0; int_index < checkCode.Length; int_index++)
            {
                findex = newRandom.Next(fonts.Length - 1);
                string str_char = checkCode.Substring(int_index, 1);
                Brush newBrush = new SolidBrush(GetRandomColor());
                Point thePos = new Point(int_index * letterWidth + newRandom.Next(2)+1, newRandom.Next(2)+1);
                g.DrawString(str_char, new Font(fonts[findex], fontSize, FontStyle.Bold), newBrush, thePos);
            }
            g.DrawRectangle(new Pen(Color.LightGray, 1), 0, 0, int_ImageWidth - 1, (letterHeight - 1));
            MemoryStream ms = new MemoryStream();
            image.Save(ms, ImageFormat.Png);
            g.Dispose();
            image.Dispose();
            return ms.ToArray();
        }

   

        protected Color GetRandomColor()
        {
            Random RandomNum_First = new Random((int)DateTime.Now.Ticks);
            System.Threading.Thread.Sleep(RandomNum_First.Next(50));
            Random RandomNum_Sencond = new Random((int)DateTime.Now.Ticks);
            int int_Red = RandomNum_First.Next(210);
            int int_Green = RandomNum_Sencond.Next(180);
            int int_Blue = (int_Red + int_Green > 300) ? 0 : 400 - int_Red - int_Green;
            int_Blue = (int_Blue > 255) ? 255 : int_Blue;
            return Color.FromArgb(int_Red, int_Green, int_Blue);
        }

        public string CreateValidateCode(int numberLength)
        {
            Random random = new Random();
            string validateCode = "";

            do
            {
                string sel = chars[random.Next(0, chars.Length)].ToString();
                if (!validateCode.Contains(sel))
                    validateCode += sel;
            } while (validateCode.Length < numberLength);

            return validateCode;
        }
    }
}