﻿using Newtonsoft.Json;

namespace CE.Utility.Common
{

    /// <summary>
    /// 提供了一个关于json的辅助类
    /// </summary>
    public static class JsonHelper
   {

       #region Method
       /// <summary>
        /// 对象转换成json字符串
        /// </summary> 
        /// <returns></returns>
        public static string ToJson(object t)
        {
            return JsonConvert.SerializeObject(t);
            //return  JsonConvert.SerializeObject(t, Formatting.Indented, new JsonSerializerSettings { NullValueHandling =  NullValueHandling.Include });
        }
        /// <summary>
        /// 对象转换成json字符串
        /// </summary>
        /// <param name="t"></param>
        /// <param name="HasNullIgnore">是否忽略NULL值</param>
        /// <returns></returns>
        public static string ToJson(object t, bool HasNullIgnore)
        {
            if (HasNullIgnore)
                return JsonConvert.SerializeObject(t, Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            else
               return ToJson(t);
        }
        /// <summary>
        /// json字符串转换成对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="strJson"></param>
        /// <returns></returns>
        public static T FromJson<T>(string strJson) where T : class
        {
            if (!string.IsNullOrEmpty(strJson))
                return JsonConvert.DeserializeObject<T>(strJson);
            return null;
        }
        #endregion

    }
}
