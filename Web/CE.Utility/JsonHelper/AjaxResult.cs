﻿namespace CE.Utility.Common
{
    /// <summary>
    /// 前台Ajax请求的统一返回结果类
    /// </summary>
    public class AjaxResult
    {
        public AjaxResult()
        {

        }

        //private bool _status = false;

        /// <summary>
        /// 状态
        /// </summary>
        public bool status { get; set; }

        /// <summary>
        /// 返回信息
        /// </summary>
        public string msg { get; set; }

        /// <summary>
        /// 返回数据记录数
        /// </summary>
        public int count { get; set; }

        /// <summary>
        /// 返回数据
        /// </summary>
        public object data { get; set; }

        #region Error
        public static AjaxResult Error()
        {
            return new AjaxResult()
            {
                status = false
            };
        }
        public static AjaxResult Error(string msg)
        {
            return new AjaxResult()
            {
                status = false,
                msg = msg
            };
        }
        #endregion

        #region Success
        public static AjaxResult Success()
        {
            return new AjaxResult()
            {
                status = true
            };
        }
        public static AjaxResult Success(string msg)
        {
            return new AjaxResult()
            {
                status = true,
                msg = msg
            };
        }
        public static AjaxResult Success(object data)
        {
            return new AjaxResult()
            {
                status = true,
                data = data
            };
        }
        public static AjaxResult Success(object data, string msg)
        {
            return new AjaxResult()
            {
                status = true,
                data = data,
                msg = msg
            };
        }
        public static AjaxResult Success(object data, int count, string msg)
        {
            return new AjaxResult()
            {
                status = true,
                msg = msg,
                data = data,
                count = count
            };
        }
        #endregion

        /// <summary>
        /// 序列化
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return JsonHelper.ToJson(this);
        }

        /// <summary>
        /// 对象化
        /// </summary>
        /// <typeparam name="T">类型</typeparam>
        /// <returns>T</returns>
        public T ToObject<T>()
        {
            if (status && data != null)
                return (T)data;
            else
                return default(T);
        }

        ///// <summary>
        ///// 对象化
        ///// </summary>
        ///// <typeparam name="T">类型</typeparam>
        ///// <param name="ar"></param>
        ///// <returns></returns>
        //public static AjaxResult ToObject<T>(this AjaxResult ar)
        //{
        //    if (ar.status && ar.data != null)
        //        ar.data = JsonConvert.DeserializeObject<T>(ar.data.ToString());
        //    return ar;
        //}
    }
}
