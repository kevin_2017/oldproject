﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CE.Utility.Bank
{
    public class BankUtil
    {
        public static bool CheckCardNo(string cardNo)
        {
            if (cardNo != null && Regex.IsMatch(cardNo.Trim(), @"^\d*$")) // The bank card number is not null,and it is of type int.
            {
                cardNo = cardNo.Trim();
                string nonCardId = cardNo.Substring(0, cardNo.Length - 1);
                char code = cardNo.ElementAt(cardNo.Length - 1);
                char[] chs = nonCardId.Trim().ToCharArray();
                int luhmSum = 0;
                for (int i = chs.Length - 1, j = 0; i >= 0; i--, j++)
                {
                    int k = chs[i] - '0';
                    if (j % 2 == 0)
                    {
                        k *= 2;
                        k = k / 10 + k % 10;
                    }
                    luhmSum += k;
                }
                char bit = (luhmSum % 10 == 0) ? '0' : (char)((10 - luhmSum % 10) + '0');
                return bit == code;//return result of verify
            }
            else
            {
                return false;
            }
        }
    }
}
