﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.Utility.Web
{
    public class IPLocation
    {
        public string IP { get; set; }
        public string Country { get; set; }
        public string Local { get; set; }
    }
}
