using System;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Resources;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace CE.Utility.Web
{
    public class WebUtil
    {
        #region Helper Methods

        /// <summary>
        /// Gets the string param.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The param value.</returns>
        public static string GetStringInput(System.Web.HttpRequestBase request)
        {
            request.InputStream.Position = 0;
            string retStr = string.Empty;
            StreamReader reader = new StreamReader(request.InputStream);
            retStr = reader.ReadToEnd();
            request.InputStream.Position = 0;

            return retStr.Trim();
        }

        /// <summary>
        /// Gets the string param.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="paramName">Name of the param.</param>
        /// <param name="errorReturn">The error return.</param>
        /// <returns>The param value.</returns>
        public static string GetStringParam(System.Web.HttpRequestBase request, string paramName, string errorReturn)
        {
            string retStr = request.Form[paramName];
            if (retStr == null)
            {
                retStr = request.QueryString[paramName];
            }
            if (retStr == null)
            {
                return errorReturn;
            }
            return retStr;
        }

        /// <summary>
        /// Gets the int param.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="paramName">Name of the param.</param>
        /// <param name="errorReturn">The error return.</param>
        /// <returns>The param value.</returns>
        public static int GetIntParam(System.Web.HttpRequestBase request, string paramName, int errorReturn)
        {
            string retStr = request.Form[paramName];
            if (retStr == null)
            {
                retStr = request.QueryString[paramName];
            }
            if (retStr == null || retStr.Trim() == string.Empty)
            {
                return errorReturn;
            }
            try
            {
                return Convert.ToInt32(retStr);
            }
            catch
            {
                return errorReturn;
            }
        }
        /// <summary>
        /// Gets the int param.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="paramName">Name of the param.</param>
        /// <param name="errorReturn">The error return.</param>
        /// <returns>The param value.</returns>
        public static decimal GetDecimalParam(System.Web.HttpRequestBase request, string paramName, decimal errorReturn)
        {
            string retStr = request.Form[paramName];
            if (retStr == null)
            {
                retStr = request.QueryString[paramName];
            }
            if (retStr == null || retStr.Trim() == string.Empty)
            {
                return errorReturn;
            }
            try
            {
                return Convert.ToDecimal(retStr);
            }
            catch
            {
                return errorReturn;
            }
        }

         
        /// <summary>
        /// Gets the date time param.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="paramName">Name of the param.</param>
        /// <param name="errorReturn">The error return.</param>
        /// <returns>The param value.</returns>
        public static DateTime GetDateTimeParam(System.Web.HttpRequestBase request, string paramName, DateTime errorReturn)
        {
            string retStr = request.Form[paramName];
            if (retStr == null)
            {
                retStr = request.QueryString[paramName];
            }
            if (retStr == null || retStr.Trim() == string.Empty)
            {
                return errorReturn;
            }
            try
            {
                return Convert.ToDateTime(retStr);
            }
            catch
            {
                return errorReturn;
            }
        }
        /// <summary>
        /// 获取客户端IP
        /// </summary>
        /// <returns></returns>
        public static string ClientIP()
        {
            string clientIP = "";
            if (HttpContext.Current == null)
            {
                return clientIP;
            }
            clientIP = HttpContext.Current.Request.Headers["http_client_ip"];
            if (string.IsNullOrEmpty(clientIP))
            {
                string[] proxs = { "http_true_client_IP", "HTTP_X_FORWARDED_FOR", "X-Forwarded-For", "Proxy-Client-IP", "WL-Proxy-Client-IP", "HTTP_CLIENT_IP" };
                foreach (var item in proxs)
                {
                    clientIP = HttpContext.Current.Request.ServerVariables[item];
                    if (string.IsNullOrEmpty(clientIP) || clientIP.ToLower().Equals("unknown"))
                        continue;
                    else
                        break;
                }
            }
            if (string.IsNullOrEmpty(clientIP))
            {
                clientIP = HttpContext.Current.Request.UserHostAddress;
            }
            clientIP = clientIP.Split(',')[0];
            return clientIP;
        }
        /// <summary>
        /// 获取客户端Host
        /// </summary>
        /// <returns></returns>
        public static string ClientHost()
        {
            if (HttpContext.Current.Request.UrlReferrer != null)
                return HttpContext.Current.Request.UrlReferrer.Host;
            else
                return string.Empty;
        }
        /// <summary>
        /// 获取客户端Host
        /// </summary>
        /// <returns></returns>
        public static string ClientReferrer()
        {
            if (HttpContext.Current.Request.UrlReferrer != null)
                return HttpContext.Current.Request.UrlReferrer.ToString();
            else
                return string.Empty;
        }
        /// <summary>
        /// 获取客户端UserAgent
        /// </summary>
        /// <returns></returns>
        public static string UserAgent()
        {
            if (HttpContext.Current.Request.UserAgent != null)
                return HttpContext.Current.Request.UserAgent;
            else
                return string.Empty;
        }

        /// <summary>
        /// Strongs the typed.
        /// </summary>
        /// <param name="obj">The obj.</param>
        /// <returns>The strong typed instance.</returns>
        public static ObjectType StrongTyped<ObjectType>(object obj)
        {
            return (ObjectType)obj;
        }
        #endregion

        #region Resource

        private static Dictionary<string, Hashtable> stringResources = new Dictionary<string, Hashtable>();

        private static System.Globalization.CultureInfo defaultCulture = null;

        /// <summary>
        /// Gets or sets the default culture.
        /// </summary>
        /// <value>The default culture.</value>
        public static System.Globalization.CultureInfo DefaultCulture
        {
            get
            {
                return defaultCulture ?? System.Threading.Thread.CurrentThread.CurrentUICulture;
            }
            set
            {
                defaultCulture = value;
            }
        }

        /// <summary>
        /// Loads the resources.
        /// </summary>
        /// <param name="resourceName">Name of the resource.</param>
        /// <param name="ci">The ci.</param>
        public static void LoadResources(string resourceName, System.Globalization.CultureInfo ci)
        {
            string resFileName = System.Web.HttpRuntime.BinDirectory + resourceName + "." + ci.ToString() + ".resources";
            if (System.IO.File.Exists(resFileName))
            {
                lock (stringResources)
                {
                    if (!stringResources.ContainsKey(ci.ToString()))
                    {
                        stringResources.Add(ci.ToString(), new Hashtable());

                        try
                        {
                            ResourceReader reader = new ResourceReader(resFileName);
                            IDictionaryEnumerator en = reader.GetEnumerator();
                            while (en.MoveNext())
                            {
                                stringResources[ci.ToString()].Add(en.Key, en.Value);
                            }
                            reader.Close();
                        }
                        catch
                        {
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Loads the resources.
        /// </summary>
        /// <param name="resourceName">Name of the resource.</param>
        public static void LoadResources(string resourceName)
        {
            LoadResources(resourceName, DefaultCulture);
        }

        /// <summary>
        /// Gets the string.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>The resouce value.</returns>
        public static string GetString(string key)
        {
            return GetString(key, WebUtil.DefaultCulture);
        }

        /// <summary>
        /// Gets the string.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="ci">The ci.</param>
        /// <returns>The resouce value.</returns>
        public static string GetString(string key, System.Globalization.CultureInfo ci)
        {
            if (stringResources.ContainsKey(ci.ToString()))
            {
                if (stringResources[ci.ToString()].Contains(key))
                {
                    return stringResources[ci.ToString()][key].ToString();
                }
            }

            return string.Empty;
        }

        #endregion
        
    }
}
