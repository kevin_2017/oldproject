﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using DotNet4.Utilities;

namespace CE.Utility.Web
{
    public class HttpRequestUtil
    {
        private string _url = null;
        private Method _method = Method.Get;
        private ContentType _contentType = ContentType.Form;
        private string _postData = string.Empty;
        private int _timeout = 30000;
        private int _readWriteTimeout = 30000;

        /// <summary>
        /// HTTP提交方法
        /// </summary>
        public enum Method
        {
            Post,
            Get,
            Put
        }

        /// <summary>
        /// HTTP提交内容类型
        /// </summary>
        public enum ContentType
        {
            Text = 0,
            Xml = 1,
            Json = 2,
            Form = 3
        }

        private readonly string[] _contentTypes =
        {
            "text/html",
            "application/xml",
            "application/json",
            "application/x-www-form-urlencoded"
        };

        /// <summary>
        /// 实例化HTTP请求对象
        /// </summary>
        /// <param name="url">请求地址</param>
        public HttpRequestUtil(string url)
        {
            this._url = url;
        }

        /// <summary>
        /// 实例化HTTP请求对象
        /// </summary>
        /// <param name="url">请求地址</param>
        /// <param name="method">请求方法，默认GET方法</param>
        public HttpRequestUtil(string url, Method method)
        {
            this._url = url;
            this._method = method;
        }

        /// <summary>
        /// 实例化HTTP请求对象
        /// </summary>
        /// <param name="url">请求地址</param>
        /// <param name="method">请求方法，默认GET方法</param>
        /// <param name="contentType">请求内容类型，默认application/x-www-form-urlencoded</param>

        public HttpRequestUtil(string url, Method method, ContentType contentType)
        {
            this._url = url;
            this._method = method;
            this._contentType = contentType;
        }

        /// <summary>
        /// 实例化HTTP请求对象
        /// </summary>
        /// <param name="url">请求地址</param>
        /// <param name="method">请求方法，默认GET方法</param>
        /// <param name="postData">提交的数据</param>
        public HttpRequestUtil(string url, Method method,string postData)
        {
            this._url = url;
            this._method = method;
            this._postData = postData;
        }

        /// <summary>
        /// 实例化HTTP请求对象
        /// </summary>
        /// <param name="url">请求地址</param>
        /// <param name="method">请求方法，默认GET方法</param>
        /// <param name="contentType">请求内容类型，默认application/x-www-form-urlencoded</param>
        /// <param name="postData">提交的数据</param>
        public HttpRequestUtil(string url, Method method, ContentType contentType, string postData)
        {
            this._url = url;
            this._method = method;
            this._contentType = contentType;
            this._postData = postData;
        }

        /// <summary>
        /// 获取或设置超时时间 默认30000毫秒
        /// </summary>
        public int Timeout
        {
            get { return _timeout; }
            set { _timeout = value; }
        }

        /// <summary>
        /// 获取或设置写入Post数据超时时间 默认30000毫秒
        /// </summary>
        public int ReadWriteTimeout
        {
            get { return _readWriteTimeout; }
            set { _readWriteTimeout = value; }
        }

        /// <summary>
        /// 获取或设置Post提交的数据
        /// </summary>
        public string PostData
        {
            get { return _postData; }
            set { _postData = value; }
        }

        /// <summary>
        /// 请求远程服务器
        /// </summary>
        /// <returns></returns>
        public HttpResult Request()
        {
            var httpHelper = new HttpHelper();
            var httpItem = GetHttpItem();

            var httpResult = httpHelper.GetHtml(httpItem);
            return httpResult;
        }

        /// <summary>
        /// 生成Http请求类
        /// </summary>
        /// <returns></returns>
        private HttpItem GetHttpItem()
        {
            var item = new HttpItem()
            {
                URL = _url,//URL     必需项
                Encoding = Encoding.UTF8,//编码格式（utf-8,gb2312,gbk）
                Method = _method.ToString(),//post
                Timeout = Timeout,//连接超时时间
                ReadWriteTimeout = ReadWriteTimeout,//写入Post数据超时时间
                UserAgent = "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)",//用户的浏览器类型，版本，操作系统     可选项有默认值
                Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",//    可选项有默认值
                ContentType = _contentTypes[(int)(_contentType)],//返回类型    可选项有默认值
            };
            if (_method == Method.Post)
            {
                item.PostDataType= PostDataType.Byte;
                item.Postdata = PostData;
            }
            return item;
        }

    }
}
