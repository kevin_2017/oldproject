﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace CE.Utility.Web
{
    public static class IPSearch
    {
        public static string GetLocation(string ip)
        {
            // 某些IP库包含有广告信息，显示的时候会过滤以下的广告信息
            var filterCountry = new Dictionary<int, string>
            {
                { 1, "CZ88.NET"},
                { 2, "未获得IP"},
                { 3, "同一个局域网内"},
                { 4, "局域网"}
            };

            //当地区信息包含以下内容时，被忽略
            var filterArea = new Dictionary<int, string>
            {
                {1, "电信"},
                {2, "移动"},
                {3, "联通"},
                {4, "宽带"},
                {5, "有线通"},
                {6, "铁通"}
            };

            var ipfilePath = HttpContext.Current.Request.MapPath("~/App_Data/qqwry.dat");
            var qqWry = new IPScanner(ipfilePath);

            var ipLocation = qqWry.Query(ip);
            var country = ipLocation.Country;
            var local = ipLocation.Local;
            foreach (var item in filterCountry)
            {
                country = country.Replace(item.Value, string.Empty);
            }
            foreach (var item in filterArea)
            {
                local = local.Replace(item.Value, string.Empty);
            }

            return $"{country}{local}";
        }
    }
}
