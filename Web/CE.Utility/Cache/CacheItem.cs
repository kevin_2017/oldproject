﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CE.Utility.Cache
{
    public class CacheItem<T>
    {
        private string key;
        private T obj;

        private static string PrefixKey = "celottery";
        public DateTime CreateTime { get; private set; }
        public DateTime LastActiveTime { get; private set; }
        public int Actives { get; private set; }

        public CacheItem()
        {
            key = string.Empty;
            obj = default(T);
            CreateTime = DateTime.Now;
            LastActiveTime = DateTime.Now;
            Actives = 0;
        }

        public CacheItem(string key, T obj)
        {
            key = PrefixKey + key;
            this.obj = obj;
            CreateTime = DateTime.Now;
            LastActiveTime = DateTime.Now;
            Actives = 0;
        }

        public string Key
        {
            get
            {
                Actives++;
                return key.RemovePrefix();
            }
            set
            {
                key = value.AddPrefix();
            }
        }

        public T Object
        {
            get
            {
                Actives++;
                LastActiveTime = DateTime.Now;
                return obj;
            }
            set
            {
                Actives++;
                LastActiveTime = DateTime.Now;
                obj = value;
            }
        }
    }
}
