﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Caching;

namespace CE.Utility.Cache
{
    public static class ObjectCache
    {
        private static System.Web.Caching.Cache cache;
        private static double SaveTime{get;set;}
        private static string PrefixKey = "celottery";

        public static string AddPrefix(this string s)
        {
            return PrefixKey + s;
        }

        public static string RemovePrefix(this string s)
        {
            if (string.IsNullOrEmpty(s))
                return s;
            else if (s.Length < PrefixKey.Length)
                return s;
            else
                return s.Substring(PrefixKey.Length);
        }
        static ObjectCache()
        {
            //cache = System.Web.HttpContext.Current.Cache;
            cache = System.Web.Hosting.HostingEnvironment.Cache;
            SaveTime = 15.0;
        }

        public static object Get(string key)
        {
            return cache.Get(key.AddPrefix());
        }

        public static T Get<T>(string key)
        {
            object obj = Get(key);
            if (obj == null)
                return default(T);
            else
                return (T)obj;
        }

        public static CacheItem<T> GetItem<T>(string key)
        {
            return Get<CacheItem<T>>(key);
        }

        public static void Insert<VType>(CacheItem<VType> item)
        {
            Insert(item.Key, item, null, CacheItemPriority.Default, null);
        }

        public static void Insert(string key, object value)
        {
            Insert(key, value, null, CacheItemPriority.Default, null);
        }

        public static void Insert(string key, object value, CacheDependency dependency)
        {
            Insert(key, value, dependency, CacheItemPriority.Default, null);
        }

        public static void Insert(string key, object value, CacheDependency dependency, CacheItemRemovedCallback callback)
        {
            Insert(key, value, dependency, CacheItemPriority.Default, callback);
        }

        public static void Insert(string key, object value, CacheDependency dependency, CacheItemPriority priority, CacheItemRemovedCallback callback)
        {
            cache.Insert(key.AddPrefix(), value, dependency, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(SaveTime), priority, callback);
        }

        public static void Remove(string key)
        {
            cache.Remove(key.AddPrefix());
        }

        public static IList<string> GetKeys()
        {
            List<string> keys = new List<string>();
            IDictionaryEnumerator e = cache.GetEnumerator();
            while (e.MoveNext())
            {
                keys.Add(e.Key.ToString().RemovePrefix());
            }
            return keys.AsReadOnly();
        }

        public static void RemoveAll()
        {
            IList<string> keys = GetKeys();
            foreach (string key in keys)
            {
                cache.Remove(key.AddPrefix());
            }
        }

        public static IList<string> RegexSearch(string pattern)
        {
            IList<string> keys = GetKeys();
            IList<string> result = keys.Where(k =>
            {
                if (Regex.IsMatch(k, pattern))
                    return true;
                else
                    return false;
            }).ToList();
            return result;
        }

        public static void RegexRemove(string pattern)
        {
            IList<string> keys = RegexSearch(pattern);
            foreach (string key in keys)
            {
                cache.Remove(key.AddPrefix());
            }
        }
    }
}
