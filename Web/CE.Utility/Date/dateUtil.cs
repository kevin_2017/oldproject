﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.Utility.Date
{
    public static class DateUtil
    {
        public static long CurrentTimeMillis(this DateTime d)
        {
            var jan1St1970 = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return (long)((DateTime.UtcNow - jan1St1970).TotalMilliseconds);
        }

        public static string TimeStamp()
        {
            DateTime dt = DateTime.Now;
            DateTime dt1 = new DateTime(0x7b2, 1, 1, 0, 0, 0);
            TimeSpan ts = new TimeSpan();
            ts = (TimeSpan)(dt - dt1);
            double tt = ts.TotalMilliseconds;
            return tt.ToString("0");
        }

        public static DateTime TimeStampToDate(double date)
        {
            var dt = new DateTime(0x7b2, 1, 1, 0, 0, 0).AddMilliseconds(date);
            return dt;
        }
    }
}
