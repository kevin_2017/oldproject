﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Web;

namespace CE.Utility.IO
{
    public class FileUploadUtil
    {

        /// <summary>
        /// 获取上传文件存放目录。
        /// </summary>
        /// <param name="DirectoryPath">存放文件的物理路径。</param>
        /// <returns>返回存放文件的目录。</returns>
        public static string GetSaveDirectory(string DirectoryPath)
        {
            if (!Directory.Exists(DirectoryPath))  // 判断当前目录是否存在。
            {
                Directory.CreateDirectory(DirectoryPath);  // 建立上传文件存放目录。
            }
            return DirectoryPath;
        }

        /// <summary>
        /// 获取文件扩展名
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static string GetExtension(string fileName)
        {
            try
            {
                int startPos = fileName.LastIndexOf(".");
                string ext = fileName.Substring(startPos, fileName.Length - startPos);
                return ext;
            }
            catch
            {
                return string.Empty;
            }
        }
        /// <summary>
        /// 获取不带扩展名的文件名
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static string GetFileName(string fileName)
        {
            try
            {
                fileName = fileName.Replace(@"/", @"\");
                int startPos = fileName.LastIndexOf(@"\");
                int endPos = fileName.LastIndexOf(@".");
                string ext = fileName.Substring(startPos + 1, fileName.Length - (startPos + 1) - (fileName.Length - endPos));
                return ext;
            }
            catch
            {
                return string.Empty;
            }
        }
        ///<summary> 
        ///是否允许该扩展名上传 
        ///</summary> 
        ///<param name="fileName">要上传的文件扩展名,如:(.jpg,.gif)</param>
        ///<param name="flag">文件类型(image,image1,media,file,flash)</param>
        public static bool IsAllowedExtension(string fileName, string flag)
        {
            //string strOldFilePath = "";
            string strExtension = "";
            string[] arrExtension = { ".zip", ".rar" };

            //允许上传的扩展名，可以改成从配置文件中读出
            switch (flag) 
            { 
                case "image":
					arrExtension = new string[] { ".gif", ".jpg", ".jpeg", ".bmp", ".png" };
                    break;
                case "media":
					arrExtension = new string[] { ".avi", ".mpg", ".mpeg", ".mp3", ".wmv", ".wav", ".wma"};
                    break;
                case "file":
                    arrExtension = new string[] { ".docx", ".txt", ".doc", ".ppt", ".pptx", ".pdf", ".zip", ".rar", ".swf", ".gif", ".jpg", ".jpeg", ".bmp", ".png", ".xls", ".xlsx", ".rtf", ".avi", ".mpg", ".mpeg", ".mp3", ".wmv", ".wav", ".wma" ,".flv"};
                    break;
                case "flash":
					arrExtension = new string[] { ".swf" };
                    break;
                default:
                    break;
            }

            if (fileName != string.Empty)
            {
                //取得上传文件的扩展名 
                strExtension = fileName.Substring(fileName.LastIndexOf("."));
                //判断该扩展名是否合法 
                for (int i = 0; i < arrExtension.Length; i++)
                {
                    if (strExtension.ToLower().Equals(arrExtension[i]))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// 获取时间格式的文件名.
        /// </summary>
        /// <returns></returns>
        public static string GetDateName()
        {
            return DateTime.Now.ToString("yyyyMMddHHmmssffff");
        }
        /// <summary> 
        /// 删除指定文件 
        /// </summary> 
        /// <param name="strAbsolutePath">文件绝对路径</param> 
        public static void DeleteFile(string strAbsolutePath)
        {
            //判断要删除的文件是否存在 
            if (File.Exists(strAbsolutePath))
            {
                //删除文件 
                File.Delete(strAbsolutePath);
            }
        }
        /// <summary> 
        /// 删除指定文件 
        /// </summary> 
        /// <param name="strAbsolutePath">文件绝对路径</param> 
        /// <param name="strFileName">文件名</param> 
        public static void DeleteFile(string strAbsolutePath, string strFileName)
        {
            //判断路径最后有没有\符号，没有则自动加上 
            if (strAbsolutePath.LastIndexOf("\\") == strAbsolutePath.Length)
            {
                //判断要删除的文件是否存在 
                if (File.Exists(strAbsolutePath + strFileName))
                {
                    //删除文件 
                    File.Delete(strAbsolutePath + strFileName);
                }
            }
            else
            {
                if (File.Exists(strAbsolutePath + "\\" + strFileName))
                {
                    File.Delete(strAbsolutePath + "\\" + strFileName);
                }
            }
        }

        /// <summary>
        /// 重命名文件
        /// </summary>
        /// <param name="soucePath">原文件绝对地址</param> 
        /// <param name="newPath">新文件绝对地址</param> 
        public static void ReName(string soucePath, string newPath)
        {
            File.Move(soucePath, newPath);
        }

        #region 执行多文件上传
        public static string UploadFile(HttpContext context)
        {
            string UploadPath = System.Configuration.ConfigurationManager.AppSettings["UploadPath"];
            string imgFolder = DateTime.Now.ToString("yyyyMM") + "/";
            string uploadPath = UploadPath + imgFolder;
            string fileName = string.Empty;
            string Width = string.Empty;
            string Height = string.Empty;
            string Size = string.Empty;
            string fileid = string.Empty;
            //如果目录不存在则创建该目录
            string srcName = string.Empty;
            string pathCurrent = string.Empty;
            FileDirectoryUtility.CreateDirectory(context.Server.MapPath(uploadPath));
            //循环要上传的文件列表
            for (int j = 0; j < context.Request.Files.Count; j++)
            {
                //得到当前文件
                HttpPostedFile uploadFile = context.Request.Files[j];
                // 如果有文件要上传
                if (uploadFile.ContentLength > 0)
                {
                    Size = (uploadFile.ContentLength / 1024).ToString();

                    string name = FileUploadUtil.GetDateName();
                    string extension = FileUploadUtil.GetExtension(uploadFile.FileName);
                    srcName = FileUploadUtil.GetFileName(uploadFile.FileName);
                    //当前路径
                    pathCurrent = uploadPath + name + extension;
                    //文件基础名
                    fileName = uploadPath + name + extension;
                    uploadFile.SaveAs(context.Server.MapPath(pathCurrent));
                }
            }
            return pathCurrent;
        }
        #endregion
    }
}
