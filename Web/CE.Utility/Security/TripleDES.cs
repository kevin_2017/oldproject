﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using CE.Utility.Text;

namespace CE.Utility.Security
{
    public class TripleDES
    {
        private static byte[] NULL_IV = Convert.FromBase64String("AAAAAAAAAAA=");
        private static byte[] KEY = Convert.FromBase64String("GU457nLh66tPb4tO2fQJGx8qGbshNGv2");

        private TripleDES() { }


        /// <summary>
        /// 加密
        /// </summary>
        /// <param name="data">被加密的字符串</param>
        /// <returns></returns>
        public static string Encrypt(string data)
        {
            return Convert.ToBase64String(
                Encrypt(Encoding.UTF8.GetBytes(data), KEY, NULL_IV)
                );
        }
        /// <summary>
        /// 加密
        /// </summary>
        /// <param name="data">被加密的字符串</param>
        /// <param name="base64Key">base64编码过的Key</param>
        /// <param name="base64edIv">base64编码过的向量, 如果传递null, 则使用默认的向量"AAAAAAAAAAA="</param>
        /// <returns></returns>
        public static string Encrypt(string data, string base64Key, string base64edIv)
        {
            return Convert.ToBase64String(
                Encrypt(Encoding.UTF8.GetBytes(data)
                , Convert.FromBase64String(base64Key)
                , base64edIv == null ? NULL_IV : Convert.FromBase64String(base64edIv)));
        }

        /// <summary>
        /// 加密
        /// </summary>
        /// <param name="dataBytes"></param>
        /// <param name="keyBytes"></param>
        /// <param name="ivBytes"></param>
        /// <returns></returns>
        public static byte[] Encrypt(byte[] dataBytes, byte[] keyBytes, byte[] ivBytes)
        {
            TripleDESCryptoServiceProvider des = new TripleDESCryptoServiceProvider();
            des.Mode = CipherMode.CBC;
            des.BlockSize = 64;
            des.Padding = PaddingMode.PKCS7;
            des.Key = keyBytes;
            des.IV = ivBytes;

            MemoryStream stream = new MemoryStream();

            CryptoStream encStream = new CryptoStream(stream, des.CreateEncryptor(), CryptoStreamMode.Write);
            encStream.Write(dataBytes, 0, dataBytes.Length);
            encStream.FlushFinalBlock();
            return stream.ToArray();
        }
        /// <summary>
        /// 解密
        /// </summary>
        /// <param name="data">被解密的字符串</param>
        /// <returns></returns>
        public static string Decrypt(string data)
        {
            if (string.IsNullOrEmpty(data))
                return string.Empty;
            else
                return Encoding.UTF8.GetString(Decrypt(Convert.FromBase64String(data), KEY, NULL_IV));
        }
        /// <summary>
        /// 解密
        /// </summary>
        /// <param name="data">被解密的字符串</param>
        /// <param name="base64edKey">base64编码过的Key</param>
        /// <param name="base64edIv">base64编码过的向量, 如果传递null, 则使用默认的向量"AAAAAAAAAAA="</param>
        /// <returns></returns>
        public static string Decrypt(string data, string base64edKey, string base64edIv)
        {

            return Encoding.UTF8.GetString(
                Decrypt(Convert.FromBase64String(data), Convert.FromBase64String(base64edKey), base64edIv == null ? NULL_IV : Convert.FromBase64String(base64edIv)));
        }
        /// <summary>
        /// 解密
        /// </summary>
        /// <param name="dataBytes"></param>
        /// <param name="keyBytes"></param>
        /// <param name="ivBytes"></param>
        /// <returns></returns>
        public static byte[] Decrypt(byte[] dataBytes, byte[] keyBytes, byte[] ivBytes)
        {
            TripleDESCryptoServiceProvider des = new TripleDESCryptoServiceProvider();
            des.Mode = CipherMode.CBC;
            des.BlockSize = 64;
            des.Padding = PaddingMode.PKCS7;
            des.Key = keyBytes;
            des.IV = ivBytes;

            MemoryStream stream = new MemoryStream();

            CryptoStream encStream = new CryptoStream(stream, des.CreateDecryptor(), CryptoStreamMode.Write);
            encStream.Write(dataBytes, 0, dataBytes.Length);
            encStream.FlushFinalBlock();
            return stream.ToArray();

        }
    }
}
