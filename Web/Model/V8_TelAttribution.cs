﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class V8_TelAttribution
    {

        /// <summary>
        /// ID
        /// </summary>		

        public long ID { get; set; }

        /// <summary>
        /// PhoneNum
        /// </summary>		

        public string PhoneNum { get; set; }

        /// <summary>
        /// NO
        /// </summary>		

        public decimal NO { get; set; }

        /// <summary>
        /// AREA
        /// </summary>		

        public string AREA { get; set; }

        /// <summary>
        /// TYPE
        /// </summary>		

        public string TYPE { get; set; }

        /// <summary>
        /// ProvinceCode
        /// </summary>		

        public long ProvinceCode { get; set; }

        /// <summary>
        /// CityCode
        /// </summary>		

        public long CityCode { get; set; }

        /// <summary>
        /// IsDelete
        /// </summary>		

        public int IsDelete { get; set; }

        /// <summary>
        /// 运营商类型
        /// </summary>
        public int ISPType { get; set; }
    }
}