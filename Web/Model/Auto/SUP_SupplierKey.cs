﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{/// <summary>
 /// SUP_SupplierKey:实体类(属性说明自动提取数据库字段的描述信息)
 /// </summary>
    [Serializable]
    public partial class SUP_SupplierKey
    {
        public SUP_SupplierKey()
        { }
        #region Model
     
        /// <summary>
        /// 
        /// </summary>
        public long ID
        {
            set;
            get;
        }
        /// <summary>
        /// 
        /// </summary>
        public long SupplierID
        {
            set;
            get;
        }
        /// <summary>
        /// 
        /// </summary>
        public string SupplierName
        {
           set;
            get;
        }
        /// <summary>
        /// 
        /// </summary>
        public string ConfigCode
        {
            set;
            get;
        }
        /// <summary>
        /// 
        /// </summary>
        public string ConfigName
        {
            set;
            get;
        }
        /// <summary>
        /// 
        /// </summary>
        public string ConfigTitle
        {
            set;
            get;
        }
        #endregion Model

    }
}

