﻿using System; 
using System.Text;
using System.Collections.Generic; 
using System.Data;
namespace Model
{
		/// <summary>
 	///系统用户操作日志
 	 /// </summary>
		public class SYS_OperatLog
	{
        /// <summary>
        /// 是否删除0、未删除，1、已删除
        /// </summary>
        public int IsDelete
        {
            get;
            set;
        }
      	/// <summary>
		/// ID
        /// </summary>		
		
        public long ID {get;set;}
        
		/// <summary>
		/// UserID
        /// </summary>		
		
        public long UserID {get;set;}
        
		/// <summary>
		/// 操作IP
        /// </summary>		
		
        public string OperateIP {get;set;}
        
		/// <summary>
		/// 操作时间
        /// </summary>		
		
        public DateTime OperateTime {get;set;}
        
		/// <summary>
		/// 操作描述
        /// </summary>		
		
        public string OperateDescriptions {get;set;}
        
		   
	}
}