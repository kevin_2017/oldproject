﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
namespace Model
{
    /// <summary>
    ///MS_OperationLog
    /// </summary>
    public class MS_OperationLog
    {
        /// <summary>
        /// 是否删除0、未删除，1、已删除
        /// </summary>
        public int IsDelete
        {
            get;
            set;
        }
        /// <summary>
        /// ID
        /// </summary>		

        public long ID { get; set; }

        /// <summary>
        /// 网店ID
        /// </summary>		

        public long BranchID { get; set; }

        /// <summary>
        /// 网店用户ID
        /// </summary>		

        public long UserID { get; set; }

        /// <summary>
        /// 操作时间
        /// </summary>		

        public DateTime OperationTime { get; set; }

        /// <summary>
        /// 操作IP
        /// </summary>		

        public string OperationIP { get; set; }

        /// <summary>
        /// 操作详情
        /// </summary>		

        public string Info { get; set; }


    }
}
/*------ 代码生成时出现错误: ------
C:\Program Files (x86)\Maticsoft\Codematic2\Template\TemplateFile\简单三层模板\FinalModel.cmt(0,0) : warning CS0219: 正在编译转换: 变量“exNameSpace”已赋值，但其值从未使用过
*/