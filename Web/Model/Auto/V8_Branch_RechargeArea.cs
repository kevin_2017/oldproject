﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
namespace Model
{
    /// <summary>
    ///代理可充区域管理
    /// </summary>	
    public class V8_Branch_RechargeArea
    {

        /// <summary>
        /// ID
        /// </summary>		 
        public long Id
        {
            get;
            set;
        }
        /// <summary>
        /// 代理商ID
        /// </summary>		 
        public long BranchId
        {
            get;
            set;
        }
        /// <summary>
        /// 代理编码
        /// </summary>		 
        public string BranchCode
        {
            get;
            set;
        }

        /// <summary>
        /// 1、联通，2、移动，3、电信
        /// </summary>		 
        public int IspType
        {
            get;
            set;
        }

        /// <summary>
        /// IspName
        /// </summary>		 
        public string IspName
        {
            get;
            set;
        }
        /// <summary>
        /// 代理商品ID
        /// </summary>		 
        public long BranchProductId
        {
            get;
            set;
        }

        public int ProvinceId { get; set; }


        public string ProvinceName { get; set; }


        public int CityId { get; set; }


        /// <summary>
        /// 城市名称
        /// </summary>		

        public string CityName { get; set; }
    }
}