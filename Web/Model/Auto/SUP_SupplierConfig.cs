﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    ///SUP_SupplierConfig
    /// </summary>
    public class SUP_SupplierConfig
    {
        /// <summary>
        /// 是否删除0、未删除，1、已删除
        /// </summary>
        public int IsDelete
        {
            get;
            set;
        }
        /// <summary>
        /// ID
        /// </summary>		

        public long ID { get; set; }

        /// <summary>
        /// 供应商ID
        /// </summary>		

        public long SupplierID { get; set; }

        /// <summary>
        /// 配置名称
        /// </summary>		

        public string ConfigName { get; set; }

        /// <summary>
        /// 值
        /// </summary>		

        public string ConfigValue { get; set; }

        /// <summary>
        /// 备注
        /// </summary>		

        public string Remark { get; set; }


    }
}