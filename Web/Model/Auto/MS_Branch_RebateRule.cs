﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
namespace Model
{
    /// <summary>
    ///代理返利规则
    /// </summary>	
    public class MS_Branch_RebateRule
    {

        /// <summary>
        /// ID
        /// </summary>		 
        public long ID
        {
            get;
            set;
        }
        /// <summary>
        /// 代理商ID
        /// </summary>		 
        public long BranchID
        {
            get;
            set;
        }
        /// <summary>
        /// 代理名称
        /// </summary>		 
        public string BranchName
        {
            get;
            set;
        }
        /// <summary>
        /// 商品类型ID
        /// </summary>		 
        public long ProductTypeId
        {
            get;
            set;
        }
        /// <summary>
        /// 商品类型名称
        /// </summary>		 
        public string ProductTypeName
        {
            get;
            set;
        }

        /// <summary>
        /// 运营商类型（1、中国联通，2、中国移动，3、中国电信）
        /// </summary>		 
        public int IspType
        {
            get;
            set;
        }
        /// <summary>
        /// 运营商名称
        /// </summary>		 
        public string IspName
        {
            get;
            set;
        }

        /// <summary>
        /// 返利方式（1、金额， 2、比例）
        /// </summary>		 
        public int RebateMethod
        {
            get;
            set;
        }
        /// <summary>
        /// 返利值
        /// </summary>		 
        public decimal Rebate
        {
            get;
            set;
        }
        /// <summary>
        /// 最小充值金额
        /// </summary>		 
        public decimal MinMoney
        {
            get;
            set;
        }/// <summary>
         /// 最大充值金额
         /// </summary>		 
        public decimal MaxMoney
        {
            get;
            set;
        }
        /// <summary>
        /// 排序（序号越大，越先使用）
        /// </summary>		 
        public long Sort
        {
            get;
            set;
        }

        /// <summary>
        /// IsDelete
        /// </summary>		 
        public int IsDelete
        {
            get;
            set;
        }

        /// <summary>
        /// 添加时间
        /// </summary>		 
        public DateTime CreateTime
        {
            get;
            set;
        }
        /// <summary>
        /// 操作人
        /// </summary>		 
        public string Creator
        {
            get;
            set;
        }
        /// <summary>
        /// 更新时间
        /// </summary>		 
        public DateTime UpdateTime
        {
            get;
            set;
        }
        /// <summary>
        /// 更新人
        /// </summary>		 
        public string Updator
        {
            get;
            set;
        }


        /// <summary>
        /// 该返利规则的返利省份
        /// </summary>
        public string ProvinceIds { get; set; }

    }
}