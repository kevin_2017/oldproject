﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    ///订单管理
    /// </summary>	
    public class V8_OrderManage
    {

        /// <summary>
        /// ID
        /// </summary>		 
        public long ID
        {
            get;
            set;
        }
        /// <summary>
        /// 系统订单号
        /// </summary>		 
        public string OrderNo
        {
            get;
            set;
        }
        /// <summary>
        /// 商品ID
        /// </summary>		 
        public long ProductID
        {
            get;
            set;
        }
        /// <summary>
        /// 商品名称
        /// </summary>		 
        public string ProductName
        {
            get;
            set;
        }
        /// <summary>
        /// 1、联通，2、移动，3、电信
        /// </summary>		 
        public int IspType
        {
            get;
            set;
        }
        /// <summary>
        /// IspName
        /// </summary>		 
        public string IspName
        {
            get;
            set;
        }
        /// <summary>
        /// 号码类型1、座机，2、手机
        /// </summary>		 
        public int PhoneType
        {
            get;
            set;
        }
        /// <summary>
        /// 订单来源（ECARD_MOBILE：商城直充，ECARD_FLOWCHARGE：商城流量充值，TBCP_MOBILE：充值平台，FIXEDLINE：固定电话充值，NETWORK ：有线宽带充值）
        /// </summary>		 
        public string ComFrom
        {
            get;
            set;
        }
        /// <summary>
        /// 下游代理商ID
        /// </summary>		 
        public long BranchID
        {
            get;
            set;
        }


        public string BranchCode { get; set; }

        /// <summary>
        /// 代理名称
        /// </summary>		 
        public string BranchName
        {
            get;
            set;
        }
        /// <summary>
        /// 充值账号
        /// </summary>		 
        public string RechargeNo
        {
            get;
            set;
        }
        /// <summary>
        /// 号码归属地（V8_TelAttribution）ID
        /// </summary>		 
        public long AttributionID
        {
            get;
            set;
        }
        /// <summary>
        /// 商户订单号
        /// </summary>		 
        public string BranchOrderNo
        {
            get;
            set;
        }
        /// <summary>
        /// 当前状态(1、充值成功，2、等待充值，3、充值中，4、问题订单，5、充值失败)
        /// </summary>		 
        public int CurentStatus
        {
            get;
            set;
        }
        /// <summary>
        /// 商品数量
        /// </summary>		 
        public int Counts
        {
            get;
            set;
        }
        /// <summary>
        /// 商品单价
        /// </summary>		 
        public decimal UnitPrice
        {
            get;
            set;
        }
        /// <summary>
        /// 产品总金额
        /// </summary>		 
        public decimal TotalPrice
        {
            get;
            set;
        }
        /// <summary>
        /// 实际扣费金额
        /// </summary>		 
        public decimal RechargeMoney
        {
            get;
            set;
        }
        /// <summary>
        /// 代理商折扣方式（1、折扣率,2、加减值,3、固定值）
        /// </summary>		 
        public int DiscountMethod
        {
            get;
            set;
        }
        /// <summary>
        /// 该代理商商品售价
        /// </summary>		 
        public decimal ProductSalePrice
        {
            get;
            set;
        }
        /// <summary>
        /// 订单创建时间
        /// </summary>		 
        public DateTime CreateTime
        {
            get;
            set;
        }
        /// <summary>
        /// 订单处理时间
        /// </summary>		 
        public DateTime OperationTime
        {
            get;
            set;
        }
        /// <summary>
        /// 上游通知回调时间
        /// </summary>		 
        public DateTime CallBackTime
        {
            get;
            set;
        }
        /// <summary>
        /// 操作人
        /// </summary>		 
        public string Operator
        {
            get;
            set;
        }
        /// <summary>
        /// 当前状态描述
        /// </summary>		 
        public string Descriptions
        {
            get;
            set;
        }
        /// <summary>
        /// 要求得到充值结果的时间限制，单位为秒，以timeStart为计时起点
        /// </summary>		 
        public int TimeLimit
        {
            get;
            set;
        }
        /// <summary>
        /// 系统流水号
        /// </summary>		 
        public string SysSerialNum
        {
            get;
            set;
        }
        /// <summary>
        /// 商户流水号
        /// </summary>		 
        public string BranchSerialNum
        {
            get;
            set;
        }
        /// <summary>
        /// 是否返销（1、返销，0、未返销）
        /// </summary>		 
        public int IsBuyBack
        {
            get;
            set;
        }
        /// <summary>
        /// IsDelete
        /// </summary>		 
        public int IsDelete
        {
            get;
            set;
        }
        /// <summary>
        /// 充值请求地址
        /// </summary>		 
        public string ClientIp
        {
            get;
            set;
        }
        /// <summary>
        /// 通知地址
        /// </summary>		 
        public string NotifyUrl
        {
            get;
            set;
        }
        /// <summary>
        /// 扩展参数
        /// </summary>		 
        public string ExtendParam
        {
            get;
            set;
        }
        /// <summary>
        /// 通知次数
        /// </summary>		 
        public int NoticeCount
        {
            get;
            set;
        }
        /// <summary>
        /// 通知状态（0、未通知，1、通知成功，2、通知失败，3、通知中）
        /// </summary>		 
        public int NotifyStatus
        {
            get;
            set;
        }

        /// <summary>
        /// 当前充值请求的ID
        /// </summary>
        public long CurrentRequestId { get; set; }

        public int ProvinceId { get; set; }

        public string ProvinceName { get; set; }

        public int CityId { get; set; }

        public string CityName { get; set; }
         
    }
}