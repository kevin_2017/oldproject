﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
namespace Model
{
    /// <summary>
    ///系统权限按钮管理
    /// </summary>
    public class SYS_AuthButton
    {
        /// <summary>
        /// 是否删除0、未删除，1、已删除
        /// </summary>
        public int IsDelete
        {
            get;
            set;
        }
        /// <summary>
        /// ID
        /// </summary>		

        public long ID { get; set; }

        /// <summary>
        /// 权限ID
        /// </summary>		

        public long AuthID { get; set; }

        /// <summary>
        /// 按钮名称
        /// </summary>		

        public string ButtonName { get; set; }

        /// <summary>
        /// 显示名称
        /// </summary>		

        public string DisplayName { get; set; }

        /// <summary>
        /// 是否可用
        /// </summary>		

        public bool IsEnable { get; set; }


    }
}