﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    /// <summary>
    ///V8_OrderNotifyRecs
    /// </summary>	
    public class V8_OrderNotifyRecs
    {

        /// <summary>
        /// ID
        /// </summary>		 
        public long ID
        {
            get;
            set;
        }
        /// <summary>
        /// 订单号
        /// </summary>		 
        public string OrderNo
        {
            get;
            set;
        }
        /// <summary>
        /// 代理ID
        /// </summary>		 
        public long BranchId
        {
            get;
            set;
        }
        /// <summary>
        /// 通知下游时间
        /// </summary>		 
        public DateTime NotifyTime
        {
            get;
            set;
        }
        /// <summary>
        /// 通知结果
        /// </summary>		 
        public int NotifyResult
        {
            get;
            set;
        }
        /// <summary>
        /// 通知内容
        /// </summary>		 
        public string NotifyConent
        {
            get;
            set;
        }
        /// <summary>
        /// 通知返回信息
        /// </summary>		 
        public string NotifyResponse
        {
            get;
            set;
        }

    }
}
