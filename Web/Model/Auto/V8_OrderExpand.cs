﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public partial class V8_OrderExpand
    {
        public V8_OrderExpand()
        { }
        #region Model

        /// <summary>
        /// ID
        /// </summary>		

        public long ID { get; set; }

        /// <summary>
        /// 订单号ID
        /// </summary>		

        public long OrderID { get; set; }

        /// <summary>
        /// 缴费请求源数据
        /// </summary>		

        public string RequestSourceData { get; set; }

        /// <summary>
        /// 缴费响应源数据
        /// </summary>		

        public string ResponseSourceData { get; set; }

        /// <summary>
        /// 回调返回数据
        /// </summary>		

        public string NotifyResponeData { get; set; }

        /// <summary>
        /// IsDelete
        /// </summary>		

        public int IsDelete { get; set; }

        /// <summary>
        /// 真正用于充值的商品ID
        /// </summary>		

        public long RealyProductID { get; set; }

        /// <summary>
        /// 供应商ID
        /// </summary>		 
        public long SupplierID
        {
            get;
            set;
        }

        /// <summary>
        /// 供应商商品ID
        /// </summary>		 
        public long SupProductID
        {
            get;
            set;
        }

        /// <summary>
        /// 当前状态(1、充值成功，2、等待充值，3、充值中，4、问题订单，5、特供慢充，6、成功通知，7、充值失败)
        /// </summary>
        public int Result { get; set; }

        /// <summary>
        /// 充值请求时间
        /// </summary>
        public DateTime RequestTime { get; set; }


        /// <summary>
        /// 通知时间
        /// </summary>
        public DateTime CallbackTime { get; set; }


        #endregion Model

    }
}

