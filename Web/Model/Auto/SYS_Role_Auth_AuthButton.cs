﻿using System; 
using System.Text;
using System.Collections.Generic; 
using System.Data;
namespace Model
{
		/// <summary>
 	///角色权限按钮
 	 /// </summary>
		public class SYS_Role_Auth_AuthButton
	{
        /// <summary>
        /// 是否删除0、未删除，1、已删除
        /// </summary>
        public int IsDelete
        {
            get;
            set;
        }
      	/// <summary>
		/// ID
        /// </summary>		
		
        public long ID {get;set;}
        
		/// <summary>
		/// 角色权限表ID
        /// </summary>		
		
        public long RAID {get;set;}
        
		/// <summary>
		/// 权限按钮表ID
        /// </summary>		
		
        public long ABID {get;set;}
        
		   
	}
}