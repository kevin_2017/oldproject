﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
namespace Model
{
    /// <summary>
    ///商品管理
    /// </summary>	
    public class V8_Product_Manage
    {

        /// <summary>
        /// ID
        /// </summary>		 
        public long ID
        {
            get;
            set;
        }
        /// <summary>
        /// 商品类型ID
        /// </summary>		 
        public long ProductTypeID
        {
            get;
            set;
        }
        /// <summary>
        /// 商品类型名称
        /// </summary>		 
        public string ProductTypeName
        {
            get;
            set;
        }
        /// <summary>
        /// 商品名称
        /// </summary>		 
        public string ProductName
        {
            get;
            set;
        }
        /// <summary>
        /// 产品面值
        /// </summary>		 
        public decimal denomination
        {
            get;
            set;
        }
        /// <summary>
        /// 当前状态（ 1、正常上架，2、全部下架）
        /// </summary>		 
        public int Status
        {
            get;
            set;
        }
        /// <summary>
        /// 运营商类型（1、中国联通，2、中国移动，3、中国电信）
        /// </summary>		 
        public int ISPType
        {
            get;
            set;
        }
        /// <summary>
        /// 运营商名称
        /// </summary>		 
        public string ISPName
        {
            get;
            set;
        }
        /// <summary>
        /// 号码类型（1、手机，2、固话）
        /// </summary>		 
        public int PhoneNumType
        {
            get;
            set;
        }
        /// <summary>
        /// 添加时间
        /// </summary>		 
        public DateTime AddTime
        {
            get;
            set;
        }
        /// <summary>
        /// 充值范围(1、固定2、自定义)
        /// </summary>		 
        public int RechargeRange
        {
            get;
            set;
        }
        /// <summary>
        /// 添加时间
        /// </summary>		 
        public DateTime CreateTime
        {
            get;
            set;
        }
        /// <summary>
        /// 操作人
        /// </summary>		 
        public string Creator
        {
            get;
            set;
        }
        /// <summary>
        /// 更新时间
        /// </summary>		 
        public DateTime UpdateTime
        {
            get;
            set;
        }
        /// <summary>
        /// 更新人
        /// </summary>		 
        public string Updator
        {
            get;
            set;
        }
        /// <summary>
        /// IsDelete
        /// </summary>		 
        public int IsDelete
        {
            get;
            set;
        }

    }
}