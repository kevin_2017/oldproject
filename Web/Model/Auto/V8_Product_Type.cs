﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
namespace Model
{
    /// <summary>
    ///供应商商品类型
    /// </summary>	
    public class V8_Product_Type
    {

        /// <summary>
        /// ID
        /// </summary>		 
        public long ID
        {
            get;
            set;
        }
        /// <summary>
        /// 商品类型名称
        /// </summary>		 
        public string TypeName
        {
            get;
            set;
        }
        /// <summary>
        /// IsDelete
        /// </summary>		 
        public int IsDelete
        {
            get;
            set;
        }

    }
}