﻿using System; 
using System.Text;
using System.Collections.Generic; 
using System.Data;
namespace Model
{
		/// <summary>
 	///系统角色管理
 	 /// </summary>
		public class SYS_Role
        {  /// <summary>
            /// 是否删除0、未删除，1、已删除
            /// </summary>
            public int IsDelete
            {
                get;
                set;
            }
   		     
      	/// <summary>
		/// ID
        /// </summary>		
		
        public int ID {get;set;}
        
		/// <summary>
		/// 名字
        /// </summary>		
		
        public string RoleName {get;set;}
        
		/// <summary>
		/// 备注
        /// </summary>		
		
        public string Remark {get;set;}
        
		/// <summary>
		/// 是否为系统角色
        /// </summary>		
		
        public bool ISSystem {get;set;}
        
		   
	}
}