﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
namespace Model
{
    /// <summary>
    ///系统管理用户
    /// </summary>
    public class SYS_UserInfo
    {
        /// <summary>
        /// 是否删除0、未删除，1、已删除
        /// </summary>
        public int IsDelete
        {
            get;
            set;
        }
        /// <summary>
        /// ID
        /// </summary>

        public long ID { get; set; }

        /// <summary>
        /// 用户名
        /// </summary>

        public string UserName { get; set; }

        /// <summary>
        /// 昵称
        /// </summary>		

        public string TrueName { get; set; }

        /// <summary>
        /// 密码
        /// </summary>		

        public string PWD { get; set; }

        /// <summary>
        /// 性别(0、未知，1、男，2、女)
        /// </summary>		

        public int Sex { get; set; }

        /// <summary>
        /// 状态（0 禁用,1 正常,2  锁定,3 离职）
        /// </summary>		

        public int Status { get; set; }

        /// <summary>
        /// 上次登录IP
        /// </summary>		

        public string LastLoginIP { get; set; }

        /// <summary>
        /// 上次登录时间
        /// </summary>		

        public DateTime LastLoginTime { get; set; }

        /// <summary>
        /// 登录IP
        /// </summary>		

        public string LoginIP { get; set; }

        /// <summary>
        /// 登录时间
        /// </summary>		

        public DateTime LoginTime { get; set; }

        /// <summary>
        /// 登录次数
        /// </summary>		

        public long LoginCounts { get; set; }

        /// <summary>
        /// 是否删除
        /// </summary>		

        public int ISDelete { get; set; }

        /// <summary>
        /// 是否锁定登录（1、锁定，0、未锁定）
        /// </summary>		

        public int ISLocking { get; set; }


        public string LastLoginIPAttr { get; set; }

        public string LoginIPAttr { get; set; }

        


    }
}