﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    /// <summary>
    /// 运营商各充值接口的使用顺序
    /// </summary>
    public class V8_ISP_Supplier_SortManage
    { /// <summary>
      /// ID
      /// </summary>		 
        public long ID
        {
            get;
            set;
        }
        /// <summary>
        /// 运营商类型（1、中国联通，2、中国移动，3、中国电信）
        /// </summary>		 
        public int ISPType
        {
            get;
            set;
        }
        /// <summary>
        /// 运营商名称
        /// </summary>		 
        public string ISPName
        {
            get;
            set;
        }

        /// <summary>
        /// 供应商ID
        /// </summary>		 
        public long SupplierID
        {
            get;
            set;
        }
        /// <summary>
        /// 供应商名称
        /// </summary>		 
        public string SupplierName
        {
            get;
            set;
        }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// 添加时间
        /// </summary>		 
        public DateTime CreateTime
        {
            get;
            set;
        }
    }
}
