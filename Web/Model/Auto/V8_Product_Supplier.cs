﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
namespace Model
{
    /// <summary>
    ///V8_Product_Supplier
    /// </summary>	
    public class V8_Product_Supplier
    {

        /// <summary>
        /// ID
        /// </summary>		 
        public long ID
        {
            get;
            set;
        }
        /// <summary>
        /// 供应商ID
        /// </summary>		 
        public long SupplierID
        {
            get;
            set;
        }
        /// <summary>
        /// 供应商名称
        /// </summary>		 
        public string SupplierName
        {
            get;
            set;
        }
        /// <summary>
        /// 商品类型ID
        /// </summary>		 
        public long ProductTypeId
        {
            get;
            set;
        }
        /// <summary>
        /// 商品类型名称
        /// </summary>		 
        public string ProductTypeName
        {
            get;
            set;
        }
        /// <summary>
        /// 商品ID
        /// </summary>		 
        public long ProductID
        {
            get;
            set;
        }
        /// <summary>
        /// 商品名称
        /// </summary>		 
        public string ProductName
        {
            get;
            set;
        }
        /// <summary>
        /// 运营商类型（1、中国联通，2、中国移动，3、中国电信）
        /// </summary>		 
        public int ISPType
        {
            get;
            set;
        }
        /// <summary>
        /// 运营商名称
        /// </summary>		 
        public string ISPName
        {
            get;
            set;
        }
        /// <summary>
        /// 商品进价
        /// </summary>		 
        public decimal Inprice
        {
            get;
            set;
        }
        /// <summary>
        /// 排序（序号越大，越先使用）
        /// </summary>		 
        public long Sort
        {
            get;
            set;
        }
        /// <summary>
        /// 当前商品状态(是否被关闭1、关闭，0、未关闭)
        /// </summary>		 
        public int IsClosed
        {
            get;
            set;
        }
        /// <summary>
        /// IsDelete
        /// </summary>		 
        public int IsDelete
        {
            get;
            set;
        }
        /// <summary>
        /// 到帐方式（0，当月到帐，1、次月到帐）
        /// </summary>		 
        public int ArriveMethod
        {
            get;
            set;
        }
        /// <summary>
        /// 供应商产品编码
        /// </summary>		 
        public string SupplierProductCode
        {
            get;
            set;
        }
        /// <summary>
        /// 省份(V8_Province)IDs(','分隔)
        /// </summary>		 
        public string ProvinceIds
        {
            get;
            set;
        }
        
        /// <summary>
        /// 流量类型（0、无，1、本地，2、全国）
        /// </summary>		 
        public int FlowType
        {
            get;
            set;
        }
        /// <summary>
        /// 添加时间
        /// </summary>		 
        public DateTime CreateTime
        {
            get;
            set;
        }
        /// <summary>
        /// 操作人
        /// </summary>		 
        public string Creator
        {
            get;
            set;
        }
        /// <summary>
        /// 更新时间
        /// </summary>		 
        public DateTime UpdateTime
        {
            get;
            set;
        }
        /// <summary>
        /// 更新人
        /// </summary>		 
        public string Updator
        {
            get;
            set;
        }

    }
}