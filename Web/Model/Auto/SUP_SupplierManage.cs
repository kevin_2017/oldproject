﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
namespace Model
{
    [Serializable]
    public partial class SUP_SupplierManage
    {
        public SUP_SupplierManage()
        { }
        #region Model

        /// <summary>
        /// 
        /// </summary>
        public long ID
        {
            set;
            get;
        }


        /// <summary>
        /// 上游供应商编码
        /// </summary>
        public string ApiCode { get; set; }

        /// <summary>
        /// 登录用户名
        /// </summary>
        public string UserName
        {
            set;
            get;
        }
        /// <summary>
        /// 登录密码
        /// </summary>
        public string PassWord
        {
            set;
            get;
        }
        /// <summary>
        /// 供应商名
        /// </summary>
        public string TrueName
        {
            set;
            get;
        }
        /// <summary>
        /// 状态（1001、开通，1002、暂停，1003、停用,1004、暂停获取）
        /// </summary>
        public int? Status
        {
            set;
            get;
        }
        /// <summary>
        /// 用户类型（1001、接口用户,1002、普通用户）
        /// </summary>
        public int? UserType
        {
            set;
            get;
        }
        /// <summary>
        /// 可充值号码段(可以输入多个，但必须以英文字符的逗号进行分割)
        /// </summary>
        public string RechargeNum
        {
            set;
            get;
        }
        /// <summary>
        /// 不能充值号码段(可以输入多个，但必须以英文字符的逗号进行分割)
        /// </summary>
        public string NotRechargeNum
        {
            set;
            get;
        }
        /// <summary>
        /// 可用余额(加密方式：实际余额*ID*100)
        /// </summary>
        public decimal? Balance
        {
            set;
            get;
        }
        /// <summary>
        /// 联系人
        /// </summary>
        public string Contactor
        {
            set;
            get;
        }
        /// <summary>
        /// 联系电话
        /// </summary>
        public string ContactPhone
        {
            set;
            get;
        }
        /// <summary>
        /// 查询余额地址
        /// </summary>
        public string Address
        {
            set;
            get;
        }
        /// <summary>
        /// 当前商品状态(是否被关闭1、关闭，0、未关闭)
        /// </summary>
        public int? IsCloseProduct
        {
            set;
            get;
        }
        /// <summary>
        /// 请求网关地址
        /// </summary>
        public string GateWay
        {
            set;
            get;
        }
        /// <summary>
        /// 充值支付地址
        /// </summary>
        public string RechargePayDomain
        {
            set;
            get;
        }
        /// <summary>
        /// 充值查询地址
        /// </summary>
        public string RechargeSearchUrl
        {
            set;
            get;
        }
        /// <summary>
        /// 充值异步通知地址
        /// </summary>
        public string RechargeNoticeUrl
        {
            set;
            get;
        }
        /// <summary>
        /// 流量充值地址
        /// </summary>
        public string FlowPayDomain
        {
            set;
            get;
        }
        /// <summary>
        /// 流量查询
        /// </summary>
        public string FlowSearchUrl
        {
            set;
            get;
        }
        /// <summary>
        /// 流量异步通知
        /// </summary>
        public string FlowNoticeUrl
        {
            set;
            get;
        }
        /// <summary>
        /// 
        /// </summary>
        public int? IsDelete
        {
            set;
            get;
        }
        /// <summary>
        /// 话费充值笔数
        /// </summary>
        public long? RechargeCount
        {
            set;
            get;
        }
        /// <summary>
        /// 流量充值最大笔数
        /// </summary>
        public long? FlowCount
        {
            set;
            get;
        }
        public int Sort
        {
            set;
            get;
        }

        /// <summary>
        /// 座机话费充值地址
        /// </summary>
        public string TelPhonePayDomain { get; set; }

        #endregion Model

    }
}

