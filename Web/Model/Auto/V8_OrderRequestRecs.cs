﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    /// <summary>
    ///V8_OrderRequestRecs
    /// </summary>	
    public class V8_OrderRequestRecs
    {

        /// <summary>
        /// ID
        /// </summary>		 
        public long ID
        {
            get;
            set;
        }
        /// <summary>
        /// 订单号
        /// </summary>		 
        public string OrderNo
        {
            get;
            set;
        }
        /// <summary>
        /// 供应商Id
        /// </summary>		 
        public long SupplierId
        {
            get;
            set;
        }
        /// <summary>
        /// 上游商品ID
        /// </summary>		 
        public long ProSuppId
        {
            get;
            set;
        }
        /// <summary>
        /// 请求内容
        /// </summary>		 
        public string RequestContent
        {
            get;
            set;
        }
        /// <summary>
        /// 请求时间
        /// </summary>		 
        public DateTime RequestTime
        {
            get;
            set;
        }
        /// <summary>
        /// 充值请求返回内容
        /// </summary>		 
        public string ResponseContent
        {
            get;
            set;
        }
        /// <summary>
        /// 通知结果
        /// </summary>		 
        public int NotifyResult
        {
            get;
            set;
        }
        /// <summary>
        /// 通知时间
        /// </summary>		 
        public DateTime NotifyTime
        {
            get;
            set;
        }

        /// <summary>
        /// 通知返回内容
        /// </summary>
        public string NoticeContent { get; set; }

    }
}
