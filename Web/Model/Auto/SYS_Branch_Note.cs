﻿using System; 
using System.Text;
using System.Collections.Generic; 
using System.Data;
namespace Model
{
		/// <summary>
 	///网店通知关系
 	 /// </summary>
		public class SYS_Branch_Note
	{
        /// <summary>
        /// 是否删除0、未删除，1、已删除
        /// </summary>
        public int IsDelete
        {
            get;
            set;
        }
      	/// <summary>
		/// ID
        /// </summary>		
		
        public long ID {get;set;}
        
		/// <summary>
		/// 下游网店ID
        /// </summary>		
		
        public long BranchID {get;set;}
        
		/// <summary>
		/// 通知ID
        /// </summary>		
		
        public long NoteID {get;set;}
        
		/// <summary>
		/// 阅读次数
        /// </summary>		
		
        public int ReadCount {get;set;}
        
		   
	}
}