﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    /// <summary>
    /// 代理充值供应商
    /// </summary>
    public class MS_Branch_Supplier
    { /// <summary>
      /// ID
      /// </summary>		 
        public long ID
        {
            get;
            set;
        }
        /// <summary>
        /// 代理商ID
        /// </summary>		 
        public long BranchID
        {
            get;
            set;
        }
        /// <summary>
        /// 代理名称
        /// </summary>		 
        public string BranchName
        {
            get;
            set;
        }

        /// <summary>
        /// 供应商ID
        /// </summary>		 
        public long SupplierID
        {
            get;
            set;
        }
        /// <summary>
        /// 供应商名称
        /// </summary>		 
        public string SupplierName
        {
            get;
            set;
        }

        public int Sort { get; set; }
    }
}
