﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
namespace Model
{
    /// <summary>
    ///代理基本信息
    /// </summary>
    public class MS_BranchManage
    {  /// <summary>
       /// 是否删除0、未删除，1、已删除
       /// </summary>
        public int IsDelete
        {
            get;
            set;
        }

        /// <summary>
        /// ID
        /// </summary>		

        public long ID { get; set; }

        /// <summary>
        /// 代理编码
        /// </summary>
        public string BranchCode { get; set; }
        /// <summary>
        /// 网店名称
        /// </summary>		

        public string BranchName { get; set; }

        /// <summary>
        /// 联系人
        /// </summary>		

        public string StoreKeeper { get; set; }

        /// <summary>
        /// 联系方式
        /// </summary>		

        public string Phones { get; set; }

        /// <summary>
        /// 联系地址
        /// </summary>		

        public string Address { get; set; }

        /// <summary>
        /// 当前状态（1、正常，2、暂停，3、停店）
        /// </summary>		

        public int Status { get; set; }

        /// <summary>
        /// 终端数量
        /// </summary>		

        public int ClientCount { get; set; }

        /// <summary>
        /// 员工数
        /// </summary>		

        public int EmployeeCount { get; set; }

        /// <summary>
        /// 已充值金额
        /// </summary>		

        public decimal RechargeMOney { get; set; }

        /// <summary>
        /// 当前余额
        /// </summary>		

        public decimal Balance { get; set; }

        /// <summary>
        /// 冻结金额
        /// </summary>
        public decimal FrozenMoney { get; set; }

        /// <summary>
        /// 结算方式(1、普通，2、按日，3、签约，4、年租)
        /// </summary>		

        public int SettlementMethod { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>		

        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 代理等级(越小越高)
        /// </summary>		

        public int Level { get; set; }

        /// <summary>
        /// 是否为系统添加用户（0、不是，1、是），不是系统用户是parentID为0
        /// </summary>		

        public int ISSystem { get; set; }

        /// <summary>
        /// 上级代理（0为一级代理）
        /// </summary>		

        public long ParentID { get; set; }


        /// <summary>
        /// 密钥
        /// </summary>
        public string PrivateKey { get; set; }


        /// <summary>
        /// 充值方式 0、轮循,1、指定供应商
        /// </summary>
        public int RechargeType { get; set; }

        /// <summary>
        /// 后台登录密码
        /// </summary>

        public string Pwd { get; set; }


    }
}