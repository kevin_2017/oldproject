﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
namespace Model
{
    /// <summary>
    ///下级网点用户信息
    /// </summary>
    public class MS_UserInfo
    {

        /// <summary>
        /// 主键
        /// </summary>		

        public long ID { get; set; }

        /// <summary>
        /// 网点ID MS_BranchManage(外键)
        /// </summary>		

        public long BranchID { get; set; }

        /// <summary>
        /// 用户角色(1、管理员，2、普通员工)
        /// </summary>		

        public int UserRole { get; set; }

        /// <summary>
        /// 用户名
        /// </summary>		

        public string UserName { get; set; }

        /// <summary>
        /// 密码
        /// </summary>		

        public string Password { get; set; }

        /// <summary>
        /// 用户状态 (1:正常 2:锁定 3未激活，4离职)
        /// </summary>		

        public int Status { get; set; }

        /// <summary>
        /// 登录IP
        /// </summary>		

        public string LoginIP { get; set; }

        /// <summary>
        /// 上次登录IP
        /// </summary>		

        public string LastLoginIP { get; set; }

        /// <summary>
        /// 登录次数
        /// </summary>		

        public int LoginCount { get; set; }

        /// <summary>
        /// 本次登录时间
        /// </summary>		

        public DateTime LoginTime { get; set; }

        /// <summary>
        /// 上次登录时间
        /// </summary>		

        public DateTime LastLoginTime { get; set; }

        /// <summary>
        /// IsDelete
        /// </summary>		

        public int IsDelete { get; set; }


    }
}