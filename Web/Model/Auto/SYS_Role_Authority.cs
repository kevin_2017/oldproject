﻿using System; 
using System.Text;
using System.Collections.Generic; 
using System.Data;
namespace Model
{
		/// <summary>
 	///系统角色权限
 	 /// </summary>
		public class SYS_Role_Authority
	{
        /// <summary>
        /// 是否删除0、未删除，1、已删除
        /// </summary>
        public int IsDelete
        {
            get;
            set;
        }
      	/// <summary>
		/// ID
        /// </summary>		
		
        public long ID {get;set;}
        
		/// <summary>
		/// 权限ID
        /// </summary>		
		
        public long AuthorityID {get;set;}
        
		/// <summary>
		/// 角色ID
        /// </summary>		
		
        public int RoleID {get;set;}
        
		   
	}
}