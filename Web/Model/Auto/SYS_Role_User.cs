﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
namespace Model
{
    /// <summary>
    ///用户角色
    /// </summary>
    public class SYS_Role_User
    {
        /// <summary>
        /// 是否删除0、未删除，1、已删除
        /// </summary>
        public int IsDelete
        {
            get;
            set;
        }
        /// <summary>
        /// ID
        /// </summary>		

        public int ID { get; set; }

        /// <summary>
        /// 用户ID
        /// </summary>		

        public long UserID { get; set; }

        /// <summary>
        /// 用户角色
        /// </summary>		

        public int RoleID { get; set; }


    }
}