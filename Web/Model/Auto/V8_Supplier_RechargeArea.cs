﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
namespace Model
{
    /// <summary>
    ///供应商可充区域管理
    /// </summary>	
    public class V8_Supplier_RechargeArea
    {

        /// <summary>
        /// ID
        /// </summary>		 
        public long Id
        {
            get;
            set;
        }
        /// <summary>
        /// 供应商ID
        /// </summary>		 
        public long SupplierId
        {
            get;
            set;
        }
        /// <summary>
        /// 供应商编码
        /// </summary>		 
        public string SupplierName
        {
            get;
            set;
        }
        /// <summary>
        /// 1、联通，2、移动，3、电信
        /// </summary>		 
        public int IspType
        {
            get;
            set;
        }
        /// <summary>
        /// IspName
        /// </summary>		 
        public string IspName
        {
            get;
            set;
        }
        /// <summary>
        /// 省份Id
        /// </summary>
        public int ProvinceId { get; set; }


        /// <summary>
        /// 省份名称
        /// </summary>
        public string ProvinceName { get; set; }

        /// <summary>
        /// 城市Id
        /// </summary>
        public int CityId { get; set; }


        /// <summary>
        /// 城市名称
        /// </summary>		

        public string CityName { get; set; }
    }
}