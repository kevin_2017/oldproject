﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    ///订单退款管理
    /// </summary>
    public class V8_OrderBuyBackRecords
    {  /// <summary>
        /// 是否删除0、未删除，1、已删除
        /// </summary>
        public int IsDelete
        {
            get;
            set;
        }

        /// <summary>
        /// ID
        /// </summary>		

        public long ID { get; set; }

        /// <summary>
        /// 订单ID (V8_OrderManage)
        /// </summary>		

        public long OrderID { get; set; }

        /// <summary>
        /// 帐户类型(1、淘宝，2、腾讯)
        /// </summary>		

        public int BuyBackType { get; set; }

        /// <summary>
        /// 返销帐号
        /// </summary>		

        public string Accounts { get; set; }

        /// <summary>
        /// 返销金额
        /// </summary>		

        public decimal BuyBackMoney { get; set; }

        /// <summary>
        /// 当前状态（0、等待支付，1、支付成功，2、支付失败，3、查询支付宝，4、维权）
        /// </summary>		

        public int CurrentStatus { get; set; }

        /// <summary>
        /// 提交时间
        /// </summary>		

        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 处理时间
        /// </summary>		

        public DateTime OperationTime { get; set; }

        /// <summary>
        /// 处理人(SYs_userInfo)ID
        /// </summary>		

        public long OperationUser { get; set; }

        /// <summary>
        /// 备注
        /// </summary>		

        public string Remarks { get; set; }


    }
}