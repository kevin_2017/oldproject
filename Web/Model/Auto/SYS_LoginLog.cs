﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
namespace Model
{
    /// <summary>
    ///系统用户登录日志
    /// </summary>
    public class SYS_LoginLog
    {  /// <summary>
       /// 是否删除0、未删除，1、已删除
       /// </summary>
        public int IsDelete
        {
            get;
            set;
        }

        /// <summary>
        /// ID
        /// </summary>		

        public long ID { get; set; }

        /// <summary>
        /// UserID
        /// </summary>		

        public long UserID { get; set; }

        /// <summary>
        /// LoginTime
        /// </summary>		

        public DateTime LoginTime { get; set; }

        /// <summary>
        /// LoginIP
        /// </summary>		

        public string LoginIP { get; set; }

        public string LoginIpAttr { get; set; }



    }
}