﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
namespace Model
{
    /// <summary>
    ///代理充值供应商
    /// </summary>
    public class MS_Branch_RechargeSupp
    {  

        /// <summary>
        /// ID
        /// </summary>		

        public long ID { get; set; }

        public long BranchId { get;set;}

        public long SupplierId { get; set; }
    }
}