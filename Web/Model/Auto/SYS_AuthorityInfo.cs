﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
namespace Model
{
    /// <summary>
    ///系统权限详情
    /// </summary>
    public class SYS_AuthorityInfo
    {
        /// <summary>
        /// 是否删除0、未删除，1、已删除
        /// </summary>
        public int IsDelete
        {
            get;
            set;
        }
        /// <summary>
        /// ID
        /// </summary>		

        public long ID { get; set; }

        /// <summary>
        /// 分类名字
        /// </summary>		

        public string TypeName { get; set; }

        /// <summary>
        /// Url地址
        /// </summary>		

        public string Url { get; set; }

        /// <summary>
        /// 状态是否可用
        /// </summary>		

        public bool ISEnable { get; set; }

        /// <summary>
        /// 上级URL
        /// </summary>		

        public int ParentID { get; set; }

        /// <summary>
        /// 排序
        /// </summary>		

        public int Sort { get; set; }

        /// <summary>
        /// 备注
        /// </summary>		

        public string Remarks { get; set; }

        /// <summary>
        /// LogImg
        /// </summary>		

        public string LogImg { get; set; }


    }
}