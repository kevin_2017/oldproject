﻿using System; 
using System.Text;
using System.Collections.Generic; 
using System.Data;
namespace Model
{
		/// <summary>
 	///通知管理
 	 /// </summary>
		public class SYS_NoteManage
	{
        /// <summary>
        /// 是否删除0、未删除，1、已删除
        /// </summary>
        public int IsDelete
        {
            get;
            set;
        }
      	/// <summary>
		/// ID
        /// </summary>		
		
        public long ID {get;set;}
        
		/// <summary>
		/// 通知标题
        /// </summary>		
		
        public string Title {get;set;}
        
		/// <summary>
		/// 通知内容
        /// </summary>		
		
        public string Contents {get;set;}
        
		/// <summary>
		/// 创建时间
        /// </summary>		
		
        public DateTime CreateTime {get;set;}
        
		/// <summary>
		/// 失效时间
        /// </summary>		
		
        public DateTime InvalidTime {get;set;}
        
		/// <summary>
		/// 创建人
        /// </summary>		
		
        public long CreateUserID {get;set;}
        
		   
	}
}