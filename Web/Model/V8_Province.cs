﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    ///V8_Province
    /// </summary>
    public class V8_Province
    {  /// <summary>
        /// 是否删除0、未删除，1、已删除
        /// </summary>
        public int IsDelete
        {
            get;
            set;
        }

        /// <summary>
        /// ID
        /// </summary>		

        public long ID { get; set; }

        /// <summary>
        /// 省份名称
        /// </summary>		

        public string ProvinceName { get; set; }

        /// <summary>
        /// 省份编码
        /// </summary>
        public string Pro_Code
        {
            get;
            set;
        }

    }
}
