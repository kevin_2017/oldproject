﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    ///V8_City
    /// </summary>
    public class V8_City
    {
        /// <summary>
        /// 是否删除0、未删除，1、已删除
        /// </summary>
        public int IsDelete
        {
            get;
            set;
        }
        /// <summary>
        /// ID
        /// </summary>		

        public long ID { get; set; }

        /// <summary>
        /// ProvinceID
        /// </summary>		

        public long ProvinceID { get; set; }

        /// <summary>
        /// 城市名称
        /// </summary>		

        public string CityName { get; set; }


    }
}