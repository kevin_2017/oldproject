﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class VW_OrderInfoManage
    {

        /// <summary>
        /// ID
        /// </summary>		

        public long ID { get; set; }

        /// <summary>
        /// ProductName
        /// </summary>		

        public string ProductName { get; set; }

        /// <summary>
        /// TmallProductID
        /// </summary>		

        public string TmallProductID { get; set; }

        /// <summary>
        /// ProductID
        /// </summary>		

        public long ProductID { get; set; }

        /// <summary>
        /// ComFrom
        /// </summary>		

        public string ComFrom { get; set; }

        /// <summary>
        /// BranchID
        /// </summary>		

        public long BranchID { get; set; }

        /// <summary>
        /// RechargeNo
        /// </summary>		

        public string RechargeNo { get; set; }

        /// <summary>
        /// SystemOrderNo
        /// </summary>		

        public string SystemOrderNo { get; set; }

        /// <summary>
        /// BranchOrderNo
        /// </summary>		

        public string BranchOrderNo { get; set; }

        /// <summary>
        /// CurentStatus
        /// </summary>		

        public int CurentStatus { get; set; }

        /// <summary>
        /// Counts
        /// </summary>		

        public int Counts { get; set; }

        /// <summary>
        /// UnitPrice
        /// </summary>		

        public decimal UnitPrice { get; set; }

        /// <summary>
        /// RechargeMoney
        /// </summary>		

        public decimal RechargeMoney { get; set; }

        /// <summary>
        /// DiscountMethod
        /// </summary>		

        public int DiscountMethod { get; set; }

        /// <summary>
        /// ProductSalePrice
        /// </summary>		

        public decimal ProductSalePrice { get; set; }

        /// <summary>
        /// CreateTime
        /// </summary>		

        public DateTime CreateTime { get; set; }

        /// <summary>
        /// OperationTime
        /// </summary>		

        public DateTime OperationTime { get; set; }


        /// <summary>
        /// CallBackTime
        /// </summary>		

        public DateTime CallBackTime { get; set; }

        /// <summary>
        /// Operator
        /// </summary>		

        public string Operator { get; set; }

        /// <summary>
        /// TmallOrderNo
        /// </summary>		

        public string TmallOrderNo { get; set; }

        /// <summary>
        /// BizType
        /// </summary>		

        public string BizType { get; set; }

        /// <summary>
        /// SupplierID
        /// </summary>		

        public long SupplierID { get; set; }

        /// <summary>
        /// denomination
        /// </summary>		

        public decimal denomination { get; set; }

        /// <summary>
        /// TrueName
        /// </summary>		

        public string TrueName { get; set; }

        /// <summary>
        /// BranchName
        /// </summary>		

        public string BranchName { get; set; }

        /// <summary>
        /// SupplierOrderNo
        /// </summary>		

        public string SupplierOrderNo { get; set; }

        /// <summary>
        /// SystemSerialNum
        /// </summary>		

        public string SystemSerialNum { get; set; }

        /// <summary>
        /// BranchSerialNum
        /// </summary>		

        public string BranchSerialNum { get; set; }

        /// <summary>
        /// SupplierSerialNum
        /// </summary>		

        public string SupplierSerialNum { get; set; }

        /// <summary>
        /// IsBuyBack
        /// </summary>		

        public int IsBuyBack { get; set; }


        public string NotifyUrl
        {
            get;
            set;
        }
        /// <summary>
        /// 结果返回时间
        /// </summary>
        public DateTime ReturnResultTime
        {
            get;
            set;
        }

        /// <summary>
        /// 描述
        /// </summary>
        public string Descriptions
        {
            get;
            set;
        }

        public string ErrorMsg
        {
            get;
            set;
        }
    }
}