﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    [Serializable]
    public class SystemUserInfo
    {
        /// <summary>
        /// UserID
        /// </summary>
        public long ID
        {
            get;
            set;
        }
        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName
        {
            get;
            set;
        }
        /// <summary>
        /// 密码
        /// </summary>
        public string pwd
        {
            get;
            set;
        }
        /// <summary>
        /// 当前登录IP
        /// </summary>
        public string LoginIP
        {
            get;
            set;
        }
        /// <summary>
        /// 当前登录时间
        /// </summary>
        public DateTime LoginTime
        {
            get;
            set;
        }

        /// <summary>
        /// 登录次数
        /// </summary>
        public long LoginCounts
        {
            get;
            set;
        }
        /// <summary>
        /// 角色ID
        /// </summary>
        public long RoleID
        {
            get;
            set;
        }
        /// <summary>
        /// 角色名称
        /// </summary>
        public string RoleName
        {
            get;
            set;
        }
        /// <summary>
        /// 当前用户状态 （0 禁用,1 正常,2  锁定,3 离职）
        /// </summary>
        public int Status
        {
            get;
            set;
        }
        /// <summary>
        /// 当前用户真实姓名
        /// </summary>
        public string TrueName
        {
            get;
            set;
        }
        /// <summary>
        /// 是否被删除
        /// </summary>
        public int ISDelete
        {
            get;
            set;
        }
        /// <summary>
        /// 是否锁定登录（1、锁定，0、未锁定）
        /// </summary>
        public int ISLocking
        {
            get;
            set;
        }
        /// <summary>
        /// 是否为系统角色
        /// </summary>
        public bool ISSystem
        {
            get;
            set;
        }
        public string LastLoginIpAttr { get; set; }
        public string LoginIpAttr { get; set; }
        public string LastLoginIP { get; set; }
        public DateTime LastLoginTime { get; set; }

        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }
}
