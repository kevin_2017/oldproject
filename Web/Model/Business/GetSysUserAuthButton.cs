﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// 用户的权限按钮
    /// </summary>

    public partial class GetSysUserAuthButton
    {



        public long AuthButtonID
        {
            get;
            set;
        }
        private string _ButtonName;

        public string ButtonName
        {
            get { return _ButtonName; }
            set { _ButtonName = value; }
        }

        private string _DisplayName;

        public string DisplayName
        {
            get { return _DisplayName; }
            set { _DisplayName = value; }
        }
        private int _UserID;

        public int UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }
        private string _Url;

        public string Url
        {
            get { return _Url; }
            set { _Url = value; }
        }

        
    }
}
