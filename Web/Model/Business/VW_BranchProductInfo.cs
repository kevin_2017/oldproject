﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class VW_BranchProductInfo
    {

        /// <summary>
        /// ID
        /// </summary>		

        public long ID { get; set; }

        /// <summary>
        /// BranchID
        /// </summary>		

        public long BranchID { get; set; }

        /// <summary>
        /// ProductID
        /// </summary>		

        public long ProductID { get; set; }

        /// <summary>
        /// DiscountMethod
        /// </summary>		

        public int DiscountMethod { get; set; }

        /// <summary>
        /// SalePrice
        /// </summary>		

        public decimal SalePrice { get; set; }

        /// <summary>
        /// RealySalePrice
        /// </summary>		

        public decimal RealySalePrice { get; set; }

        /// <summary>
        /// BranchName
        /// </summary>		

        public string BranchName { get; set; }

        /// <summary>
        /// ProductName
        /// </summary>		

        public string ProductName { get; set; }

        /// <summary>
        /// ProductTypeID
        /// </summary>		

        public long ProductTypeID { get; set; }

        /// <summary>
        /// denomination
        /// </summary>		

        public decimal denomination { get; set; }

        /// <summary>
        /// TypeName
        /// </summary>		

        public string TypeName { get; set; }

        /// <summary>
        /// ParentID
        /// </summary>		

        public long ParentID { get; set; }

        /// <summary>
        /// Level
        /// </summary>		

        public int Level { get; set; }

        /// <summary>
        /// Status
        /// </summary>		

        public int Status { get; set; }

        /// <summary>
        /// ISPType
        /// </summary>		

        public int ISPType { get; set; }

        /// <summary>
        /// ProvinceID
        /// </summary>		

        public long ProvinceID { get; set; }

        /// <summary>
        /// ProvinceName
        /// </summary>		

        public string ProvinceName { get; set; }

        public int ISDelete
        {
            get;
            set;
        }
    }
}
