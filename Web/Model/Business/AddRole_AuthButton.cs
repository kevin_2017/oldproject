﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public partial class AddRole_AuthButton
    {
        public AddRole_AuthButton()
        {

        }
        #region Model
        private long _id; 
        private long _abid;
        private long _aid;

        /// <summary>
        /// 权限ID
        /// </summary>
        public long Aid
        {
            get { return _aid; }
            set { _aid = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public long ID
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 角色ID
        /// </summary>
        public long RID
        {
            set;
            get;
        }
        /// <summary>
        /// 权限按钮表ID
        /// </summary>
        public long ABID
        {
            set { _abid = value; }
            get { return _abid; }
        }
        #endregion Model
    }
}