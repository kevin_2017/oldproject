﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public partial class BranchLoginUserInfo
    {

        public long ID { get; set; }
        public long BranchID { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int Status { get; set; }
        public string BranchName { get; set; }
        public int UserRole { get; set; }
    }
}
