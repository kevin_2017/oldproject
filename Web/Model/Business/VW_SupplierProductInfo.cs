﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    ///VW_SupplierProductInfo
    /// </summary>
    public class VW_SupplierProductInfo
    {

        /// <summary>
        /// ID
        /// </summary>		

        public long ID { get; set; }

        /// <summary>
        /// TrueName
        /// </summary>		

        public string TrueName { get; set; }

        /// <summary>
        /// SupplierID
        /// </summary>		

        public long SupplierID { get; set; }

        /// <summary>
        /// ProductName
        /// </summary>		

        public string ProductName { get; set; }

        /// <summary>
        /// denomination
        /// </summary>		

        public decimal denomination { get; set; }

        /// <summary>
        /// Inprice
        /// </summary>		

        public decimal Inprice { get; set; }

        /// <summary>
        /// Sort
        /// </summary>		

        public int Sort { get; set; }

        /// <summary>
        /// SPMID
        /// </summary>		

        public long SPMID { get; set; }

        public int IsCloseProduct { get; set; }

        public string SupplierProductCode { get; set; }

        public int ArriveMethod { get; set; }
    }
}