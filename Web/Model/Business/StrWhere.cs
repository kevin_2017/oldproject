﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 参数形式的Sql查询条件
    /// </summary>
    public class StrWhere
    {
        private bool _isWhereExist = false;
        private StringBuilder _strWhere = new StringBuilder();
        private SqlParameter[] _parameters;
        /// <summary>
        /// 是否有where条件
        /// </summary>
        public bool IsWhereExist 
        {
            get
            {
                return _isWhereExist;
            }
            set
            {
                this._isWhereExist = value;
            }
        }
        /// <summary>
        /// where条件
        /// </summary>
        public StringBuilder strWhere 
        {
            get
            {
                return _strWhere;
            }
            set
            {
                this._strWhere = value;
            }
        }
        /// <summary>
        /// where条件参数
        /// </summary>
        public SqlParameter[] parameters 
        { 
            get
            {
                return this._parameters;
            }
            set
            {
                this._parameters = value;
            } 
        }

    }
}
