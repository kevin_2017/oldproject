﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace BLL
{
    public class Reportform
    {
        DAL.Reportform dal = new DAL.Reportform();
        #region 订单分析-平均时间
        public DataTable GetfrmTimetable(string strWhere)
        {
            return dal.GetfrmTimetable(strWhere);
        }
        #endregion
        #region 订单分析-运营地区
        public DataTable GetfrmAreatable(string strWhere)
        {
            return dal.GetfrmAreatable(strWhere);
        }
        #endregion
        #region 订单分析-商品
        public DataTable GetfrmProducttable(string strWhere)
        {
            return dal.GetfrmProducttable(strWhere);
        }
        #endregion

        #region 订单分析-供应商
        public DataTable GetfrmSuppliertable(string strWhere)
        {
            return dal.GetfrmSuppliertable(strWhere);
        }


        public DataTable Proc_GetReportForSupplierTotal(string strWhere)
        {
            return dal.Proc_GetReportForSupplierTotal(strWhere);
        }

        #endregion

        #region 订单分析-运营商
        public DataTable GetReprotForIspType(string strWhere)
        {
            return dal.GetReprotForIspType(strWhere);
        }
        #endregion

        #region 订单分析-商品地区
        public DataTable GetReprotForProductProvinceName(string strWhere)
        {
            return dal.GetReprotForProductProvinceName(strWhere);
        }
        #endregion

        #region 订单分析-商品地区
        public DataTable GetReprotForBranch(string strWhere)
        {
            return dal.GetReprotForBranch(strWhere);
        }

        #endregion
        #region 订单分析--时间段

        public DataSet GetReportForHour(string strWhere)
        {
            return dal.GetReportForHour(strWhere);


        }



        #endregion
    }
}
