﻿
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using System.Collections.Generic;


namespace BLL
{
    ///<summary>
    ///BLL层,供应商管理
    /// </summary>
    public class SUP_SupplierManage : BllBase
    {
        private readonly DAL.SUP_SupplierManage _dal = new DAL.SUP_SupplierManage();
        private readonly string TableName = "SUP_SupplierManage";

        #region 基础方法
        ///// <summary>
        ///// 是否存在该记录
        ///// </summary>
        ///// <param name="TableName">表名</param>
        ///// <param name="strWhere">条件</param>
        ///// <returns></returns>
        public bool Exists(Model.StrWhere strWhere)
        {

            return _dal.Exists(TableName, strWhere);
        }



        /// <summary>
        ///  获得数据 Dataset
        /// </summary>
        /// <param name="strWhere">条件</param>
        /// <returns></returns>
        public DataSet GetDataSet(Model.StrWhere strWhere)
        {
            return _dal.GetDataSet(TableName, strWhere);
        }





        /// <summary>
        /// 增加一条数据
        /// </summary> >
        /// <param name="model">实体</param>
        /// <returns>受影响行数</returns>
        public long Add(Model.SUP_SupplierManage model)
        {
            return _dal.Add(model);
        }

        /// <summary>
        ///  修改一条数据
        /// </summary>
        /// <returns></returns>
        public bool Update(Model.SUP_SupplierManage model)
        {
            return _dal.Update(model);
        }

        #endregion


        #region 反射方法

        /// <summary>
        /// 通过主键
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public Model.SUP_SupplierManage GetModel(long ID)
        {
            Model.StrWhere sw = new Model.StrWhere();
            sw.IsWhereExist = true;
            sw.strWhere.Append(" ID = @ID");
            sw.parameters = new SqlParameter[]{
            new SqlParameter("@ID", SqlDbType.BigInt,8)
            };
            sw.parameters[0].Value = ID;
            return _dal.GetModel<Model.SUP_SupplierManage>(TableName, sw);
        }


        public Model.SUP_SupplierManage GetModel(string apiCode)
        {
            Model.StrWhere sw = new Model.StrWhere
            {
                IsWhereExist = true,
                strWhere = new StringBuilder($" ApiCode='{apiCode}'")
            };
            return _dal.GetModel<Model.SUP_SupplierManage>(TableName, sw);
        }


        /// <summary>
        ///  获得数据列表 GetModelList
        /// </summary>
        /// <returns></returns>
        public List<Model.SUP_SupplierManage> GetModelList(Model.StrWhere strWhere)
        {
            return _dal.GetModelList<Model.SUP_SupplierManage>(TableName, strWhere);
        }


        #endregion


        #region 补充

        #endregion
    }
}