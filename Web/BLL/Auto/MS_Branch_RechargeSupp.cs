﻿
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using System.Collections.Generic;
using Model;

namespace BLL
{
    ///<summary>
    ///BLL层,网店基本信息
    /// </summary>
    public class MS_Branch_RechargeSupp : BllBase
    {
        private readonly DAL.MS_Branch_RechargeSupp Dal = new DAL.MS_Branch_RechargeSupp();
        private readonly string TableName = "MS_Branch_RechargeSupp";

        #region 基础方法
        ///// <summary>
        ///// 是否存在该记录
        ///// </summary>
        ///// <param name="TableName">表名</param>
        ///// <param name="strWhere">条件</param>
        ///// <returns></returns>
        public bool Exists(Model.StrWhere strWhere)
        {

            return Dal.Exists(TableName, strWhere);
        }

        /// <summary>
        /// 获取数据条数
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public long GetCount(Model.StrWhere strWhere)
        {
            return Dal.GetCount(TableName, strWhere);
        }

        /// <summary>
        ///  获得数据 Dataset
        /// </summary>
        /// <param name="TableName">表名</param>
        /// <param name="strWhere">条件</param>
        /// <returns></returns>
        public DataSet GetDataSet(Model.StrWhere strWhere)
        {
            return Dal.GetDataSet(TableName, strWhere);
        }

        ///// <summary>
        ///// 获得前几行数据
        ///// </summary>
        ///// <param name="TableName">表名</param>
        ///// <param name="Top">行数</param>
        ///// <param name="strWhere">条件</param>
        ///// <returns></returns>
        //public DataSet GetDataSet(int Top, string strWhere)
        //{
        //    return Dal.GetDataSet(TableName, Top, strWhere);
        //}

        /// <summary>
        /// 分页获取数据列表
        /// </summary> 
        /// <param name="strWhere">条件</param>
        /// <param name="orderby">排序列</param>
        /// <param name="startIndex">开始行</param>
        /// <param name="endIndex">结束行</param>
        /// <returns></returns>
        public DataSet GetDataSetByPage(Model.StrWhere strWhere, string orderby, int startIndex, int endIndex)
        {
            return Dal.GetDataSetByPage(TableName, strWhere, orderby, startIndex, endIndex);
        }
        /// <summary>
        /// 删除数据
        /// </summary> 
        /// <param name="strWhere">条件</param>
        /// <returns></returns>
        public int Delete(Model.StrWhere strWhere)
        {
            return Dal.Delete(TableName, strWhere);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns>受影响行数</returns>
        public long Add(Model.MS_Branch_RechargeSupp model)
        {
            return Dal.Add(model);
        }

        
        #endregion


         


       
    }
}