﻿
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using System.Collections.Generic;
using Model;

namespace BLL
{
    ///<summary>
    ///BLL层,网店基本信息
    /// </summary>
    public class MS_BranchManage : BllBase
    {
        private readonly DAL.MS_BranchManage Dal = new DAL.MS_BranchManage();
        private readonly string TableName = "MS_BranchManage";

        #region 基础方法
        ///// <summary>
        ///// 是否存在该记录
        ///// </summary>
        ///// <param name="TableName">表名</param>
        ///// <param name="strWhere">条件</param>
        ///// <returns></returns>
        public bool Exists(Model.StrWhere strWhere)
        {

            return Dal.Exists(TableName, strWhere);
        }

        /// <summary>
        /// 获取数据条数
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public long GetCount(Model.StrWhere strWhere)
        {
            return Dal.GetCount(TableName, strWhere);
        }

        /// <summary>
        ///  获得数据 Dataset
        /// </summary>
        /// <param name="TableName">表名</param>
        /// <param name="strWhere">条件</param>
        /// <returns></returns>
        public DataSet GetDataSet(Model.StrWhere strWhere)
        {
            return Dal.GetDataSet(TableName, strWhere);
        }

        ///// <summary>
        ///// 获得前几行数据
        ///// </summary>
        ///// <param name="TableName">表名</param>
        ///// <param name="Top">行数</param>
        ///// <param name="strWhere">条件</param>
        ///// <returns></returns>
        //public DataSet GetDataSet(int Top, string strWhere)
        //{
        //    return Dal.GetDataSet(TableName, Top, strWhere);
        //}

        /// <summary>
        /// 分页获取数据列表
        /// </summary> 
        /// <param name="strWhere">条件</param>
        /// <param name="orderby">排序列</param>
        /// <param name="startIndex">开始行</param>
        /// <param name="endIndex">结束行</param>
        /// <returns></returns>
        public DataSet GetDataSetByPage(Model.StrWhere strWhere, string orderby, int startIndex, int endIndex)
        {
            return Dal.GetDataSetByPage(TableName, strWhere, orderby, startIndex, endIndex);
        }
        /// <summary>
        /// 删除数据
        /// </summary> 
        /// <param name="strWhere">条件</param>
        /// <returns></returns>
        public int Delete(Model.StrWhere strWhere)
        {
            return Dal.Delete(TableName, strWhere);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns>受影响行数</returns>
        public long Add(Model.MS_BranchManage model)
        {
            return Dal.Add(model);
        }

        /// <summary>
        ///  修改一条数据
        /// </summary>
        /// <returns></returns>
        public bool Update(Model.MS_BranchManage model)
        {
            return Dal.Update(model);
        }
        ///// <summary>
        ///// 删除数据
        ///// </summary>
        //public bool Delete(int ID)
        //{
        //    return Dal.Delete(ID);
        //}
        #endregion


        #region 反射方法

        ///// <summary>
        /////  分页获取数据列表 List<对象>
        ///// </summary>
        ///// <returns></returns>
        public List<Model.MS_BranchManage> GetByPage(Model.StrWhere strWhere, string orderby, int startIndex, int endIndex)
        {
            DataSet ds = Dal.GetDataSetByPage(TableName, strWhere, orderby, startIndex, endIndex);
            return Dal.DataTableToList<Model.MS_BranchManage>(ds.Tables[0]);
        }
        ///// <summary>
        ///// 得到一个对象实体
        ///// </summary>
        ///// <typeparam name="T"></typeparam>
        ///// <param name="strWhere"></param>
        ///// <returns></returns>
        public Model.MS_BranchManage GetModel(Model.StrWhere strWhere)
        {
            return Dal.GetModel<Model.MS_BranchManage>(TableName, strWhere);
        }


        ///// <summary>
        ///// 通过主键
        ///// </summary>
        ///// <param name="ID"></param>
        ///// <returns></returns>
        public Model.MS_BranchManage GetModel(long ID)
        {
            Model.StrWhere sw = new Model.StrWhere();
            sw.IsWhereExist = true;
            sw.strWhere.Append(" ID = @ID");
            sw.parameters = new SqlParameter[]{
            new SqlParameter("@ID", SqlDbType.BigInt,8)  
            };
            sw.parameters[0].Value = ID;
            return Dal.GetModel<Model.MS_BranchManage>(TableName, sw);
        }
        public Model.MS_BranchManage GetModel(string branchCode)
        {
            Model.StrWhere sw = new Model.StrWhere();
            sw.IsWhereExist = true;
            sw.strWhere.Append(" BranchCode = @BranchCode");
            sw.parameters = new SqlParameter[]{
            new SqlParameter("@BranchCode", SqlDbType.NVarChar,50)  
            };
            sw.parameters[0].Value = branchCode;
            return Dal.GetModel<Model.MS_BranchManage>(TableName, sw);
        }
        ///// <summary>
        /////  获得数据列表 GetModelList
        ///// </summary>
        ///// <returns></returns>
        public List<Model.MS_BranchManage> GetModelList(Model.StrWhere strWhere)
        {
            return Dal.GetModelList<Model.MS_BranchManage>(TableName, strWhere);
        }

        ///// <summary>
        /////  分页获取数据列表 List<对象>
        ///// </summary>
        ///// <returns></returns>
        public List<Model.MS_BranchManage> GetByPage<T>(Model.StrWhere strWhere, string orderby, int startIndex, int endIndex)
        {
            return Dal.GetByPage<Model.MS_BranchManage>(TableName, strWhere, orderby, startIndex, endIndex);
        }


        #endregion


        #region 补充
        /// <summary>
        /// 
        /// </summary>
        /// <param name="changeType">0消费 1充值，2、返利</param>
        /// <param name="balance">金额</param>
        /// <param name="brachid">代理商ID</param>
        /// <param name="orderId">订单ID</param>
        /// <param name="changeMethod">变化方式:（1、消费。[现金，微信，pos机,余额]2、充值[现金，微信，pos机，余额]）</param>
        /// <param name="descriptions">描述</param>
        /// <returns></returns>
        public bool Proc_BalanceChange(int changeType, decimal balance, long brachid, long orderId, string changeMethod, string descriptions)
        {
            return Dal.Proc_BalanceChange(changeType, balance, brachid, orderId, changeMethod, descriptions);
        }

        /// <summary>
        /// 根据代理商ID，修改密钥
        /// </summary>
        /// <param name="branchID"></param>
        /// <param name="privateKey"></param>
        /// <returns></returns>
        public bool UpdateBranchPrivateKeyByBranchID(long branchID, string privateKey)
        {
            return Dal.UpdateBranchPrivateKeyByBranchID(branchID, privateKey);
        }
        #endregion
    }
}