﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Model;
namespace BLL
{
    /// <summary>
    /// 代理返利规则
    /// </summary>
    public partial class MS_Branch_RebateRule : BllBase
    {

        private readonly DAL.MS_Branch_RebateRule Dal = new DAL.MS_Branch_RebateRule();
        private readonly string TableName = "MS_Branch_RebateRule";

        #region 基础方法
        ///// <summary>
        ///// 是否存在该记录
        ///// </summary>
        ///// <param name="TableName">表名</param>
        ///// <param name="strWhere">条件</param>
        ///// <returns></returns>
        public bool Exists(Model.StrWhere strWhere)
        {

            return Dal.Exists(TableName, strWhere);
        }

        /// <summary>
        /// 获取数据条数
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public long GetCount(Model.StrWhere strWhere)
        {
            return Dal.GetCount(TableName, strWhere);
        }

        /// <summary>
        ///  获得数据 Dataset
        /// </summary> 
        /// <param name="strWhere">条件</param>
        /// <returns></returns>
        public DataSet GetDataSet(Model.StrWhere strWhere)
        {
            return Dal.GetDataSet(TableName, strWhere);
        }



        /// <summary>
        /// 分页获取数据列表
        /// </summary> 
        /// <param name="strWhere">条件</param>
        /// <param name="orderby">排序列</param>
        /// <param name="startIndex">开始行</param>
        /// <param name="endIndex">结束行</param>
        /// <returns></returns>
        public DataSet GetDataSetByPage(Model.StrWhere strWhere, string orderby, int startIndex, int endIndex)
        {
            return Dal.GetDataSetByPage(TableName, strWhere, orderby, startIndex, endIndex);
        }
        /// <summary>
        /// 删除数据
        /// </summary> 
        /// <param name="strWhere">条件</param>
        /// <returns></returns>
        public int Delete(Model.StrWhere strWhere)
        {
            return Dal.Delete(TableName, strWhere);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        /// <param name="model">实体</param>
        /// <returns>受影响行数</returns>
        public long Add(Model.MS_Branch_RebateRule model)
        {
            return Dal.Add(model);
        }

        /// <summary>
        ///  修改一条数据
        /// </summary>
        /// <returns></returns>
        public bool Update(Model.MS_Branch_RebateRule model)
        {
            return Dal.Update(model);
        }
        ///// <summary>
        ///// 删除数据
        ///// </summary>
        //public bool Delete(int ID)
        //{
        //    return Dal.Delete(ID);
        //}
        #endregion


        #region 反射方法

        ///// <summary>
        /////  分页获取数据列表 List<对象>
        ///// </summary>
        ///// <returns></returns>
        public List<Model.MS_Branch_RebateRule> GetByPage(Model.StrWhere strWhere, string orderby, int startIndex, int endIndex)
        {
            DataSet ds = Dal.GetDataSetByPage(TableName, strWhere, orderby, startIndex, endIndex);
            return Dal.DataTableToList<Model.MS_Branch_RebateRule>(ds.Tables[0]);
        }
        ///// <summary>
        ///// 得到一个对象实体
        ///// </summary>
        ///// <typeparam name="T"></typeparam>
        ///// <param name="strWhere"></param>
        ///// <returns></returns>
        public Model.MS_Branch_RebateRule GetModel(Model.StrWhere strWhere)
        {
            return Dal.GetModel<Model.MS_Branch_RebateRule>(TableName, strWhere);
        }


        ///// <summary>
        ///// 通过主键
        ///// </summary>
        ///// <param name="ID"></param>
        ///// <returns></returns>
        public Model.MS_Branch_RebateRule GetModel(long ID)
        {
            Model.StrWhere sw = new Model.StrWhere {IsWhereExist = true};
            sw.strWhere.Append(" ID = @ID");
            sw.parameters = new SqlParameter[]{
            new SqlParameter("@ID", SqlDbType.BigInt,8)
            };
            sw.parameters[0].Value = ID;
            return Dal.GetModel<Model.MS_Branch_RebateRule>(TableName, sw);
        }
        ///// <summary>
        /////  获得数据列表 GetModelList
        ///// </summary>
        ///// <returns></returns>
        public List<Model.MS_Branch_RebateRule> GetModelList(Model.StrWhere strWhere)
        {
            return Dal.GetModelList<Model.MS_Branch_RebateRule>(TableName, strWhere);
        }

        ///// <summary>
        /////  分页获取数据列表 List<对象>
        ///// </summary>
        ///// <returns></returns>
        public List<Model.MS_Branch_RebateRule> GetByPage<T>(Model.StrWhere strWhere, string orderby, int startIndex, int endIndex)
        {
            return Dal.GetByPage<Model.MS_Branch_RebateRule>(TableName, strWhere, orderby, startIndex, endIndex);
        }


        #endregion




    }
}