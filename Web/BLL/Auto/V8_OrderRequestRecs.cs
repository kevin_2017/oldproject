﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    //V8_OrderRequestRecs
    public partial class V8_OrderRequestRecs : BllBase
    {

        private readonly DAL.V8_OrderRequestRecs Dal = new DAL.V8_OrderRequestRecs();
        private readonly string TableName = "V8_OrderRequestRecs";

        #region 基础方法
        ///// <summary>
        ///// 是否存在该记录
        ///// </summary>
        ///// <param name="TableName">表名</param>
        ///// <param name="strWhere">条件</param>
        ///// <returns></returns>
        public bool Exists(Model.StrWhere strWhere)
        {

            return Dal.Exists(TableName, strWhere);
        }

        /// <summary>
        /// 获取数据条数
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public long GetCount(Model.StrWhere strWhere)
        {
            return Dal.GetCount(TableName, strWhere);
        }

        /// <summary>
        ///  获得数据 Dataset
        /// </summary>
        /// <param name="TableName">表名</param>
        /// <param name="strWhere">条件</param>
        /// <returns></returns>
        public DataSet GetDataSet(Model.StrWhere strWhere)
        {
            return Dal.GetDataSet(TableName, strWhere);
        }



        /// <summary>
        /// 分页获取数据列表
        /// </summary> 
        /// <param name="strWhere">条件</param>
        /// <param name="orderby">排序列</param>
        /// <param name="startIndex">开始行</param>
        /// <param name="endIndex">结束行</param>
        /// <returns></returns>
        public DataSet GetDataSetByPage(Model.StrWhere strWhere, string orderby, int startIndex, int endIndex)
        {
            return Dal.GetDataSetByPage(TableName, strWhere, orderby, startIndex, endIndex);
        }
        /// <summary>
        /// 删除数据
        /// </summary> 
        /// <param name="strWhere">条件</param>
        /// <returns></returns>
        public int Delete(Model.StrWhere strWhere)
        {
            return Dal.Delete(TableName, strWhere);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        /// <param name="Model">实体</param>
        /// <returns>受影响行数</returns>
        public long Add(Model.V8_OrderRequestRecs model)
        {
            return Dal.Add(model);
        }

        /// <summary>
        ///  修改一条数据
        /// </summary>
        /// <returns></returns>
        public bool Update(Model.V8_OrderRequestRecs model)
        {
            return Dal.Update(model);
        }
        ///// <summary>
        ///// 删除数据
        ///// </summary>
        //public bool Delete(int ID)
        //{
        //    return Dal.Delete(ID);
        //}
        #endregion


        #region 反射方法

        ///// <summary>
        /////  分页获取数据列表 List<对象>
        ///// </summary>
        ///// <returns></returns>
        public List<Model.V8_OrderRequestRecs> GetByPage(Model.StrWhere strWhere, string orderby, int startIndex, int endIndex)
        {
            DataSet ds = Dal.GetDataSetByPage(TableName, strWhere, orderby, startIndex, endIndex);
            return Dal.DataTableToList<Model.V8_OrderRequestRecs>(ds.Tables[0]);
        }
        ///// <summary>
        ///// 得到一个对象实体
        ///// </summary>
        ///// <typeparam name="T"></typeparam>
        ///// <param name="strWhere"></param>
        ///// <returns></returns>
        public Model.V8_OrderRequestRecs GetModel(Model.StrWhere strWhere)
        {
            return Dal.GetModel<Model.V8_OrderRequestRecs>(TableName, strWhere);
        }


        ///// <summary>
        ///// 通过主键
        ///// </summary>
        ///// <param name="ID"></param>
        ///// <returns></returns>
        public Model.V8_OrderRequestRecs GetModel(long ID)
        {
            Model.StrWhere sw = new Model.StrWhere();
            sw.IsWhereExist = true;
            sw.strWhere.Append(" ID = @ID");
            sw.parameters = new SqlParameter[]{
            new SqlParameter("@ID", SqlDbType.BigInt,8)
            };
            sw.parameters[0].Value = ID;
            return Dal.GetModel<Model.V8_OrderRequestRecs>(TableName, sw);
        }
        ///// <summary>
        /////  获得数据列表 GetModelList
        ///// </summary>
        ///// <returns></returns>
        public List<Model.V8_OrderRequestRecs> GetModelList(Model.StrWhere strWhere)
        {
            return Dal.GetModelList<Model.V8_OrderRequestRecs>(TableName, strWhere);
        }

        ///// <summary>
        /////  分页获取数据列表 List<对象>
        ///// </summary>
        ///// <returns></returns>
        public List<Model.V8_OrderRequestRecs> GetByPage<T>(Model.StrWhere strWhere, string orderby, int startIndex, int endIndex)
        {
            return Dal.GetByPage<Model.V8_OrderRequestRecs>(TableName, strWhere, orderby, startIndex, endIndex);
        }


        #endregion




    }
}