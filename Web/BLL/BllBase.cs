﻿

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Model;
using DAL;
using Common;
namespace BLL
{
    public class BllBase
    {
        DAL.DalBase Dal = new DAL.DalBase();
        #region 基础方法
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        /// <param name="TableName">表名</param>
        /// <param name="strWhere">条件</param>
        /// <returns></returns>
        public bool Exists(string TableName, Model.StrWhere strWhere)
        {
            return Dal.Exists(TableName, strWhere);
        }

        /// <summary>
        /// 获取数据条数
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public long GetCount(string TableName, Model.StrWhere strWhere)
        {
            return Dal.GetCount(TableName, strWhere);
        }

        /// <summary>
        ///  获得数据 Dataset
        /// </summary>
        /// <param name="TableName">表名</param>
        /// <param name="strWhere">条件</param>
        /// <returns></returns>
        public DataSet GetDataSet(string TableName, Model.StrWhere strWhere)
        {
            return Dal.GetDataSet(TableName, strWhere);
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        /// <param name="TableName">表名</param>
        /// <param name="Top">行数</param>
        /// <param name="strWhere">条件</param>
        /// <returns></returns>
        public DataSet GetDataSet(string TableName, int Top, Model.StrWhere strWhere)
        {
            return Dal.GetDataSet(TableName, Top, strWhere);
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary> 
        /// <param name="strWhere">条件</param>
        /// <param name="orderby">排序列</param>
        /// <param name="startIndex">开始行</param>
        /// <param name="endIndex">结束行</param>
        /// <returns></returns>
        public DataSet GetDataSetByPage(string TableName, Model.StrWhere strWhere, string orderby, int startIndex, int endIndex)
        {
            return Dal.GetDataSetByPage(TableName, strWhere, orderby, startIndex, endIndex);
        }
        /// <summary>
        /// 删除数据
        /// </summary> 
        /// <param name="strWhere">条件</param>
        /// <returns></returns>
        public int Delete(string TableName, Model.StrWhere strWhere)
        {
            return Dal.Delete(TableName, strWhere);
        }

        /// <summary>
        /// 执行一条查询语句
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public DataSet Query(string sql)
        {
            return Dal.Query(sql);
        }


        /// <summary>
        /// 执行带参数sql语句，返回结果集
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="pas"></param>
        /// <returns></returns>
        public DataSet Query(string sql, SqlParameter[] pas)
        {
            return Dal.Query(sql, pas);
        }

        /// <summary>
        /// 获取对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="TableName"></param>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public T GetModel<T>(string TableName, Model.StrWhere strWhere) where T : class,new()
        {
            return Dal.GetModel<T>(TableName, strWhere);
        }

        /// <summary>
        /// 获取对象集合
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="TableName"></param>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public List<T> GetModelList<T>(string TableName, Model.StrWhere strWhere) where T : class,new()
        {
            return Dal.GetModelList<T>(TableName, strWhere);
        }

        #endregion

        #region 自定义方法

        public DataTable GetListByPage(PageAttribute page)
        {
            return Dal.GetListByPage(page);
        }

        #endregion


        /// <summary>
        /// 执行SQL语句
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public int ExecuteSql(string sql)
        {
            return Dal.ExecuteSql(sql);
        }


    }
}
