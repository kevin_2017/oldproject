﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Recharge.AgentApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API 配置和服务

            // Web API 路由
           // config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}",
                defaults: new { id = RouteParameter.Optional }
            );
            config.Routes.MapHttpRoute(
                name: "NewDefaultApi",
                routeTemplate: "api/{code}/{controller}/{action}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
