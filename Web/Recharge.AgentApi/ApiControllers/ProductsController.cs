﻿using CE.Utility.Text;
using Model;
using Newtonsoft.Json;
using Recharge.AgentApi.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Http;
using WebGrease;

namespace Recharge.AgentApi.ApiControllers
{
    /// <summary>
    /// 商品
    /// </summary>
    public class ProductsController : BaseController
    {
        /// <summary>
        /// 获取商品
        /// </summary>
        /// <param name="param"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        // GET: GetProducts
        [HttpGet, ActionName("GetProducts")]
        public string GetProducts(string param, string key)
        {
            var agentCode = Tools.ParseCode(RequestContext.RouteData.Values);
            var resp = GetBranchProducts(param, key);
            WriteLog.Write(this.GetType().ToString(), $"代理{agentCode},返回数据：{JsonConvert.SerializeObject(resp)}");
            return JsonConvert.SerializeObject(resp);
        }
        /// <summary>
        /// 获取商品
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost, ActionName("GetProducts")]
        public string GetProducts([FromBody]RequestParams req)
        {
            var agentCode = Tools.ParseCode(RequestContext.RouteData.Values);
            var resp = GetBranchProducts(req.Param, req.Key);
            WriteLog.Write(this.GetType().ToString(), $"代理{agentCode},返回数据：{JsonConvert.SerializeObject(resp)}");
            return JsonConvert.SerializeObject(resp);
        }
        readonly Dictionary<string, string> _parmaName = new Dictionary<string, string>()
        {
            {"branchcode","代理号"},
            {"typeid","商品类型"},
            {"requesttime","请求时间"},
            {"sign","签名"},
        };
        private AgentProductResp GetBranchProducts(string param, string key)
        {
            var agentCode = Tools.ParseCode(RequestContext.RouteData.Values);
            WriteLog.Write(this.GetType().ToString(), $"代理{agentCode},请求原始数据：param【{param}】,key【{key}】");

            var resp = new AgentProductResp();

            if (string.IsNullOrEmpty(agentCode))
            {
                resp.CurrentStatus = "REQUEST_FAILED";
                resp.Success = false;
                resp.Code = "118";
                resp.Message = $"代理编码错误";
                return resp;
            }

            List<AgentProduct> list = new List<AgentProduct>();
            if (string.IsNullOrEmpty(key))
            {
                resp.CurrentStatus = "REQUEST_FAILED";
                resp.Success = false;
                resp.Code = "101";
                resp.Message = $"缺少参数：Key";
                return resp;
            }

            if (string.IsNullOrEmpty(param))
            {
                resp.CurrentStatus = "REQUEST_FAILED";
                resp.Success = false;
                resp.Code = "101";
                resp.Message = $"缺少参数：param";
                return resp;
            }

            key = key.DecodeBase64String();

            param = param.DecodeBase64String();
            WriteLog.Write(this.GetType().ToString(), $"代理{agentCode},请求解密数据：param【{param}】,key【{key}】");
            var dic = Tools.GetParamArray(param);
            if (dic.Count == 0)
            {
                resp.CurrentStatus = "REQUEST_FAILED";
                resp.Success = false;
                resp.Code = "101";
                resp.Message = $"缺少参数：param";
                return resp;
            }
            foreach (var item in _parmaName)
            {
                if (!dic.ContainsKey(item.Key))
                {
                    resp.CurrentStatus = "REQUEST_FAILED";
                    resp.Success = false;
                    resp.Code = "101";
                    resp.Message = $"缺少参数：{item.Value}";
                    return resp;
                }

                if (string.IsNullOrEmpty(dic[item.Key]))
                {
                    resp.CurrentStatus = "REQUEST_FAILED";
                    resp.Success = false;
                    resp.Code = "102";
                    resp.Message = $"{item.Value} 值不能为空";
                    return resp;
                }
            }
            var sign = dic["sign"];
            var branchcode = dic["branchcode"];
            var typeId = dic["typeid"];
            var requestTime = dic["requesttime"];
            dic.Remove("sign");

            #region 判断请求时间是否大于3分钟

            var requestDate = DateTime.MinValue;

            if (!DateTime.TryParseExact(requestTime, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out requestDate))
            {
                resp.CurrentStatus = "REQUEST_FAILED";
                resp.Success = false;
                resp.Code = "113";
                resp.Message = $"请求时间格式错误";
                return resp;
            }

            if ((DateTime.Now - requestDate).TotalMinutes > 3)
            {
                resp.CurrentStatus = "REQUEST_FAILED";
                resp.Success = false;
                resp.Code = "114";
                resp.Message = $"请求时间超过三分钟";
                return resp;
            }

            #endregion

            var agentInfo = new BLL.MS_BranchManage().GetModel(branchcode);

            if (!agentInfo.BranchCode.Equals(agentCode))
            {
                resp.CurrentStatus = "REQUEST_FAILED";
                resp.Success = false;
                resp.Code = "118";
                resp.Message = $"代理编码错误";
                return resp;
            }
            if (!Tools.VerifyParam(branchcode, key, agentInfo.PrivateKey, 6, 8, requestDate.ToString("yyyyMMdd")))
            {
                resp.CurrentStatus = "REQUEST_FAILED";
                resp.Success = false;
                resp.Code = "104";
                resp.Message = $"账户密钥信息错误";
                return resp;
            }

            var requeststr = Tools.CreateLinkString(Tools.SortDictionary(dic), "&", true);
            var verifySign = $"{requeststr}&key={ agentInfo.PrivateKey}".ToMD5().ToLower();
            WriteLog.Write(this.GetType().ToString(), $"代理{agentCode},请求签名数据：requeststr【{requeststr}】key:【{key}】,verifySign【{verifySign}】");

            if (verifySign.Equals(sign.ToLower()))
            {
                Model.StrWhere str = new StrWhere()
                {
                    IsWhereExist = true,
                    strWhere = new StringBuilder($" BranchID={agentInfo.ID} and ProductTypeId={typeId} and IsClosed=0 and IsDelete=0")
                };
                var allList = new BLL.V8_Product_Branch().GetModelList(str);
                if (allList != null && allList.Count > 0)
                {
                    foreach (var item in allList)
                    {
                        AgentProduct pModel = new AgentProduct()
                        {
                            ProductId = item.ProductID,
                            ProductName = item.ProductName,
                            TypeId = item.ProductTypeId,
                            TypeName = item.ProductTypeName,
                            IspType = item.ISPType,
                            IspName = item.ISPName
                        };
                         
                        list.Add(pModel);
                    }
                }

                resp.BranchId = branchcode;
                resp.CurrentStatus = "SUCCESS";
                resp.Success = true;
                resp.Code = "000";
                resp.Message = list.Count == 0 ? "查询成功,暂无数据" : "查询成功";
                resp.Data = list;
                return resp;
            }
            else
            {
                resp.CurrentStatus = "REQUEST_FAILED";
                resp.Success = false;
                resp.Code = "105";
                resp.Message = $"签名验证失败";
                return resp;
            }
        }
    }
}