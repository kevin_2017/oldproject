﻿using CE.Utility.Text;
using Newtonsoft.Json;
using Recharge.AgentApi.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Http;

namespace Recharge.AgentApi.ApiControllers
{
    /// <summary>
    /// 订单
    /// </summary>
    public class OrderController : BaseController
    {

        private readonly string _fileName = DateTime.Now.Hour.ToString();
        /// <summary>
        /// 查询订单
        /// </summary>
        /// <param name="param"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        [HttpGet, ActionName("QueryOrder")]
        public string QueryOrderStatus(string param, string key)
        {
            var agentCode = Tools.ParseCode(RequestContext.RouteData.Values);
            var resp = QueryStatus(param, key);
            WriteLog.Write(this.GetType().ToString(), $"代理{agentCode},返回数据：{JsonConvert.SerializeObject(resp)}");
            return JsonConvert.SerializeObject(resp);
        }
        /// <summary>
        /// 查询订单
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost, ActionName("QueryOrder")]
        public string QueryOrderStatus([FromBody]RequestParams req)
        {
            var agentCode = Tools.ParseCode(RequestContext.RouteData.Values);
            var resp = QueryStatus(req.Param, req.Key);
            WriteLog.Write(this.GetType().ToString(), $"代理{agentCode},返回数据：{JsonConvert.SerializeObject(resp)}");
            return JsonConvert.SerializeObject(resp);
        }

        readonly Dictionary<string, string> _parmaName = new Dictionary<string, string>()
        {
            {"branchcode","代理号"},
            {"branchorderno","订单号"},
            {"requesttime","请求时间"},
            {"sign","签名"},
        };

        private QueryResp QueryStatus(string param, string key)
        {
            var resp = new QueryResp();

            var agentCode = Tools.ParseCode(RequestContext.RouteData.Values);
            if (string.IsNullOrEmpty(agentCode))
            {
                resp.CurrentStatus = "REQUEST_FAILED";
                resp.Success = false;
                resp.Code = "118";
                resp.Message = $"代理编码错误";
                return resp;
            }

            WriteLog.Write(this.GetType().ToString(), $"代理{agentCode},请求原始数据：param【{param}】,key【{key}】");

           

            if (string.IsNullOrEmpty(key))
            {
                resp.CurrentStatus = "REQUEST_FAILED";
                resp.Success = false;
                resp.Code = "101";
                resp.Message = $"缺少参数：Key";
                return resp;
            }

            if (string.IsNullOrEmpty(param))
            {
                resp.CurrentStatus = "REQUEST_FAILED";
                resp.Success = false;
                resp.Code = "101";
                resp.Message = $"缺少参数：param";
                return resp;
            }


            key = key.DecodeBase64String();

            param = param.DecodeBase64String();
            WriteLog.Write(this.GetType().ToString(), $"代理{agentCode},请求解密数据：param【{param}】,key【{key}】");
            var dic = Tools.GetParamArray(param);
            if (dic.Count == 0)
            {
                resp.CurrentStatus = "REQUEST_FAILED";
                resp.Success = false;
                resp.Code = "101";
                resp.Message = $"缺少参数：param";
                return resp;
            }
            foreach (var item in _parmaName)
            {
                if (!dic.ContainsKey(item.Key))
                {
                    resp.CurrentStatus = "REQUEST_FAILED";
                    resp.Success = false;
                    resp.Code = "101";
                    resp.Message = $"缺少参数：{item.Value}";
                    return resp;
                }

                if (string.IsNullOrEmpty(dic[item.Key]))
                {
                    resp.CurrentStatus = "REQUEST_FAILED";
                    resp.Success = false;
                    resp.Code = "102";
                    resp.Message = $"{item.Value} 值不能为空";
                    return resp;
                }
            }
            #region 判断当前代理相关信息
            var sign = dic["sign"];
            var branchcode = dic["branchcode"];
            var requestTime = dic["requesttime"];
            var branchOrderNo = dic["branchorderno"];
            dic.Remove("sign");
            var agentInfo = new BLL.MS_BranchManage().GetModel(branchcode);
            if (agentInfo == null || agentInfo.IsDelete == 1)
            {
                resp.CurrentStatus = "REQUEST_FAILED";
                resp.Success = false;
                resp.Code = "103";
                resp.Message = $"代理不能正常使用";
                return resp;
            }

            #region 判断请求时间是否大于3分钟

            var requestDate = DateTime.MinValue;

            if (!DateTime.TryParseExact(requestTime, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out requestDate))
            {
                resp.CurrentStatus = "REQUEST_FAILED";
                resp.Success = false;
                resp.Code = "113";
                resp.Message = $"请求时间格式错误";
                return resp;
            }

            if ((DateTime.Now - requestDate).TotalMinutes > 3)
            {
                resp.CurrentStatus = "REQUEST_FAILED";
                resp.Success = false;
                resp.Code = "114";
                resp.Message = $"请求时间超过三分钟";
                return resp;
            }

            #endregion

            if (!Tools.VerifyParam(branchcode, key, agentInfo.PrivateKey, 6, 8, requestDate.ToString("yyyyMMdd")))
            {
                resp.CurrentStatus = "REQUEST_FAILED";
                resp.Success = false;
                resp.Code = "104";
                resp.Message = $"账户密钥信息不正确";
                return resp;
            }
            var requeststr = Tools.CreateLinkString(Tools.SortDictionary(dic), "&", true);
            var verifySign = $"{requeststr}&key={ agentInfo.PrivateKey}".ToMD5().ToLower();
            WriteLog.Write(this.GetType().ToString(), $"代理{agentCode},请求签名数据：requeststr【{requeststr}】key:【{key}】,verifySign【{verifySign}】");

            if (verifySign.Equals(sign.ToLower()))
            {
                Model.StrWhere str = new Model.StrWhere()
                {
                    IsWhereExist = true,
                    strWhere = new StringBuilder($" BranchID={agentInfo.ID} and BranchOrderNo='{branchOrderNo}' ")
                };
                Model.V8_OrderManage orderModel = new BLL.V8_OrderManage().GetModel(str);
                if (orderModel == null)
                {
                    resp.CurrentStatus = "REQUEST_FAILED";
                    resp.Success = false;
                    resp.Code = "107";
                    resp.Message = $"该订单号不存在";
                }
                else
                {
                    //当前状态(1、充值成功，2、等待充值，3、充值中，4、问题订单，5、充值失败)

                    resp.Success = true;
                    resp.Code = "000";
                    switch (orderModel.CurentStatus)
                    {
                        case 1:
                            resp.Message = $"充值成功";
                            resp.CurrentStatus = "SUCCESS";
                            break;
                        case 2:
                            resp.Message = $"等待充值";
                            resp.CurrentStatus = "WAITFORGPAY";
                            break;
                        case 3:
                            resp.Message = $"充值中";
                            resp.CurrentStatus = "PAYING";
                            break;
                        case 4:
                            resp.Message = $"问题订单";
                            resp.CurrentStatus = "PROBLEMORDER";
                            break;
                        case 5:
                            resp.Message = $"充值失败";
                            resp.CurrentStatus = "RECHARGEFAILED";
                            break;
                        default:
                            resp.Message = $"未知状态";
                            resp.CurrentStatus = "UNKNOWN";
                            break;
                    }
                    return resp;
                }
            }
            else
            {
                resp.CurrentStatus = "REQUEST_FAILED";
                resp.Success = false;
                resp.Code = "105";
                resp.Message = $"签名验证失败";
                return resp;
            }

            #endregion

            return resp;
        }
    }
}