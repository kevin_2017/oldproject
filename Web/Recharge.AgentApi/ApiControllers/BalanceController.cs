﻿using CE.Utility.Text;
using Newtonsoft.Json;
using Recharge.AgentApi.Models;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace Recharge.AgentApi.ApiControllers
{
    /// <summary>
    /// 余额
    /// </summary>
    public class BalanceController : BaseController
    {
        // GET: GetBalance
        /// <summary>
        /// 查询余额
        /// </summary>
        /// <param name="param">branchcode=代理号,sign=签名值</param>
        /// <param name="key">商户私钥进行Base64加密后值</param>
        /// <returns></returns>
        [HttpGet, ActionName("GetBalance")]
        public string Get(string param, string key)
        {
            var agentCode = Tools.ParseCode(RequestContext.RouteData.Values);
            var resp = GetBranchProducts(param, key);
            WriteLog.Write(this.GetType().ToString(), $"代理{agentCode},返回数据：{JsonConvert.SerializeObject(resp)}");
            return JsonConvert.SerializeObject(resp);
        }
        /// <summary>
        /// 查询余额
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost, ActionName("GetBalance")]
        public string Get([FromBody]RequestParams req)
        {
            var agentCode = Tools.ParseCode(RequestContext.RouteData.Values);
            var resp = GetBranchProducts(req.Param, req.Key);
            WriteLog.Write(this.GetType().ToString(), $"代理{agentCode},返回数据：{JsonConvert.SerializeObject(resp)}");
            return JsonConvert.SerializeObject(resp);
        }
        readonly Dictionary<string, string> _parmaName = new Dictionary<string, string>()
        {
            {"branchcode","代理号"},
            {"requesttime","请求时间"},
            {"sign","签名"},
        };
        private AgentBalanceResp GetBranchProducts(string param, string key)
        {
            var agentCode = Tools.ParseCode(RequestContext.RouteData.Values);

            WriteLog.Write(this.GetType().ToString(), $"代理{agentCode},请求原始数据：param【{param}】,key【{key}】");
            var resp = new AgentBalanceResp();

            if (string.IsNullOrEmpty(agentCode))
            {
                resp.CurrentStatus = "REQUEST_FAILED";
                resp.Success = false;
                resp.Code = "118";
                resp.Message = $"代理编码错误";
                return resp;
            }

            if (string.IsNullOrEmpty(key))
            {
                resp.CurrentStatus = "REQUEST_FAILED";
                resp.Success = false;
                resp.Code = "101";
                resp.Message = $"缺少参数：Key";
                return resp;
            }

            if (string.IsNullOrEmpty(param))
            {
                resp.CurrentStatus = "REQUEST_FAILED";
                resp.Success = false;
                resp.Code = "101";
                resp.Message = $"缺少参数：param";
                return resp;
            }

            key = key.DecodeBase64String();

            param = param.DecodeBase64String();
            WriteLog.Write(this.GetType().ToString(), $"代理{agentCode},请求解密数据：param【{param}】,key【{key}】");
            var dic = Tools.GetParamArray(param);
            if (dic.Count == 0)
            {
                resp.CurrentStatus = "REQUEST_FAILED";
                resp.Success = false;
                resp.Code = "101";
                resp.Message = $"缺少参数：param";
                return resp;
            }
            foreach (var item in _parmaName)
            {
                if (!dic.ContainsKey(item.Key))
                {
                    resp.CurrentStatus = "REQUEST_FAILED";
                    resp.Success = false;
                    resp.Code = "101";
                    resp.Message = $"缺少参数：{item.Value}";
                    return resp;
                }

                if (string.IsNullOrEmpty(dic[item.Key]))
                {
                    resp.CurrentStatus = "REQUEST_FAILED";
                    resp.Success = false;
                    resp.Code = "102";
                    resp.Message = $"{item.Value} 值不能为空";
                    return resp;
                }
            }
            var sign = dic["sign"];
            var branchcode = dic["branchcode"];
            var requestTime = dic["requesttime"];
            dic.Remove("sign");

            #region 判断请求时间是否大于3分钟

            var requestDate = DateTime.MinValue;

            if (!DateTime.TryParseExact(requestTime, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out requestDate))
            {
                resp.CurrentStatus = "REQUEST_FAILED";
                resp.Success = false;
                resp.Code = "113";
                resp.Message = $"请求时间格式错误";
                return resp;
            }

            if ((DateTime.Now - requestDate).TotalMinutes > 3)
            {
                resp.CurrentStatus = "REQUEST_FAILED";
                resp.Success = false;
                resp.Code = "114";
                resp.Message = $"请求时间超过三分钟";
                return resp;
            }

            #endregion

            var agentInfo = new BLL.MS_BranchManage().GetModel(branchcode);

            if (!agentInfo.BranchCode.Equals(agentCode))
            {
                resp.CurrentStatus = "REQUEST_FAILED";
                resp.Success = false;
                resp.Code = "118";
                resp.Message = $"代理编码错误";
                return resp;
            }
            if (!Tools.VerifyParam(branchcode, key, agentInfo.PrivateKey, 6, 8, requestDate.ToString("yyyyMMdd")))
            {
                resp.CurrentStatus = "REQUEST_FAILED";
                resp.Success = false;
                resp.Code = "104";
                resp.Message = $"密钥错误";
                return resp;
            }
            var requeststr = Tools.CreateLinkString(Tools.SortDictionary(dic), "&", true);
            var verifySign = $"{requeststr}&key={agentInfo.PrivateKey}".ToMD5().ToLower();
            WriteLog.Write(this.GetType().ToString(), $"代理{agentCode},请求签名数据：requeststr【{requeststr}】key:【{key}】,verifySign【{verifySign}】");
            if (verifySign.Equals(sign.ToLower()))
            {
                resp.Data = agentInfo.Balance - agentInfo.FrozenMoney;
                resp.CurrentStatus = "SUCCESS";
                resp.Success = true;
                resp.Code = "000";
                resp.Message = "查询成功";
                return resp;

            }
            else
            {
                resp.CurrentStatus = "REQUEST_FAILED";
                resp.Success = false;
                resp.Code = "105";
                resp.Message = $"签名验证失败";
                return resp;

            }
        }
    }
}