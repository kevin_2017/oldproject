﻿using CE.Utility;
using CE.Utility.Text;
using Common;
using Model;
using Newtonsoft.Json;
using Recharge.AgentApi.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Web.Http;
using Common.DBUtility;

namespace Recharge.AgentApi.ApiControllers
{
    /// <summary>
    /// 话费充值
    /// </summary>
    public class RechargeApiController : BaseController
    {
        private readonly string _fileName = DateTime.Now.Hour.ToString();

        /// <summary>
        /// 手机充值
        /// </summary>
        /// <param name="param"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        [HttpGet, ActionName("PhoneRecharge")]
        public string PhoneRecharge(string param, string key)
        {
            var agentCode = Tools.ParseCode(RequestContext.RouteData.Values);
            var resp = PayRecharge(param, key);
            WriteLog.Write(this.GetType().ToString(), $"代理{agentCode},返回数据：{JsonConvert.SerializeObject(resp)}");
            return JsonConvert.SerializeObject(resp);
        }
        /// <summary>
        /// 手机充值
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost, ActionName("PhoneRecharge")]
        public string PhoneRecharge([FromBody]RequestParams req)
        {
            var agentCode = Tools.ParseCode(RequestContext.RouteData.Values);
            var resp = PayRecharge(req.Param, req.Key);
            WriteLog.Write(this.GetType().ToString(), $"代理{agentCode},返回数据：{JsonConvert.SerializeObject(resp)}");
            return JsonConvert.SerializeObject(resp);
        }


        readonly Dictionary<string, string> _parmaName = new Dictionary<string, string>()
        {
            {"branchcode","代理号"},
            {"branchorderno","订单号"},
            {"requesttime","请求时间"},
            {"phonenum","充值号码"},
            {"phonetype","号码类型（1、手机，2、固话）"},
            {"isptype","运营商类型（1、中国联通，2、中国移动，3、中国电信）"},
            {"rechargemoney","充值金额"},
            {"notifyurl","通知地址"},
            {"productid","商品ID"},
            {"extendparam","自定义扩展参数，原样返回"},
            {"sign","签名"},
        };

        private RechargeResp PayRecharge(string param, string key)
        {
            var agentCode = Tools.ParseCode(RequestContext.RouteData.Values);
            var orderNo = Tools.OrderNo();

            var methodName = Tools.GetCurrentMethod(1).Name;
            WriteLog.Write(methodName, $"代理{agentCode},请求原始数据：param【{param}】,key【{key}】");
            var resp = new RechargeResp();
            try
            {


                if (string.IsNullOrEmpty(agentCode))
                {
                    resp.CurrentStatus = "REQUEST_FAILED";
                    resp.Success = false;
                    resp.Code = "118";
                    resp.Message = $"代理编码错误";
                    return resp;
                }
                #region 判断是否缺少key和Param参数

                if (string.IsNullOrEmpty(key))
                {
                    resp.CurrentStatus = "REQUEST_FAILED";
                    resp.Success = false;
                    resp.Code = "101";
                    resp.Message = $"缺少参数：Key";
                    return resp;
                }

                if (string.IsNullOrEmpty(param))
                {
                    resp.CurrentStatus = "REQUEST_FAILED";
                    resp.Success = false;
                    resp.Code = "101";
                    resp.Message = $"缺少参数：param";
                    return resp;
                }

                try
                {
                    key = key.DecodeBase64String();
                }
                catch (Exception ex)
                {
                    WriteLog.WriteError(methodName, $"代理{agentCode},请求数据：param【{param}】,key【{key}】", ex);
                    resp.CurrentStatus = "REQUEST_FAILED";
                    resp.Success = false;
                    resp.Code = "119";
                    resp.Message = $"请求参数Key数据错误";
                    return resp;
                }

                try
                {
                    param = param.DecodeBase64String();
                }
                catch (Exception ex)
                {
                    WriteLog.WriteError(methodName, $"代理{agentCode},请求数据：param【{param}】,key【{key}】", ex);
                    resp.CurrentStatus = "REQUEST_FAILED";
                    resp.Success = false;
                    resp.Code = "120";
                    resp.Message = $"请求参数Param数据错误";
                    return resp;
                }
                WriteLog.Write(methodName, $"代理{agentCode},请求数据：param【{param}】,key【{key}】");

                var dic = Tools.GetParamArray(param);

                if (dic.Count == 0)
                {
                    resp.CurrentStatus = "REQUEST_FAILED";
                    resp.Success = false;
                    resp.Code = "101";
                    resp.Message = $"缺少参数：param";
                    return resp;
                }

                #endregion

                #region 判断Param里的参数是否缺少及是否为空

                foreach (var item in _parmaName)
                {
                    if (!dic.ContainsKey(item.Key))
                    {
                        resp.CurrentStatus = "REQUEST_FAILED";
                        resp.Success = false;
                        resp.Code = "101";
                        resp.Message = $"缺少参数：[{item.Key}:{item.Value}]";
                        return resp;
                    }

                    if (string.IsNullOrEmpty(dic[item.Key]) && !item.Key.Equals("extendparam"))
                    {
                        resp.CurrentStatus = "REQUEST_FAILED";
                        resp.Success = false;
                        resp.Code = "102";
                        resp.Message = $"{item.Value} 值不能为空";
                        return resp;
                    }
                }

                #endregion

                var sign = dic["sign"];
                var branchcode = dic["branchcode"];
                var productId = dic["productid"];
                var requestTime = dic["requesttime"];
                var phoneNum = dic["phonenum"];
                var rechargeMoney = dic["rechargemoney"];
                var notifyUrl = dic["notifyurl"];
                var branchOrderNo = dic["branchorderno"];
                var ispType = dic["isptype"];
                var phonetype = dic["phonetype"];
                var extendparam = dic["extendparam"];

                #region 判断请求时间是否大于3分钟

                var requestDate = DateTime.MinValue;

                if (!DateTime.TryParseExact(requestTime, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out requestDate))
                {
                    resp.CurrentStatus = "REQUEST_FAILED";
                    resp.Success = false;
                    resp.Code = "113";
                    resp.Message = $"请求时间格式错误";
                    return resp;
                }

                if ((DateTime.Now - requestDate).TotalMinutes > 3)
                {
                    resp.CurrentStatus = "REQUEST_FAILED";
                    resp.Success = false;
                    resp.Code = "114";
                    resp.Message = $"请求时间超过三分钟";
                    return resp;
                }

                #endregion

                #region 判断订单号是否存在

                Model.StrWhere strWhere = new StrWhere()
                {
                    IsWhereExist = true,
                    strWhere = new StringBuilder($" BranchCode='{branchcode}' and BranchOrderNo='{branchOrderNo}' ")
                };

                var oldOrder = new BLL.V8_OrderManage().GetModel(strWhere);
                if (oldOrder != null)
                {
                    resp.CurrentStatus = "REQUEST_FAILED";
                    resp.Success = false;
                    resp.Code = "112";
                    resp.Message = $"{branchOrderNo} 重复";
                    return resp;
                }

                #endregion

                #region 判断运营商是否正确
                //if (phonetype == "1")
                //    if (!StringUtil.IsPhoneNum(phoneNum))
                //    {
                //        resp.CurrentStatus = "REQUEST_FAILED";
                //        resp.Success = false;
                //        resp.Code = "104";
                //        resp.Message = $"手机号码格式错误";
                //        return resp;
                //    }

                phoneNum = phoneNum.Replace("-", "");

                if (phoneNum.Length > 11)
                    phoneNum = phoneNum.Substring(phoneNum.Length - 11, 11);

                var searchUrl = ConfigurationManager.AppSettings["QueryPhoneIspUrl"];

                WebClient wc = new WebClient();

                var resultStr = Encoding.UTF8.GetString(wc.GetData($"{searchUrl}{phoneNum}"));

                var resultJson = JsonConvert.DeserializeObject<PhoneIsp>(resultStr);

                WriteLog.Write(methodName, $"代理{agentCode},{orderNo} 请求号码归属地信息：{JsonConvert.SerializeObject(resultJson)}");
                if (resultJson.code != 0)
                {
                    resp.CurrentStatus = "REQUEST_FAILED";
                    resp.Success = false;
                    resp.Code = "108";
                    resp.Message = $"电话号码格式错误";
                    return resp;
                }

                var provinceName = resultJson.data.province;
                var cityName = resultJson.data.city;

                var provinceId = new BLL.V8_Province().GetModel(new StrWhere() { IsWhereExist = true, strWhere = new StringBuilder($" ProvinceName like'%{resultJson.data.province}%' ") })?.ID;
                var cityId = new BLL.V8_City().GetModel(new StrWhere() { IsWhereExist = true, strWhere = new StringBuilder($" CityName like'%{resultJson.data.city}%' ") })?.ID;
                WriteLog.Write(methodName, $"代理{agentCode},{orderNo} 请求号码归属地信息：{provinceId},{resultJson.data.province}");
                phonetype = resultJson.data.sp == "" ? "1" : "2";

                //（1、中国联通，2、中国移动，3、中国电信）

                if (resultJson.data.sp != "")
                    switch (resultJson.data.sp)
                    {
                        case "电信": ispType = "3"; break;
                        case "移动": ispType = "2"; break;
                        case "联通": ispType = "1"; break;
                        default:
                            ispType = ""; break;
                    }
                if (string.IsNullOrEmpty(ispType))
                {
                    resp.CurrentStatus = "REQUEST_FAILED";
                    resp.Success = false;
                    resp.Code = "106";
                    resp.Message = $"{phoneNum}未能查询到运营商信息";
                    return resp;
                }

                #endregion

                #region 判断当前代理相关信息 代理状态、余额、密钥信息

                var agentInfo = new BLL.MS_BranchManage().GetModel(branchcode);
                if (!agentInfo.BranchCode.Equals(agentCode))
                {
                    resp.CurrentStatus = "REQUEST_FAILED";
                    resp.Success = false;
                    resp.Code = "118";
                    resp.Message = $"代理编码错误";
                    return resp;
                }

                if (agentInfo.IsDelete == 1)
                {
                    resp.CurrentStatus = "REQUEST_FAILED";
                    resp.Success = false;
                    resp.Code = "103";
                    resp.Message = $"代理不能正常使用";
                    return resp;
                }

                if ((agentInfo.Balance - agentInfo.FrozenMoney) < Decimal.Parse(rechargeMoney))
                {
                    resp.CurrentStatus = "REQUEST_FAILED";
                    resp.Success = false;
                    resp.Code = "109";
                    resp.Message = $"账户余额不足";
                    return resp;
                }

                if (!Tools.VerifyParam(branchcode, key, agentInfo.PrivateKey, 6, 8, requestDate.ToString("yyyyMMdd")))
                {
                    resp.CurrentStatus = "REQUEST_FAILED";
                    resp.Success = false;
                    resp.Code = "104";
                    resp.Message = $"账户密钥信息错误";
                    return resp;
                }
                #endregion

                #region 判断代理商品是否可用

                Model.StrWhere str = new StrWhere()
                {
                    IsWhereExist = true,
                    strWhere = new StringBuilder($" BranchID={agentInfo.ID} and ProductID={productId} and ISPType={ispType} ")
                };
                var branchProduct = new BLL.V8_Product_Branch().GetModel(str);

                if (branchProduct == null)
                {
                    resp.CurrentStatus = "REQUEST_FAILED";
                    resp.Success = false;
                    resp.Code = "110";
                    resp.Message = $"暂不支持此商品";
                    return resp;
                }

                if (branchProduct.MinValue > Convert.ToDecimal(rechargeMoney) || branchProduct.MaxValue < Convert.ToDecimal(rechargeMoney))
                {
                    resp.CurrentStatus = "REQUEST_FAILED";
                    resp.Success = false;
                    resp.Code = "116";
                    resp.Message = $"充值金额范围：{branchProduct.MinValue}-{branchProduct.MaxValue}";
                    return resp;
                }

                if (branchProduct.ProvinceIds != null && branchProduct.ProvinceIds.Trim().Length > 3 && (!branchProduct.ProvinceIds.Contains(provinceId.ToString())))
                {
                    resp.CurrentStatus = "REQUEST_FAILED";
                    resp.Success = false;
                    resp.Code = "117";
                    resp.Message = $"暂不支持该区域充值";
                    return resp;
                }

                #endregion

                #region 判断是否有Jiaofei365的接口供应商如果有，则判断充值金额是不是为10的倍数

                var suppList = new BLL.SUP_SupplierManage().GetModelList(new StrWhere() { IsWhereExist = true, strWhere = new StringBuilder($" id in (select SupplierId  from MS_Branch_Supplier where BranchId={agentInfo.ID})") });
                foreach (var suppinfo in suppList)
                {
                    if (suppinfo.ApiCode.Equals("Jiaofei365"))
                    {
                        if (Convert.ToDecimal(rechargeMoney) % 10 != 0)
                        {
                            resp.CurrentStatus = "REQUEST_FAILED";
                            resp.Success = false;
                            resp.Code = "115";
                            resp.Message = $"充值金额只能是10的倍数元";
                            return resp;
                        }
                    }
                }

                #endregion


                dic.Remove("sign");

                var requeststr = Tools.CreateLinkString(Tools.SortDictionary(dic), "&", true);
                var verifySign = $"{requeststr}&key={ agentInfo.PrivateKey}".ToMD5().ToLower();
                WriteLog.Write(methodName, $"代理{agentCode},请求签名数据：requeststr【{requeststr}】key:【{key}】,verifySign【{verifySign}】");

                if (verifySign.Equals(sign.ToLower()))
                {

                    //2、 写订单
                    var orderModel = new Model.V8_OrderManage
                    {
                        CurentStatus = 2,
                        ComFrom = "接口",
                        Counts = 1,
                        OperationTime = DateTime.Now,
                        ProductName = branchProduct.ProductName,
                        RechargeNo = phoneNum,
                        TotalPrice = Convert.ToDecimal(rechargeMoney),
                        NotifyUrl = notifyUrl,
                        ProductID = Convert.ToInt64(productId),
                        BranchID = agentInfo.ID,
                        BranchName = agentInfo.BranchName,
                        OrderNo = orderNo,
                        PhoneType = Convert.ToInt32(phonetype),
                        IspType = Convert.ToInt32(ispType),
                        IspName = resultJson.data.sp,
                        ExtendParam = extendparam,
                        ProvinceId = provinceId == null ? 0 : (int)provinceId,
                        ProvinceName = provinceName,
                        BranchCode = agentCode,
                        CityId = cityId == null ? 0 : (int)cityId,
                        CityName = cityName
                    };

                    WriteLog.Write(methodName, string.Concat($"添加订单", $"代理{agentCode},订单信息：", Common.XmlSerializerHelp.XMLSerialize<Model.V8_OrderManage>(orderModel)));
                    orderModel.BranchOrderNo = branchOrderNo;
                    //添加数据入库

                    long orderId = 0;

                    orderId = new BLL.V8_OrderManage().Add(orderModel);

                    if (orderId > 0)
                    {
                        WriteLog.Write(methodName, string.Concat($"添加订单", $"代理{agentCode},订单号：{orderModel.OrderNo}", "增加订单成功"));
                        orderModel.BranchOrderNo = branchOrderNo;
                        //修改代理的冻结金额
                        DBHelper.ExecuteSql($"update MS_BranchManage set FrozenMoney=FrozenMoney+{orderModel.TotalPrice} where ID={orderModel.BranchID}");

                        //3、 往消息队列中发送订单号
                        try
                        {
                            var msmqName = ConfigurationManager.AppSettings["RechargeOrderQueueName"];
                            var msmqHelper = new MsmqHelper(msmqName);
                            msmqHelper.Send<string>(orderModel.OrderNo);

                            //4、返回消息
                            resp.CurrentStatus = "PAYING";
                            resp.Success = true;
                            resp.Code = "000";
                            resp.Message = $"充值请求成功";
                            resp.BranchId = branchcode;
                            resp.BranchOrderNo = branchOrderNo;
                        }
                        catch (Exception ex)
                        {
                            WriteLog.WriteError(_fileName, $"代理{agentCode},添加消息队列失败", ex);
                        }
                    }
                    else
                    {
                        resp.CurrentStatus = "REQUEST_FAILED";
                        resp.Success = false;
                        resp.Code = "111";
                        resp.Message = $"订单录入失败，请联系供应商";
                    }
                }
                else
                {
                    resp.CurrentStatus = "REQUEST_FAILED";
                    resp.Success = false;
                    resp.Code = "105";
                    resp.Message = $"签名验证失败";

                }
            }
            catch (Exception ex)
            {
                WriteLog.WriteError(methodName, $"代理{agentCode},请求数据：param【{param}】,key【{key}】", ex);
                resp.CurrentStatus = "REQUEST_FAILED";
                resp.Success = false;
                resp.Code = "121";
                resp.Message = $"其他错误";
            }
            return resp;
        }


    }
}