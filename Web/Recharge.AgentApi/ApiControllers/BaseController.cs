﻿using CE.Utility.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Recharge.AgentApi.ApiControllers
{
    public class BaseController : ApiController
    {
        #region 基本函数

        private string GetCurrDate()
        {
            return DateTime.Now.ToString("yyyyMMdd");
        }

        /// <summary>
        /// 转换时间
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        private DateTime GetTimestamp(string date)
        {
            double time = date.ToDouble();
            DateTime dt = new DateTime(0x7b2, 1, 1, 0, 0, 0).AddMilliseconds(time);
            return dt;
        }

        /// <summary>
        /// 将任意类型转换为日期型，当无法转换时返回 DateTime.MinValue 。
        /// </summary>
        /// <param name="dt">object 类型。</param>
        /// <returns>返回转换结果。</returns>
        protected static DateTime GetDateTime(object dt)
        {
            DateTime newdt;
            DateTime.TryParse(GetString(dt), out newdt);
            return newdt;
        }
        /// <summary>
        /// 将任意类型转换为整型，当无法转换时返回 0 。
        /// </summary>
        /// <param name="id">object 类型。</param>
        /// <returns>返回转换结果。</returns>
        protected static int GetInt(object id)
        {
            int newid;
            int.TryParse(GetString(id).Trim(), out newid);
            return newid;
        }
        /// <summary>
        /// 将任意类型转换为整型，当无法转换时返回 0 。
        /// </summary>
        /// <param name="id">object 类型。</param>
        /// <returns>返回转换结果。</returns>
        protected static decimal GetDecimal(object id)
        {
            decimal newid;
            decimal.TryParse(GetString(id).Trim(), out newid);
            return newid;
        }
        /// <summary>
        /// 将任意类型转换为字符串型，当无法转换时返回空字符串。
        /// </summary>
        /// <param name="str">object 类型。</param>
        /// <returns>返回转换结果。</returns>
        protected static string GetString(object str)
        {
            if (str == null)
                return String.Empty;
            else
                return str.ToString();
        }

        protected static string GetImgUrl(object strhtml)
        {
            return GetImgUrl(GetString(strhtml));
        }

        protected static string GetDate(object dt)
        {
            if (String.IsNullOrEmpty(GetString(dt))) return string.Empty;
            DateTime newdt;
            DateTime.TryParse(GetString(dt), out newdt);
            return newdt.ToString("yyyy-MM-dd");
        }

        protected static string GetTime(object dt)
        {
            if (String.IsNullOrEmpty(GetString(dt))) return string.Empty;
            DateTime newdt;
            DateTime.TryParse(GetString(dt), out newdt);
            return newdt.ToString("HH-mm-ss");
        }

        protected static string GetDateTime(object dt, string format)
        {
            if (String.IsNullOrEmpty(GetString(dt))) return string.Empty;
            DateTime newdt;
            DateTime.TryParse(GetString(dt), out newdt);
            return newdt.ToString(format);
        }
        #endregion

        
        #region 验证参数正确性
        protected bool VerifyParam(string client, string key, string keyB, decimal left, decimal right)
        {
            bool result = false;
            try
            {
                key = key.Substring((int)left, key.Length - (int)left);
                key = key.Substring(0, key.Length - (int)right);
                string tmpKey = (client + keyB + GetCurrDate()).ToMD5().ToLower();
                if (key.ToLower().Equals(tmpKey))
                {
                    result = true;
                }
            }
            catch (Exception)
            {
                result = false;
            }
            return result;
        }
        #endregion

         

        
    }
}