﻿using CE.Utility.Text;
using CE.Utility.Web;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Recharge.AgentApi
{
    public class Tools
    {


        #region 解析Url代码的值
        public static string ParseCode(IDictionary<string, object> dic)
        {
            var code = string.Empty;
            if (dic != null && dic["code"] != null)
            {
                code = dic["code"].ToString();
            }
            return code;
        }
        #endregion

        public static string GetCurrDate()
        {
            return DateTime.Now.ToString("yyyyMMdd");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="supplierCode"></param>
        /// <returns></returns>
        public static IList<Model.SUP_SupplierConfig> GetKeys(string supplierCode)
        {
            Model.StrWhere str = new Model.StrWhere()
            {
                IsWhereExist = true,
                strWhere = new StringBuilder($" SupplierName='{supplierCode}' order by Sort")
            };
            var list = new BLL.SUP_SupplierConfig().GetModelList(str);
            foreach (var key in list)
            {
                key.ConfigValue = key.ConfigValue.Replace("\r", "").Replace("\n", "").Replace(" ", "");
            }
            return list;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        public static Dictionary<string, string> SortDictionary(Dictionary<string, string> dic)
        {
            List<KeyValuePair<string, string>> myList = new List<KeyValuePair<string, string>>(dic);
            myList.Sort(delegate (KeyValuePair<string, string> s1, KeyValuePair<string, string> s2)
            {
                return s1.Key.CompareTo(s2.Key);
            });
            dic.Clear();
            foreach (KeyValuePair<string, string> pair in myList)
            {
                dic.Add(pair.Key, pair.Value);
            }
            return dic;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dicArray"></param>
        /// <param name="splitSymbol"></param>
        /// <param name="isRemoveEmpty"></param>
        /// <returns></returns>
        public static string CreateLinkString(Dictionary<string, string> dicArray, string splitSymbol = "&", bool isRemoveEmpty = false)
        {
            StringBuilder prestr = new StringBuilder();
            foreach (KeyValuePair<string, string> temp in dicArray)
            {
                if (isRemoveEmpty)
                {
                    if (string.IsNullOrEmpty(temp.Value))
                    {
                        continue;
                    }
                }
                prestr.Append($"{temp.Key}={temp.Value}{(string.IsNullOrEmpty(splitSymbol) ? "&" : splitSymbol)}");
            }

            //去掉最后一个&字符
            int nLen = prestr.Length;
            prestr.Remove(nLen - 1, 1);

            return prestr.ToString();
        }

        #region 生成支付订单号
        /// <summary>
        /// OrderNo
        /// </summary>
        /// <returns></returns>
        public static string OrderNo()
        {
            char[] constant = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
            System.Text.StringBuilder newRandom = new System.Text.StringBuilder(10);
            Random rd = new Random();
            for (int i = 0; i < 5; i++)
            {
                newRandom.Append(constant[rd.Next(5)]);
            }
            return $"{DateTime.Now.ToString("yyMMddHHmmssfff")}{newRandom}";
        }
        #endregion

        #region 获取调用链上的方法
        /// <summary>  
        /// 获取调用链上的方法  
        /// </summary>  
        /// <param name="depth">回朔深度</param>  
        public static MethodBase GetCurrentMethod(int depth)
        {
            try
            {
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace();
                return st.GetFrame(depth).GetMethod();
            }
            catch
            {
                return null;
            }
        }
        #endregion

        #region 获取URL中参数

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public static Dictionary<string, string> GetParamArray(string param)
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            param = param.Replace("\"", "");
            var sArray = param.Split('&');
            foreach (var item in sArray)
            {
                if (item.IndexOf("=") > 0)
                {
                    var index = item.IndexOf("=");
                    var key = item.Substring(0, index).Trim();
                    var value = item.Substring(index + 1, item.Length - (index + 1)).Trim();
                    dic.Add(key.ToLower(), value);
                }
                //dic.Add(item.Split('=')[0].ToLower(), item.Split('=')[1]);
            }
            return dic;
        }
        #endregion



        #region 验证参数正确性

        /// <summary>
        /// 验证参数合法性
        /// </summary>
        /// <param name="agent">代理ID</param>
        /// <param name="key">传输密钥</param>
        /// <param name="keyB">代理原始密钥</param>
        /// <param name="left">左长度</param>
        /// <param name="right">右长度</param>
        /// <param name="requestTime"></param>
        /// <returns></returns>
        public static bool VerifyParam(string agent, string key, string keyB, decimal left, decimal right,string requestTime)
        {
            var result = false;
            try
            {
                key = key.Substring((int)left, key.Length - (int)left);
                key = key.Substring(0, key.Length - (int)right);
                var tmpKey = (agent + keyB + requestTime).ToMD5().ToLower();
                if (key.ToLower().Equals(tmpKey))
                {
                    result = true;
                }
            }
            catch (Exception)
            {
                result = false;
            }
            return result;
        }
        #endregion


    }
}
