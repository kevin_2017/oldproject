﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Recharge.AgentApi
{
    public class ResponseParam
    {
        public string CurrentStatus { get; set; }

        public string ErrorCode { get; set; }

        public string ErrDesc { get; set; }
        public bool Result { get; set; }
    }
}