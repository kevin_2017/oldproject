﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Recharge.AgentApi.Models
{
    public class RechargeResp: BaseResp
    {  

        public string BranchOrderNo { get; set; }

        public string BranchId { get; set; } 
    }
}