﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Recharge.AgentApi.Models
{
    public class RequestParams
    {
        /// <summary>
        /// 参数信息
        /// </summary>
        public string Param
        {
            get;
            set;
        }

        /// <summary>
        /// 安全Key
        /// </summary>
        public string Key
        {
            get;
            set;
        }
    }
}