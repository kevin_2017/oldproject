﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Recharge.AgentApi
{
    public class PhoneIsp
    {
        public int code { get; set; }
        public Data data { get; set; }
    }

    public class Data
    {
        public string province { get; set; }
        public string city { get; set; }
        public string sp { get; set; }
    }
}