﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Recharge.AgentApi.Models
{
    public class AgentProduct
    {
        /// <summary>
        /// 商品类型ID
        /// </summary>		 
        public long TypeId { get; set; }

        /// <summary>
        /// 商品类型名称
        /// </summary>		 
        public string TypeName { get; set; }

        public long ProductId { get; set; }

        public string ProductName { get; set; }

        public int IspType { get; set; }

        public string IspName { get; set; }

        // public List<RechargeProvince> RechargeProvinces { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    //public class RechargeProvince
    //{
    //    public long ProvinceID { get; set; }

    //    public string ProvinceName { get; set; }
    //}
}