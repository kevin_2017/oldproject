﻿using CE.Utility;
using CE.Utility.Text;
using Common.DBUtility;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CE.Utility.Web;
using Newtonsoft.Json;
using Recharge.AgentApi.Models;

namespace Recharge.AgentApi.Controllers
{

    public class HunanwangdaController : BaseController
    {
        private string merCode = string.Empty;
        private string merKey = string.Empty;
        private readonly Model.SUP_SupplierManage suppModel = new BLL.SUP_SupplierManage().GetModel("Hunanwangda");

        /// <summary>
        /// 异步回调
        /// </summary>
        /// <returns></returns> 
        public string Callback()
        {
            WriteLog.Write("HunanwangdaController", $"地址:{Request.Url.ToString()}");
            var keys = Tools.GetKeys("Hunanwangda");
            merCode = keys[0].ConfigValue;
            merKey = keys[1].ConfigValue;

           //var  resultStr = GetStringInput();
           // WriteLog.Write("HunanwangdaController", $"地址:{Request.Url.ToString()}    返回数据{ HttpUtility.UrlDecode(resultStr)}");

            var User = GetStringParam("User", "");
            var OrderNo = GetStringParam("OrderNo", "");
            var State = GetStringParam("State", "");
            var Sign = GetStringParam("Sign", "");
            WriteLog.Write("HunanwangdaController", $"地址:{Request.Url.ToString()}?User={User}&OrderNo={OrderNo}&State={State}&Sign={Sign}");
            var supplierId = suppModel.ID;

            // User=cs558&OrderNo=18082022553554512121&State=success&Sign=e9261e72d3a6cb7ebb196c80f2787cf2

            if (string.IsNullOrEmpty(OrderNo))
            {
                WriteLog.Write("HunanwangdaController", $"没有接收到任何参数");
                return "没有获取到值";
            }

            var orderInfo = new BLL.V8_OrderManage().GetModel(OrderNo);
            if (orderInfo.CurentStatus == 3)
            {
                OperRequestRec(suppModel.ID, OrderNo, $"{OrderNo},充值返回状态:{(State.ToLower().Equals("success") ? "成功" : "失败")}");

                var signStr = $"{User}{OrderNo}{State}{merKey}".ToMD5().ToUpper();
                WriteLog.Write("HunanwangdaController", $"{OrderNo}  原始签名：{signStr}     返回签名值{Sign.ToUpper()}");

                if (signStr.Equals(Sign.ToUpper()))
                {
                    //1 成功，0、失败
                    if (State.ToLower().Equals("success"))
                    {
                        WriteLog.Write("HunanwangdaController", $"{OrderNo} 订单状态：{orderInfo.CurentStatus}, 充值返回状态:{(State.ToLower().Equals("success") ? "成功" : "失败")}");

                        //修改成功信息
                        orderInfo.RechargeMoney = orderInfo.TotalPrice;
                        orderInfo.CurentStatus = 1;
                        OperateSuccessOrder(orderInfo);
                        WriteLog.Write($"HunanwangdaController", $"{OrderNo} 发送成功通知");
                        return "OK";
                    }
                    else if (State.ToLower().Equals("fail"))
                    {
                        OperateErrorOrder(orderInfo.OrderNo);
                        WriteLog.Write($"HunanwangdaController", $"{OrderNo} 发送失败通知");
                        return "OK";
                    }
                    else
                    {
                        return "";
                    }
                }
                else
                {
                    WriteLog.Write("HunanwangdaController", $"{OrderNo}  原始签名：{signStr}     返回签名值{Sign} 签名错误");
                    return "";
                }
            }
            else
            {
                WriteLog.Write("HunanwangdaController", $"{OrderNo}  订单当前状态：{orderInfo.CurentStatus}");
                return "";
            }
        }




        public ActionResult PayResult()
        {
            ViewBag.Msg = "支付成功";
            return View();
        }

        /// <summary>
        /// 查询订单
        /// </summary>
        /// <param name="orderNo"></param>
        /// <returns></returns>
        public string QueryOrder(string orderNo)
        {
            var resp = new ChkQueryResp();
            var orderInfo = new BLL.V8_OrderManage().GetModel(orderNo);
            var keys = Tools.GetKeys("Hunanwangda");
            merCode = keys[0].ConfigValue;
            merKey = keys[1].ConfigValue;
            var Action = "QueryOrder";
            var User = merCode;
            var OrderNo = orderNo;
            var sign = $"{User}{OrderNo}{merKey}".ToMD5();
            var dic = new Dictionary<string, string>()
            {
                { "Action",Action},
                { "User",User},
                { "OrderNo",OrderNo},
                { "Sign",sign},
            };

            var rechargeSearchUrl = suppModel.RechargeSearchUrl;
            var paramData = GenGetParams(dic);
            WriteLog.Write("Hunanwangda.QueryOrder", $"查询请求数据：{rechargeSearchUrl},{paramData}");
            var httpRequestUtil = new HttpRequestUtil($"{rechargeSearchUrl}", HttpRequestUtil.Method.Post, HttpRequestUtil.ContentType.Form, paramData);
            var httpResult = httpRequestUtil.Request();
            var html = httpResult.Html;
            WriteLog.Write("Hunanwangda.QueryOrder", $"查询返回数据：{html}");
            if (httpResult.StatusCode == HttpStatusCode.OK ||
                httpResult.StatusCode == HttpStatusCode.InternalServerError)
            {
                if (!html.Contains("|"))
                {
                    resp.Success = false;
                    resp.Message = $"访问第三方服务器出错";
                }
                else
                {
                    var result = html.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                    if (result.Length == 2)
                    {
                        if (result[0].Equals("success"))
                        {
                            orderInfo.RechargeMoney = orderInfo.TotalPrice;
                            OperateSuccessOrder(orderInfo);

                        }
                        else if (result[0].Equals("fail"))
                        {
                            OperateErrorOrder(orderNo);
                        }

                        var msg = "";
                        msg = GetErrMsg(result[0]);

                        resp.Success = true;
                        resp.Message = $"请求成功,返回信息：{msg}";
                    }
                    else
                    {
                        resp.Success = false;
                        resp.Message = "数据错误";
                    }
                }
            }
            else
            {
                resp.Success = false;
                resp.Message = $"访问第三方服务器出错,状态:{httpResult.StatusCode},状态码:{(int)httpResult.StatusCode}";
            }

            return JsonConvert.SerializeObject(resp);
        }

        /// <summary>
        /// 查询余额
        /// </summary>
        /// <returns></returns>
        public string QueryBalance()
        {
            var keys = Tools.GetKeys("Hunanwangda");
            merCode = keys[0].ConfigValue;
            merKey = keys[1].ConfigValue;

            var Action = "QueryBalance";
            var User = merCode;
            var sign = $"{User}{merKey}".ToMD5();
            var dic = new Dictionary<string, string>()
            {
                { "Action",Action},
                { "User",User},
                { "Sign",sign}

            };
            var paramData = GenGetParams(dic);

            // 从远程接口获取

            var httpRequestUtil = new HttpRequestUtil($"{suppModel.Address}", HttpRequestUtil.Method.Post, HttpRequestUtil.ContentType.Form, paramData);
            var httpResult = httpRequestUtil.Request();
            var html = httpResult.Html;

            if (httpResult.StatusCode == HttpStatusCode.OK ||
                httpResult.StatusCode == HttpStatusCode.InternalServerError)
            {
                if (!html.Contains("|"))
                {
                    return "0";
                }
                else
                {
                    var result = html.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                    if (result.Length == 2)
                    {
                        if (result[0].Equals("success"))
                        {
                            return result[1];
                        }
                        else if (result[0].Equals("fail"))
                        {
                            return "0";
                        }
                    }
                    else
                    {
                        return "0";
                    }
                }
            }

            return "";
        }

        private string GetErrMsg(string status)
        {
            var msg = "";
            switch (status)
            {
                case "wait":
                    msg = "订单缴费等待";
                    break;
                case "success":
                    msg = "缴费成功";
                    break;

                case "fail":
                    msg = "失败";
                    break;
                case "Doubt":
                    msg = "存疑订单";
                    break;
                case "Action Error or Null":
                    msg = "调用方式错误或空";
                    break;
                case "Unauthorized access":
                    msg = "用户未被授权";
                    break;
                case "Mobile Error or Null":
                    msg = "号码错误或空";
                    break;
                case "OrderNo Error or Null":
                    msg = "订单错误或空";
                    break;
                case "Mobile_APIS_NetError":
                    msg = "接口网络错误(移动)";
                    break;
                case "Mobile_APIS_closed":
                    msg = "接口关闭或暂停(移动)";
                    break;
                case "Unicom_APIS_NetError":
                    msg = "接口网络错误(联通)";
                    break;
                case "Unicom_APIS_closed":
                    msg = "接口关闭或暂停(联通)";
                    break;
                case "Telecom_APIS_NetError":
                    msg = "接口网络错误(电信)";
                    break;
                case "Telecom_APIS_closed":
                    msg = "接口关闭或暂停(电信) ";
                    break;
                case "No data or Error!":
                    msg = "数据为空或错误";
                    break;
                case "Mobile User Money Null or Error":
                    msg = "号码 用户或金额为空或错误";
                    break;
                case "Insufficient account balance":
                    msg = "用户余额不足";
                    break;
                case "The same Mobile, After X Seconds to re submit":
                    msg = "重号,请在X秒后重新提交";
                    break;
                case "Failure of the network or other reasons":
                    msg = "网络中断或其它原因失败";
                    break;
                case "Duplicate Order":
                    msg = "因订单号重复失败";
                    break;
            }
            return msg;
        }
    }




}