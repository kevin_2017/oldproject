﻿using CE.Utility;
using CE.Utility.Text;
using Common.DBUtility;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CE.Utility.Web;
using Newtonsoft.Json;
using Recharge.AgentApi.Models;

namespace Recharge.AgentApi.Controllers
{
    //未开发
    public class Jiaofei100Controller : BaseController
    {
        private string merCode = string.Empty;
        private string merKey = string.Empty;
        private readonly Model.SUP_SupplierManage suppModel = new BLL.SUP_SupplierManage().GetModel("Jiaofei100");

        /// <summary>
        /// 异步回调
        /// </summary>
        /// <returns></returns> 
        public string Callback()
        {
            var keys = Tools.GetKeys("Jiaofei100");
            merCode = keys[0].ConfigValue;
            merKey = keys[1].ConfigValue;         

            var apiid = GetStringParam("APIID", "");
            var tradeType = GetStringParam("TradeType", "");
            var outId = GetStringParam("OutID", "");
            var orderId = GetStringParam("OrderID", "");
            var account = GetStringParam("Account", "");
            var totalPrice = GetStringParam("TotalPrice", "");
            var state = GetStringParam("State", "");
            var supOrderInfo = GetStringParam("OrderInfo", "");
            var sign = GetStringParam("Sign", "");

            WriteLog.Write("Jiaofei100Controller", $"地址:{Request.Url.ToString()}，apiid={apiid},tradeType={tradeType},outId={outId},orderId={orderId},account={account},totalPrice={totalPrice},state={state},supOrderInfo={supOrderInfo},sign={sign}");

            if (string.IsNullOrEmpty(outId))
            {
                WriteLog.Write("Jiaofei100Controller", $"没有接收到任何参数");
                return "没有获取到值";
            }
            OperRequestRec(suppModel.ID, outId, $"{Request.Url.ToString()}，充值返回状态：{(state.Equals("10027")?"成功":"失败")}");
            var orderInfo = new BLL.V8_OrderManage().GetModel(outId);
            if (orderInfo == null)
            {
                WriteLog.Write($"Jiaofei100Controller", $"{outId} 该订单号没有相关订单信息");
                return "";
            }
            else if (orderInfo.CurentStatus == 3)
            {
                if (state.Equals("10027"))
                {
                    var signStr = $"APIID={apiid}&Account={account}&OrderID={orderId}&OutID={outId}&State={state}&TradeType={tradeType}&TotalPrice={totalPrice}&APIKEY={merKey}".ToMD5().ToUpper();

                    if (signStr.Equals(sign))
                    {
                        orderInfo.SysSerialNum = orderId;
                        orderInfo.RechargeMoney = Convert.ToDecimal(totalPrice) / 1000;
                        OperateSuccessOrder(orderInfo);

                        WriteLog.Write($"Jiaofei100Controller", $"{outId} 发送成功通知");

                        return "success";
                    }
                    else
                    {
                        WriteLog.Write("Jiaofei100Controller", $"{outId}  原始签名：{signStr}     返回签名值{sign} 签名错误");
                        return "";
                    }

                }
                else
                {
                    OperateErrorOrder(orderInfo.OrderNo);
                    WriteLog.Write($"Jiaofei100Controller", $"{outId} 发送失败通知");

                    return "success";
                }
            }
            else
            {
                WriteLog.Write("Jiaofei100Controller", $"{outId}  订单当前状态{orderInfo.CurentStatus}");
                return "success";
            }
        }


        public ActionResult PayResult()
        {
            ViewBag.Msg = "支付成功";
            return View();
        }

        /// <summary>
        /// 查询订单
        /// </summary>
        /// <param name="orderNo"></param>
        /// <returns></returns>
        public string QueryOrder(string orderNo)
        {
            var resp = new ChkQueryResp();
            var orderInfo = new BLL.V8_OrderManage().GetModel(orderNo);
            var keys = Tools.GetKeys("Jiaofei100");
            merCode = keys[0].ConfigValue;
            merKey = keys[1].ConfigValue;
            var account = merCode;
            var orderid = orderNo;
            var sign = $"APIID={merCode}&OrderID={orderid}&APIKEY={merKey}".ToMD5().ToUpper();
            var dic = new Dictionary<string, string>()
            {
                { "APIID",account},
                { "OrderID",orderid},
                { "Sign",sign},
            };

            var rechargeSearchUrl = suppModel.RechargeSearchUrl;
            var paramData = GenGetParams(dic);
            WriteLog.Write("Jiaofei100.QueryOrder", $"查询请求数据：{rechargeSearchUrl},{paramData}");
            var httpRequestUtil = new HttpRequestUtil($"{rechargeSearchUrl}?{paramData}", HttpRequestUtil.Method.Get);
            var httpResult = httpRequestUtil.Request();
            var html = httpResult.Html;
            WriteLog.Write("Jiaofei100.QueryOrder", $"查询返回数据：{html}");
            if (httpResult.StatusCode == HttpStatusCode.OK ||
                httpResult.StatusCode == HttpStatusCode.InternalServerError)
            {
                if (!html.Contains("Code"))
                {
                    resp.Success = false;
                    resp.Message = $"访问第三方服务器出错";
                }
                else
                {
                    var result = JsonConvert.DeserializeAnonymousType(html, new { Code = "", OrderID = "", OutID = "", Msg = "" });

                    if (result.Code.Equals("10027"))
                    {
                        orderInfo.RechargeMoney = orderInfo.TotalPrice;
                        orderInfo.SysSerialNum = result.OrderID;
                        OperateSuccessOrder(orderInfo);
                    }
                    else
                    {
                        OperateErrorOrder(orderNo);
                    }

                    var msg = ErrorMsg(result.Code);

                    resp.Success = true;
                    resp.Message = $"请求成功,返回信息：{result.Code}，{msg}";
                }

            }
            else
            {
                resp.Success = false;
                resp.Message = $"访问第三方服务器出错,状态:{httpResult.StatusCode},状态码:{(int)httpResult.StatusCode}";
            }

            return JsonConvert.SerializeObject(resp);
        }

        /// <summary>
        /// 查询余额
        /// </summary>
        /// <returns></returns>
        public string QueryBalance()
        {
            var keys = Tools.GetKeys("Jiaofei100");
            merCode = keys[0].ConfigValue;
            merKey = keys[1].ConfigValue;

            var account = merCode;
            var sign = $"APIID={account}&APIKEY={merKey}".ToMD5().ToUpper();
            var dic = new Dictionary<string, string>()
            {
                { "APIID",account},
                { "Sign",sign}

            };
            var paramData = GenGetParams(dic);

            // 从远程接口获取
            var httpRequestUtil = new HttpRequestUtil($"{suppModel.Address}?{paramData}", HttpRequestUtil.Method.Get);
            var httpResult = httpRequestUtil.Request();
            var html = httpResult.Html;
            if (httpResult.StatusCode == HttpStatusCode.OK ||
                httpResult.StatusCode == HttpStatusCode.InternalServerError)
            {
                if (!html.Contains("APIID"))
                {
                    return "0";
                }
                else
                {
                    var result = JsonConvert.DeserializeAnonymousType(html, new { APIID = "", Money = "" });

                    return result.Money;
                }
            }

            return "";
        }

        private string ErrorMsg(string errCode)
        {
            var msg = "";
            switch (errCode)
            {
                case "10007":
                    msg = "参数错误";
                    break;
                case "10008":
                    msg = "订单超时";
                    break;
                case "10009":
                    msg = "参数校验错误";
                    break;
                case "10010":
                    msg = "代理商 ID 不存在";
                    break;
                case "10011":
                    msg = "订单号长度大于36";
                    break;
                case "10012":
                    msg = "代理商状态错误";
                    break;
                case "10013":
                    msg = "账户余额不足";
                    break;
                case "10014":
                    msg = "IP 地址验证失败";
                    break;
                case "10015":
                    msg = "充值号码有误";
                    break;
                case "10016":
                    msg = "暂不支持该号码";
                    break;
                case "10017":
                    msg = "禁止采购该商品";
                    break;
                case "10018":
                    msg = "订单提交成功";
                    break;
                case "10020":
                    msg = "订单提交失败";
                    break;
                case "10021":
                    msg = "未知错误";
                    break;
                case "10022":
                    msg = "订单号重复";
                    break;
                case "10024":
                    msg = "暂不支持该面值";
                    break;
                case "10025":
                    msg = "订单处理中";
                    break;
                case "10026":
                    msg = "交易失败";
                    break;
                case "10027":
                    msg = "交易成功";
                    break;
                case "10029":
                    msg = "订单不存在";
                    break;
            }

            return msg;
        }
    }
}