﻿using CE.Utility;
using CE.Utility.Text;
using Common.DBUtility;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using CE.Utility.Web;
using Newtonsoft.Json;
using Recharge.AgentApi.Models;

namespace Recharge.AgentApi.Controllers
{
    public class YouchuangController : BaseController
    {

        private string merCode = string.Empty;
        private string merKey = string.Empty;
        private readonly Model.SUP_SupplierManage suppModel = new BLL.SUP_SupplierManage().GetModel("Youchuang");
        /// <summary>
        /// 异步回调
        /// </summary>
        /// <returns></returns> 
        public string Callback()
        {
            WriteLog.Write("YouchuangController", $"地址:{Request.Url.ToString()}");
            var orderid = GetStringParam("id", "");
            var status = GetStringParam("st", "");
            var supplierId = suppModel.ID;
            var orderInfo = new BLL.V8_OrderManage().GetModel(orderid);
            WriteLog.Write($"YouchuangController", $"{orderid} 订单状态：{orderInfo.CurentStatus},充值返回状态:{(status.Equals("2") ? "成功" : "失败")}");
            if (orderInfo.CurentStatus == 3)
            {
                OperRequestRec(supplierId, orderid,$"{ Request.Url.ToString()}，充值返回状态:{(status.Equals("2") ? "成功" : "失败")}");
                if (status.Equals("2"))
                {
                    orderInfo.RechargeMoney = orderInfo.TotalPrice;
                    OperateSuccessOrder(orderInfo);

                    WriteLog.Write($"YouchuangController", $"{orderid} 发送成功通知");
                }
                else
                {
                    OperateErrorOrder(orderInfo.OrderNo);
                    WriteLog.Write($"YouchuangController", $"{orderid} 发送失败通知");
                }
                return "ok";
            }
            else
            {
                WriteLog.Write("YouchuangController", $"{orderid}  订单当前状态{orderInfo.CurentStatus}");
                return "ok";
            }
        }


        public ActionResult PayResult()
        {
            ViewBag.Msg = "支付成功";
            return View();
        }

        /// <summary>
        /// 查询订单
        /// </summary>
        /// <param name="orderNo"></param>
        /// <returns></returns>
        public string QueryOrder(string orderNo)
        {
            var resp = new ChkQueryResp();
            var orderInfo = new BLL.V8_OrderManage().GetModel(orderNo);
            var keys = Tools.GetKeys("Youchuang");
            merCode = keys[0].ConfigValue;
            merKey = keys[1].ConfigValue;

            var rechargeSearchUrl = suppModel.RechargeSearchUrl;
            var uid = merCode;
            var od = orderNo;
            var key = $"uid={uid}&od={od}&sn={merKey}".ToMD5();
            var xmlStr = @"<items>" +
                         "<ob>odstate</ob>" +
                         "<uid>" + uid + "</uid>" +
                         "<od>" + od + "</od>" +
                         "<key>" + key + "</key>" +
                         "</items>";

            WriteLog.Write("Youchuang.QueryOrder", $"查询请求数据：{rechargeSearchUrl},{xmlStr}");
            var httpRequestUtil = new HttpRequestUtil($"{rechargeSearchUrl}", HttpRequestUtil.Method.Post, HttpRequestUtil.ContentType.Xml, xmlStr);
            var httpResult = httpRequestUtil.Request();
            var html = httpResult.Html;
            WriteLog.Write("Youchuang.QueryOrder", $"充值返回数据：{html}");

            if (httpResult.StatusCode == HttpStatusCode.OK ||
            httpResult.StatusCode == HttpStatusCode.InternalServerError)
            {
                if (!html.Contains("items"))
                {
                    resp.Success = false;
                    resp.Message = $"访问第三方服务器出错:";
                }
                else
                {
                    var xml = new XmlDocument();
                    xml.LoadXml(html);
                    var status = xml?.SelectSingleNode("items/state")?.InnerText;

                    var errMsg = "";

                    switch (status)
                    {
                        case "100":
                            errMsg = "成功";
                            break;
                        case "101":
                            errMsg = "处理中";
                            break;
                        case "102":
                            errMsg = "退款";
                            break;
                        case "1006":
                            errMsg = "无此订单号";
                            break;

                    }

                    resp.Success = true;
                    resp.Message = $"请求成功,返回信息：{status},{errMsg}";

                    if (status.Equals("100"))
                    {
                        orderInfo.RechargeMoney = orderInfo.TotalPrice;
                        OperateSuccessOrder(orderInfo);
                    }
                    else if (status.Equals("102"))
                    {
                        OperateErrorOrder(orderNo);
                    }
                }
            }
            else
            {
                resp.Success = false;
                resp.Message = $"访问第三方服务器出错,状态:{httpResult.StatusCode},状态码:{(int)httpResult.StatusCode}";
            }
            return JsonConvert.SerializeObject(resp); ;
        }


        /// <summary>
        /// 查询余额
        /// </summary>
        /// <returns></returns>
        public string QueryBalance()
        {
            var keys = Tools.GetKeys("Youchuang");
            merCode = keys[0].ConfigValue;
            merKey = keys[1].ConfigValue;
            var supplierInfo = new BLL.SUP_SupplierManage().GetModel("Youchuang");
            var uid = merCode;
            var key = $"uid={merCode}&sn={merKey}".ToMD5();
            var xmlStr = @"<items>" +
                         "<ob>balance</ob>" +
                         "<uid>" + uid + "</uid>" +
                         "<key>" + key + "</key>" +
                         "</items>";
            var httpRequestUtil = new HttpRequestUtil($"{supplierInfo.Address}", HttpRequestUtil.Method.Post, HttpRequestUtil.ContentType.Xml, xmlStr);
            var httpResult = httpRequestUtil.Request();
            var html = httpResult.Html;

            var xml = new XmlDocument();
            xml.LoadXml(html);
            var balance = xml?.SelectSingleNode("items/balance")?.InnerText;
            return balance;
        }



        private string GetErrMsg(string status)
        {
            var errMsg = "";

            switch (status)
            {
                case "1000":
                    errMsg = "接口关闭中";
                    break;
                case "1001":
                    errMsg = "充值关闭中";
                    break;
                case "1002":
                    errMsg = "签名错误";
                    break;
                case "1003":
                    errMsg = "参数提交错误";
                    break;
                case "1004":
                    errMsg = "地区维护或当日充值数超过五笔";
                    break;
                case "1005":
                    errMsg = "余额不足";
                    break;
                case "1006":
                    errMsg = "无此订单号";
                    break;
                case "1007":
                    errMsg = "订单号重复";
                    break;
            }

            return errMsg;
        }
    }
}