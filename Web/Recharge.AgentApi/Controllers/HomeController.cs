﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Recharge.AgentApi.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

        public ActionResult BaseMethod()
        {
            ViewBag.Title = "";

            return View();
        }
        public ActionResult MethodInfo()
        {
            ViewBag.Title = "";

            return View();
        }
        public ActionResult UpdateLog()
        {
            ViewBag.Title = "";

            return View();
        }
    }


}
