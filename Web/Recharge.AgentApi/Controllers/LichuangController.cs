﻿
using CE.Utility;
using CE.Utility.Text;
using Common.DBUtility;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using CE.Utility.Web;
using Newtonsoft.Json;
using Recharge.AgentApi.Models;
using Newtonsoft.Json.Linq;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Text;

namespace Recharge.AgentApi.Controllers
{
    public class LichuangController : BaseController
    {

        private string merCode = string.Empty;
        private string merKey = string.Empty;
        private readonly Model.SUP_SupplierManage suppModel = new BLL.SUP_SupplierManage().GetModel("Lichuang");
        /// <summary>
        /// 异步回调
        /// </summary>
        /// <returns></returns> 
        public string Callback()
        {

            var Param = GetStringParam("Param", "");
            var Key = GetStringParam("Key", "");
            try
            {
                Key = Key.DecodeBase64String();

                Param = Param.DecodeBase64String();
            }
            catch (Exception ex)
            {
                WriteLog.Write("LichuangController", $"错误信息:{ex.Message},Param:{Param},Key:{Key}");
            }
            WriteLog.Write("LichuangController", $"地址:{Request.Url.ToString()},Param:{Param},Key:{Key}");
            var dic = Tools.GetParamArray(Param);
            var requestTime = dic["requesttime"];
            var requestDate = DateTime.MinValue;
            var orderid = dic["branchorderno"];
            var sign = dic["sign"].ToString();
            var status = dic["status"].ToString();
            dic.Remove("sign");
            DateTime.TryParseExact(requestTime, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out requestDate);

            if (!Tools.VerifyParam(merCode, Key, merKey, 6, 8, requestDate.ToString("yyyyMMdd")))
            {
                WriteLog.Write("LichuangController", $"订单号:{orderid},Param:{Param},Key:{Key},密钥错误");
                return "FAIL";
            }

            var requeststr = Tools.CreateLinkString(Tools.SortDictionary(dic), "&", true);
            var verifySign = $"{requeststr}&key={ merKey}".ToMD5().ToLower();
            if (sign.Equals(verifySign))
            {
                WriteLog.Write($"LichuangController", $"{orderid} 订单状态：{status},充值返回状态:{(status.Equals("1") ? "成功" : "失败")}");
                var orderInfo = new BLL.V8_OrderManage().GetModel(orderid);
                if (status == "1")
                {
                    orderInfo.RechargeMoney = orderInfo.TotalPrice;
                    OperateSuccessOrder(orderInfo);

                    WriteLog.Write($"LichuangController", $"{orderid} 发送成功通知");
                }
                else
                {
                    OperateErrorOrder(orderInfo.OrderNo);
                    WriteLog.Write($"LichuangController", $"{orderid} 发送失败通知");
                }
                return "SUCCESS";
            }
            else
            {
                WriteLog.Write("LichuangController", $"订单号:{orderid},Param:{Param},Key:{Key},生成签名：{verifySign},签名验证失败");
                return "FAIL";
            }

        }


        public ActionResult PayResult()
        {
            ViewBag.Msg = "支付成功";
            return View();
        }

        /// <summary>
        /// 查询订单
        /// </summary>
        /// <param name="orderNo"></param>
        /// <returns></returns>
        public string QueryOrder(string orderNo)
        {
            var resp = new ChkQueryResp();
            var orderInfo = new BLL.V8_OrderManage().GetModel(orderNo);
            var keys = Tools.GetKeys("Lichuang");
            merCode = keys[0].ConfigValue;
            merKey = keys[1].ConfigValue;

            Dictionary<string, string> dic = new Dictionary<string, string>() {
                {"branchcode",merCode },
                {"branchorderno",orderNo },
                {"requesttime",DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")},
            };
            var signStr = $"{Tools.CreateLinkString(Tools.SortDictionary(dic), "&", true)}&key={merKey}";
            dic.Add("sign", signStr.ToMD5().ToLower());
            var Param = Tools.CreateLinkString(Tools.SortDictionary(dic)).EncodeBase64String();
            var Key = GetKeyB(merCode, merKey).EncodeBase64String();
            var requestUrl = $"{suppModel.Address}/api/{merCode}/Order/QueryOrder";
            var requestContent = $"Param={Param}&&Key={Key}";

            WriteLog.Write("Lichuang.QueryOrder", $"查询请求数据：{requestUrl},{requestContent}");
            var httpRequestUtil = new HttpRequestUtil(requestUrl, HttpRequestUtil.Method.Post, requestContent);
            var httpResult = httpRequestUtil.Request();
            var html = httpResult.Html;
            WriteLog.Write("Lichuang.QueryOrder", $"充值返回数据：{html}");
            if (httpResult.StatusCode == HttpStatusCode.OK || httpResult.StatusCode == HttpStatusCode.InternalServerError)
            {
                var result = (BalanceResult)JsonConvert.DeserializeObject(JsonConvert.DeserializeObject(html).ToString(), typeof(BalanceResult));
                if (result.Success && result.Code.Equals("000"))
                {
                    if (result.CurrentStatus.Equals("SUCCESS"))
                    {
                        orderInfo.RechargeMoney = orderInfo.TotalPrice;
                        OperateSuccessOrder(orderInfo);
                    }
                    else
                    {
                        OperateErrorOrder(orderNo);
                    }
                }

                resp.Success = true;
                resp.Message = $"访问第三方返回信息,状态:{result.Message},状态码:{result.CurrentStatus}";

            }
            else
            {
                resp.Success = false;
                resp.Message = $"访问第三方服务器出错,状态:{httpResult.StatusCode},状态码:{(int)httpResult.StatusCode}";
            }

            return JsonConvert.SerializeObject(resp); ;
        }

        private static string GetKeyB(string _branchCode, string _key)
        {
            var newStr = $"{_branchCode}{_key}{Tools.GetCurrDate()}".ToMD5().ToLower();
            newStr = $"{StringUtil.GetRandomString(6)}{newStr}{StringUtil.GetRandomString(8)}";
            return newStr;
        }
        /// <summary>
        /// 查询余额
        /// </summary>
        /// <returns></returns>
        public string QueryBalance()
        {
            var keys = Tools.GetKeys("Lichuang");
            merCode = keys[0].ConfigValue;
            merKey = keys[1].ConfigValue;
            var balance = "0.00";
            Dictionary<string, string> dic = new Dictionary<string, string>() {
                {"branchcode",merCode },
                {"requesttime",DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")},
            };
            var signStr = $"{Tools.CreateLinkString(Tools.SortDictionary(dic), "&", true)}&key={merKey}";
            dic.Add("sign", signStr.ToMD5().ToLower());
            var Param = Tools.CreateLinkString(Tools.SortDictionary(dic)).EncodeBase64String();
            var Key = GetKeyB(merCode, merKey).EncodeBase64String();
            var httpRequestUtil = new HttpRequestUtil($"{suppModel.Address}/api/{merCode}/Balance/getBalance", HttpRequestUtil.Method.Post, $"Param={Param}&&Key={Key}");
            var httpResult = httpRequestUtil.Request();
            var html = httpResult.Html;
            if (httpResult.StatusCode == HttpStatusCode.OK || httpResult.StatusCode == HttpStatusCode.InternalServerError)
            {

                var result = (BalanceResult)JsonConvert.DeserializeObject(JsonConvert.DeserializeObject(html).ToString(), typeof(BalanceResult));
                if (result.Success && result.Code.Equals("000"))
                    balance = result.Data.ToString();
            }
            return balance;
        }




        private string GetErrMsg(string status)
        {
            var errMsg = "";

            switch (status)
            {
                case "1000":
                    errMsg = "接口关闭中";
                    break;
                case "1001":
                    errMsg = "充值关闭中";
                    break;
                case "1002":
                    errMsg = "签名错误";
                    break;
                case "1003":
                    errMsg = "参数提交错误";
                    break;
                case "1004":
                    errMsg = "地区维护或当日充值数超过五笔";
                    break;
                case "1005":
                    errMsg = "余额不足";
                    break;
                case "1006":
                    errMsg = "无此订单号";
                    break;
                case "1007":
                    errMsg = "订单号重复";
                    break;
            }

            return errMsg;
        }
    }

    public class BalanceResult
    {
        public bool Success { get; set; }

        public string Code { get; set; }

        public string Message { get; set; }

        public string CurrentStatus { get; set; }

        public decimal Data { get; set; }

    }


}
