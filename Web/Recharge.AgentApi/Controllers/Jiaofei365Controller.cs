﻿using CE.Utility;
using CE.Utility.Text;
using Common.DBUtility;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CE.Utility.Web;
using Newtonsoft.Json;
using Recharge.AgentApi.Models;

namespace Recharge.AgentApi.Controllers
{

    public class Jiaofei365Controller : BaseController
    {
        private string merCode = string.Empty;
        private string merKey = string.Empty;
        private readonly Model.SUP_SupplierManage suppModel = new BLL.SUP_SupplierManage().GetModel("Jiaofei365");

        /// <summary>
        /// 异步回调
        /// </summary>
        /// <returns></returns> 
        public string Callback()
        {
            WriteLog.Write("Jiaofei365Controller", $"地址:{Request.Url.ToString()}");
            var keys = Tools.GetKeys("Jiaofei365");
            merCode = keys[0].ConfigValue;
            merKey = keys[1].ConfigValue;
            
            var resultStr = "";
            try
            {
                resultStr = GetStringInput();
                WriteLog.Write("Jiaofei365Controller", $"地址:{Request.Url.ToString()}    返回数据{ HttpUtility.UrlDecode(resultStr)}");
            }
            catch (Exception ex)
            {
                WriteLog.Write("Jiaofei365Controller", $"地址:{Request.Url.ToString()}    返回数据错误：{ex.Message}");
                return "返回参数错误";
            }

            Dictionary<string, string> dic = GetValue(resultStr);

            var phone = dic["phone"];
            var cutAmount = dic["cut_amount"];
            var billid = dic["billid"];
            var message = dic["message"];
            var orderid = dic["orderid"];
            var state = dic["state"];
            var sign = dic["sign"];

            if (string.IsNullOrEmpty(orderid))
            {
                WriteLog.Write("Jiaofei365Controller", $"没有接收到任何参数");
                return "没有获取到值";
            }

            var orderInfo = new BLL.V8_OrderManage().GetModel(orderid);
            if (orderInfo.CurentStatus == 3)
            { 
                OperRequestRec(suppModel.ID, orderid, $"{resultStr},充值返回状态:{(state.Equals("1") ? "成功" : "失败")}");

                var signStr = $"{orderid}{billid}{state}&{merKey}".ToMD5().ToUpper();
                WriteLog.Write("Jiaofei365Controller", $"{orderid}  原始签名：{signStr}     返回签名值{sign}");

                if (signStr.Equals(sign))
                {
                    //1 成功，0、失败
                    if (state.Equals("1"))
                    {
                        WriteLog.Write("Jiaofei365Controller", $"{orderid} 订单状态：{orderInfo.CurentStatus}, 充值返回状态:{(state.Equals("1") ? "成功" : "失败")}");
                        
                        //修改成功信息
                        orderInfo.RechargeMoney = Convert.ToDecimal(cutAmount);
                        orderInfo.CurentStatus = 1;
                        OperateSuccessOrder(orderInfo);
                        WriteLog.Write($"Jiaofei365Controller", $"{orderid} 发送成功通知");
                        return "ok";
                    }
                    else if (state.Equals("0"))
                    {
                        OperateErrorOrder(orderInfo.OrderNo);
                        WriteLog.Write($"Jiaofei365Controller", $"{orderid} 发送失败通知");
                        return "ok";
                    }
                    else
                    {
                        return "";
                    }
                }
                else
                {
                    WriteLog.Write("Jiaofei365Controller", $"{orderid}  原始签名：{signStr}     返回签名值{sign} 签名错误");
                    return "";
                }
            }
            else
            {
                WriteLog.Write("Jiaofei365Controller", $"{orderid}  订单当前状态{orderInfo.CurentStatus}");
                return "";
            }
        }


        private Dictionary<string, string> GetValue(string urlParm)
        {
            string[] parStr = urlParm.Split(new[] { '&' }, StringSplitOptions.RemoveEmptyEntries);
            Dictionary<string, string> dic = new Dictionary<string, string>();

            foreach (var item in parStr)
            {
                string[] keyValue = item.Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries);
                if (keyValue.Length == 2)
                {
                    dic.Add(keyValue[0], keyValue[1]);
                }
            }
            return dic;
        }

        public ActionResult PayResult()
        {
            ViewBag.Msg = "支付成功";
            return View();
        }

        /// <summary>
        /// 查询订单
        /// </summary>
        /// <param name="orderNo"></param>
        /// <returns></returns>
        public string QueryOrder(string orderNo)
        {
            var resp = new ChkQueryResp();
            var orderInfo = new BLL.V8_OrderManage().GetModel(orderNo);
            var keys = Tools.GetKeys("Jiaofei365");
            merCode = keys[0].ConfigValue;
            merKey = keys[1].ConfigValue;
            var account = merCode;
            var orderid = orderNo;
            var sign = $"{account}{orderid}&{merKey}".ToMD5().ToUpper();
            var dic = new Dictionary<string, string>()
            {
                { "account",account},
                { "orderid",orderid},
                { "sign",sign},
            };

            var rechargeSearchUrl = suppModel.RechargeSearchUrl;
            var paramData = GenGetParams(dic);
            WriteLog.Write("Jiaofei365.QueryOrder", $"查询请求数据：{rechargeSearchUrl},{paramData}");
            var httpRequestUtil = new HttpRequestUtil($"{rechargeSearchUrl}?{paramData}", HttpRequestUtil.Method.Get);
            var httpResult = httpRequestUtil.Request();
            var html = httpResult.Html;
            WriteLog.Write("Jiaofei365.QueryOrder", $"查询返回数据：{html}");
            if (httpResult.StatusCode == HttpStatusCode.OK ||
                httpResult.StatusCode == HttpStatusCode.InternalServerError)
            {
                if (!html.Contains("account"))
                {
                    resp.Success = false;
                    resp.Message = $"访问第三方服务器出错";
                }
                else
                {
                    var result = JsonConvert.DeserializeAnonymousType(html, new { account = "", phone = "", amount = "", billid = "", message = "", orderid = "", state = "", cut = "", sign = "" });
                    var verifySign = $"{result.orderid}{result.billid}{result.state}&{merKey}".ToMD5().ToUpper();
                    if (result.sign.Equals(verifySign))
                    {
                        if (result.state == "1")
                        {
                            orderInfo.RechargeMoney = Convert.ToDecimal(result.amount);
                            OperateSuccessOrder(orderInfo);

                        }
                        else if (result.state == "0" || result.state == "-1")
                        {
                            OperateErrorOrder(orderNo);
                        }

                        var msg = "";
                        msg = GetErrMsg(result.state);

                        resp.Success = true;
                        resp.Message = $"请求成功,返回信息：{result.state}，{msg}";
                    }
                    else
                    {
                        resp.Success = false;
                        resp.Message = "签名验证失败";
                    }
                }
            }
            else
            {
                resp.Success = false;
                resp.Message = $"访问第三方服务器出错,状态:{httpResult.StatusCode},状态码:{(int)httpResult.StatusCode}";
            }

            return JsonConvert.SerializeObject(resp);
        }

        /// <summary>
        /// 查询余额
        /// </summary>
        /// <returns></returns>
        public string QueryBalance()
        {
            var keys = Tools.GetKeys("Jiaofei365");
            merCode = keys[0].ConfigValue;
            merKey = keys[1].ConfigValue;

            var account = merCode;
            var sign = $"{account}&{merKey}".ToMD5().ToUpper();
            var dic = new Dictionary<string, string>()
            {
                { "account",account},
                { "sign",sign}

            };
            var paramData = GenGetParams(dic);

            // 从远程接口获取
            var httpRequestUtil = new HttpRequestUtil($"{suppModel.Address}?{paramData}", HttpRequestUtil.Method.Get);
            var httpResult = httpRequestUtil.Request();
            var html = httpResult.Html;
            if (httpResult.StatusCode == HttpStatusCode.OK ||
                httpResult.StatusCode == HttpStatusCode.InternalServerError)
            {
                if (!html.Contains("account"))
                {
                    return "0";
                }
                else
                {
                    var result = JsonConvert.DeserializeAnonymousType(html, new { account = "", all_fund = "", balance = "", u_name = "" });

                    return result.balance;
                }
            }

            return "";
        }

        private string GetErrMsg(string status)
        {
            var msg = "";
            switch (status)
            {
                case "0":
                    msg = "订单提交失败";
                    break;
                case "1":
                    msg = "缴费成功";
                    break;

                case "2":
                    msg = "缴费中";
                    break;
                case "-1":
                    msg = "未找到该订单";
                    break;
                case "-7":
                    msg = "业务关闭";
                    break;

                case "-8":
                    msg = "系统扎帐,暂停提交";
                    break;
            }
            return msg;
        }
    }




}