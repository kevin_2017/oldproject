﻿using CE.Utility.Text;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;

namespace Recharge.AgentApi.Controllers
{
    public class PaymentController : Controller
    {
        // GET: Payment
        //public ActionResult Index()
        //{
        //    return View();
        //}
        static string _branchCode = "Test";
        static string _key = new BLL.MS_BranchManage().GetModel(_branchCode).PrivateKey;

        private static string GetKeyB()
        {
            var newStr = $"{_branchCode}{_key}{Tools.GetCurrDate()}".ToMD5().ToLower();
            newStr = $"{StringUtil.GetRandomString(6)}{newStr}{StringUtil.GetRandomString(8)}";
            return newStr;
        }
         

        public ActionResult TestQueryBalance()
        {

            Dictionary<string, string> dic = new Dictionary<string, string>() {
                {"branchcode",_branchCode },
                {"requesttime",DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")},
            };
            var signStr = $"{Tools.CreateLinkString(Tools.SortDictionary(dic), "&", true)}&key={_key}";
            dic.Add("sign", signStr.ToMD5().ToLower());
            ViewBag.Agent = "Test";
            ViewBag.Param = Tools.CreateLinkString(Tools.SortDictionary(dic)).EncodeBase64String();
            ViewBag.Key = GetKeyB().EncodeBase64String();
            return View();
        }
        public ActionResult TestGetProducts()
        {
            Dictionary<string, string> dic = new Dictionary<string, string>() {
                {"branchcode",_branchCode },
                {"typeid","1001" },
                {"requesttime",DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")},
            };
            var signStr = $"{Tools.CreateLinkString(Tools.SortDictionary(dic), "&", true)}&key={_key}";
            dic.Add("sign", signStr.ToMD5().ToLower());
            ViewBag.Agent = "Test";
            ViewBag.Param = Tools.CreateLinkString(Tools.SortDictionary(dic)).EncodeBase64String();
            ViewBag.Key = GetKeyB().EncodeBase64String();
            return View();
        }
        public ActionResult TestQueryOrder()
        {
            Dictionary<string, string> dic = new Dictionary<string, string>() {
                {"branchcode",_branchCode },
                {"branchorderno","1001" },
                {"requesttime",DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")},
            };
            var signStr = $"{Tools.CreateLinkString(Tools.SortDictionary(dic), "&", true)}&key={_key}";
            dic.Add("sign", signStr.ToMD5().ToLower());
            ViewBag.Agent = "Test";
            ViewBag.Param = Tools.CreateLinkString(Tools.SortDictionary(dic)).EncodeBase64String();
            ViewBag.Key = GetKeyB().EncodeBase64String();
            return View();
        }
        public ActionResult TestRechargeApi()
        {
            var branchNo = Tools.OrderNo();//"18070113074251643412";//
            Dictionary<string, string> dic = new Dictionary<string, string>() {
                {"branchcode",_branchCode},
                {"branchorderno",branchNo},
                {"requesttime",DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")},
                {"phonenum","13158003399"},
                {"phonetype","1"},// 号码类型（1、手机，2、固话）
                {"isptype","1"},//运营商类型（1、中国联通，2、中国移动，3、中国电信）
                {"rechargemoney","1"},
                {"notifyurl","http://www.biadu.com"},
                {"productid","1069"},//电信：1033，联通：1069,移动：1051
                {"extendparam","test"},//自定义扩展参数，原样返回
            };
            var signStr = $"{Tools.CreateLinkString(Tools.SortDictionary(dic), "&", true)}&key={_key}";
            dic.Add("sign", signStr.ToMD5().ToLower());
            ViewBag.Agent = "Test";
            ViewBag.Param = Tools.CreateLinkString(Tools.SortDictionary(dic)).EncodeBase64String();
            ViewBag.Key = GetKeyB().EncodeBase64String();
            return View();
        }
    }
}
