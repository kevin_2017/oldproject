﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using CE.Utility;
using CE.Utility.Text;
using CE.Utility.Web;
using Common.DBUtility;
using Model;
using Newtonsoft.Json;

namespace Recharge.AgentApi.Controllers
{
    public class BaseController : Controller
    {
        #region 基本函数
        /// <summary>
        /// 获取客户端提交json型数据.
        /// </summary>
        /// <returns>参数值</returns>
        protected string GetStringInput()
        {
            return WebUtil.GetStringInput(Request);
        }
        /// <summary>
        /// 获取客户端提交Sting型数据.
        /// </summary>
        /// <param name="paramName">参数名</param>
        /// <param name="errorReturn">错误返回值</param>
        /// <returns>参数值</returns>
        protected string GetStringParam(string paramName, string errorReturn)
        {
            return WebUtil.GetStringParam(Request, paramName, errorReturn);
        }


        /// <summary>
        /// 获取客户端提交Sting型数据.
        /// </summary>
        /// <param name="paramName">参数名</param>
        /// <param name="errorReturn">错误返回值</param>
        /// <returns>参数值</returns>
        protected decimal GetDecimalParam(string paramName, decimal errorReturn)
        {
            return WebUtil.GetDecimalParam(Request, paramName, errorReturn);
        }
        /// <summary>
        /// 获取客户端提交Int型数据.
        /// </summary>
        /// <param name="paramName">参数名</param>
        /// <param name="errorReturn">错误返回值</param>
        /// <returns>参数值</returns>
        protected int GetIntParam(string paramName, int errorReturn)
        {
            return WebUtil.GetIntParam(Request, paramName, errorReturn);
        }

        /// <summary>
        /// 获取客户端提交Datetime型数据.
        /// </summary>
        /// <param name="paramName">参数名</param>
        /// <param name="errorReturn">错误返回值</param>
        /// <returns>参数值</returns>
        protected DateTime GetDateTimeParam(string paramName, DateTime errorReturn)
        {
            return WebUtil.GetDateTimeParam(Request, paramName, errorReturn);
        }
        /// <summary>
        /// 过滤非法字符，
        /// </summary>
        /// <param name="Str">要过滤的字符串。</param>
        /// <returns>过滤结果字符串。</returns>
        public static string CheckStr(string Str)
        {
            if (Str == null) return String.Empty;
            return Str.Replace("'", "''");
        }
        /// <summary>
        /// 将任意类型转换为日期型，当无法转换时返回 DateTime.MinValue 。
        /// </summary>
        /// <param name="dt">object 类型。</param>
        /// <returns>返回转换结果。</returns>
        public static DateTime GetDateTime(object dt)
        {
            DateTime newdt;
            DateTime.TryParse(GetString(dt), out newdt);
            return newdt;
        }
        /// <summary>
        /// 将任意类型转换为整型，当无法转换时返回 0 。
        /// </summary>
        /// <param name="id">object 类型。</param>
        /// <returns>返回转换结果。</returns>
        public static int GetInt(object id)
        {
            int newid;
            int.TryParse(GetString(id).Trim(), out newid);
            return newid;
        }
        /// <summary>
        /// 将任意类型转换为整型，当无法转换时返回 0 。
        /// </summary>
        /// <param name="id">object 类型。</param>
        /// <returns>返回转换结果。</returns>
        public static decimal GetDecimal(object id)
        {
            decimal newid;
            decimal.TryParse(GetString(id).Trim(), out newid);
            return newid;
        }
        /// <summary>
        /// 将任意类型转换为字符串型，当无法转换时返回空字符串。
        /// </summary>
        /// <param name="str">object 类型。</param>
        /// <returns>返回转换结果。</returns>
        public static string GetString(object str)
        {
            if (str == null)
                return String.Empty;
            else
                return str.ToString();
        }

        public static string GetDate(object dt)
        {
            if (String.IsNullOrEmpty(GetString(dt))) return string.Empty;
            DateTime newdt;
            DateTime.TryParse(GetString(dt), out newdt);
            return newdt.ToString("yyyy-MM-dd");
        }

        public static string GetTime(object dt)
        {
            if (String.IsNullOrEmpty(GetString(dt))) return string.Empty;
            DateTime newdt;
            DateTime.TryParse(GetString(dt), out newdt);
            return newdt.ToString("HH-mm-ss");
        }

        public static string GetDateTime(object dt, string format)
        {
            if (String.IsNullOrEmpty(GetString(dt))) return string.Empty;
            DateTime newdt;
            DateTime.TryParse(GetString(dt), out newdt);
            return newdt.ToString(format);
        }

        public static string GetValue(long supplierId, string configName)
        {
            return new BLL.SUP_SupplierConfig().GetModel(new StrWhere() { IsWhereExist = true, strWhere = new StringBuilder($"SupplierID={supplierId} and ConfigName='{configName}'") }).ConfigValue;
        }

        #region 反序列化JSON字符串
        /// <summary>
        /// 反序列化JSON字符串
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="json"></param>
        /// <returns></returns>
        public static T DeserializeJson<T>(string json)
        {
            var jSetting = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore };
            var info = JsonConvert.DeserializeObject<T>(json, jSetting);
            return info;
        }
        #endregion

        /// <summary>
        /// 操作成功订单
        /// </summary>
        /// <param name="model"></param>
        public static void OperateSuccessOrder(Model.V8_OrderManage model)
        {
            try
            {
                var orderModel = new BLL.V8_OrderManage().GetModel(model.OrderNo);

                if (orderModel.CurentStatus == 2 || orderModel.CurentStatus == 3)
                {
                    decimal rebate = 0;
                    #region  //计算返点
                    var productInfo = new BLL.V8_Product_Manage().GetModel(new StrWhere()
                    {
                        IsWhereExist = true,
                        strWhere = new StringBuilder($"id={model.ProductID} ")
                    });
                    Model.MS_Branch_RebateRule msBranchRebateRule = new BLL.MS_Branch_RebateRule().GetModel(new StrWhere()
                    {
                        IsWhereExist = true,
                        strWhere = new StringBuilder(
                            $"BranchId={model.BranchID} and IspType={model.IspType} and ProductTypeId={productInfo.ProductTypeID} and ({model.TotalPrice} between MinMoney and MaxMoney) and ProvinceIds like'%{orderModel.ProvinceId}%'  order by  ProvinceIds desc, Sort")
                    });

                    if (msBranchRebateRule == null)
                    {
                        msBranchRebateRule = new BLL.MS_Branch_RebateRule().GetModel(new StrWhere()
                        {
                            IsWhereExist = true,
                            strWhere = new StringBuilder(
                                  $"BranchId={model.BranchID} and IspType={model.IspType} and ProductTypeId={productInfo.ProductTypeID} and ({model.TotalPrice} between MinMoney and MaxMoney) and ProvinceIds like'%1000%'  order by  ProvinceIds desc, Sort")
                        });
                    }

                    if (msBranchRebateRule != null && msBranchRebateRule.Rebate > 0)
                    {
                        rebate = msBranchRebateRule.RebateMethod == 1 ? msBranchRebateRule.Rebate : model.TotalPrice * msBranchRebateRule.Rebate;
                    }

                    #endregion

                    //修改订单状态
                    DBHelper.ExecuteSql(
                        $"update V8_OrderManage set RechargeMoney={model.RechargeMoney},ProductSalePrice={model.TotalPrice - rebate},SysSerialNum='{model.SysSerialNum}', CallBackTime=getdate(),CurentStatus=1,NotifyStatus=0  where OrderNo='{model.OrderNo}'");

                    //修改代理余额
                    new BLL.MS_BranchManage().Proc_BalanceChange(0, Convert.ToDecimal(model.TotalPrice), model.BranchID,
                        model.ID, "现金", $"给用户{model.RechargeNo}充值{model.TotalPrice}元");

                    #region 查询代理返点，并修改余额及增加余额变动记录
                    if (rebate > 0)
                        new BLL.MS_BranchManage().Proc_BalanceChange(2, rebate, model.BranchID,
                            model.ID, "余额", $"用户{model.RechargeNo}充值{model.TotalPrice}元，返利{rebate}元");

                    #endregion

                    //发送成功订单号到通知下游的消息队列

                    var msmqName = ConfigurationManager.AppSettings["RechargeNotifyQueueName"];
                    var msmqHelper = new MsmqHelper(msmqName);
                    msmqHelper.Send<string>(model.OrderNo);

                    WriteLog.Write($"{orderModel.OrderNo}发送成功通知", $"{orderModel.OrderNo} 发送成功通知");

                }

            }
            catch (Exception ex)
            {
                WriteLog.WriteError($"{DateTime.Now:yyyy-MM-dd}_error", $"{model.OrderNo}充值成功错误日志", ex);
            }
        }


        /// <summary>
        /// 操作失败订单
        /// </summary>
        /// <param name="orderNo"></param>
        public static void OperateErrorOrder(string orderNo)
        {
            try
            {
                var orderInfo = new BLL.V8_OrderManage().GetModel(orderNo);
                if (orderInfo.CurentStatus == 2 || orderInfo.CurentStatus == 3)
                {
                    Model.StrWhere str = new StrWhere();
                    str.IsWhereExist = true;

                    var branchModel = new BLL.MS_BranchManage().GetModel(orderInfo.BranchID);

                    str.strWhere = branchModel.RechargeType == 0
                        ? new StringBuilder($" Status=1001 ")
                        : new StringBuilder(
                            $" status=1001 and id in (select SupplierId from MS_Branch_Supplier where BranchId={orderInfo.BranchID})  order by sort");
                    var supplierList = new BLL.SUP_SupplierManage().GetModelList(str);

                    var orderRequestRecList = new BLL.V8_OrderRequestRecs().GetModelList(new StrWhere()
                    {
                        IsWhereExist = true,
                        strWhere = new StringBuilder($" OrderNo='{orderNo}'")
                    });

                    if (supplierList.Count > orderRequestRecList.Count)
                    {
                        WriteLog.Write($"{orderNo}进行二次充值", $"{orderNo} 进行二次充值");
                        var msmqName = ConfigurationManager.AppSettings["RechargeOrderQueueName"];
                        var msmqHelper = new MsmqHelper(msmqName);
                        msmqHelper.Send<string>(orderNo);
                    }
                    else
                    { 

                        //修改订单状态
                        DBHelper.ExecuteSql(
                            $"update V8_OrderManage set   CallBackTime=getdate(),CurentStatus=5,NotifyStatus=0 where OrderNo='{orderNo}'");

                        //发送成功订单号到通知下游的消息队列

                        var notifyQueueName = ConfigurationManager.AppSettings["RechargeNotifyQueueName"];
                        var notifyQueue = new MsmqHelper(notifyQueueName);
                        notifyQueue.Send<string>(orderNo);

                        WriteLog.Write($"{orderNo}发送失败通知", $"{orderNo} 发送失败通知");
                    }
                }
            }
            catch (Exception ex)
            {
                WriteLog.WriteError($"{DateTime.Now:yyyy-MM-dd}_error", $"{orderNo}充值错误日志", ex);
            }
        }

        /// <summary>
        /// 操作该订单号在该接口供应商通知数据
        /// </summary>
        /// <param name="supplierId"></param>
        /// <param name="orderNo"></param>
        /// <param name="callbackConents"></param>
        public static void OperRequestRec(long supplierId, string orderNo, string callbackConents)
        {
            //修改表数据信息
            var requestModel = new BLL.V8_OrderRequestRecs().GetModel(new Model.StrWhere() { strWhere = new System.Text.StringBuilder($" SupplierId={supplierId} and OrderNo='{orderNo}'"), IsWhereExist = true });
            requestModel.NotifyTime = DateTime.Now;
            requestModel.NotifyResult = 1;
            requestModel.NoticeContent = callbackConents;
            new BLL.V8_OrderRequestRecs().Update(requestModel);

        }

        #region 生成GET方法的参数字符串
        /// <summary>
        /// 生成GET方法的参数字符串
        /// </summary>
        /// <param name="formData"></param>
        /// <returns></returns>
        protected string GenGetParams(Dictionary<string, string> formData)
        {
            var sb = new StringBuilder();
            foreach (KeyValuePair<string, string> item in formData)
            {
                sb.AppendFormat("{0}={1}&", item.Key, item.Value);
            }

            return sb.ToString(0, sb.Length - 1);
        }
        #endregion

        #region 生成POST方法的表单并且提交到指定地址
        /// <summary>
        /// 生成POST方法的表单并且提交到指定地址
        /// </summary>
        /// <param name="formData"></param>
        /// <param name="formUrl"></param>
        /// <returns></returns>
        protected string GenFormHtml(Dictionary<string, string> formData, string formUrl)
        {
            var sb = new StringBuilder();
            sb.AppendFormat("<form name='frm1' id='frm1' method='POST' action='{0}'>", formUrl).Append(Environment.NewLine);
            foreach (KeyValuePair<string, string> item in formData)
            {
                sb.AppendFormat("<input type='hidden' id='{0}' name='{0}' value='{1}' />", item.Key, item.Value).Append(Environment.NewLine);
            }
            sb.Append("</form>").Append(Environment.NewLine);
            sb.Append("<script type ='text/javascript' language='javascript'>setTimeout('document.getElementById(\"frm1\").submit();',100);</script>").Append(Environment.NewLine);

            return sb.ToString();
        }
        #endregion

        #endregion


    }
}