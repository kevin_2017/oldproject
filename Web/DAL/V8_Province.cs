﻿using Common.DBUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DAL
{ /// <summary>
    /// DAL层,V8_Province
    /// </summary>
    public partial class V8_Province : DalBase
    {
        
        #region  基础方法
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public long Add(Model.V8_Province model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into V8_Province(");
            strSql.Append("ProvinceName");
            strSql.Append(") values (");
            strSql.Append("@ProvinceName");
            strSql.Append(") ");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
			            new SqlParameter("@ProvinceName", SqlDbType.NVarChar,50)             
              
            };

            parameters[0].Value = model.ProvinceName;

            object obj = DBHelper.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {

                return Convert.ToInt64(obj);

            }

        }



        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Model.V8_Province model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update V8_Province set ");

            strSql.Append(" ProvinceName = @ProvinceName  ");
            strSql.Append(" where ID=@ID ");

            SqlParameter[] parameters = {
			            new SqlParameter("@ID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@ProvinceName", SqlDbType.NVarChar,50)             
              
            };

            parameters[0].Value = model.ID;
            parameters[1].Value = model.ProvinceName;
            int rows = DBHelper.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion


        #region 补充方法
        #endregion
    }
}