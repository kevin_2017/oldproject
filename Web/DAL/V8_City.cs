﻿using Common.DBUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DAL
{
    /// <summary>
    /// DAL层,V8_City
    /// </summary>
    public partial class V8_City : DalBase
    {
        
        #region  基础方法
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public long Add(Model.V8_City model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into V8_City(");
            strSql.Append("ProvinceID,CityName");
            strSql.Append(") values (");
            strSql.Append("@ProvinceID,@CityName");
            strSql.Append(") ");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
			            new SqlParameter("@ProvinceID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@CityName", SqlDbType.NVarChar,50)             
              
            };

            parameters[0].Value = model.ProvinceID;
            parameters[1].Value = model.CityName;

            object obj = DBHelper.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {

                return Convert.ToInt64(obj);

            }	
        }



        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Model.V8_City model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update V8_City set ");

            strSql.Append(" ProvinceID = @ProvinceID , ");
            strSql.Append(" CityName = @CityName  ");
            strSql.Append(" where ID=@ID ");

            SqlParameter[] parameters = {
			            new SqlParameter("@ID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@ProvinceID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@CityName", SqlDbType.NVarChar,50)             
              
            };

            parameters[0].Value = model.ID;
            parameters[1].Value = model.ProvinceID;
            parameters[2].Value = model.CityName;
            int rows = DBHelper.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion


        #region 补充方法
        #endregion
    }
}