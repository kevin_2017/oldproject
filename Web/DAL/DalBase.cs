﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Reflection;
using System.Web;
using Common.DBUtility;
using Model;
using Common;


namespace DAL
{
    /// <summary>
    /// DAL父类
    /// </summary>
    public class DalBase
    {
        #region   基础方法
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        /// <param name="tableName">表名</param>
        /// <param name="strWhere">条件</param>
        /// <returns></returns>
        public bool Exists(string tableName, Model.StrWhere strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append(" select count(1) from " + tableName);
            if (strWhere.IsWhereExist)
            {
                strSql.Append(" where ");
                strSql.Append(strWhere.strWhere);
            }
            return DBHelper.Exists(strSql.ToString(), strWhere.parameters);//.Exists(strSql.ToString());
        }

        /// <summary>
        /// 获取数据条数
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public long GetCount(string tableName, Model.StrWhere strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1)  from " + tableName);
            if (strWhere.IsWhereExist)
            {
                strSql.Append(" where ");
                strSql.Append(strWhere.strWhere);
            }
            //if (strWhere.Trim() != "")
            //{
            //    strSql.Append(" where " + strWhere);
            //}
            object obj = DBHelper.GetSingle(strSql.ToString(), strWhere.parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt64(obj);
            }
        }

        /// <summary>
        ///  获得数据 Dataset
        /// </summary>
        /// <param name="tableName">表名</param>
        /// <param name="strWhere">条件</param>
        /// <returns></returns>
        public DataSet GetDataSet(string tableName, Model.StrWhere strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * from  " + tableName);
            if (strWhere.IsWhereExist)
            {
                strSql.Append(" where ");
                strSql.Append(strWhere.strWhere);
            }
            //if (strWhere.Trim() != "")
            //{
            //    strSql.Append(" where " + strWhere);
            //}
            return DBHelper.Query(strSql.ToString(), strWhere.parameters);
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        /// <param name="tableName">表名</param>
        /// <param name="Top">行数</param>
        /// <param name="strWhere">条件</param>
        /// <returns></returns>
        public DataSet GetDataSet(string tableName, int Top, Model.StrWhere strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select top " + Top + "* from  " + tableName);
            //if (strWhere.Trim() != "")
            //{
            //    strSql.Append(" where " + strWhere);
            //}
            if (strWhere.IsWhereExist)
            {
                strSql.Append(" where ");
                strSql.Append(strWhere.strWhere);
            }
            return DBHelper.Query(strSql.ToString(), strWhere.parameters);
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="strWhere">条件</param>
        /// <param name="orderby">排序列</param>
        /// <param name="startIndex">开始行</param>
        /// <param name="endIndex">结束行</param>
        /// <returns></returns>
        public DataSet GetDataSetByPage(string tableName, Model.StrWhere strWhere, string orderby, int startIndex, int endIndex)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * from ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.ID desc");
            }
            strSql.Append(")AS Row, T.*  from  " + tableName + " T ");
            if (strWhere.IsWhereExist)
            {
                strSql.Append(" where ");
                strSql.Append(strWhere.strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DBHelper.Query(strSql.ToString(), strWhere.parameters);
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="strWhere">条件</param>
        /// <returns></returns>
        public int Delete(string tableName, Model.StrWhere strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete  " + tableName);
            if (strWhere.IsWhereExist)
            {
                strSql.Append(" where ");
                strSql.Append(strWhere.strWhere);
            }
            //if (strWhere.Trim() != "")
            //{
            //    strSql.Append(" where " + strWhere);
            //}
            int rows = DBHelper.ExecuteSql(strSql.ToString(), strWhere.parameters);
            return rows;
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tableName"></param>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public T GetModel<T>(string tableName, Model.StrWhere strWhere) where T : class, new()
        {
            Type type = typeof(T);
            T entity = new T();
            StringBuilder strSql = new StringBuilder();
            DataSet ds = GetDataSet(tableName, 1, strWhere);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel<T>(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="row"></param>
        /// <returns></returns>
        public T DataRowToModel<T>(DataRow row) where T : class, new()
        {
            Type type = typeof(T);
            T entity = new T();
            PropertyInfo[] pArray = type.GetProperties();

            foreach (PropertyInfo p in pArray)
            {
                try
                {
                    if (row[p.Name] is Int64)
                    {
                        p.SetValue(entity, Convert.ToInt64(row[p.Name]), null);
                        continue;
                    }
                    p.SetValue(entity, row[p.Name], null);
                }
                catch (Exception)
                {


                }
            }
            return entity;
        }
        /// <summary>
        ///  获得数据列表 GetModelList
        /// </summary>
        /// <returns></returns>
        public List<T> GetModelList<T>(string tableName, Model.StrWhere strWhere) where T : class, new()
        {
            Type type = typeof(T);
            DataSet ds = GetDataSet(tableName, strWhere);
            return DataTableToList<T>(ds.Tables[0]);
        }
        /// <summary>
        /// Datatable 转 list<对象>
        /// </summary>
        /// <typeparam name="T">对象</typeparam>
        /// <param name="dt">Datatable</param>
        /// <returns>list<对象></returns>
        public List<T> DataTableToList<T>(DataTable dt) where T : class, new()
        {
            Type type = typeof(T);
            List<T> list = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                PropertyInfo[] pArray = type.GetProperties();
                T entity = new T();
                foreach (PropertyInfo p in pArray)
                {
                    try
                    {
                        if (row[p.Name] is Int64)
                        {
                            p.SetValue(entity, Convert.ToInt32(row[p.Name]), null);
                            continue;
                        }
                        p.SetValue(entity, row[p.Name], null);
                    }
                    catch (Exception)
                    {

                    }
                }
                list.Add(entity);
            }
            return list;
        }
        /// <summary>
        ///  分页获取数据列表 List<对象>
        /// </summary>
        /// <returns></returns>
        public List<T> GetByPage<T>(string tableName, Model.StrWhere strWhere, string orderby, int startIndex, int endIndex) where T : class, new()
        {
            DataSet ds = GetDataSetByPage(tableName, strWhere, orderby, startIndex, endIndex);
            return DataTableToList<T>(ds.Tables[0]);
        }


        public DataSet RunProc(string procedureName, SqlParameter[] parameters)
        {
            return DBHelper.RunProcedure(procedureName, parameters, "ds");
        }

        //#region 把DataRow转化成对象
        ///// <summary>
        ///// 把DataRow转化成对象
        ///// </summary>
        ///// <param name="dr">dr</param>
        ///// <param name="obj">对象</param>
        ///// <returns></returns>
        //public static object DataRowToObj(System.Data.DataRow dr, object obj)
        //{
        //    try
        //    {
        //        if (dr != null && obj != null)
        //        {
        //            Type t = obj.GetType();
        //            PropertyInfo[] f = t.GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.NonPublic);
        //            string tableName = t.Name;
        //            foreach (PropertyInfo pi in f)
        //            {
        //                string field = pi.Name;
        //                if (dr.Table.Columns.Contains(field) && dr[field] != DBNull.Value)
        //                    pi.SetValue(obj, dr[field], null);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        new Exception(ex.ToString());
        //        return null;
        //    }
        //    return obj;
        //}
        //#endregion
        /// <summary>
        /// 执行一条查询语句
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public DataSet Query(string sql)
        {
            return DBHelper.Query(sql);
        }
        #endregion


        /// <summary>
        /// 执行带参数sql语句，返回结果集
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="pas"></param>
        /// <returns></returns>
        public DataSet Query(string sql, SqlParameter[] pas)
        {
            return DBHelper.Query(sql, pas);
        }


        #region 自定义方法


        public DataTable GetListByPage(PageAttribute page)
        {
            return DBHelper.QuerySQLByPaging(page);

        }


        /// <summary>
        /// 执行SQL语句
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public int ExecuteSql(string sql)
        {
            return DBHelper.ExecuteSql(sql);
        }

        #endregion
    }
}
