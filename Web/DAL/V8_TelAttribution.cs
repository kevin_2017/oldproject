﻿using Common.DBUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DAL
{
    public partial class V8_TelAttribution : DalBase
    {
        
        #region  基础方法
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public long Add(Model.V8_TelAttribution model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into V8_TelAttribution(");
            strSql.Append("PhoneNum,NO,AREA,ISPType,TYPE,ProvinceCode,CityCode,IsDelete");
            strSql.Append(") values (");
            strSql.Append("@PhoneNum,@NO,@AREA,@ISPType,@TYPE,@ProvinceCode,@CityCode,@IsDelete");
            strSql.Append(") ");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
			            new SqlParameter("@PhoneNum", SqlDbType.NVarChar,50) ,            
                        new SqlParameter("@NO", SqlDbType.Float,8) ,            
                        new SqlParameter("@AREA", SqlDbType.NVarChar,255) ,            
                        new SqlParameter("@ISPType", SqlDbType.Int,4) ,            
                        new SqlParameter("@TYPE", SqlDbType.NVarChar,255) ,            
                        new SqlParameter("@ProvinceCode", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@CityCode", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@IsDelete", SqlDbType.Int,4)             
              
            };

            parameters[0].Value = model.PhoneNum;
            parameters[1].Value = model.NO;
            parameters[2].Value = model.AREA;
            parameters[3].Value = model.ISPType;
            parameters[4].Value = model.TYPE;
            parameters[5].Value = model.ProvinceCode;
            parameters[6].Value = model.CityCode;
            parameters[7].Value = model.IsDelete;          

            object obj = DBHelper.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {

                return Convert.ToInt64(obj);

            }

        }



        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Model.V8_TelAttribution model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update V8_TelAttribution set ");
            strSql.Append(" PhoneNum = @PhoneNum , ");
            strSql.Append(" NO = @NO , ");
            strSql.Append(" AREA = @AREA , ");
            strSql.Append(" ISPType = @ISPType , ");
            strSql.Append(" TYPE = @TYPE , ");
            strSql.Append(" ProvinceCode = @ProvinceCode , ");
            strSql.Append(" CityCode = @CityCode , ");
            strSql.Append(" IsDelete = @IsDelete  ");
            strSql.Append(" where ID=@ID ");

            SqlParameter[] parameters = {
			            new SqlParameter("@ID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@PhoneNum", SqlDbType.NVarChar,50) ,            
                        new SqlParameter("@NO", SqlDbType.Float,8) ,            
                        new SqlParameter("@AREA", SqlDbType.NVarChar,255) ,            
                        new SqlParameter("@ISPType", SqlDbType.Int,4) ,            
                        new SqlParameter("@TYPE", SqlDbType.NVarChar,255) ,            
                        new SqlParameter("@ProvinceCode", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@CityCode", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@IsDelete", SqlDbType.Int,4)             
              
            };

            parameters[0].Value = model.ID;
            parameters[1].Value = model.PhoneNum;
            parameters[2].Value = model.NO;
            parameters[3].Value = model.AREA;
            parameters[4].Value = model.ISPType;
            parameters[5].Value = model.TYPE;
            parameters[6].Value = model.ProvinceCode;
            parameters[7].Value = model.CityCode;
            parameters[8].Value = model.IsDelete;            
            int rows = DBHelper.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion


        #region 补充方法
        #endregion
    }
}