﻿using System;
using System.Text;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Data;
using Common.DBUtility;
namespace DAL
{
    //供应商商品管理
    public partial class V8_Product_Manage : DalBase
    {
        #region  基础方法

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public long Add(Model.V8_Product_Manage model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into V8_Product_Manage(");
            strSql.Append("AddTime,RechargeRange,CreateTime,Creator,UpdateTime,Updator,IsDelete,ProductTypeID,ProductTypeName,ProductName,denomination,Status,ISPType,ISPName,PhoneNumType");
            strSql.Append(") values (");
            strSql.Append("@AddTime,@RechargeRange,@CreateTime,@Creator,@UpdateTime,@Updator,@IsDelete,@ProductTypeID,@ProductTypeName,@ProductName,@denomination,@Status,@ISPType,@ISPName,@PhoneNumType");
            strSql.Append(") ");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
                        new SqlParameter("@AddTime", SqlDbType.DateTime) ,
                        new SqlParameter("@RechargeRange", SqlDbType.Int,4) ,
                        new SqlParameter("@CreateTime", SqlDbType.DateTime) ,
                        new SqlParameter("@Creator", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@UpdateTime", SqlDbType.DateTime) ,
                        new SqlParameter("@Updator", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@IsDelete", SqlDbType.Int,4) ,
                        new SqlParameter("@ProductTypeID", SqlDbType.BigInt,8) ,
                        new SqlParameter("@ProductTypeName", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@ProductName", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@denomination", SqlDbType.Decimal,9) ,
                        new SqlParameter("@Status", SqlDbType.Int,4) ,
                        new SqlParameter("@ISPType", SqlDbType.Int,4) ,
                        new SqlParameter("@ISPName", SqlDbType.NVarChar,20) ,
                        new SqlParameter("@PhoneNumType", SqlDbType.Int,4)

            };

            parameters[0].Value =DateTime.Now;
            parameters[1].Value = model.RechargeRange;
            parameters[2].Value = DateTime.Now;
            parameters[3].Value = model.Creator;
            parameters[4].Value = DateTime.Now;
            parameters[5].Value = model.Updator;
            parameters[6].Value = model.IsDelete;
            parameters[7].Value = model.ProductTypeID;
            parameters[8].Value = model.ProductTypeName;
            parameters[9].Value = model.ProductName;
            parameters[10].Value = model.denomination;
            parameters[11].Value = model.Status;
            parameters[12].Value = model.ISPType;
            parameters[13].Value = model.ISPName;
            parameters[14].Value = model.PhoneNumType;

            object obj = DBHelper.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {

                return Convert.ToInt64(obj);

            }

        }


        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Model.V8_Product_Manage model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update V8_Product_Manage set ");
            strSql.Append(" AddTime = @AddTime , ");
            strSql.Append(" RechargeRange = @RechargeRange , ");
            strSql.Append(" CreateTime = @CreateTime , ");
            strSql.Append(" Creator = @Creator , ");
            strSql.Append(" UpdateTime = @UpdateTime , ");
            strSql.Append(" Updator = @Updator , ");
            strSql.Append(" IsDelete = @IsDelete , ");
            strSql.Append(" ProductTypeID = @ProductTypeID , ");
            strSql.Append(" ProductTypeName = @ProductTypeName , ");
            strSql.Append(" ProductName = @ProductName , ");
            strSql.Append(" denomination = @denomination , ");
            strSql.Append(" Status = @Status , ");
            strSql.Append(" ISPType = @ISPType , ");
            strSql.Append(" ISPName = @ISPName , ");
            strSql.Append(" PhoneNumType = @PhoneNumType  ");
            strSql.Append(" where ID=@ID ");
            SqlParameter[] parameters = {
                        new SqlParameter("@ID", SqlDbType.BigInt,8) ,
                        new SqlParameter("@AddTime", SqlDbType.DateTime) ,
                        new SqlParameter("@RechargeRange", SqlDbType.Int,4) ,
                        new SqlParameter("@CreateTime", SqlDbType.DateTime) ,
                        new SqlParameter("@Creator", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@UpdateTime", SqlDbType.DateTime) ,
                        new SqlParameter("@Updator", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@IsDelete", SqlDbType.Int,4) ,
                        new SqlParameter("@ProductTypeID", SqlDbType.BigInt,8) ,
                        new SqlParameter("@ProductTypeName", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@ProductName", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@denomination", SqlDbType.Decimal,9) ,
                        new SqlParameter("@Status", SqlDbType.Int,4) ,
                        new SqlParameter("@ISPType", SqlDbType.Int,4) ,
                        new SqlParameter("@ISPName", SqlDbType.NVarChar,20) ,
                        new SqlParameter("@PhoneNumType", SqlDbType.Int,4)

            };

            parameters[0].Value = model.ID;
            parameters[1].Value = model.AddTime;
            parameters[2].Value = model.RechargeRange;
            parameters[3].Value = model.CreateTime;
            parameters[4].Value = model.Creator;
            parameters[5].Value = model.UpdateTime;
            parameters[6].Value = model.Updator;
            parameters[7].Value = model.IsDelete;
            parameters[8].Value = model.ProductTypeID;
            parameters[9].Value = model.ProductTypeName;
            parameters[10].Value = model.ProductName;
            parameters[11].Value = model.denomination;
            parameters[12].Value = model.Status;
            parameters[13].Value = model.ISPType;
            parameters[14].Value = model.ISPName;
            parameters[15].Value = model.PhoneNumType;
            int rows = DBHelper.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region 补充方法
        #endregion


    }
}

