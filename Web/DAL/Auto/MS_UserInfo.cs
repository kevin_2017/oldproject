﻿
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Common.DBUtility;
using Model;
using System.Collections.Generic;
namespace DAL
{
    /// <summary>
    /// DAL层,下级网点用户信息
    /// </summary>
    public partial class MS_UserInfo : DalBase
    {
        
        #region  基础方法
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public long Add(Model.MS_UserInfo model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into MS_UserInfo(");
            strSql.Append("IsDelete,BranchID,UserRole,UserName,Password,Status,LoginIP,LastLoginIP,LoginCount");
            strSql.Append(") values (");
            strSql.Append("@IsDelete,@BranchID,@UserRole,@UserName,@Password,@Status,@LoginIP,@LastLoginIP,0");
            strSql.Append(") ");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {          
                        new SqlParameter("@IsDelete", SqlDbType.Int,4) ,            
                        new SqlParameter("@BranchID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@UserRole", SqlDbType.Int,4) ,            
                        new SqlParameter("@UserName", SqlDbType.NVarChar,100) ,            
                        new SqlParameter("@Password", SqlDbType.NVarChar,500) ,            
                        new SqlParameter("@Status", SqlDbType.Int,4) ,            
                        new SqlParameter("@LoginIP", SqlDbType.VarChar,50) ,            
                        new SqlParameter("@LastLoginIP", SqlDbType.VarChar,20)            
              
            };

            parameters[0].Value = model.IsDelete;
            parameters[1].Value = model.BranchID;
            parameters[2].Value = model.UserRole;
            parameters[3].Value = model.UserName;
            parameters[4].Value = model.Password;
            parameters[5].Value = model.Status;
            parameters[6].Value = "127.0.0.1";
            parameters[7].Value = "127.0.0.1";

            object obj = DBHelper.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {

                return Convert.ToInt64(obj);

            }

        }



        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Model.MS_UserInfo model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update MS_UserInfo set ");
            strSql.Append(" LastLoginIP = LoginIP , ");
            strSql.Append(" LoginIP = @LoginIP , ");
            strSql.Append(" LastLoginTime =LoginTime , ");
            strSql.Append(" LoginTime = getdate() , ");
            strSql.Append(" IsDelete = @IsDelete , ");
            strSql.Append(" BranchID = @BranchID , ");
            strSql.Append(" UserRole = @UserRole , ");
            strSql.Append(" UserName = @UserName , ");
            strSql.Append(" Password = @Password , ");
            strSql.Append(" Status = @Status  ");
            strSql.Append(" where ID=@ID ");

            SqlParameter[] parameters = {
			            new SqlParameter("@ID", SqlDbType.BigInt,8) ,               
                        new SqlParameter("@IsDelete", SqlDbType.Int,4) ,            
                        new SqlParameter("@BranchID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@UserRole", SqlDbType.Int,4) ,            
                        new SqlParameter("@UserName", SqlDbType.NVarChar,100) ,            
                        new SqlParameter("@Password", SqlDbType.NVarChar,500) ,            
                        new SqlParameter("@Status", SqlDbType.Int,4) ,            
                        new SqlParameter("@LoginIP", SqlDbType.VarChar,50)             
              
            };

            parameters[0].Value = model.ID;
            parameters[1].Value = model.IsDelete;
            parameters[2].Value = model.BranchID;
            parameters[3].Value = model.UserRole;
            parameters[4].Value = model.UserName;
            parameters[5].Value = model.Password;
            parameters[6].Value = model.Status;
            parameters[7].Value = model.LoginIP;
            int rows = DBHelper.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion


        #region 补充方法


        /// <summary>
        /// 获取用户登录信息
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="loginIP"></param>
        /// <param name="errcount"></param>
        /// <returns></returns>
        public Model.BranchLoginUserInfo GetBranchUserInfo(string username, string password, string loginIP)
        {

            DataTable dt = new DataTable();

            SqlParameter UserName = new SqlParameter("@userName", SqlDbType.NVarChar, 100);
            UserName.Value = username;

            SqlParameter PWD = new SqlParameter("@Password", SqlDbType.NVarChar, 500);
            PWD.Value = password;

            SqlParameter LoginIP = new SqlParameter("@LoginIP", SqlDbType.VarChar, 20);
            LoginIP.Value = loginIP;




            List<SqlParameter> list = new List<SqlParameter>();
            list.Add(UserName);
            list.Add(PWD);
            list.Add(LoginIP);



            dt = DBHelper.ExecuteProcedureOutputTable("Proc_BranchUserLogin", list);

            if (dt.Rows.Count > 0)
            {
                return DataRowToModel<Model.BranchLoginUserInfo>(dt.Rows[0]);
            }
            else
            {
                return null;
            }

        }

        #endregion
    }
}