﻿using Common.DBUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DAL
{
    public class V8_OrderExpand : DalBase
    {
        
        #region  基础方法
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public long Add(Model.V8_OrderExpand model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into V8_OrderExpand(");
            strSql.Append("OrderID,RequestSourceData,ResponseSourceData,NotifyResponeData,IsDelete,RealyProductID");
            strSql.Append(") values (");
            strSql.Append("@OrderID,@RequestSourceData,@ResponseSourceData,@NotifyResponeData,@IsDelete,@RealyProductID");
            strSql.Append(") ");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
			            new SqlParameter("@OrderID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@RequestSourceData", SqlDbType.Text) ,            
                        new SqlParameter("@ResponseSourceData", SqlDbType.Text) ,            
                        new SqlParameter("@NotifyResponeData", SqlDbType.Text) ,            
                        new SqlParameter("@IsDelete", SqlDbType.Int,4) ,            
                        new SqlParameter("@RealyProductID", SqlDbType.BigInt,8)             
              
            };

            parameters[0].Value = model.OrderID;
            parameters[1].Value = model.RequestSourceData;
            parameters[2].Value = model.ResponseSourceData;
            parameters[3].Value = model.NotifyResponeData;
            parameters[4].Value = model.IsDelete;
            parameters[5].Value = model.RealyProductID;                            

            object obj = DBHelper.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt64(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Model.V8_OrderExpand model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update V8_OrderExpand set ");

            strSql.Append(" OrderID = @OrderID , ");
            strSql.Append(" RequestSourceData = @RequestSourceData , ");
            strSql.Append(" ResponseSourceData = @ResponseSourceData , ");
            strSql.Append(" NotifyResponeData = @NotifyResponeData , ");
            strSql.Append(" IsDelete = @IsDelete , ");
            strSql.Append(" RealyProductID = @RealyProductID  ");
            strSql.Append(" where ID=@ID ");

            SqlParameter[] parameters = {
			            new SqlParameter("@ID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@OrderID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@RequestSourceData", SqlDbType.Text) ,            
                        new SqlParameter("@ResponseSourceData", SqlDbType.Text) ,            
                        new SqlParameter("@NotifyResponeData", SqlDbType.Text) ,            
                        new SqlParameter("@IsDelete", SqlDbType.Int,4) ,            
                        new SqlParameter("@RealyProductID", SqlDbType.BigInt,8)             
              
            };

            parameters[0].Value = model.ID;
            parameters[1].Value = model.OrderID;
            parameters[2].Value = model.RequestSourceData;
            parameters[3].Value = model.ResponseSourceData;
            parameters[4].Value = model.NotifyResponeData;
            parameters[5].Value = model.IsDelete;
            parameters[6].Value = model.RealyProductID;                   

            int rows = DBHelper.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
    }
}
