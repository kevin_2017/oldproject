﻿
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Common.DBUtility;

namespace DAL
{
    /// <summary>
    /// DAL层,系统用户操作日志
    /// </summary>
    public partial class SYS_OperatLog : DalBase
    {
        
        #region  基础方法
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public long Add(Model.SYS_OperatLog model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into SYS_OperatLog(");
            strSql.Append("UserID,OperateIP,OperateTime,OperateDescriptions");
            strSql.Append(") values (");
            strSql.Append("@UserID,@OperateIP,getdate(),@OperateDescriptions");
            strSql.Append(") ");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
			            new SqlParameter("@UserID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@OperateIP", SqlDbType.NVarChar,20) ,             
                        new SqlParameter("@OperateDescriptions", SqlDbType.Text)             
              
            };

            parameters[0].Value = model.UserID;
            parameters[1].Value = model.OperateIP;
            parameters[2].Value = model.OperateDescriptions;

            object obj = DBHelper.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {

                return Convert.ToInt64(obj);

            }

        }



        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Model.SYS_OperatLog model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update SYS_OperatLog set ");

            strSql.Append(" UserID = @UserID , ");
            strSql.Append(" OperateIP = @OperateIP , ");
            strSql.Append(" OperateTime = @OperateTime , ");
            strSql.Append(" OperateDescriptions = @OperateDescriptions  ");
            strSql.Append(" where ID=@ID ");

            SqlParameter[] parameters = {
			            new SqlParameter("@ID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@UserID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@OperateIP", SqlDbType.NVarChar,20) ,            
                        new SqlParameter("@OperateTime", SqlDbType.DateTime) ,            
                        new SqlParameter("@OperateDescriptions", SqlDbType.Text)             
              
            };

            parameters[0].Value = model.ID;
            parameters[1].Value = model.UserID;
            parameters[2].Value = model.OperateIP;
            parameters[3].Value = model.OperateTime;
            parameters[4].Value = model.OperateDescriptions;
            int rows = DBHelper.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion


        #region 补充方法
        #endregion
    }
}