﻿
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Common.DBUtility;

namespace DAL
{
    /// <summary>
    /// DAL层,系统用户与通知关系
    /// </summary>
    public partial class SYS_User_Note : DalBase
    {
        
        #region  基础方法
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public long Add(Model.SYS_User_Note model)
        {
           StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into SYS_User_Note(");			
            strSql.Append("UserID,NoteID,ReadCount");
			strSql.Append(") values (");
            strSql.Append("@UserID,@NoteID,@ReadCount");            
            strSql.Append(") ");            
            strSql.Append(";select @@IDENTITY");		
			SqlParameter[] parameters = {
			            new SqlParameter("@UserID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@NoteID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@ReadCount", SqlDbType.Int,4)             
              
            };
            			            
            parameters[0].Value = model.UserID;                        
            parameters[1].Value = model.NoteID;                        
            parameters[2].Value = model.ReadCount;                        
			   
			object obj = DBHelper.GetSingle(strSql.ToString(),parameters);			
			if (obj == null)
			{
				return 0;
			}
			else
			{
				                                    
            	return Convert.ToInt64(obj);
                                                  
			}			   
            		
        }
        
        
        
        /// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Model.SYS_User_Note model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update SYS_User_Note set ");
			                                                
            strSql.Append(" UserID = @UserID , ");                                    
            strSql.Append(" NoteID = @NoteID , ");                                    
            strSql.Append(" ReadCount = @ReadCount  ");            			
			strSql.Append(" where ID=@ID ");
						
SqlParameter[] parameters = {
			            new SqlParameter("@ID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@UserID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@NoteID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@ReadCount", SqlDbType.Int,4)             
              
            };
						            
            parameters[0].Value = model.ID;                        
            parameters[1].Value = model.UserID;                        
            parameters[2].Value = model.NoteID;                        
            parameters[3].Value = model.ReadCount;                        
            int rows=DBHelper.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
        #endregion
        
        
        #region 补充方法
		#endregion
    }
}