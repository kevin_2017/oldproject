﻿
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Common.DBUtility;

namespace DAL
{
    /// <summary>
    /// DAL层,系统角色权限
    /// </summary>
    public partial class SYS_Role_Authority : DalBase
    {
        
        #region  基础方法
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public long Add(Model.SYS_Role_Authority model)
        {
           StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into SYS_Role_Authority(");			
            strSql.Append("AuthorityID,RoleID");
			strSql.Append(") values (");
            strSql.Append("@AuthorityID,@RoleID");            
            strSql.Append(") ");            
            strSql.Append(";select @@IDENTITY");		
			SqlParameter[] parameters = {
			            new SqlParameter("@AuthorityID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@RoleID", SqlDbType.Int,4)             
              
            };
            			            
            parameters[0].Value = model.AuthorityID;                        
            parameters[1].Value = model.RoleID;                        
			   
			object obj = DBHelper.GetSingle(strSql.ToString(),parameters);			
			if (obj == null)
			{
				return 0;
			}
			else
			{
				                                    
            	return Convert.ToInt64(obj);
                                                  
			}			   
            		
        }
        
        
        
        /// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Model.SYS_Role_Authority model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update SYS_Role_Authority set ");
			                                                
            strSql.Append(" AuthorityID = @AuthorityID , ");                                    
            strSql.Append(" RoleID = @RoleID  ");            			
			strSql.Append(" where ID=@ID ");
						
SqlParameter[] parameters = {
			            new SqlParameter("@ID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@AuthorityID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@RoleID", SqlDbType.Int,4)             
              
            };
						            
            parameters[0].Value = model.ID;                        
            parameters[1].Value = model.AuthorityID;                        
            parameters[2].Value = model.RoleID;                        
            int rows=DBHelper.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
        #endregion
        
        
        #region 补充方法

        /// <summary>
        /// 
        /// </summary>
        /// <param name="RoleID"></param>
        /// <returns></returns>
        public bool DeleteByRoleID(long RoleID)
        {
            int rowsAffected = 0;
            SqlParameter[] parameters = {
					new SqlParameter("@RoleID", SqlDbType.BigInt)
			};
            parameters[0].Value = RoleID;

            DBHelper.RunProcedure("PROC_DeleteSys_Role_AuthorityByRoleID", parameters, out rowsAffected);
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
		#endregion
    }
}