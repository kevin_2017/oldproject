﻿using Common.DBUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DAL
{
    /// <summary>
    /// DAL层,订单退款管理
    /// </summary>
    public partial class V8_OrderBuyBackRecords : DalBase
    {
        
        #region  基础方法
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public long Add(Model.V8_OrderBuyBackRecords model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into V8_OrderBuyBackRecords(");
            strSql.Append("Remarks,OrderID,BuyBackType,Accounts,BuyBackMoney,CurrentStatus,CreateTime,OperationTime,OperationUser");
            strSql.Append(") values (");
            strSql.Append("@Remarks,@OrderID,@BuyBackType,@Accounts,@BuyBackMoney,@CurrentStatus,@CreateTime,@OperationTime,@OperationUser");
            strSql.Append(") ");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
			            new SqlParameter("@Remarks", SqlDbType.NVarChar,1000) ,            
                        new SqlParameter("@OrderID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@BuyBackType", SqlDbType.Int,4) ,            
                        new SqlParameter("@Accounts", SqlDbType.NVarChar,50) ,            
                        new SqlParameter("@BuyBackMoney", SqlDbType.Decimal,9) ,            
                        new SqlParameter("@CurrentStatus", SqlDbType.Int,4) ,            
                        new SqlParameter("@CreateTime", SqlDbType.DateTime) ,            
                        new SqlParameter("@OperationTime", SqlDbType.DateTime) ,            
                        new SqlParameter("@OperationUser", SqlDbType.BigInt,8)             
              
            };

            parameters[0].Value = model.Remarks;
            parameters[1].Value = model.OrderID;
            parameters[2].Value = model.BuyBackType;
            parameters[3].Value = model.Accounts;
            parameters[4].Value = model.BuyBackMoney;
            parameters[5].Value = model.CurrentStatus;
            parameters[6].Value = model.CreateTime;
            parameters[7].Value = model.OperationTime;
            parameters[8].Value = model.OperationUser;

            object obj = DBHelper.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {

                return Convert.ToInt64(obj);

            }

        }



        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Model.V8_OrderBuyBackRecords model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update V8_OrderBuyBackRecords set ");

            strSql.Append(" Remarks = @Remarks , ");
            strSql.Append(" OrderID = @OrderID , ");
            strSql.Append(" BuyBackType = @BuyBackType , ");
            strSql.Append(" Accounts = @Accounts , ");
            strSql.Append(" BuyBackMoney = @BuyBackMoney , ");
            strSql.Append(" CurrentStatus = @CurrentStatus , ");
            strSql.Append(" CreateTime = @CreateTime , ");
            strSql.Append(" OperationTime = @OperationTime , ");
            strSql.Append(" OperationUser = @OperationUser  ");
            strSql.Append(" where ID=@ID ");

            SqlParameter[] parameters = {
			            new SqlParameter("@ID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@Remarks", SqlDbType.NVarChar,1000) ,            
                        new SqlParameter("@OrderID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@BuyBackType", SqlDbType.Int,4) ,            
                        new SqlParameter("@Accounts", SqlDbType.NVarChar,50) ,            
                        new SqlParameter("@BuyBackMoney", SqlDbType.Decimal,9) ,            
                        new SqlParameter("@CurrentStatus", SqlDbType.Int,4) ,            
                        new SqlParameter("@CreateTime", SqlDbType.DateTime) ,            
                        new SqlParameter("@OperationTime", SqlDbType.DateTime) ,            
                        new SqlParameter("@OperationUser", SqlDbType.BigInt,8)             
              
            };

            parameters[0].Value = model.ID;
            parameters[1].Value = model.Remarks;
            parameters[2].Value = model.OrderID;
            parameters[3].Value = model.BuyBackType;
            parameters[4].Value = model.Accounts;
            parameters[5].Value = model.BuyBackMoney;
            parameters[6].Value = model.CurrentStatus;
            parameters[7].Value = model.CreateTime;
            parameters[8].Value = model.OperationTime;
            parameters[9].Value = model.OperationUser;
            int rows = DBHelper.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion


        #region 补充方法
        #endregion
    }
}