﻿using Common.DBUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DAL
{ /// <summary>
  /// DAL层,订单管理
  /// </summary>
    public partial class V8_OrderManage : DalBase
    {

        #region  基础方法 

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public long Add(Model.V8_OrderManage model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into V8_OrderManage(");
            strSql.Append("BranchName,RechargeNo,AttributionID,BranchOrderNo,CurentStatus,Counts,UnitPrice,TotalPrice,RechargeMoney,DiscountMethod,OrderNo,ProductSalePrice,Operator,Descriptions,TimeLimit,SysSerialNum,BranchSerialNum,IsBuyBack,ProductID,IsDelete,ClientIp,NotifyUrl,ExtendParam,NoticeCount,NotifyStatus,ProductName,IspType,IspName,PhoneType,ComFrom,BranchID,ProvinceId,ProvinceName,BranchCode,CityId,CityName");
            strSql.Append(") values (");
            strSql.Append("@BranchName,@RechargeNo,@AttributionID,@BranchOrderNo,@CurentStatus,@Counts,@UnitPrice,@TotalPrice,@RechargeMoney,@DiscountMethod,@OrderNo,@ProductSalePrice,@Operator,@Descriptions,@TimeLimit,@SysSerialNum,@BranchSerialNum,@IsBuyBack,@ProductID,@IsDelete,@ClientIp,@NotifyUrl,@ExtendParam,@NoticeCount,@NotifyStatus,@ProductName,@IspType,@IspName,@PhoneType,@ComFrom,@BranchID,@ProvinceId,@ProvinceName,@BranchCode,@CityId,@CityName");
            strSql.Append(") ");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
                        new SqlParameter("@BranchName", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@RechargeNo", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@AttributionID", SqlDbType.BigInt,8) ,
                        new SqlParameter("@BranchOrderNo", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@CurentStatus", SqlDbType.Int,4) ,
                        new SqlParameter("@Counts", SqlDbType.Int,4) ,
                        new SqlParameter("@UnitPrice", SqlDbType.Decimal,9) ,
                        new SqlParameter("@TotalPrice", SqlDbType.Decimal,9) ,
                        new SqlParameter("@RechargeMoney", SqlDbType.Decimal,9) ,
                        new SqlParameter("@DiscountMethod", SqlDbType.Int,4) ,
                        new SqlParameter("@OrderNo", SqlDbType.NVarChar,20) ,
                        new SqlParameter("@ProductSalePrice", SqlDbType.Decimal,9) ,
                        new SqlParameter("@Operator", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@Descriptions", SqlDbType.NVarChar,1000) ,
                        new SqlParameter("@TimeLimit", SqlDbType.Int,4) ,
                        new SqlParameter("@SysSerialNum", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@BranchSerialNum", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@IsBuyBack", SqlDbType.Int,4) ,
                        new SqlParameter("@ProductID", SqlDbType.BigInt,8) ,
                        new SqlParameter("@IsDelete", SqlDbType.Int,4) ,
                        new SqlParameter("@ClientIp", SqlDbType.NVarChar,20) ,
                        new SqlParameter("@NotifyUrl", SqlDbType.NVarChar,500) ,
                        new SqlParameter("@ExtendParam", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@NoticeCount", SqlDbType.Int,4) ,
                        new SqlParameter("@NotifyStatus", SqlDbType.Int,4) ,
                        new SqlParameter("@ProductName", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@IspType", SqlDbType.Int,4) ,
                        new SqlParameter("@IspName", SqlDbType.NVarChar,20) ,
                        new SqlParameter("@PhoneType", SqlDbType.Int,4) ,
                        new SqlParameter("@ComFrom", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@BranchID", SqlDbType.BigInt,8),
                        new SqlParameter("@ProvinceId", SqlDbType.Int,4) ,
                        new SqlParameter("@ProvinceName", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@BranchCode", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@CityId", SqlDbType.Int,4) ,
                        new SqlParameter("@CityName", SqlDbType.NVarChar,50) ,

            };

            parameters[0].Value = model.BranchName;
            parameters[1].Value = model.RechargeNo;
            parameters[2].Value = model.AttributionID;
            parameters[3].Value = model.BranchOrderNo;
            parameters[4].Value = model.CurentStatus;
            parameters[5].Value = model.Counts;
            parameters[6].Value = model.UnitPrice;
            parameters[7].Value = model.TotalPrice;
            parameters[8].Value = model.RechargeMoney;
            parameters[9].Value = model.DiscountMethod;
            parameters[10].Value = model.OrderNo;
            parameters[11].Value = model.ProductSalePrice;
            parameters[12].Value = model.Operator;
            parameters[13].Value = model.Descriptions;
            parameters[14].Value = model.TimeLimit;
            parameters[15].Value = model.SysSerialNum;
            parameters[16].Value = model.BranchSerialNum;
            parameters[17].Value = model.IsBuyBack;
            parameters[18].Value = model.ProductID;
            parameters[19].Value = model.IsDelete;
            parameters[20].Value = model.ClientIp;
            parameters[21].Value = model.NotifyUrl;
            parameters[22].Value = model.ExtendParam;
            parameters[23].Value = model.NoticeCount;
            parameters[24].Value = model.NotifyStatus;
            parameters[25].Value = model.ProductName;
            parameters[26].Value = model.IspType;
            parameters[27].Value = model.IspName;
            parameters[28].Value = model.PhoneType;
            parameters[29].Value = model.ComFrom;
            parameters[30].Value = model.BranchID;
            parameters[31].Value = model.ProvinceId;
            parameters[32].Value = model.ProvinceName;
            parameters[33].Value = model.BranchCode;
            parameters[34].Value = model.CityId;
            parameters[35].Value = model.CityName;

            object obj = DBHelper.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {

                return Convert.ToInt64(obj);

            }

        }


        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Model.V8_OrderManage model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update V8_OrderManage set ");
            strSql.Append(" BranchName = @BranchName , ");
            strSql.Append(" RechargeNo = @RechargeNo , ");
            strSql.Append(" AttributionID = @AttributionID , ");
            strSql.Append(" BranchOrderNo = @BranchOrderNo , ");
            strSql.Append(" CurentStatus = @CurentStatus , ");
            strSql.Append(" Counts = @Counts , ");
            strSql.Append(" UnitPrice = @UnitPrice , ");
            strSql.Append(" TotalPrice = @TotalPrice , ");
            strSql.Append(" RechargeMoney = @RechargeMoney , ");
            strSql.Append(" DiscountMethod = @DiscountMethod , ");
            strSql.Append(" OrderNo = @OrderNo , ");
            strSql.Append(" ProductSalePrice = @ProductSalePrice , ");
            strSql.Append(" CreateTime = @CreateTime , ");
            strSql.Append(" OperationTime = @OperationTime , ");
            strSql.Append(" CallBackTime = @CallBackTime , ");
            strSql.Append(" Operator = @Operator , ");
            strSql.Append(" Descriptions = @Descriptions , ");
            strSql.Append(" TimeLimit = @TimeLimit , ");
            strSql.Append(" SysSerialNum = @SysSerialNum , ");
            strSql.Append(" BranchSerialNum = @BranchSerialNum , ");
            strSql.Append(" IsBuyBack = @IsBuyBack , ");
            strSql.Append(" ProductID = @ProductID , ");
            strSql.Append(" IsDelete = @IsDelete , ");
            strSql.Append(" ClientIp = @ClientIp , ");
            strSql.Append(" NotifyUrl = @NotifyUrl , ");
            strSql.Append(" ExtendParam = @ExtendParam , ");
            strSql.Append(" NoticeCount = @NoticeCount , ");
            strSql.Append(" NotifyStatus = @NotifyStatus , ");
            strSql.Append(" ProductName = @ProductName , ");
            strSql.Append(" IspType = @IspType , ");
            strSql.Append(" IspName = @IspName , ");
            strSql.Append(" PhoneType = @PhoneType , ");
            strSql.Append(" ComFrom = @ComFrom , ");
            strSql.Append(" BranchID = @BranchID  ");
            strSql.Append(" where ID=@ID ");

            SqlParameter[] parameters = {
                        new SqlParameter("@ID", SqlDbType.BigInt,8) ,
                        new SqlParameter("@BranchName", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@RechargeNo", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@AttributionID", SqlDbType.BigInt,8) ,
                        new SqlParameter("@BranchOrderNo", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@CurentStatus", SqlDbType.Int,4) ,
                        new SqlParameter("@Counts", SqlDbType.Int,4) ,
                        new SqlParameter("@UnitPrice", SqlDbType.Decimal,9) ,
                        new SqlParameter("@TotalPrice", SqlDbType.Decimal,9) ,
                        new SqlParameter("@RechargeMoney", SqlDbType.Decimal,9) ,
                        new SqlParameter("@DiscountMethod", SqlDbType.Int,4) ,
                        new SqlParameter("@OrderNo", SqlDbType.NVarChar,20) ,
                        new SqlParameter("@ProductSalePrice", SqlDbType.Decimal,9) ,
                        new SqlParameter("@CreateTime", SqlDbType.DateTime) ,
                        new SqlParameter("@OperationTime", SqlDbType.DateTime) ,
                        new SqlParameter("@CallBackTime", SqlDbType.DateTime) ,
                        new SqlParameter("@Operator", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@Descriptions", SqlDbType.NVarChar,1000) ,
                        new SqlParameter("@TimeLimit", SqlDbType.Int,4) ,
                        new SqlParameter("@SysSerialNum", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@BranchSerialNum", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@IsBuyBack", SqlDbType.Int,4) ,
                        new SqlParameter("@ProductID", SqlDbType.BigInt,8) ,
                        new SqlParameter("@IsDelete", SqlDbType.Int,4) ,
                        new SqlParameter("@ClientIp", SqlDbType.NVarChar,20) ,
                        new SqlParameter("@NotifyUrl", SqlDbType.NVarChar,500) ,
                        new SqlParameter("@ExtendParam", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@NoticeCount", SqlDbType.Int,4) ,
                        new SqlParameter("@NotifyStatus", SqlDbType.Int,4) ,
                        new SqlParameter("@ProductName", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@IspType", SqlDbType.Int,4) ,
                        new SqlParameter("@IspName", SqlDbType.NVarChar,20) ,
                        new SqlParameter("@PhoneType", SqlDbType.Int,4) ,
                        new SqlParameter("@ComFrom", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@BranchID", SqlDbType.BigInt,8)

            };

            parameters[0].Value = model.ID;
            parameters[1].Value = model.BranchName;
            parameters[2].Value = model.RechargeNo;
            parameters[3].Value = model.AttributionID;
            parameters[4].Value = model.BranchOrderNo;
            parameters[5].Value = model.CurentStatus;
            parameters[6].Value = model.Counts;
            parameters[7].Value = model.UnitPrice;
            parameters[8].Value = model.TotalPrice;
            parameters[9].Value = model.RechargeMoney;
            parameters[10].Value = model.DiscountMethod;
            parameters[11].Value = model.OrderNo;
            parameters[12].Value = model.ProductSalePrice;
            parameters[13].Value = model.CreateTime;
            parameters[14].Value = model.OperationTime;
            parameters[15].Value = model.CallBackTime;
            parameters[16].Value = model.Operator;
            parameters[17].Value = model.Descriptions;
            parameters[18].Value = model.TimeLimit;
            parameters[19].Value = model.SysSerialNum;
            parameters[20].Value = model.BranchSerialNum;
            parameters[21].Value = model.IsBuyBack;
            parameters[22].Value = model.ProductID;
            parameters[23].Value = model.IsDelete;
            parameters[24].Value = model.ClientIp;
            parameters[25].Value = model.NotifyUrl;
            parameters[26].Value = model.ExtendParam;
            parameters[27].Value = model.NoticeCount;
            parameters[28].Value = model.NotifyStatus;
            parameters[29].Value = model.ProductName;
            parameters[30].Value = model.IspType;
            parameters[31].Value = model.IspName;
            parameters[32].Value = model.PhoneType;
            parameters[33].Value = model.ComFrom;
            parameters[34].Value = model.BranchID;
            int rows = DBHelper.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion


        #region 补充方法

        #endregion
    }
}