﻿using System;
using System.Text;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Data;
using Common.DBUtility;
namespace DAL
{
    //网店商品进价
    public partial class V8_Product_Branch : DalBase
    {
        #region  基础方法

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public long Add(Model.V8_Product_Branch model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into V8_Product_Branch(");
            strSql.Append("Inprice,Sort,IsClosed,IsDelete,ArriveMethod,FlowType,CreateTime,BranchID,Creator,UpdateTime,Updator,BranchName,ProductTypeId,ProductTypeName,ProductID,ProductName,ISPType,ISPName,MinValue,MaxValue,IsMultiple,ProvinceIds");
            strSql.Append(") values (");
            strSql.Append("@Inprice,@Sort,@IsClosed,@IsDelete,@ArriveMethod,@FlowType,@CreateTime,@BranchID,@Creator,@UpdateTime,@Updator,@BranchName,@ProductTypeId,@ProductTypeName,@ProductID,@ProductName,@ISPType,@ISPName,@MinValue,@MaxValue,@IsMultiple,@ProvinceIds");
            strSql.Append(") ");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
                        new SqlParameter("@Inprice", SqlDbType.Decimal,9) ,
                        new SqlParameter("@Sort", SqlDbType.BigInt,8) ,
                        new SqlParameter("@IsClosed", SqlDbType.Int,4) ,
                        new SqlParameter("@IsDelete", SqlDbType.Int,4) ,
                        new SqlParameter("@ArriveMethod", SqlDbType.Int,4) ,
                        new SqlParameter("@FlowType", SqlDbType.Int,4) ,
                        new SqlParameter("@CreateTime", SqlDbType.DateTime) ,
                        new SqlParameter("@BranchID", SqlDbType.BigInt,8) ,
                        new SqlParameter("@Creator", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@UpdateTime", SqlDbType.DateTime) ,
                        new SqlParameter("@Updator", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@BranchName", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@ProductTypeId", SqlDbType.BigInt,8) ,
                        new SqlParameter("@ProductTypeName", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@ProductID", SqlDbType.BigInt,8) ,
                        new SqlParameter("@ProductName", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@ISPType", SqlDbType.Int,4) ,
                        new SqlParameter("@ISPName", SqlDbType.NVarChar,50),
                        new SqlParameter("@MinValue", SqlDbType.Decimal,9) ,
                        new SqlParameter("@MaxValue", SqlDbType.Decimal,9) ,
                        new SqlParameter("@IsMultiple", SqlDbType.Int,4) ,
                        new SqlParameter("@ProvinceIds", SqlDbType.NVarChar,4000) ,

            };

            parameters[0].Value = model.Inprice;
            parameters[1].Value = model.Sort;
            parameters[2].Value = model.IsClosed;
            parameters[3].Value = model.IsDelete;
            parameters[4].Value = model.ArriveMethod;
            parameters[5].Value = model.FlowType;
            parameters[6].Value = DateTime.Now;
            parameters[7].Value = model.BranchID;
            parameters[8].Value = model.Creator;
            parameters[9].Value = DateTime.Now;
            parameters[10].Value = model.Updator;
            parameters[11].Value = model.BranchName;
            parameters[12].Value = model.ProductTypeId;
            parameters[13].Value = model.ProductTypeName;
            parameters[14].Value = model.ProductID;
            parameters[15].Value = model.ProductName;
            parameters[16].Value = model.ISPType;
            parameters[17].Value = model.ISPName;
            parameters[18].Value = model.MinValue;
            parameters[19].Value = model.MaxValue;
            parameters[20].Value = model.IsMultiple;
            parameters[21].Value = model.ProvinceIds;

            object obj = DBHelper.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {

                return Convert.ToInt64(obj);

            }

        }


        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Model.V8_Product_Branch model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update V8_Product_Branch set ");
            strSql.Append(" Inprice = @Inprice , ");
            strSql.Append(" Sort = @Sort , ");
            strSql.Append(" IsClosed = @IsClosed , ");
            strSql.Append(" IsDelete = @IsDelete , ");
            strSql.Append(" ArriveMethod = @ArriveMethod , ");
            strSql.Append(" FlowType = @FlowType , ");
            strSql.Append(" BranchID = @BranchID , ");
            strSql.Append(" UpdateTime = @UpdateTime , ");
            strSql.Append(" Updator = @Updator , ");
            strSql.Append(" BranchName = @BranchName , ");
            strSql.Append(" ProductTypeId = @ProductTypeId , ");
            strSql.Append(" ProductTypeName = @ProductTypeName , ");
            strSql.Append(" ProductID = @ProductID , ");
            strSql.Append(" ProductName = @ProductName , ");
            strSql.Append(" ISPType = @ISPType , ");
            strSql.Append(" ISPName = @ISPName,  ");
            strSql.Append(" MinValue = @MinValue , ");
            strSql.Append(" MaxValue = @MaxValue , ");
            strSql.Append(" IsMultiple = @IsMultiple  ");
            strSql.Append(" where ID=@ID ");

            SqlParameter[] parameters = {
                        new SqlParameter("@ID", SqlDbType.BigInt,8) ,
                        new SqlParameter("@Inprice", SqlDbType.Decimal,9) ,
                        new SqlParameter("@Sort", SqlDbType.BigInt,8) ,
                        new SqlParameter("@IsClosed", SqlDbType.Int,4) ,
                        new SqlParameter("@IsDelete", SqlDbType.Int,4) ,
                        new SqlParameter("@ArriveMethod", SqlDbType.Int,4) ,
                        new SqlParameter("@FlowType", SqlDbType.Int,4) ,
                        new SqlParameter("@BranchID", SqlDbType.BigInt,8) ,
                        new SqlParameter("@UpdateTime", SqlDbType.DateTime) ,
                        new SqlParameter("@Updator", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@BranchName", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@ProductTypeId", SqlDbType.BigInt,8) ,
                        new SqlParameter("@ProductTypeName", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@ProductID", SqlDbType.BigInt,8) ,
                        new SqlParameter("@ProductName", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@ISPType", SqlDbType.Int,4) ,
                        new SqlParameter("@ISPName", SqlDbType.NVarChar,50),
                new SqlParameter("@MinValue", SqlDbType.Decimal,9) ,
                new SqlParameter("@MaxValue", SqlDbType.Decimal,9) ,
                new SqlParameter("@IsMultiple", SqlDbType.Int,4) 

            };

            parameters[0].Value = model.ID;
            parameters[1].Value = model.Inprice;
            parameters[2].Value = model.Sort;
            parameters[3].Value = model.IsClosed;
            parameters[4].Value = model.IsDelete;
            parameters[5].Value = model.ArriveMethod;
            parameters[6].Value = model.FlowType;
            parameters[7].Value = model.BranchID;
            parameters[8].Value = model.UpdateTime;
            parameters[9].Value = model.Updator;
            parameters[10].Value = model.BranchName;
            parameters[11].Value = model.ProductTypeId;
            parameters[12].Value = model.ProductTypeName;
            parameters[13].Value = model.ProductID;
            parameters[14].Value = model.ProductName;
            parameters[15].Value = model.ISPType;
            parameters[16].Value = model.ISPName;
            parameters[17].Value = model.MinValue;
            parameters[18].Value = model.MaxValue;
            parameters[19].Value = model.IsMultiple;

            int rows = DBHelper.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region 补充方法
        #endregion


    }
}

