﻿using Common.DBUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public partial class V8_OrderRequestRecs : DalBase
    {



        #region  基础方法

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public long Add(Model.V8_OrderRequestRecs model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into V8_OrderRequestRecs(");
            strSql.Append("OrderNo,SupplierId,ProSuppId,RequestContent,RequestTime,ResponseContent,NotifyResult");
            strSql.Append(") values (");
            strSql.Append("@OrderNo,@SupplierId,@ProSuppId,@RequestContent,@RequestTime,@ResponseContent,@NotifyResult");
            strSql.Append(") ");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
                        new SqlParameter("@OrderNo", SqlDbType.NVarChar,20) ,
                        new SqlParameter("@SupplierId", SqlDbType.BigInt,8) ,
                        new SqlParameter("@ProSuppId", SqlDbType.BigInt,8) ,
                        new SqlParameter("@RequestContent", SqlDbType.NVarChar,4000) ,
                        new SqlParameter("@RequestTime", SqlDbType.DateTime) ,
                        new SqlParameter("@ResponseContent", SqlDbType.NVarChar,4000) ,
                        new SqlParameter("@NotifyResult", SqlDbType.Int,4) 

            };

            parameters[0].Value = model.OrderNo;
            parameters[1].Value = model.SupplierId;
            parameters[2].Value = model.ProSuppId;
            parameters[3].Value = model.RequestContent;
            parameters[4].Value = DateTime.Now;
            parameters[5].Value = model.ResponseContent;
            parameters[6].Value = model.NotifyResult;

            object obj = DBHelper.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {

                return Convert.ToInt64(obj);

            }

        }


        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Model.V8_OrderRequestRecs model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update V8_OrderRequestRecs set ");

            strSql.Append(" OrderNo = @OrderNo , ");
            strSql.Append(" SupplierId = @SupplierId , ");
            strSql.Append(" ProSuppId = @ProSuppId , ");
            strSql.Append(" RequestContent = @RequestContent , ");
            strSql.Append(" RequestTime = @RequestTime , ");
            strSql.Append(" ResponseContent = @ResponseContent , ");
            strSql.Append(" NotifyResult = @NotifyResult , ");
            strSql.Append(" NotifyTime = @NotifyTime, ");
            strSql.Append(" NoticeContent = @NoticeContent  ");
            strSql.Append(" where ID=@ID ");

            SqlParameter[] parameters = {
                        new SqlParameter("@ID", SqlDbType.NVarChar,20) ,
                        new SqlParameter("@OrderNo", SqlDbType.NVarChar,20) ,
                        new SqlParameter("@SupplierId", SqlDbType.BigInt,8) ,
                        new SqlParameter("@ProSuppId", SqlDbType.BigInt,8) ,
                        new SqlParameter("@RequestContent", SqlDbType.NVarChar,4000) ,
                        new SqlParameter("@RequestTime", SqlDbType.DateTime) ,
                        new SqlParameter("@ResponseContent", SqlDbType.NVarChar,4000) ,
                        new SqlParameter("@NotifyResult", SqlDbType.Int,4) ,
                        new SqlParameter("@NotifyTime", SqlDbType.DateTime),
                        new SqlParameter("@NoticeContent", SqlDbType.NVarChar,4000) ,

            };

            parameters[0].Value = model.ID;
            parameters[1].Value = model.OrderNo;
            parameters[2].Value = model.SupplierId;
            parameters[3].Value = model.ProSuppId;
            parameters[4].Value = model.RequestContent;
            parameters[5].Value = model.RequestTime;
            parameters[6].Value = model.ResponseContent;
            parameters[7].Value = model.NotifyResult;
            parameters[8].Value = model.NotifyTime;
            parameters[9].Value = model.NoticeContent;
            int rows = DBHelper.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region 补充方法
        #endregion


    }
}


