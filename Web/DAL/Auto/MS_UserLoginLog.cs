﻿
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Common.DBUtility;
using Model;
namespace DAL
{
    /// <summary>
    /// DAL层,网店用户登录日志
    /// </summary>
    public partial class MS_UserLoginLog : DalBase
    {
        
        #region  基础方法
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public long Add(Model.MS_UserLoginLog model)
        {
           StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into MS_UserLoginLog(");			
            strSql.Append("BranchID,UserID,LoginTime,LoginIP");
			strSql.Append(") values (");
            strSql.Append("@BranchID,@UserID,@LoginTime,@LoginIP");            
            strSql.Append(") ");            
            strSql.Append(";select @@IDENTITY");		
			SqlParameter[] parameters = {
			            new SqlParameter("@BranchID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@UserID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@LoginTime", SqlDbType.DateTime) ,            
                        new SqlParameter("@LoginIP", SqlDbType.NVarChar,20)             
              
            };
            			            
            parameters[0].Value = model.BranchID;                        
            parameters[1].Value = model.UserID;                        
            parameters[2].Value = model.LoginTime;                        
            parameters[3].Value = model.LoginIP;                        
			   
			object obj = DBHelper.GetSingle(strSql.ToString(),parameters);			
			if (obj == null)
			{
				return 0;
			}
			else
			{
				                                    
            	return Convert.ToInt64(obj);
                                                  
			}			   
            		
        }
        
        
        
        /// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Model.MS_UserLoginLog model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update MS_UserLoginLog set ");
			                                                
            strSql.Append(" BranchID = @BranchID , ");                                    
            strSql.Append(" UserID = @UserID , ");                                    
            strSql.Append(" LoginTime = @LoginTime , ");                                    
            strSql.Append(" LoginIP = @LoginIP  ");            			
			strSql.Append(" where ID=@ID ");
						
SqlParameter[] parameters = {
			            new SqlParameter("@ID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@BranchID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@UserID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@LoginTime", SqlDbType.DateTime) ,            
                        new SqlParameter("@LoginIP", SqlDbType.NVarChar,20)             
              
            };
						            
            parameters[0].Value = model.ID;                        
            parameters[1].Value = model.BranchID;                        
            parameters[2].Value = model.UserID;                        
            parameters[3].Value = model.LoginTime;                        
            parameters[4].Value = model.LoginIP;                        
            int rows=DBHelper.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
        #endregion
        
        
        #region 补充方法
		#endregion
    }
}