﻿
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Common.DBUtility;

namespace DAL
{
    /// <summary>
    /// DAL层,角色权限按钮
    /// </summary>
    public partial class SYS_Role_Auth_AuthButton : DalBase
    {
        
        #region  基础方法
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public long Add(Model.SYS_Role_Auth_AuthButton model)
        {
           StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into SYS_Role_Auth_AuthButton(");			
            strSql.Append("RAID,ABID");
			strSql.Append(") values (");
            strSql.Append("@RAID,@ABID");            
            strSql.Append(") ");            
            strSql.Append(";select @@IDENTITY");		
			SqlParameter[] parameters = {
			            new SqlParameter("@RAID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@ABID", SqlDbType.BigInt,8)             
              
            };
            			            
            parameters[0].Value = model.RAID;                        
            parameters[1].Value = model.ABID;                        
			   
			object obj = DBHelper.GetSingle(strSql.ToString(),parameters);			
			if (obj == null)
			{
				return 0;
			}
			else
			{
				                                    
            	return Convert.ToInt64(obj);
                                                  
			}			   
            		
        }
        
        
        
        /// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Model.SYS_Role_Auth_AuthButton model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update SYS_Role_Auth_AuthButton set ");
			                                                
            strSql.Append(" RAID = @RAID , ");                                    
            strSql.Append(" ABID = @ABID  ");            			
			strSql.Append(" where ID=@ID ");
						
SqlParameter[] parameters = {
			            new SqlParameter("@ID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@RAID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@ABID", SqlDbType.BigInt,8)             
              
            };
						            
            parameters[0].Value = model.ID;                        
            parameters[1].Value = model.RAID;                        
            parameters[2].Value = model.ABID;                        
            int rows=DBHelper.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
        #endregion
        
        
        #region 补充方法

        /// <summary>
        /// 根据条件查询当前角色的按钮
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet GetRolesButton(string strWhere)
        {
            StringBuilder strSQL = new StringBuilder();
            strSQL.Append("select ID ,RAID,ABID,RoleID from  vw_GetSystemRoleAuthButton ");
            strSQL.Append(" where 1=1 ");
            strSQL.Append(strWhere);
            return DBHelper.Query(strSQL.ToString());
        }


        /// <summary>
        ///  增加一条数据
        /// </summary>
        public long Add(Model.AddRole_AuthButton model)
        {
            int rowsAffected;
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.BigInt,8),
					new SqlParameter("@RID", SqlDbType.BigInt),
					new SqlParameter("@AID", SqlDbType.BigInt,8),
					new SqlParameter("@ABID", SqlDbType.BigInt,8)};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = model.RID;
            parameters[2].Value = model.Aid;
            parameters[3].Value = model.ABID;
            DBHelper.RunProcedure("PROC_Addsys_Role_Auth_AuthButton", parameters, out rowsAffected);
            return (long)parameters[0].Value;
        }


		#endregion
    }
}