﻿using System;
using System.Text;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Data;
using Common.DBUtility;
namespace DAL
{
    //供应商商品类型
    public partial class V8_Product_Type : DalBase
    {
        #region  基础方法

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public long Add(Model.V8_Product_Type model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into V8_Product_Type(");
            strSql.Append("TypeName,IsDelete");
            strSql.Append(") values (");
            strSql.Append("@TypeName,@IsDelete");
            strSql.Append(") ");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
                        new SqlParameter("@TypeName", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@IsDelete", SqlDbType.Int,4)

            };

            parameters[0].Value = model.TypeName;
            parameters[1].Value = model.IsDelete;

            object obj = DBHelper.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {

                return Convert.ToInt64(obj);

            }

        }


        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Model.V8_Product_Type model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update V8_Product_Type set ");

            strSql.Append(" TypeName = @TypeName , ");
            strSql.Append(" IsDelete = @IsDelete  ");
            strSql.Append(" where ID=@ID ");

            SqlParameter[] parameters = {
                        new SqlParameter("@ID", SqlDbType.BigInt,8) ,
                        new SqlParameter("@TypeName", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@IsDelete", SqlDbType.Int,4)

            };

            parameters[0].Value = model.ID;
            parameters[1].Value = model.TypeName;
            parameters[2].Value = model.IsDelete;
            int rows = DBHelper.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region 补充方法
        #endregion


    }
}

