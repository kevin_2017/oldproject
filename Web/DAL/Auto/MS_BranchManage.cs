﻿
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using CE.Utility.Text;
using Common.DBUtility;
using Model;
namespace DAL
{
    /// <summary>
    /// DAL层,网店基本信息
    /// </summary>
    public partial class MS_BranchManage : DalBase
    {

        #region  基础方法
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public long Add(Model.MS_BranchManage model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into MS_BranchManage(");
            strSql.Append("BranchName,StoreKeeper,Phones,Address,Status,ClientCount,EmployeeCount,RechargeMOney,Balance,SettlementMethod,CreateTime,Level,ISSystem,ParentID,PrivateKey,RechargeType,BranchCode,Pwd");
            strSql.Append(") values (");
            strSql.Append("@BranchName,@StoreKeeper,@Phones,@Address,@Status,@ClientCount,@EmployeeCount,@RechargeMOney,@Balance,@SettlementMethod,getdate(),@Level,@ISSystem,@ParentID,@PrivateKey,@RechargeType,@BranchCode,@Pwd");
            strSql.Append(") ");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
                        new SqlParameter("@BranchName", SqlDbType.NVarChar,400) ,
                        new SqlParameter("@StoreKeeper", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@Phones", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@Address", SqlDbType.NVarChar,500) ,
                        new SqlParameter("@Status", SqlDbType.Int,4) ,
                        new SqlParameter("@ClientCount", SqlDbType.Int,4) ,
                        new SqlParameter("@EmployeeCount", SqlDbType.Int,4) ,
                        new SqlParameter("@RechargeMOney", SqlDbType.Decimal,9) ,
                        new SqlParameter("@Balance", SqlDbType.Decimal,9) ,
                        new SqlParameter("@SettlementMethod", SqlDbType.Int,4) ,
                        new SqlParameter("@Level", SqlDbType.Int,4) ,
                        new SqlParameter("@ISSystem", SqlDbType.Int,4) ,
                        new SqlParameter("@ParentID", SqlDbType.BigInt,8)  ,
                        new SqlParameter("@PrivateKey", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@RechargeType", SqlDbType.Int,4) ,
                        new SqlParameter("@BranchCode", SqlDbType.NVarChar,50)    ,
                        new SqlParameter("@Pwd", SqlDbType.NVarChar,50)

            };

            parameters[0].Value = model.BranchName;
            parameters[1].Value = model.StoreKeeper;
            parameters[2].Value = model.Phones;
            parameters[3].Value = model.Address;
            parameters[4].Value = model.Status;
            parameters[5].Value = model.ClientCount;
            parameters[6].Value = model.EmployeeCount;
            parameters[7].Value = model.RechargeMOney;
            parameters[8].Value = model.Balance;
            parameters[9].Value = model.SettlementMethod;
            parameters[10].Value = model.Level;
            parameters[11].Value = model.ISSystem;
            parameters[12].Value = model.ParentID;
            parameters[13].Value = StringUtil.GetRandomString(32);
            parameters[14].Value = model.RechargeType;
            parameters[15].Value = model.BranchCode;
            parameters[16].Value = Common.Method.GetMD5("123456", Common.Method.MD5EncryptBit.bit32);
            object obj = DBHelper.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {

                return Convert.ToInt64(obj);

            }

        }


        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Model.MS_BranchManage model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update MS_BranchManage set ");
            strSql.Append(" PrivateKey = @PrivateKey , ");
            strSql.Append(" BranchName = @BranchName , ");
            strSql.Append(" StoreKeeper = @StoreKeeper , ");
            strSql.Append(" Phones = @Phones , ");
            strSql.Append(" Address = @Address , ");
            strSql.Append(" Status = @Status , ");
            strSql.Append(" ClientCount = @ClientCount , ");
            strSql.Append(" EmployeeCount = @EmployeeCount , ");
            strSql.Append(" SettlementMethod = @SettlementMethod , ");
            strSql.Append(" ISSystem = @ISSystem,  ");
            strSql.Append(" IsDelete = @IsDelete,  ");
            strSql.Append(" RechargeType = @RechargeType,  ");
            strSql.Append(" BranchCode = @BranchCode  ");
            strSql.Append(" where ID=@ID ");

            SqlParameter[] parameters = {
                        new SqlParameter("@ID", SqlDbType.BigInt,8) ,
                        new SqlParameter("@BranchName", SqlDbType.NVarChar,400) ,
                        new SqlParameter("@StoreKeeper", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@Phones", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@Address", SqlDbType.NVarChar,500) ,
                        new SqlParameter("@Status", SqlDbType.Int,4) ,
                        new SqlParameter("@ClientCount", SqlDbType.Int,4) ,
                        new SqlParameter("@EmployeeCount", SqlDbType.Int,4) ,
                        new SqlParameter("@SettlementMethod", SqlDbType.Int,4) ,
                        new SqlParameter("@ISSystem", SqlDbType.Int,4) ,
                        new SqlParameter("@PrivateKey", SqlDbType.NVarChar,36) ,
                        new SqlParameter("@IsDelete", SqlDbType.Int,4) ,
                        new SqlParameter("@RechargeType", SqlDbType.Int,4),
                        new SqlParameter("@BranchCode", SqlDbType.NVarChar,50) ,
            };

            parameters[0].Value = model.ID;
            parameters[1].Value = model.BranchName;
            parameters[2].Value = model.StoreKeeper;
            parameters[3].Value = model.Phones;
            parameters[4].Value = model.Address;
            parameters[5].Value = model.Status;
            parameters[6].Value = model.ClientCount;
            parameters[7].Value = model.EmployeeCount;
            parameters[8].Value = model.SettlementMethod;
            parameters[9].Value = model.ISSystem;
            parameters[10].Value = model.PrivateKey;
            parameters[11].Value = model.IsDelete;
            parameters[12].Value = model.RechargeType;
            parameters[13].Value = model.BranchCode;
            int rows = DBHelper.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion


        #region 补充方法
        public bool Proc_BalanceChange(int ChangeType, decimal balance, long brachid, long OrderID, string ChangeMethod, string Descriptions)
        {
            int rowsAffected;
            SqlParameter[] parameters = {
                          new SqlParameter("@ChangeType", SqlDbType.Int,4),
                          new SqlParameter("@balance", SqlDbType.Decimal,9),
                          new SqlParameter("@brachid", SqlDbType.BigInt,8),
                          new SqlParameter("@OrderID", SqlDbType.BigInt,8),
                          new SqlParameter("@ChangeMethod", SqlDbType.NVarChar,20),
                          new SqlParameter("@Descriptions", SqlDbType.NVarChar,1000)
                          };
            parameters[0].Value = ChangeType;
            parameters[1].Value = balance;
            parameters[2].Value = brachid;
            parameters[3].Value = OrderID;
            parameters[4].Value = ChangeMethod;
            parameters[5].Value = Descriptions;
            int result = DBHelper.RunProcedure_("Proc_BalanceChange", parameters, out rowsAffected);
            if (result > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        public bool UpdateBranchPrivateKeyByBranchID(long branchID, string privateKey)
        {
            StringBuilder strSQL = new StringBuilder();
            strSQL.Append("update MS_BranchManage set PrivateKey=@PrivateKey where ID=@ID");
            SqlParameter[] parameters = {
                          new SqlParameter("@ID", SqlDbType.BigInt,8),
                          new SqlParameter("@PrivateKey", SqlDbType.NVarChar,20)
                          };
            parameters[0].Value = branchID;
            parameters[1].Value = privateKey;
            int result = DBHelper.ExecuteSql(strSQL.ToString(), parameters);
            if (result > 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        

        #endregion
    }
}