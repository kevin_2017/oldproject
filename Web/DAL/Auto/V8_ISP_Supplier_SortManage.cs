﻿using System;
using System.Text;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Data;
using Common.DBUtility;
namespace DAL
{

    /// <summary>
    /// 运营商各充值接口的使用顺序
    /// </summary>
    public partial class V8_ISP_Supplier_SortManage : DalBase
    {
        #region  基础方法

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public long Add(Model.V8_ISP_Supplier_SortManage model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into V8_ISP_Supplier_SortManage(");
            strSql.Append("ISPType,ISPName,SupplierID,SupplierName,Sort");
            strSql.Append(") values (");
            strSql.Append("@ISPType,@ISPName,@SupplierID,@SupplierName,@Sort");
            strSql.Append(") ");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
                        new SqlParameter("@ISPType", SqlDbType.Int,4) ,
                        new SqlParameter("@ISPName", SqlDbType.NVarChar,50) ,
                         new SqlParameter("@SupplierID", SqlDbType.BigInt,8) ,
                        new SqlParameter("@SupplierName", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@Sort", SqlDbType.Int,4) ,

            };

            parameters[0].Value = model.ISPType;
            parameters[1].Value = model.ISPName;
            parameters[2].Value = model.SupplierID;
            parameters[3].Value = model.SupplierName;
            parameters[4].Value = model.Sort;

            object obj = DBHelper.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {

                return Convert.ToInt64(obj);

            }

        }


        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Model.V8_ISP_Supplier_SortManage model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update V8_ISP_Supplier_SortManage set ");
            strSql.Append(" ISPType = @ISPType , ");
            strSql.Append(" ISPName = @ISPName , ");
            strSql.Append(" SupplierID = @SupplierID , ");
            strSql.Append(" SupplierName = @SupplierName,  ");
            strSql.Append(" Sort = @Sort  ");
            strSql.Append(" where ID=@ID ");

            SqlParameter[] parameters = {
                        new SqlParameter("@ISPType", SqlDbType.Int,4) ,
                        new SqlParameter("@ISPName", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@SupplierID", SqlDbType.BigInt,8) ,
                        new SqlParameter("@SupplierName", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@Sort", SqlDbType.Int,4) ,
                        new SqlParameter("@ID", SqlDbType.BigInt,8) ,
            };

            parameters[0].Value = model.ISPType;
            parameters[1].Value = model.ISPName;
            parameters[2].Value = model.SupplierID;
            parameters[3].Value = model.SupplierName;
            parameters[4].Value = model.Sort;
            parameters[5].Value = model.ID;

            int rows = DBHelper.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region 补充方法


        #endregion


    }
}

