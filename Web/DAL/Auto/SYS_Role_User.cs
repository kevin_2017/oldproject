﻿
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Common.DBUtility;

namespace DAL
{
    /// <summary>
    /// DAL层,用户角色
    /// </summary>
    public partial class SYS_Role_User : DalBase
    {
        
        #region  基础方法
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public long Add(Model.SYS_Role_User model)
        {
           StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into SYS_Role_User(");			
            strSql.Append("UserID,RoleID");
			strSql.Append(") values (");
            strSql.Append("@UserID,@RoleID");            
            strSql.Append(") ");            
            strSql.Append(";select @@IDENTITY");		
			SqlParameter[] parameters = {
			            new SqlParameter("@UserID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@RoleID", SqlDbType.Int,4)             
              
            };
            			            
            parameters[0].Value = model.UserID;                        
            parameters[1].Value = model.RoleID;                        
			   
			object obj = DBHelper.GetSingle(strSql.ToString(),parameters);			
			if (obj == null)
			{
				return 0;
			}
			else
			{
				                    
            	return Convert.ToInt32(obj);
                                                                  
			}			   
            		
        }
        
        
        
        /// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Model.SYS_Role_User model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update SYS_Role_User set ");
			                                                
            strSql.Append(" UserID = @UserID , ");                                    
            strSql.Append(" RoleID = @RoleID  ");            			
			strSql.Append(" where ID=@ID ");
						
SqlParameter[] parameters = {
			            new SqlParameter("@ID", SqlDbType.Int,4) ,            
                        new SqlParameter("@UserID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@RoleID", SqlDbType.Int,4)             
              
            };
						            
            parameters[0].Value = model.ID;                        
            parameters[1].Value = model.UserID;                        
            parameters[2].Value = model.RoleID;                        
            int rows=DBHelper.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
        #endregion
        
        
        #region 补充方法
		#endregion
    }
}