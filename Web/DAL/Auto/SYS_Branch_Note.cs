﻿
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Common.DBUtility;

namespace DAL
{
    /// <summary>
    /// DAL层,网店通知关系
    /// </summary>
    public partial class SYS_Branch_Note : DalBase
    {
        
        #region  基础方法
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public long Add(Model.SYS_Branch_Note model)
        {
           StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into SYS_Branch_Note(");			
            strSql.Append("BranchID,NoteID,ReadCount");
			strSql.Append(") values (");
            strSql.Append("@BranchID,@NoteID,@ReadCount");            
            strSql.Append(") ");            
            strSql.Append(";select @@IDENTITY");		
			SqlParameter[] parameters = {
			            new SqlParameter("@BranchID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@NoteID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@ReadCount", SqlDbType.Int,4)             
              
            };
            			            
            parameters[0].Value = model.BranchID;                        
            parameters[1].Value = model.NoteID;                        
            parameters[2].Value = model.ReadCount;                        
			   
			object obj = DBHelper.GetSingle(strSql.ToString(),parameters);			
			if (obj == null)
			{
				return 0;
			}
			else
			{
				                                    
            	return Convert.ToInt64(obj);
                                                  
			}			   
            		
        }
        
        
        
        /// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Model.SYS_Branch_Note model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update SYS_Branch_Note set ");
			                                                
            strSql.Append(" BranchID = @BranchID , ");                                    
            strSql.Append(" NoteID = @NoteID , ");                                    
            strSql.Append(" ReadCount = @ReadCount  ");            			
			strSql.Append(" where ID=@ID ");
						
SqlParameter[] parameters = {
			            new SqlParameter("@ID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@BranchID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@NoteID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@ReadCount", SqlDbType.Int,4)             
              
            };
						            
            parameters[0].Value = model.ID;                        
            parameters[1].Value = model.BranchID;                        
            parameters[2].Value = model.NoteID;                        
            parameters[3].Value = model.ReadCount;                        
            int rows=DBHelper.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
        #endregion
        
        
        #region 补充方法
		#endregion
    }
}