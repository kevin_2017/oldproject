﻿
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using CE.Utility.Text;
using Common.DBUtility;
using Model;
namespace DAL
{
    /// <summary>
    /// DAL层,代理充值供应商
    /// </summary>
    public partial class MS_Branch_RechargeSupp : DalBase
    {
        
        #region  基础方法
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public long Add(Model.MS_Branch_RechargeSupp model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into MS_Branch_RechargeSupp(");
            strSql.Append("BranchId,SupplierId");
            strSql.Append(") values (");
            strSql.Append("@BranchId,@SupplierId");
            strSql.Append(") ");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
                        new SqlParameter("@BranchId", SqlDbType.BigInt,8)  ,
                        new SqlParameter("@SupplierId", SqlDbType.BigInt,8)  
            };

            parameters[0].Value = model.BranchId;
            parameters[1].Value = model.SupplierId; 
            object obj = DBHelper.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {

                return Convert.ToInt64(obj);

            }

        }
        

        
        #endregion


        #region 补充方法
       
        #endregion
    }
}