﻿using Common.DBUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DAL
{ /// <summary>
  /// DAL层,供应商可充区域管理
  /// </summary>
    public partial class V8_Supplier_RechargeArea : DalBase
    {

        #region  基础方法 

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public long Add(Model.V8_Supplier_RechargeArea model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into V8_Supplier_RechargeArea(");
            strSql.Append("SupplierId,SupplierName,ISPType,ISPName,ProvinceId,ProvinceName,CityId,CityName");
            strSql.Append(") values (");
            strSql.Append("@SupplierId,@SupplierName,@ISPType,@ISPName,@ProvinceId,@ProvinceName,@CityId,@CityName");
            strSql.Append(") ");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
                        new SqlParameter("@SupplierId", SqlDbType.BigInt,8) ,
                        new SqlParameter("@SupplierName", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@ISPType", SqlDbType.Int,4) ,
                        new SqlParameter("@ISPName", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@ProvinceId", SqlDbType.Int,4) ,
                        new SqlParameter("@ProvinceName", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@CityId", SqlDbType.Int,4) ,
                        new SqlParameter("@CityName", SqlDbType.NVarChar,50)  

            };

            parameters[0].Value = model.SupplierId;
            parameters[1].Value = model.SupplierName;
            parameters[2].Value = model.IspType;
            parameters[3].Value = model.IspName;
            parameters[4].Value = model.ProvinceId;
            parameters[5].Value = model.ProvinceName;
            parameters[6].Value = model.CityId;
            parameters[7].Value = model.CityName; 

            object obj = DBHelper.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {

                return Convert.ToInt64(obj);

            }

        }


        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Model.V8_Supplier_RechargeArea model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update V8_Supplier_RechargeArea set ");
            strSql.Append(" SupplierId = @SupplierId , ");
            strSql.Append(" SupplierName = @SupplierName , ");
            strSql.Append(" ISPType = @ISPType , ");
            strSql.Append(" ISPName = @ISPName , ");
            strSql.Append(" ProvinceId = @ProvinceId , ");
            strSql.Append(" ProvinceName = @ProvinceName , ");
            strSql.Append(" CityId = @CityId , ");
            strSql.Append(" CityName = @CityName  "); 
            strSql.Append(" where ID=@ID ");

            SqlParameter[] parameters = {
                        new SqlParameter("@ID", SqlDbType.BigInt,8) ,
                new SqlParameter("@SupplierId", SqlDbType.BigInt,8) ,
                new SqlParameter("@SupplierName", SqlDbType.NVarChar,50) ,
                new SqlParameter("@ISPType", SqlDbType.Int,4) ,
                new SqlParameter("@ISPName", SqlDbType.NVarChar,50) ,
                new SqlParameter("@ProvinceId", SqlDbType.Int,4) ,
                new SqlParameter("@ProvinceName", SqlDbType.Int,4) ,
                new SqlParameter("@CityId", SqlDbType.Int,4) ,
                new SqlParameter("@CityName", SqlDbType.NVarChar,50)

            };

            parameters[0].Value = model.Id;
            parameters[1].Value = model.SupplierId;
            parameters[2].Value = model.SupplierName;
            parameters[3].Value = model.IspType;
            parameters[4].Value = model.IspName;
            parameters[5].Value = model.ProvinceId;
            parameters[6].Value = model.ProvinceName;
            parameters[7].Value = model.CityId;
            parameters[8].Value = model.CityName;
            
            int rows = DBHelper.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion


        #region 补充方法

        #endregion
    }
}