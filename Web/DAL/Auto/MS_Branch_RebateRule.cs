﻿using System;
using System.Text;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Data;
using Common.DBUtility;
namespace DAL
{
    /// <summary>
    /// 代理返利规则
    /// </summary>
    public partial class MS_Branch_RebateRule : DalBase
    {
        #region  基础方法

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public long Add(Model.MS_Branch_RebateRule model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into MS_Branch_RebateRule(");
            strSql.Append("BranchId,BranchName,ProductTypeId,ProductTypeName,IspType,IspName,RebateMethod,Rebate,MinMoney,MaxMoney,Sort,Is_Delete,CreateTime,Creator,ProvinceIds");
            strSql.Append(") values (");
            strSql.Append("@BranchId,@BranchName,@ProductTypeId,@ProductTypeName,@IspType,@IspName,@RebateMethod,@Rebate,@MinMoney,@MaxMoney,@Sort,@Is_Delete,@CreateTime,@Creator,@ProvinceIds");
            strSql.Append(") ");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
                        new SqlParameter("@BranchId", SqlDbType.BigInt,8) ,
                        new SqlParameter("@BranchName", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@ProductTypeId", SqlDbType.BigInt,8) ,
                        new SqlParameter("@ProductTypeName", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@IspType", SqlDbType.Int,4) ,
                        new SqlParameter("@IspName", SqlDbType.NVarChar,50),
                        new SqlParameter("@RebateMethod", SqlDbType.Int,4) ,
                        new SqlParameter("@Rebate", SqlDbType.Decimal, 9) ,
                        new SqlParameter("@MinMoney", SqlDbType.Decimal, 9) ,
                        new SqlParameter("@MaxMoney", SqlDbType.Decimal, 9) ,
                        new SqlParameter("@Sort", SqlDbType.BigInt,8) ,
                        new SqlParameter("@Is_Delete", SqlDbType.Int,4) ,
                        new SqlParameter("@CreateTime", SqlDbType.DateTime) ,
                        new SqlParameter("@Creator", SqlDbType.NVarChar,50),
                        new SqlParameter("@ProvinceIds", SqlDbType.NVarChar,4000)

            };

            parameters[0].Value = model.BranchID;
            parameters[1].Value = model.BranchName;
            parameters[2].Value = model.ProductTypeId;
            parameters[3].Value = model.ProductTypeName;
            parameters[4].Value = model.IspType;
            parameters[5].Value = model.IspName;
            parameters[6].Value = model.RebateMethod;
            parameters[7].Value = model.Rebate;
            parameters[8].Value = model.MinMoney;
            parameters[9].Value = model.MaxMoney;
            parameters[10].Value = model.Sort;
            parameters[11].Value = model.IsDelete;
            parameters[12].Value = DateTime.Now;
            parameters[13].Value = model.Creator;
            parameters[14].Value = model.ProvinceIds;

            object obj = DBHelper.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {

                return Convert.ToInt64(obj);

            }

        }


        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Model.MS_Branch_RebateRule model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update MS_Branch_RebateRule set ");
            strSql.Append(" BranchId = @BranchId , ");
            strSql.Append(" BranchName = @BranchName , ");
            strSql.Append(" ProductTypeId = @ProductTypeId , ");
            strSql.Append(" ProductTypeName = @ProductTypeName , ");
            strSql.Append(" IspType = @IspType , ");
            strSql.Append(" IspName = @IspName , ");
            strSql.Append(" RebateMethod = @RebateMethod , ");
            strSql.Append(" Rebate = @Rebate , ");
            strSql.Append(" MinMoney = @MinMoney , ");
            strSql.Append(" MaxMoney = @MaxMoney , ");
            strSql.Append(" Sort = @Sort , ");
            strSql.Append(" Is_Delete = @Is_Delete , ");
            strSql.Append(" UpdateTime = @UpdateTime , ");
            strSql.Append(" Updator = @Updator,  ");
            strSql.Append(" ProvinceIds = @ProvinceIds  ");
            strSql.Append(" where ID=@ID ");

            SqlParameter[] parameters = {
                        new SqlParameter("@ID", SqlDbType.BigInt,8) ,
                new SqlParameter("@BranchId", SqlDbType.BigInt,8) ,
                new SqlParameter("@BranchName", SqlDbType.NVarChar,50) ,
                new SqlParameter("@ProductTypeId", SqlDbType.BigInt,8) ,
                new SqlParameter("@ProductTypeName", SqlDbType.NVarChar,50) ,
                new SqlParameter("@IspType", SqlDbType.Int,4) ,
                new SqlParameter("@IspName", SqlDbType.NVarChar,50),
                new SqlParameter("@RebateMethod", SqlDbType.Int,4) ,
                new SqlParameter("@Rebate", SqlDbType.Decimal, 9) ,
                new SqlParameter("@MinMoney", SqlDbType.Decimal, 9) ,
                new SqlParameter("@MaxMoney", SqlDbType.Decimal, 9) ,
                new SqlParameter("@Sort", SqlDbType.BigInt,8) ,
                new SqlParameter("@Is_Delete", SqlDbType.Int,4) ,
                new SqlParameter("@UpdateTime", SqlDbType.DateTime) ,
                new SqlParameter("@Updator", SqlDbType.NVarChar,50) ,
                new SqlParameter("@ProvinceIds", SqlDbType.NVarChar,4000)

            };

            parameters[0].Value = model.ID;
            parameters[1].Value = model.BranchID;
            parameters[2].Value = model.BranchName;
            parameters[3].Value = model.ProductTypeId;
            parameters[4].Value = model.ProductTypeName;
            parameters[5].Value = model.IspType;
            parameters[6].Value = model.IspName;
            parameters[7].Value = model.RebateMethod;
            parameters[8].Value = model.Rebate;
            parameters[9].Value = model.MinMoney;
            parameters[10].Value = model.MaxMoney;
            parameters[11].Value = model.Sort;
            parameters[12].Value = model.IsDelete;
            parameters[13].Value =  DateTime.Now;
            parameters[14].Value = model.Updator;
            parameters[15].Value = model.ProvinceIds;


            int rows = DBHelper.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region 补充方法
        #endregion


    }
}

