﻿
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Common.DBUtility;

namespace DAL
{
    /// <summary>
    /// DAL层,系统角色管理
    /// </summary>
    public partial class SYS_Role : DalBase
    {
        
        #region  基础方法
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public long Add(Model.SYS_Role model)
        {
           StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into SYS_Role(");			
            strSql.Append("RoleName,Remark,ISSystem");
			strSql.Append(") values (");
            strSql.Append("@RoleName,@Remark,@ISSystem");            
            strSql.Append(") ");            
            strSql.Append(";select @@IDENTITY");		
			SqlParameter[] parameters = {
			            new SqlParameter("@RoleName", SqlDbType.NVarChar,50) ,            
                        new SqlParameter("@Remark", SqlDbType.NVarChar,50) ,            
                        new SqlParameter("@ISSystem", SqlDbType.Bit,1)             
              
            };
            			            
            parameters[0].Value = model.RoleName;                        
            parameters[1].Value = model.Remark;                        
            parameters[2].Value = model.ISSystem;                        
			   
			object obj = DBHelper.GetSingle(strSql.ToString(),parameters);			
			if (obj == null)
			{
				return 0;
			}
			else
			{
				                    
            	return Convert.ToInt32(obj);
                                                                  
			}			   
            		
        }
        
        
        
        /// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Model.SYS_Role model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update SYS_Role set ");
			                                                
            strSql.Append(" RoleName = @RoleName , ");                                    
            strSql.Append(" Remark = @Remark , ");                                    
            strSql.Append(" ISSystem = @ISSystem  ");            			
			strSql.Append(" where ID=@ID ");
						
SqlParameter[] parameters = {
			            new SqlParameter("@ID", SqlDbType.Int,4) ,            
                        new SqlParameter("@RoleName", SqlDbType.NVarChar,50) ,            
                        new SqlParameter("@Remark", SqlDbType.NVarChar,50) ,            
                        new SqlParameter("@ISSystem", SqlDbType.Bit,1)             
              
            };
						            
            parameters[0].Value = model.ID;                        
            parameters[1].Value = model.RoleName;                        
            parameters[2].Value = model.Remark;                        
            parameters[3].Value = model.ISSystem;                        
            int rows=DBHelper.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
        #endregion
        
        
        #region 补充方法
		#endregion
    }
}