﻿using Common.DBUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DAL
{
    public class Reportform
    {
        

        #region 订单分析-平均时间
        public DataTable GetfrmTimetable(string strWhere)
        {
            SqlParameter tablename = new SqlParameter("@strWhere", SqlDbType.NVarChar, 4000);
            tablename.Value = strWhere;
            List<SqlParameter> list = new List<SqlParameter>();
            list.Add(tablename);

            DataTable table = DBHelper.ExecuteProcedureOutputTable("Proc_GetReportForAvgTime", list);

            return table;
        }
        #endregion
        #region 订单分析-运营地区
        public DataTable GetfrmAreatable(string strWhere)
        {
            SqlParameter tablename = new SqlParameter("@strWhere", SqlDbType.NVarChar, 4000);
            tablename.Value = strWhere;
            List<SqlParameter> list = new List<SqlParameter>();
            list.Add(tablename);

            DataTable table = DBHelper.ExecuteProcedureOutputTable("Proc_GetReportForProvince", list);

            return table;
        }
        #endregion

        #region 订单分析-商品
        public DataTable GetfrmProducttable(string strWhere)
        {
            SqlParameter tablename = new SqlParameter("@strWhere", SqlDbType.NVarChar, 4000);
            tablename.Value = strWhere;
            List<SqlParameter> list = new List<SqlParameter>();
            list.Add(tablename);

            DataTable table = DBHelper.ExecuteProcedureOutputTable("Proc_GetReportForProduct", list);

            return table;
        }
        #endregion

        #region 订单分析-供应商
        public DataTable GetfrmSuppliertable(string strWhere)
        {


            SqlParameter tablename = new SqlParameter("@strWhere", SqlDbType.NVarChar, 4000);
            tablename.Value = strWhere;
            List<SqlParameter> list = new List<SqlParameter>();
            list.Add(tablename);

            DataTable table = DBHelper.ExecuteProcedureOutputTable("Proc_GetReportForSupplier", list);

            return table;
        }
        public DataTable Proc_GetReportForSupplierTotal(string strWhere)
        {


            SqlParameter tablename = new SqlParameter("@strWhere", SqlDbType.NVarChar, 4000);
            tablename.Value = strWhere;
            List<SqlParameter> list = new List<SqlParameter>();
            list.Add(tablename);

            DataTable table = DBHelper.ExecuteProcedureOutputTable("Proc_GetReportForSupplierTotal", list);

            return table;
        }
        #endregion


        #region 订单分析-运营商
        public DataTable GetReprotForIspType(string strWhere)
        {


            SqlParameter tablename = new SqlParameter("@strWhere", SqlDbType.NVarChar, 4000);
            tablename.Value = strWhere;
            List<SqlParameter> list = new List<SqlParameter>();
            list.Add(tablename);

            DataTable table = DBHelper.ExecuteProcedureOutputTable("Proc_GetReportForISPType", list);

            return table;
        }
        #endregion

        #region 订单分析-商品地区
        public DataTable GetReprotForProductProvinceName(string strWhere)
        {


            SqlParameter tablename = new SqlParameter("@strWhere", SqlDbType.NVarChar, 4000);
            tablename.Value = strWhere;
            List<SqlParameter> list = new List<SqlParameter>();
            list.Add(tablename);

            DataTable table = DBHelper.ExecuteProcedureOutputTable("Proc_GetReportForProductProvinceName", list);

            return table;
        }
        #endregion
        #region 订单分析-商品地区
        public DataTable GetReprotForBranch(string strWhere)
        {


            SqlParameter tablename = new SqlParameter("@strWhere", SqlDbType.NVarChar, 4000);
            tablename.Value = strWhere;
            List<SqlParameter> list = new List<SqlParameter>();
            list.Add(tablename);

            DataTable table = DBHelper.ExecuteProcedureOutputTable("Proc_GetReportForBranch", list);

            return table;
        }
        #endregion

        #region 订单分析--时间段

        public DataSet GetReportForHour(string strWhere)
        {
            StringBuilder str = new StringBuilder();
            str.Append(" select  CreateTime,TotalCounts,SuccessCounts,SuccessInPrice,SuccessSalePrice,FailCounts,RechargingCounts from VW_ReportForHour");
            if (strWhere != "")
            {
                str.Append(" where ");
                str.Append(strWhere);
            }

            return DBHelper.Query(str.ToString());


        }



        #endregion
    }
}
