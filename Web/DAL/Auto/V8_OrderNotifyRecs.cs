﻿using Common.DBUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    //V8_OrderNotifyRecs
    public partial class V8_OrderNotifyRecs : DalBase
    {



        #region  基础方法

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public long Add(Model.V8_OrderNotifyRecs model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into V8_OrderNotifyRecs(");
            strSql.Append("OrderNo,BranchId,NotifyTime,NotifyResult,NotifyConent,NotifyResponse");
            strSql.Append(") values (");
            strSql.Append("@OrderNo,@BranchId,@NotifyTime,@NotifyResult,@NotifyConent,@NotifyResponse");
            strSql.Append(") ");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
                        new SqlParameter("@OrderNo", SqlDbType.NVarChar,20) ,
                        new SqlParameter("@BranchId", SqlDbType.BigInt,8) ,
                        new SqlParameter("@NotifyTime", SqlDbType.DateTime) ,
                        new SqlParameter("@NotifyResult", SqlDbType.Int,4) ,
                        new SqlParameter("@NotifyConent", SqlDbType.NVarChar,4000) ,
                        new SqlParameter("@NotifyResponse", SqlDbType.NVarChar,4000)

            };

            parameters[0].Value = model.OrderNo;
            parameters[1].Value = model.BranchId;
            parameters[2].Value = model.NotifyTime;
            parameters[3].Value = model.NotifyResult;
            parameters[4].Value = model.NotifyConent;
            parameters[5].Value = model.NotifyResponse;

            object obj = DBHelper.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {

                return Convert.ToInt64(obj);

            }

        }


        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Model.V8_OrderNotifyRecs model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update V8_OrderNotifyRecs set ");

            strSql.Append(" OrderNo = @OrderNo , ");
            strSql.Append(" BranchId = @BranchId , ");
            strSql.Append(" NotifyTime = @NotifyTime , ");
            strSql.Append(" NotifyResult = @NotifyResult , ");
            strSql.Append(" NotifyConent = @NotifyConent , ");
            strSql.Append(" NotifyResponse = @NotifyResponse  ");
            strSql.Append(" where ID=@ID ");

            SqlParameter[] parameters = {
                        new SqlParameter("@ID", SqlDbType.BigInt,8) ,
                        new SqlParameter("@OrderNo", SqlDbType.NVarChar,20) ,
                        new SqlParameter("@BranchId", SqlDbType.BigInt,8) ,
                        new SqlParameter("@NotifyTime", SqlDbType.DateTime) ,
                        new SqlParameter("@NotifyResult", SqlDbType.Int,4) ,
                        new SqlParameter("@NotifyConent", SqlDbType.NVarChar,4000) ,
                        new SqlParameter("@NotifyResponse", SqlDbType.NVarChar,4000)

            };

            parameters[0].Value = model.ID;
            parameters[1].Value = model.OrderNo;
            parameters[2].Value = model.BranchId;
            parameters[3].Value = model.NotifyTime;
            parameters[4].Value = model.NotifyResult;
            parameters[5].Value = model.NotifyConent;
            parameters[6].Value = model.NotifyResponse;
            int rows = DBHelper.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region 补充方法
        #endregion


    }
}

