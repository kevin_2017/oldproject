﻿
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Common.DBUtility;

namespace DAL
{
    /// <summary>
    /// DAL层,通知管理
    /// </summary>
    public partial class SYS_NoteManage : DalBase
    {
        
        #region  基础方法
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public long Add(Model.SYS_NoteManage model)
        {
           StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into SYS_NoteManage(");			
            strSql.Append("Title,Contents,CreateTime,InvalidTime,CreateUserID");
			strSql.Append(") values (");
            strSql.Append("@Title,@Contents,@CreateTime,@InvalidTime,@CreateUserID");            
            strSql.Append(") ");            
            strSql.Append(";select @@IDENTITY");		
			SqlParameter[] parameters = {
			            new SqlParameter("@Title", SqlDbType.NVarChar,100) ,            
                        new SqlParameter("@Contents", SqlDbType.Text) ,            
                        new SqlParameter("@CreateTime", SqlDbType.DateTime) ,            
                        new SqlParameter("@InvalidTime", SqlDbType.DateTime) ,            
                        new SqlParameter("@CreateUserID", SqlDbType.BigInt,8)             
              
            };
            			            
            parameters[0].Value = model.Title;                        
            parameters[1].Value = model.Contents;                        
            parameters[2].Value = model.CreateTime;                        
            parameters[3].Value = model.InvalidTime;                        
            parameters[4].Value = model.CreateUserID;                        
			   
			object obj = DBHelper.GetSingle(strSql.ToString(),parameters);			
			if (obj == null)
			{
				return 0;
			}
			else
			{
				                                    
            	return Convert.ToInt64(obj);
                                                  
			}			   
            		
        }
        
        
        
        /// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Model.SYS_NoteManage model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update SYS_NoteManage set ");
			                                                
            strSql.Append(" Title = @Title , ");                                    
            strSql.Append(" Contents = @Contents , ");                                    
            strSql.Append(" CreateTime = @CreateTime , ");                                    
            strSql.Append(" InvalidTime = @InvalidTime , ");                                    
            strSql.Append(" CreateUserID = @CreateUserID  ");            			
			strSql.Append(" where ID=@ID ");
						
SqlParameter[] parameters = {
			            new SqlParameter("@ID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@Title", SqlDbType.NVarChar,100) ,            
                        new SqlParameter("@Contents", SqlDbType.Text) ,            
                        new SqlParameter("@CreateTime", SqlDbType.DateTime) ,            
                        new SqlParameter("@InvalidTime", SqlDbType.DateTime) ,            
                        new SqlParameter("@CreateUserID", SqlDbType.BigInt,8)             
              
            };
						            
            parameters[0].Value = model.ID;                        
            parameters[1].Value = model.Title;                        
            parameters[2].Value = model.Contents;                        
            parameters[3].Value = model.CreateTime;                        
            parameters[4].Value = model.InvalidTime;                        
            parameters[5].Value = model.CreateUserID;                        
            int rows=DBHelper.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
        #endregion
        
        
        #region 补充方法
		#endregion
    }
}