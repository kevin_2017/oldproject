﻿
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Common.DBUtility;
using System.Collections.Generic;
namespace DAL
{
    /// <summary>
    /// DAL层,系统权限按钮管理
    /// </summary>
    public partial class SYS_AuthButton : DalBase
    {
        
        #region  基础方法
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public long Add(Model.SYS_AuthButton model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into SYS_AuthButton(");
            strSql.Append("AuthID,ButtonName,DisplayName,IsEnable");
            strSql.Append(") values (");
            strSql.Append("@AuthID,@ButtonName,@DisplayName,@IsEnable");
            strSql.Append(") ");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
			            new SqlParameter("@AuthID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@ButtonName", SqlDbType.NVarChar,50) ,            
                        new SqlParameter("@DisplayName", SqlDbType.NVarChar,50) ,            
                        new SqlParameter("@IsEnable", SqlDbType.Bit,1)             
              
            };

            parameters[0].Value = model.AuthID;
            parameters[1].Value = model.ButtonName;
            parameters[2].Value = model.DisplayName;
            parameters[3].Value = model.IsEnable;

            object obj = DBHelper.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {

                return Convert.ToInt64(obj);

            }

        }



        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Model.SYS_AuthButton model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update SYS_AuthButton set ");

            strSql.Append(" AuthID = @AuthID , ");
            strSql.Append(" ButtonName = @ButtonName , ");
            strSql.Append(" DisplayName = @DisplayName , ");
            strSql.Append(" IsEnable = @IsEnable  ");
            strSql.Append(" where ID=@ID ");

            SqlParameter[] parameters = {
			            new SqlParameter("@ID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@AuthID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@ButtonName", SqlDbType.NVarChar,50) ,            
                        new SqlParameter("@DisplayName", SqlDbType.NVarChar,50) ,            
                        new SqlParameter("@IsEnable", SqlDbType.Bit,1)             
              
            };

            parameters[0].Value = model.ID;
            parameters[1].Value = model.AuthID;
            parameters[2].Value = model.ButtonName;
            parameters[3].Value = model.DisplayName;
            parameters[4].Value = model.IsEnable;
            int rows = DBHelper.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion


        #region 补充方法

        /// <summary>
        /// 根据用户ID及当前页面的URL得到相应的权限按钮
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="uRL"></param>
        /// <returns></returns>
        public List<Model.GetSysUserAuthButton> GetModelList(long userID, string uRL)
        {

            DataSet ds = new DataSet();
            StringBuilder strSQL = new StringBuilder();
            strSQL.Append(@"SELECT   [AuthButtonID],[ButtonName],[DisplayName],[UserID],[Url] FROM  [vw_GetSysUserAuthButton]");
            strSQL.Append(" where userID=" + userID);
            strSQL.Append(@" and url='" + uRL + "'");
            ds = DBHelper.Query(strSQL.ToString());
            List<Model.GetSysUserAuthButton> list = new List<Model.GetSysUserAuthButton>();
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    Model.GetSysUserAuthButton model = new Model.GetSysUserAuthButton();
                    model.AuthButtonID = long.Parse(dr["AuthButtonID"].ToString());
                    model.ButtonName = dr["ButtonName"].ToString();
                    model.DisplayName = dr["DisplayName"].ToString();
                    model.Url = dr["Url"].ToString();
                    model.UserID = int.Parse(dr["UserID"].ToString());
                    list.Add(model);

                }
            }
            return list;

        }

        #endregion
    }
}