﻿
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Common.DBUtility;

namespace DAL
{
    /// <summary>
    /// DAL层,系统权限详情
    /// </summary>
    public partial class SYS_AuthorityInfo : DalBase
    {
        
        #region  基础方法
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public long Add(Model.SYS_AuthorityInfo model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into SYS_AuthorityInfo(");
            strSql.Append("TypeName,Url,ISEnable,ParentID,Sort,Remarks,LogImg");
            strSql.Append(") values (");
            strSql.Append("@TypeName,@Url,@ISEnable,@ParentID,@Sort,@Remarks,@LogImg");
            strSql.Append(") ");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
			            new SqlParameter("@TypeName", SqlDbType.NVarChar,20) ,            
                        new SqlParameter("@Url", SqlDbType.NVarChar,50) ,            
                        new SqlParameter("@ISEnable", SqlDbType.Bit,1) ,            
                        new SqlParameter("@ParentID", SqlDbType.Int,4) ,            
                        new SqlParameter("@Sort", SqlDbType.Int,4) ,            
                        new SqlParameter("@Remarks", SqlDbType.NVarChar,50) ,            
                        new SqlParameter("@LogImg", SqlDbType.NVarChar,50)             
              
            };

            parameters[0].Value = model.TypeName;
            parameters[1].Value = model.Url;
            parameters[2].Value = model.ISEnable;
            parameters[3].Value = model.ParentID;
            parameters[4].Value = model.Sort;
            parameters[5].Value = model.Remarks;
            parameters[6].Value = model.LogImg;

            object obj = DBHelper.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {

                return Convert.ToInt64(obj);

            }

        }



        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Model.SYS_AuthorityInfo model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update SYS_AuthorityInfo set ");

            strSql.Append(" TypeName = @TypeName , ");
            strSql.Append(" Url = @Url , ");
            strSql.Append(" ISEnable = @ISEnable , ");
            strSql.Append(" ParentID = @ParentID , ");
            strSql.Append(" Sort = @Sort , ");
            strSql.Append(" Remarks = @Remarks , ");
            strSql.Append(" LogImg = @LogImg  ");
            strSql.Append(" where ID=@ID ");

            SqlParameter[] parameters = {
			            new SqlParameter("@ID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@TypeName", SqlDbType.NVarChar,20) ,            
                        new SqlParameter("@Url", SqlDbType.NVarChar,50) ,            
                        new SqlParameter("@ISEnable", SqlDbType.Bit,1) ,            
                        new SqlParameter("@ParentID", SqlDbType.Int,4) ,            
                        new SqlParameter("@Sort", SqlDbType.Int,4) ,            
                        new SqlParameter("@Remarks", SqlDbType.NVarChar,50) ,            
                        new SqlParameter("@LogImg", SqlDbType.NVarChar,50)             
              
            };

            parameters[0].Value = model.ID;
            parameters[1].Value = model.TypeName;
            parameters[2].Value = model.Url;
            parameters[3].Value = model.ISEnable;
            parameters[4].Value = model.ParentID;
            parameters[5].Value = model.Sort;
            parameters[6].Value = model.Remarks;
            parameters[7].Value = model.LogImg;
            int rows = DBHelper.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion


        #region 补充方法


        /// <summary>
        /// 根据角色ID获取相应的权限
        /// </summary>
        /// <param name="roleID"></param>
        /// <returns></returns>
        public DataSet GetAuthorityinfoByRoleID(long roleID)
        {
            StringBuilder strSQL = new StringBuilder();

            strSQL.Append(@"select SYS_AuthorityInfo.* from SYS_Role_Authority inner join SYS_AuthorityInfo on SYS_AuthorityInfo.ID= SYS_Role_Authority.AuthorityID and SYS_AuthorityInfo.IsDelete=0");


            strSQL.Append(" where  isenable='1' and  RoleID=" + roleID +" order by sort asc");



            return DBHelper.Query(strSQL.ToString());
        }

        #endregion
    }
}