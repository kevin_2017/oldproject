﻿using Common.DBUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DAL
{/// <summary>
 /// 供应商配置管理
 /// </summary>
    public partial class SUP_SupplierConfig : DalBase
    {
        
        #region  基础方法
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public long Add(Model.SUP_SupplierConfig model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into SUP_SupplierConfig(");
            strSql.Append("SupplierID,ConfigName,ConfigValue,Remark");
            strSql.Append(") values (");
            strSql.Append("@SupplierID,@ConfigName,@ConfigValue,@Remark");
            strSql.Append(") ");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
                        new SqlParameter("@SupplierID", SqlDbType.BigInt,8) ,
                        new SqlParameter("@ConfigName", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@ConfigValue", SqlDbType.NVarChar,1000) ,
                        new SqlParameter("@Remark", SqlDbType.NVarChar,1000)

            };

            parameters[0].Value = model.SupplierID;
            parameters[1].Value = model.ConfigName;
            parameters[2].Value = model.ConfigValue;
            parameters[3].Value = model.Remark;

            object obj = DBHelper.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {

                return Convert.ToInt64(obj);

            }

        }



        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Model.SUP_SupplierConfig model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update SUP_SupplierConfig set ");

            strSql.Append(" SupplierID = @SupplierID , ");
            strSql.Append(" ConfigName = @ConfigName , ");
            strSql.Append(" ConfigValue = @ConfigValue , ");
            strSql.Append(" Remark = @Remark  ");
            strSql.Append(" where ID=@ID ");

            SqlParameter[] parameters = {
                        new SqlParameter("@ID", SqlDbType.BigInt,8) ,
                        new SqlParameter("@SupplierID", SqlDbType.BigInt,8) ,
                        new SqlParameter("@ConfigName", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@ConfigValue", SqlDbType.NVarChar,1000) ,
                        new SqlParameter("@Remark", SqlDbType.NVarChar,1000)

            };

            parameters[0].Value = model.ID;
            parameters[1].Value = model.SupplierID;
            parameters[2].Value = model.ConfigName;
            parameters[3].Value = model.ConfigValue;
            parameters[4].Value = model.Remark;
            int rows = DBHelper.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion


        #region 补充方法
        public bool SaveSupplierConfig(List<Model.SUP_SupplierConfig> list)
        {
            long rows = 0;
            if (list.Count > 0)
            {
                foreach (var item in list)
                {
                    Model.StrWhere str = new Model.StrWhere();
                    str.IsWhereExist = true;
                    str.strWhere.Append(" SupplierID=@SupplierID and ConfigName=@ConfigName");
                    str.parameters = new SqlParameter[]{
                        new SqlParameter("@SupplierID", SqlDbType.BigInt,8),
                        new SqlParameter("@ConfigName", SqlDbType.NVarChar,50)
                    };
                    str.parameters[0].Value = item.SupplierID;
                    str.parameters[1].Value = item.ConfigName;
                    var model = GetModel<Model.SUP_SupplierConfig>("SUP_SupplierConfig", str);
                    if (model == null)
                    {
                        rows += Add(item);
                    }
                    else
                    {
                        model.ConfigValue = item.ConfigValue;
                        if (Update(model))
                            rows++;
                    }
                }
                if (rows > 0)
                    return true;
                else
                    return false;
            }
            else
            { return false; }
        }
        #endregion
    }
}