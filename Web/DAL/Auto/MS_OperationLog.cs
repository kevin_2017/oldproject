﻿
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Common.DBUtility;
using Model;
namespace DAL
{
    /// <summary>
    /// DAL层,MS_OperationLog
    /// </summary>
    public partial class MS_OperationLog : DalBase
    {
        
        #region  基础方法
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public long Add(Model.MS_OperationLog model)
        {
           StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into MS_OperationLog(");			
            strSql.Append("BranchID,UserID,OperationTime,OperationIP,Info");
			strSql.Append(") values (");
            strSql.Append("@BranchID,@UserID,@OperationTime,@OperationIP,@Info");            
            strSql.Append(") ");            
            strSql.Append(";select @@IDENTITY");		
			SqlParameter[] parameters = {
			            new SqlParameter("@BranchID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@UserID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@OperationTime", SqlDbType.DateTime) ,            
                        new SqlParameter("@OperationIP", SqlDbType.NVarChar,20) ,            
                        new SqlParameter("@Info", SqlDbType.Text)             
              
            };
            			            
            parameters[0].Value = model.BranchID;                        
            parameters[1].Value = model.UserID;                        
            parameters[2].Value = model.OperationTime;                        
            parameters[3].Value = model.OperationIP;                        
            parameters[4].Value = model.Info;                        
			   
			object obj = DBHelper.GetSingle(strSql.ToString(),parameters);			
			if (obj == null)
			{
				return 0;
			}
			else
			{
				                                    
            	return Convert.ToInt64(obj);
                                                  
			}			   
            		
        }
        
        
        
        /// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Model.MS_OperationLog model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update MS_OperationLog set ");
			                                                
            strSql.Append(" BranchID = @BranchID , ");                                    
            strSql.Append(" UserID = @UserID , ");                                    
            strSql.Append(" OperationTime = @OperationTime , ");                                    
            strSql.Append(" OperationIP = @OperationIP , ");                                    
            strSql.Append(" Info = @Info  ");            			
			strSql.Append(" where ID=@ID ");
						
SqlParameter[] parameters = {
			            new SqlParameter("@ID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@BranchID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@UserID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@OperationTime", SqlDbType.DateTime) ,            
                        new SqlParameter("@OperationIP", SqlDbType.NVarChar,20) ,            
                        new SqlParameter("@Info", SqlDbType.Text)             
              
            };
						            
            parameters[0].Value = model.ID;                        
            parameters[1].Value = model.BranchID;                        
            parameters[2].Value = model.UserID;                        
            parameters[3].Value = model.OperationTime;                        
            parameters[4].Value = model.OperationIP;                        
            parameters[5].Value = model.Info;                        
            int rows=DBHelper.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
        #endregion
        
        
        #region 补充方法
		#endregion
    }
}