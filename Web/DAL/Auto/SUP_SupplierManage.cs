﻿
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Common.DBUtility;

namespace DAL
{
    /// <summary>
    /// DAL层,供应商管理
    /// </summary>
    public partial class SUP_SupplierManage : DalBase
    {
        //
        #region  基础方法
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public long Add(Model.SUP_SupplierManage model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into SUP_SupplierManage(");
            strSql.Append("ID,UserName,PassWord,TrueName,Status,UserType,RechargeNum,NotRechargeNum,Balance,Contactor,ContactPhone,Address,IsCloseProduct,GateWay,RechargePayDomain,RechargeSearchUrl,RechargeNoticeUrl,FlowPayDomain,FlowSearchUrl,FlowNoticeUrl,IsDelete,RechargeCount,FlowCount,Sort,ApiCode,TelPhonePayDomain)");
            strSql.Append(" values (");
            strSql.Append("@ID,@UserName,@PassWord,@TrueName,@Status,@UserType,@RechargeNum,@NotRechargeNum,@Balance,@Contactor,@ContactPhone,@Address,@IsCloseProduct,@GateWay,@RechargePayDomain,@RechargeSearchUrl,@RechargeNoticeUrl,@FlowPayDomain,@FlowSearchUrl,@FlowNoticeUrl,@IsDelete,@RechargeCount,@FlowCount,@Sort,@ApiCode,@TelPhonePayDomain)");
            SqlParameter[] parameters = {
                    new SqlParameter("@ID", SqlDbType.BigInt,8),
                    new SqlParameter("@UserName", SqlDbType.NVarChar,50),
                    new SqlParameter("@PassWord", SqlDbType.NVarChar,50),
                    new SqlParameter("@TrueName", SqlDbType.NVarChar,50),
                    new SqlParameter("@Status", SqlDbType.Int,4),
                    new SqlParameter("@UserType", SqlDbType.Int,4),
                    new SqlParameter("@RechargeNum", SqlDbType.NVarChar,4000),
                    new SqlParameter("@NotRechargeNum", SqlDbType.NVarChar,4000),
                    new SqlParameter("@Balance", SqlDbType.Decimal,9),
                    new SqlParameter("@Contactor", SqlDbType.NVarChar,50),
                    new SqlParameter("@ContactPhone", SqlDbType.NVarChar,20),
                    new SqlParameter("@Address", SqlDbType.NVarChar,200),
                    new SqlParameter("@IsCloseProduct", SqlDbType.Int,4),
                    new SqlParameter("@GateWay", SqlDbType.NVarChar,500),
                    new SqlParameter("@RechargePayDomain", SqlDbType.NVarChar,500),
                    new SqlParameter("@RechargeSearchUrl", SqlDbType.NVarChar,500),
                    new SqlParameter("@RechargeNoticeUrl", SqlDbType.NVarChar,500),
                    new SqlParameter("@FlowPayDomain", SqlDbType.NVarChar,500),
                    new SqlParameter("@FlowSearchUrl", SqlDbType.NVarChar,500),
                    new SqlParameter("@FlowNoticeUrl", SqlDbType.NVarChar,500),
                    new SqlParameter("@IsDelete", SqlDbType.Int,4),
                    new SqlParameter("@RechargeCount", SqlDbType.BigInt,8),
                    new SqlParameter("@FlowCount", SqlDbType.BigInt,8),
                    new SqlParameter("@Sort", SqlDbType.Int,4),
                    new SqlParameter("@ApiCode",SqlDbType.NVarChar,50),
                    new SqlParameter("@TelPhonePayDomain",SqlDbType.NVarChar,500)
            };
            parameters[0].Value = model.ID;
            parameters[1].Value = model.UserName;
            parameters[2].Value = model.PassWord;
            parameters[3].Value = model.TrueName;
            parameters[4].Value = model.Status;
            parameters[5].Value = model.UserType;
            parameters[6].Value = model.RechargeNum;
            parameters[7].Value = model.NotRechargeNum;
            parameters[8].Value = model.Balance;
            parameters[9].Value = model.Contactor;
            parameters[10].Value = model.ContactPhone;
            parameters[11].Value = model.Address;
            parameters[12].Value = model.IsCloseProduct;
            parameters[13].Value = model.GateWay;
            parameters[14].Value = model.RechargePayDomain;
            parameters[15].Value = model.RechargeSearchUrl;
            parameters[16].Value = model.RechargeNoticeUrl;
            parameters[17].Value = model.FlowPayDomain;
            parameters[18].Value = model.FlowSearchUrl;
            parameters[19].Value = model.FlowNoticeUrl;
            parameters[20].Value = model.IsDelete;
            parameters[21].Value = model.RechargeCount;
            parameters[22].Value = model.FlowCount;
            parameters[23].Value = model.Sort;
            parameters[24].Value = model.ApiCode;
            parameters[25].Value = model.TelPhonePayDomain;

            long rows = DBHelper.ExecuteSql(strSql.ToString(), parameters);

            return rows;
        }



        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Model.SUP_SupplierManage model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update SUP_SupplierManage set ");
            strSql.Append("UserName=@UserName,");
            strSql.Append("PassWord=@PassWord,");
            strSql.Append("TrueName=@TrueName,");
            strSql.Append("Status=@Status,");
            strSql.Append("UserType=@UserType,");
            strSql.Append("RechargeNum=@RechargeNum,");
            strSql.Append("NotRechargeNum=@NotRechargeNum,");
            strSql.Append("Balance=@Balance,");
            strSql.Append("Contactor=@Contactor,");
            strSql.Append("ContactPhone=@ContactPhone,");
            strSql.Append("Address=@Address,");
            strSql.Append("IsCloseProduct=@IsCloseProduct,");
            strSql.Append("GateWay=@GateWay,");
            strSql.Append("RechargePayDomain=@RechargePayDomain,");
            strSql.Append("RechargeSearchUrl=@RechargeSearchUrl,");
            strSql.Append("RechargeNoticeUrl=@RechargeNoticeUrl,");
            strSql.Append("FlowPayDomain=@FlowPayDomain,");
            strSql.Append("FlowSearchUrl=@FlowSearchUrl,");
            strSql.Append("FlowNoticeUrl=@FlowNoticeUrl,");
            strSql.Append("IsDelete=@IsDelete,");
            strSql.Append("RechargeCount=@RechargeCount,");
            strSql.Append("FlowCount=@FlowCount,");
            strSql.Append("Sort=@Sort,");
            strSql.Append("TelPhonePayDomain=@TelPhonePayDomain");
            strSql.Append(" where ID=@ID ");
            SqlParameter[] parameters = {
                    new SqlParameter("@UserName", SqlDbType.NVarChar,50),
                    new SqlParameter("@PassWord", SqlDbType.NVarChar,50),
                    new SqlParameter("@TrueName", SqlDbType.NVarChar,50),
                    new SqlParameter("@Status", SqlDbType.Int,4),
                    new SqlParameter("@UserType", SqlDbType.Int,4),
                    new SqlParameter("@RechargeNum", SqlDbType.NVarChar,4000),
                    new SqlParameter("@NotRechargeNum", SqlDbType.NVarChar,4000),
                    new SqlParameter("@Balance", SqlDbType.Decimal,9),
                    new SqlParameter("@Contactor", SqlDbType.NVarChar,50),
                    new SqlParameter("@ContactPhone", SqlDbType.NVarChar,20),
                    new SqlParameter("@Address", SqlDbType.NVarChar,200),
                    new SqlParameter("@IsCloseProduct", SqlDbType.Int,4),
                    new SqlParameter("@GateWay", SqlDbType.NVarChar,500),
                    new SqlParameter("@RechargePayDomain", SqlDbType.NVarChar,500),
                    new SqlParameter("@RechargeSearchUrl", SqlDbType.NVarChar,500),
                    new SqlParameter("@RechargeNoticeUrl", SqlDbType.NVarChar,500),
                    new SqlParameter("@FlowPayDomain", SqlDbType.NVarChar,500),
                    new SqlParameter("@FlowSearchUrl", SqlDbType.NVarChar,500),
                    new SqlParameter("@FlowNoticeUrl", SqlDbType.NVarChar,500),
                    new SqlParameter("@IsDelete", SqlDbType.Int,4),
                    new SqlParameter("@RechargeCount", SqlDbType.BigInt,8),
                    new SqlParameter("@FlowCount", SqlDbType.BigInt,8),
                    new SqlParameter("@ID", SqlDbType.BigInt,8),
                    new SqlParameter("@Sort", SqlDbType.Int,4),
                    new SqlParameter("@TelPhonePayDomain", SqlDbType.NVarChar,500)};
            parameters[0].Value = model.UserName;
            parameters[1].Value = model.PassWord;
            parameters[2].Value = model.TrueName;
            parameters[3].Value = model.Status;
            parameters[4].Value = model.UserType;
            parameters[5].Value = model.RechargeNum;
            parameters[6].Value = model.NotRechargeNum;
            parameters[7].Value = model.Balance;
            parameters[8].Value = model.Contactor;
            parameters[9].Value = model.ContactPhone;
            parameters[10].Value = model.Address;
            parameters[11].Value = model.IsCloseProduct;
            parameters[12].Value = model.GateWay;
            parameters[13].Value = model.RechargePayDomain;
            parameters[14].Value = model.RechargeSearchUrl;
            parameters[15].Value = model.RechargeNoticeUrl;
            parameters[16].Value = model.FlowPayDomain;
            parameters[17].Value = model.FlowSearchUrl;
            parameters[18].Value = model.FlowNoticeUrl;
            parameters[19].Value = model.IsDelete;
            parameters[20].Value = model.RechargeCount;
            parameters[21].Value = model.FlowCount;
            parameters[22].Value = model.ID;
            parameters[23].Value = model.Sort;
            parameters[24].Value = model.TelPhonePayDomain;

            int rows = DBHelper.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        #endregion


        #region 补充方法

        /// <summary>
        /// 
        /// </summary>
        public bool ResetPassword(long id, string password)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update SUP_SupplierManage set ");

            strSql.Append(" PassWord = @PassWord  ");
            strSql.Append(" where ID=@ID ");

            SqlParameter[] parameters = {
                        new SqlParameter("@ID", SqlDbType.BigInt,8) ,
                        new SqlParameter("@PassWord", SqlDbType.NVarChar,50)

            };

            parameters[0].Value = id;
            parameters[1].Value = Common.Method.GetMD5(password, Common.Method.MD5EncryptBit.bit32);
            int rows = DBHelper.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion
    }
}