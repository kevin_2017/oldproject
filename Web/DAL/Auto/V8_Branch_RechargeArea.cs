﻿using Common.DBUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DAL
{ /// <summary>
  /// DAL层,代理可充区域管理
  /// </summary>
    public partial class V8_Branch_RechargeArea : DalBase
    {

        #region  基础方法 

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public long Add(Model.V8_Branch_RechargeArea model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into V8_Branch_RechargeArea(");
            strSql.Append("BranchId,BranchCode,ISPType,ISPName,ProvinceId,ProvinceName,CityId,CityName,BranchProductId");
            strSql.Append(") values (");
            strSql.Append("@BranchId,@BranchCode,@ISPType,@ISPName,@ProvinceId,@ProvinceName,@CityId,@CityName,@BranchProductId");
            strSql.Append(") ");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
                        new SqlParameter("@BranchId", SqlDbType.BigInt,8) ,
                        new SqlParameter("@BranchCode", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@ISPType", SqlDbType.Int,4) ,
                        new SqlParameter("@ISPName", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@ProvinceId", SqlDbType.Int,4) ,
                        new SqlParameter("@ProvinceName", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@CityId", SqlDbType.Int,4) ,
                        new SqlParameter("@CityName", SqlDbType.NVarChar,50),  
                        new SqlParameter("@BranchProductId", SqlDbType.BigInt,8) ,

            };

            parameters[0].Value = model.BranchId;
            parameters[1].Value = model.BranchCode;
            parameters[2].Value = model.IspType;
            parameters[3].Value = model.IspName;
            parameters[4].Value = model.ProvinceId;
            parameters[5].Value = model.ProvinceName;
            parameters[6].Value = model.CityId;
            parameters[7].Value = model.CityName;
            parameters[8].Value = model.BranchProductId; 

            object obj = DBHelper.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {

                return Convert.ToInt64(obj);

            }

        }


        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Model.V8_Branch_RechargeArea model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update V8_Branch_RechargeArea set ");
            strSql.Append(" BranchId = @BranchId , ");
            strSql.Append(" BranchCode = @BranchCode , ");
            strSql.Append(" ISPType = @ISPType , ");
            strSql.Append(" ISPName = @ISPName , ");
            strSql.Append(" ProvinceId = @ProvinceId , ");
            strSql.Append(" ProvinceName = @ProvinceName , ");
            strSql.Append(" CityId = @CityId , ");
            strSql.Append(" CityName = @CityName,  "); 
            strSql.Append(" BranchProductId = @BranchProductId  "); 
            strSql.Append(" where ID=@ID ");

            SqlParameter[] parameters = {
                        new SqlParameter("@ID", SqlDbType.BigInt,8) ,
                new SqlParameter("@BranchId", SqlDbType.BigInt,8) ,
                new SqlParameter("@BranchCode", SqlDbType.NVarChar,50) ,
                new SqlParameter("@ISPType", SqlDbType.Int,4) ,
                new SqlParameter("@ISPName", SqlDbType.NVarChar,50) ,
                new SqlParameter("@ProvinceId", SqlDbType.Int,4) ,
                new SqlParameter("@ProvinceName", SqlDbType.Int,4) ,
                new SqlParameter("@CityId", SqlDbType.Int,4) ,
                new SqlParameter("@CityName", SqlDbType.NVarChar,50),
                        new SqlParameter("@BranchProductId", SqlDbType.BigInt,8) ,

            };

            parameters[0].Value = model.Id;
            parameters[1].Value = model.BranchId;
            parameters[2].Value = model.BranchCode;
            parameters[3].Value = model.IspType;
            parameters[4].Value = model.IspName;
            parameters[5].Value = model.ProvinceId;
            parameters[6].Value = model.ProvinceName;
            parameters[7].Value = model.CityId;
            parameters[8].Value = model.CityName;
            parameters[9].Value = model.BranchProductId;
            
            int rows = DBHelper.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion


        #region 补充方法

        #endregion
    }
}