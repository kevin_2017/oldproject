﻿
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Common.DBUtility;
using System.Collections.Generic;
using CE.Utility.Web;

namespace DAL
{
    /// <summary>
    /// DAL层,系统管理用户
    /// </summary>
    public partial class SYS_UserInfo : DalBase
    {
        
        #region  基础方法
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public long Add(Model.SYS_UserInfo model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into SYS_UserInfo(");
            strSql.Append("UserName,TrueName,PWD,Sex,Status");
            strSql.Append(") values (");
            strSql.Append("@UserName,@TrueName,@PWD,@Sex,@Status");
            strSql.Append(") ");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
                        new SqlParameter("@UserName", SqlDbType.NVarChar,20) ,
                        new SqlParameter("@TrueName", SqlDbType.NVarChar,20) ,
                        new SqlParameter("@PWD", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@Sex", SqlDbType.Int,4) ,
                        new SqlParameter("@Status", SqlDbType.Int,4)

            };

            parameters[0].Value = model.UserName;
            parameters[1].Value = model.TrueName;
            parameters[2].Value = Common.Method.GetMD5(model.PWD, Common.Method.MD5EncryptBit.bit32);
            parameters[3].Value = model.Sex;
            parameters[4].Value = model.Status;

            object obj = DBHelper.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {

                return Convert.ToInt64(obj);

            }

        }






        #endregion


        #region 补充方法

        /// <summary>
        /// 获取用户登录信息
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="loginIP"></param>
        /// <param name="errcount"></param>
        /// <returns></returns>
        public Model.SystemUserInfo GetSystemUserInfo(string username, string password, out int errcount)
        {
            string loginIP = WebUtil.ClientIP();

            var ipAttr = IPSearch.GetLocation(loginIP);
            int ErrorCount = 0;
            DataTable dt = new DataTable();

            SqlParameter UserName = new SqlParameter("@UserName", SqlDbType.NVarChar, 20);
            UserName.Value = username;

            SqlParameter PWD = new SqlParameter("@PWD", SqlDbType.NVarChar, 50);
            PWD.Value = Common.Method.GetMD5(password, Common.Method.MD5EncryptBit.bit32) ;

            SqlParameter LoginIP = new SqlParameter("@LoginIP", SqlDbType.VarChar, 20);
            LoginIP.Value = loginIP.Equals("::1")?"127.0.0.1":loginIP;

            SqlParameter LoginIpAttr = new SqlParameter("@LoginIpAttr", SqlDbType.NVarChar, 500);
            LoginIpAttr.Value = ipAttr
                ;
            SqlParameter errorCount = new SqlParameter("@errorCount", SqlDbType.Int);
            errorCount.Direction = ParameterDirection.Output;


            List<SqlParameter> list = new List<SqlParameter>();
            list.Add(UserName);
            list.Add(PWD);
            list.Add(LoginIP);
            list.Add(LoginIpAttr);
            list.Add(errorCount);


            dt = DBHelper.ExecuteProcedureOutputTable("PROC_SYSUserLogin", list);
            errcount = ErrorCount = Convert.ToInt32(errorCount.Value);
            if (dt.Rows.Count > 0)
            {
                return DataRowToModel<Model.SystemUserInfo>(dt.Rows[0]);
            }
            else
            {
                return null;
            }

        }


        /// <summary>
        /// 修改用户基础信息
        /// </summary>
        public bool UpdateBaseInfo(Model.SYS_UserInfo model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update SYS_UserInfo set ");
            strSql.Append(" UserName = @UserName , ");
            strSql.Append(" TrueName = @TrueName , ");
            strSql.Append(" Sex = @Sex , ");
            strSql.Append(" Status = @Status  ");
            strSql.Append(" where ID=@ID ");

            SqlParameter[] parameters = {
                        new SqlParameter("@ID", SqlDbType.BigInt,8) ,
                        new SqlParameter("@UserName", SqlDbType.NVarChar,20) ,
                        new SqlParameter("@TrueName", SqlDbType.NVarChar,20) ,
                        new SqlParameter("@Sex", SqlDbType.Int,4) ,
                        new SqlParameter("@Status", SqlDbType.Int,4)

            };

            parameters[0].Value = model.ID;
            parameters[1].Value = model.UserName;
            parameters[2].Value = model.TrueName;
            parameters[3].Value = model.Sex;
            parameters[4].Value = model.Status;
            int rows = DBHelper.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 修改用户密码
        /// </summary>
        public bool UpdatePassword(long userID, string password)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update SYS_UserInfo set ");
            strSql.Append(" PWD = @PWD ");
            strSql.Append(" where ID=@ID ");

            SqlParameter[] parameters = {
                        new SqlParameter("@ID", SqlDbType.BigInt,8) ,
                        new SqlParameter("@PWD", SqlDbType.NVarChar,50)

            };

            parameters[0].Value = userID;
            parameters[1].Value = Common.Method.GetMD5(password, Common.Method.MD5EncryptBit.bit32);
            int rows = DBHelper.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 修改用户为已删除状态
        /// </summary>
        public bool UpdateUserISDelete(long userID, int isDelete)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update SYS_UserInfo set ");
            strSql.Append(" ISDelete = @ISDelete ");
            strSql.Append(" where ID=@ID ");

            SqlParameter[] parameters = {
                        new SqlParameter("@ID", SqlDbType.BigInt,8) ,
                        new SqlParameter("@ISDelete", SqlDbType.Int) ,

            };

            parameters[0].Value = userID;
            parameters[1].Value = isDelete;
            int rows = DBHelper.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        #endregion
    }
}