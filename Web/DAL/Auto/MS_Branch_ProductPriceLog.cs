﻿
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Common.DBUtility;
using Model;

namespace DAL
{
    public partial class MS_Branch_ProductPriceLog : DalBase
    {
        
        #region  基础方法
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public long Add(Model.MS_Branch_ProductPriceLog model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into MS_Branch_ProductPriceLog(");
            strSql.Append("UserID,ISPType,denomination,RechargeAreaID,Code,BranchID,ProductID,OldDiscountMethod,OldSalePrice,NewDiscountMethod,NewSalePrice,OperationTime");
            strSql.Append(") values (");
            strSql.Append("@UserID,@ISPType,@denomination,@RechargeAreaID,@Code,@BranchID,@ProductID,@OldDiscountMethod,@OldSalePrice,@NewDiscountMethod,@NewSalePrice,@OperationTime");
            strSql.Append(") ");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
			            new SqlParameter("@UserID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@ISPType", SqlDbType.NVarChar,50) ,            
                        new SqlParameter("@denomination", SqlDbType.NVarChar,500) ,            
                        new SqlParameter("@RechargeAreaID", SqlDbType.NVarChar,500) ,            
                        new SqlParameter("@Code", SqlDbType.NVarChar,50) ,            
                        new SqlParameter("@BranchID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@ProductID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@OldDiscountMethod", SqlDbType.Int,4) ,            
                        new SqlParameter("@OldSalePrice", SqlDbType.Decimal,9) ,            
                        new SqlParameter("@NewDiscountMethod", SqlDbType.Int,4) ,            
                        new SqlParameter("@NewSalePrice", SqlDbType.Decimal,9) ,            
                        new SqlParameter("@OperationTime", SqlDbType.DateTime)             
              
            };

            parameters[0].Value = model.UserID;
            parameters[1].Value = model.ISPType;
            parameters[2].Value = model.denomination;
            parameters[3].Value = model.RechargeAreaID;
            parameters[4].Value = model.Code;
            parameters[5].Value = model.BranchID;
            parameters[6].Value = model.ProductID;
            parameters[7].Value = model.OldDiscountMethod;
            parameters[8].Value = model.OldSalePrice;
            parameters[9].Value = model.NewDiscountMethod;
            parameters[10].Value = model.NewSalePrice;
            parameters[11].Value = model.OperationTime;

            object obj = DBHelper.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {

                return Convert.ToInt64(obj);

            }

        }



        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Model.MS_Branch_ProductPriceLog model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update MS_Branch_ProductPriceLog set ");

            strSql.Append(" UserID = @UserID , ");
            strSql.Append(" ISPType = @ISPType , ");
            strSql.Append(" denomination = @denomination , ");
            strSql.Append(" RechargeAreaID = @RechargeAreaID , ");
            strSql.Append(" Code = @Code , ");
            strSql.Append(" BranchID = @BranchID , ");
            strSql.Append(" ProductID = @ProductID , ");
            strSql.Append(" OldDiscountMethod = @OldDiscountMethod , ");
            strSql.Append(" OldSalePrice = @OldSalePrice , ");
            strSql.Append(" NewDiscountMethod = @NewDiscountMethod , ");
            strSql.Append(" NewSalePrice = @NewSalePrice , ");
            strSql.Append(" OperationTime = @OperationTime  ");
            strSql.Append(" where ID=@ID ");

            SqlParameter[] parameters = {
			            new SqlParameter("@ID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@UserID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@ISPType", SqlDbType.NVarChar,50) ,            
                        new SqlParameter("@denomination", SqlDbType.NVarChar,500) ,            
                        new SqlParameter("@RechargeAreaID", SqlDbType.NVarChar,500) ,            
                        new SqlParameter("@Code", SqlDbType.NVarChar,50) ,            
                        new SqlParameter("@BranchID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@ProductID", SqlDbType.BigInt,8) ,            
                        new SqlParameter("@OldDiscountMethod", SqlDbType.Int,4) ,            
                        new SqlParameter("@OldSalePrice", SqlDbType.Decimal,9) ,            
                        new SqlParameter("@NewDiscountMethod", SqlDbType.Int,4) ,            
                        new SqlParameter("@NewSalePrice", SqlDbType.Decimal,9) ,            
                        new SqlParameter("@OperationTime", SqlDbType.DateTime)             
              
            };

            parameters[0].Value = model.ID;
            parameters[1].Value = model.UserID;
            parameters[2].Value = model.ISPType;
            parameters[3].Value = model.denomination;
            parameters[4].Value = model.RechargeAreaID;
            parameters[5].Value = model.Code;
            parameters[6].Value = model.BranchID;
            parameters[7].Value = model.ProductID;
            parameters[8].Value = model.OldDiscountMethod;
            parameters[9].Value = model.OldSalePrice;
            parameters[10].Value = model.NewDiscountMethod;
            parameters[11].Value = model.NewSalePrice;
            parameters[12].Value = model.OperationTime;
            int rows = DBHelper.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion


        #region 补充方法
        #endregion
    }
}
