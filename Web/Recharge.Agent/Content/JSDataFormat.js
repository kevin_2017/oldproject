﻿var format = function (time, format) {
    if (time == "")
    { return "" }
    else {
        if (time.indexOf("Date", 0) > 0) {
            var date = new Date(parseInt(time.replace("/Date(", "").replace(")/", ""), 10));
            //月份为0-11，所以+1，月份小于10时补个0
            var month = date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1;
            var currentDate = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
            var HH = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
            var mm = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
            var ss = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();
            return date.getFullYear() + "-" + month + "-" + currentDate + " " + HH + ":" + mm + ":" + ss;
        }

        var t = time.substring(6);
        t = t.substring(0, t.indexOf("+"));
        var newt = new Date(parseInt(t));
        var tf = function (i) { return (i < 10 ? '0' : '') + i };
        return format.replace(/yyyy|MM|dd|HH|mm|ss/g, function (a) {
            switch (a) {
                case 'yyyy':
                    return tf(newt.getFullYear());
                    break;
                case 'MM':
                    return tf(newt.getMonth() + 1);
                    break;
                case 'mm':
                    return tf(newt.getMinutes());
                    break;
                case 'dd':
                    return tf(newt.getDate());
                    break;
                case 'HH':
                    return tf(newt.getHours());
                    break;
                case 'ss':
                    return tf(newt.getSeconds());
                    break;
            }
        })
    }
}

function ChangeDateFormat(val) {
    if (val != null) {
        var date = new Date(parseInt(val.replace("/Date(", "").replace(")/", ""), 10));
        //月份为0-11，所以+1，月份小于10时补个0
        var month = date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1;
        var currentDate = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
        return date.getFullYear() + "-" + month + "-" + currentDate;
    }
    return "";
}