﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace Recharge.Agent.Content
{
    /// <summary>
    /// ValidateCode 的摘要说明
    /// </summary>
    public class ValidateCode : IHttpHandler, IRequiresSessionState
    {
        private int letterWidth = 28;
        private int letterHeight = 34;
        private int fontSize = 18;
        private string[] fonts = { "Courier New", "Arial", "Georgia", "Tahoma", "Verdana", "Comic sans MS", "Lucida console" };
        string chars = "A2B3C4D5E6F7G8H9G2K3L4M5N6P7Q8R9S2T3U4V5W6X7Y8Z";

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "image/jpeg";


            string checkCode = "";
            Random newRandom = new Random();
            Random rand = new Random();//初始化随机数

            do
            {
                string sel = chars[rand.Next(0, chars.Length)].ToString();
                if (!checkCode.Contains(sel))
                    checkCode += sel;
            } while (checkCode.Length < 4);

            HttpContext.Current.Session["validateCode"] = checkCode;


            int int_ImageWidth = checkCode.Length * letterWidth + 5;

            Bitmap img = new Bitmap(int_ImageWidth, letterHeight);

            Graphics g = Graphics.FromImage(img);//
            g.Clear(Color.White);
            for (int i = 0; i < 10; i++)
            {
                int x1 = rand.Next(img.Width);
                int x2 = rand.Next(img.Width);
                int y1 = rand.Next(img.Height);
                int y2 = rand.Next(img.Height);

                g.DrawLine(new Pen(Color.Silver), x1, y1, x2, y2);
            }

            for (int i = 0; i < 10; i++)
            {
                int x = rand.Next(img.Width);
                int y = rand.Next(img.Height);

                img.SetPixel(x, y, Color.FromArgb(rand.Next()));
            }


            for (int int_index = 0; int_index < checkCode.Length; int_index++)
            {
                var findex = rand.Next(fonts.Length - 1);
                string str_char = checkCode.Substring(int_index, 1);
                Brush newBrush = new SolidBrush(GetRandomColor());
                Point thePos = new Point(int_index * letterWidth + newRandom.Next(2) + 1, newRandom.Next(2) + 1);
                g.DrawString(str_char, new Font(fonts[findex], fontSize, FontStyle.Bold), newBrush, thePos);
            }
            g.DrawRectangle(new Pen(Color.LightGray, 1), 0, 0, int_ImageWidth - 1, (letterHeight - 1));

            img.Save(context.Response.OutputStream, ImageFormat.Jpeg);
        }

        protected Color GetRandomColor()
        {
            Random RandomNum_First = new Random((int)DateTime.Now.Ticks);
            System.Threading.Thread.Sleep(RandomNum_First.Next(50));
            Random RandomNum_Sencond = new Random((int)DateTime.Now.Ticks);
            int int_Red = RandomNum_First.Next(210);
            int int_Green = RandomNum_Sencond.Next(180);
            int int_Blue = (int_Red + int_Green > 300) ? 0 : 400 - int_Red - int_Green;
            int_Blue = (int_Blue > 255) ? 255 : int_Blue;
            return Color.FromArgb(int_Red, int_Green, int_Blue);
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}