﻿using CE.Utility;
using System;
using System.Web;
using System.Web.Services;

namespace Recharge.Agent
{
    public partial class frmLogin : System.Web.UI.Page, IHttpHandler
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static JsonResult<bool> UserLogin(string userName, string password, string validateCode)
        {
            var code = HttpContext.Current.Session["validateCode"].ToString();
            if (code.ToLower().Equals(validateCode.ToLower()))
            {

                Model.MS_BranchManage logininfo = new Model.MS_BranchManage();
                Model.StrWhere str = new Model.StrWhere()
                {
                    IsWhereExist = true,
                    strWhere = new System.Text.StringBuilder($"  LOWER(BranchCode)='{userName.ToLower()}' and pwd='{ Common.Method.GetMD5(password, Common.Method.MD5EncryptBit.bit32)}'")
                };
                logininfo = new BLL.MS_BranchManage().GetModel(str);
                HttpContext.Current.Session["BranchLoginInfo"] = logininfo;
                var result = logininfo != null;
                return new JsonResult<bool>(result, result ? "登录成功" : "登录失败,用户名或者密码错误");
            }
            else
            {
                return new JsonResult<bool>(false, "验证码错误");
            }
        }
    }
}