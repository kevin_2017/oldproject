﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmLogin.aspx.cs" Inherits="Recharge.Agent.frmLogin" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>充值后台管理系统</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <!-- #CSS Links -->
    <!-- Basic Styles -->
    <!-- Bootstrap 3.3.4 -->
    <link href="/Content/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="/Content/dist/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="/Content/dist/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="/Content/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />

    <link href="/Content/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <%--<link href="/Content/plugins/iCheck/square/blue.css" rel="stylesheet" type="text/css" />--%>

    <!-- iOS web-app metas : hides Safari UI Components and Changes Status Bar Appearance -->
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />



</head>
<body class="login-page">

    <div class="login-box">
        <div class="login-logo">
            <a href="javascript:void(0)"><b>充值</b>管理系统</a>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg">Sign in to start your session</p>
            <form id="form1" runat="server">
                <div class="form-group has-feedback">
                    <input type="text" id="txtUserName" class="form-control" placeholder="用户名" />
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" id="txtPassword" class="form-control" placeholder="密码" />
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="row">
                    <div class="col-xs-7">
                        <input type="text" id="txtValidateCode" class="form-control" placeholder="验证码" />
                    </div>
                    <div class="col-xs-5">
                        <a href="javascript:changeImg()">
                            <img id="imgCheckCode" style="height: 34px" /></a>
                    </div>
                </div>
                <div class="form-group">
                </div>
                <div class="row">
                    <div class="col-xs-8">
                        <%--<div class="checkbox icheck">
                            <label>
                                <input type="checkbox" />
                                Remember Me
               
                            </label>
                        </div>--%>
                    </div>
                    <!-- /.col -->
                    <div class="col-xs-4">
                        <button type="button" onclick="DoLogin()" class="btn btn-primary btn-block btn-flat">登录</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>

            <div class="social-auth-links text-center">
                <p>- OR -</p>
                <%-- <a href="javascript:void(0" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i>Sign in using Facebook</a>
                <a href="javascript:void(0" class="btn btn-block btn-social btn-google-plus btn-flat"><i class="fa fa-google-plus"></i>Sign in using Google+</a>--%>
            </div>
            <!-- /.social-auth-links -->

            <%-- <a href="#">I forgot my password</a><br>
            <a href="register.html" class="text-center">Register a new membership</a>--%>
        </div>
        <!-- /.login-box-body -->
    </div>
    <!-- /.login-box -->

    <!-- jQuery 2.1.4 -->
    <script src="/Content/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="/Content/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/Content/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>

    <script src="/Content/dist/js/app.min.js" type="text/javascript"></script>
    <script src="/Content/layer/layer.js"></script>
    <script src="/Content/jquery.json.min.js"></script>
    <!-- iCheck -->
    <%--    <script src="/Content/plugins/iCheck/icheck.min.js" type="text/javascript"></script>--%>
    <script>
        $(function () {
            changeImg();
            

            $('#txtValidateCode').bind('keypress', function (event) {
                if (event.keyCode == "13") { 
                    DoLogin();
                }
            });
        });

        function changeImg() {
            $("#imgCheckCode").attr("src", "/Content/ValidateCode.ashx?time=" + (new Date()).getTime());
        }

        function DoLogin() {
            var userName = $("#txtUserName").val();
            var password = $("#txtPassword").val();
            var validateCode = $("#txtValidateCode").val();
            if (userName == "") {
                layer.alert("用户名不能为空", { icon: 2 }, function () { layer.closeAll(); $("#txtUserName").focus() });
            } else if (password == "") {
                layer.alert("密码不能为空", { icon: 2 }, function () { layer.closeAll(); $("#txtPassword").focus() });
            } else if (validateCode == "") {
                layer.alert("验证码不能为空", { icon: 2 }, function () { layer.closeAll(); $("#txtValidateCode").focus() });
            } else {
                $.ajax({
                    type: "POST",
                    url: "/frmLogin.aspx/UserLogin",
                    async: false,
                    data: $.toJSON({ userName: userName, password: password, validateCode: validateCode }),
                    contentType: "application/json;charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        var index = layer.load(1, {
                            shade: [0.8, '#fff'] //0.1透明度的白色背景 
                        });
                    },
                    success: function (data) {
                        if (data.d.Data) {
                            window.location.href = "/index.aspx";
                        } else {
                            layer.alert(data.d.Message + '！', { icon: 2 }, function () { layer.closeAll(); });
                        }

                    },
                    error: function (err) {
                        layer.alert(err.responseText, { icon: 2 }, function () { layer.closeAll(); });
                    }
                });
            }
        }
    </script>
</body>
</html>

