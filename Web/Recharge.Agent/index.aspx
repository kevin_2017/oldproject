﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="Recharge.Agent.index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/Content/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <table id="example1" class="table table-bordered table-striped">

                            <tbody>

                                <tr>
                                    <td>当前余额：</td>
                                    <td>
                                        <asp:Literal runat="server" ID="litBlance"></asp:Literal>
                                        元
                                    </td>
                                    <td>冻结金额：</td>
                                    <td>
                                        <asp:Literal runat="server" ID="litFrozenMoney"></asp:Literal>
                                        元
                                    </td>
                                     <td>可用余额：</td>
                                    <td>
                                        <asp:Literal runat="server" ID="litTrueBlance"></asp:Literal>
                                        元
                                    </td>
                                    <td>  <label>
                                <asp:Button runat="server" ID="btnSearch" Text="查询" CssClass="btn btn-success" OnClick="btnSearch_Click" /></label></td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th colspan="6">
                                        <asp:Literal runat="server" ID="litDate"></asp:Literal>
                                        充值统计
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="6">充值笔数</td>

                                </tr>
                                <tr>
                                    <td>电信：</td>
                                    <td>
                                        <asp:Literal runat="server" ID="litDianXinCount"></asp:Literal></td>
                                    <td>移动：</td>
                                    <td>
                                        <asp:Literal runat="server" ID="litYiDongCount"></asp:Literal></td>
                                    <td>联通：</td>
                                    <td>
                                        <asp:Literal runat="server" ID="litLianTongCount"></asp:Literal></td>
                                </tr>
                                <tr>
                                    <td colspan="6">充值金额</td>

                                </tr>
                                <tr>
                                    <td>电信：</td>
                                    <td>
                                        <asp:Literal runat="server" ID="litDianXinMoney"></asp:Literal>元</td>
                                    <td>移动：</td>
                                    <td>
                                        <asp:Literal runat="server" ID="litYiDongMoney"></asp:Literal>元</td>
                                    <td>联通：</td>
                                    <td>
                                        <asp:Literal runat="server" ID="litLianTongMoney"></asp:Literal>元</td>
                                </tr>
                                <tr>
                                    <td colspan="6">汇总统计</td>

                                </tr>
                                <tr>
                                    <td colspan="6">
                                        <div class="dataTables_info" id="divTotal" runat="server"></div>
                                    </td>

                                </tr>
                            </tbody>

                        </table>
                    </div>
                </div>

            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</asp:Content>
