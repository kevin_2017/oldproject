﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="frmInterfaceInfo.aspx.cs" Inherits="Recharge.Agent.Interface.frmInterfaceInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        $(function () {
            var divHeigh = $(window).height() - 50;
            if ($("#divHtml").height() > divHeigh) {
                $("#divHtml").css({ "overflow": "scroll", "height": (divHeigh + "px") });
            }
        })
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="content">
        <div class="row">

            <div class="col-sm-3">
                <h3>2、方法</h3>
                <h4><a href="#2.1" rel="nofollow" target="_self">2.1、查询余额</a></h4>
                <h4><a href="#2.2" rel="nofollow" target="_self">2.2、获取商品</a></h4>
                <h4><a href="#2.3" rel="nofollow" target="_self">2.3、话费充值</a></h4>
                <h4><a href="#2.4" rel="nofollow" target="_self">2.4、订单查询</a></h4>
                <h4><a href="#2.5" rel="nofollow" target="_self">2.5、订单通知</a></h4>
                <h4><a href="#2.6" rel="nofollow" target="_self">2.6、返回码说明</a></h4>
                <h4><a href="#2.7" rel="nofollow" target="_self">2.7、可充省份</a></h4>
                <h5>注意事项</h5>
                <pre>
1、param中使用的key值是上游提供的密钥。
2、param中使用的branchcode值是上游提供的纯字符串类型。
3、请求参数中的Key不用转换成小写。
<span style="color:red">默认话费商品ID：</span>
中国联通：1069
中国移动：1051
中国电信：1033
<span style="color:red">当前接口IP及Port地址:http://112.74.62.132:8003</span>
Java开发解析JSON用 Fastjson <a href="https://www.cnblogs.com/cdf-opensource-007/p/7106018.html" target="_blank">Demo示例</a>

                </pre>
            </div>
            <div class="col-sm-8">
                <div style="width: auto;" id="divHtml">
                    <h3 style="color: red">所有方法均提供两个参数：param和key </h3>
                    <br />
                    <span style="color: red">Key：</span>(6位无意义字串+(代理编码+系统提供私钥+yyyyMMdd).toMd5()+8位无意义字串).ToBase64()；
                    <br />
                    <span style="color: red">Param：</span>是以下各具体方法中的各参数组合后进行Base64加密数据；
                    <h4><a id="2.1"></a>2.1、查询余额 </h4>
                    请求地址：http://ip:port/api/代理编码/Balance/GetBalance
                    <br />

                    param参数：
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>参数名</th>
                                <th>名称</th>
                                <th>是否为空</th>
                                <th>说明</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>branchcode</td>
                                <td>代理编码</td>
                                <td>否</td>
                                <td>由上游提供</td>
                            </tr>
                            <tr>
                                <td>requesttime</td>
                                <td>请求时间</td>
                                <td>否</td>
                                <td>yyyy-MM-dd HH:mm:ss</td>
                            </tr>
                            <tr>
                                <td>sign</td>
                                <td>签名</td>
                                <td>否</td>
                                <td></td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td>sign组成说明</td>
                                <td colspan="3">var str=以上所有参数按ASCII进行排序,再通过'&'连接所有非空的参数再与&key=[key];
                                    <br />
                                    sign= str.ToMD5();
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                    <h4><a id="2.2"></a>2.2、获取商品</h4>
                    请求地址：http://ip:port/api/代理编码/Products/GetProducts
                    <br />
                    param参数：
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>参数名</th>
                                <th>名称</th>
                                <th>是否为空</th>
                                <th>说明</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>branchcode</td>
                                <td>代理编码</td>
                                <td>否</td>
                                <td>由上游提供</td>
                            </tr>
                            <tr>
                                <td>requesttime</td>
                                <td>请求时间</td>
                                <td>否</td>
                                <td>yyyy-MM-dd HH:mm:ss</td>
                            </tr>
                            <tr>
                                <td>typeid</td>
                                <td>商品类型ID</td>
                                <td>否</td>
                                <td>(1001、手机话费，1002、座机话费，1003、流量充值)</td>
                            </tr>
                            <tr>
                                <td>sign</td>
                                <td>签名</td>
                                <td>否</td>
                                <td></td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td>sign组成说明</td>
                                <td colspan="3">var str=以上所有参数按ASCII进行排序,再通过'&'连接所有非空的参数再与&key=[key];
                                    <br />
                                    sign= str.ToMD5();
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                    <h4><a id="2.3"></a>2.3、话费充值</h4>
                    请求地址：http://ip:port/api/代理编码/RechargeApi/PhoneRecharge
                    <br />
                    param参数：<span style="color: red">更新原参数“rechrgemoney”为“rechargemoney”</span>

                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>参数名</th>
                                <th>名称</th>
                                <th>是否为空</th>
                                <th>说明</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>branchcode</td>
                                <td>代理编码</td>
                                <td>否</td>
                                <td>由上游提供</td>
                            </tr>
                            <tr>
                                <td>branchorderno</td>
                                <td>商户订单号</td>
                                <td>否</td>
                                <td>自定义生成保证唯一</td>
                            </tr>
                            <tr>
                                <td>requesttime</td>
                                <td>请求时间</td>
                                <td>否</td>
                                <td>yyyy-MM-dd HH:mm:ss</td>
                            </tr>
                            <tr>
                                <td>phonenum</td>
                                <td>充值号码</td>
                                <td>否</td>
                                <td>手机号码和座机(座机号码)</td>
                            </tr>
                            <tr>
                                <td>phonetype</td>
                                <td>号码类型</td>
                                <td>否</td>
                                <td>号码类型（1、手机，2、固话）</td>
                            </tr>
                            <tr>
                                <td>isptype</td>
                                <td>运营商类型</td>
                                <td>否</td>
                                <td>运营商类型（1、中国联通，2、中国移动，3、中国电信）</td>
                            </tr>
                            <tr>
                                <td>rechargemoney</td>
                                <td>充值金额</td>
                                <td>否</td>
                                <td>元为单位</td>
                            </tr>
                            <tr>
                                <td>notifyurl</td>
                                <td>通知地址</td>
                                <td>否</td>
                                <td>只有成功后才会通知下游</td>
                            </tr>
                            <tr>
                                <td>productid</td>
                                <td>商品ID</td>
                                <td>否</td>
                                <td>从商品接口中获取</td>
                            </tr>
                            <tr>
                                <td>extendparam</td>
                                <td>扩展参数</td>
                                <td>是</td>
                                <td>原样返回,为空时不参与签名，但要参与param参数的组合与加密</td>
                            </tr>
                            <tr>
                                <td>sign</td>
                                <td>签名</td>
                                <td>否</td>
                                <td></td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td>sign组成说明</td>
                                <td colspan="3">var str=以上所有参数按ASCII进行排序,再通过'&'连接所有非空的参数再与&key=[key];
                                    <br />
                                    sign= str.ToMD5();
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                    <h4><a id="2.4"></a>2.4、订单查询</h4>
                    请求地址：http://ip:port/api/代理编码/Order/QueryOrder
                    <br />
                    param参数：
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>参数名</th>
                                <th>名称</th>
                                <th>是否为空</th>
                                <th>说明</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>branchcode</td>
                                <td>代理编码</td>
                                <td>否</td>
                                <td>由上游提供</td>
                            </tr>
                            <tr>
                                <td>branchorderno</td>
                                <td>商户订单号</td>
                                <td>否</td>
                                <td>自定义生成保证唯一</td>
                            </tr>
                            <tr>
                                <td>requesttime</td>
                                <td>请求时间</td>
                                <td>否</td>
                                <td>yyyy-MM-dd HH:mm:ss</td>
                            </tr>
                            <tr>
                                <td>sign</td>
                                <td>签名</td>
                                <td>否</td>
                                <td></td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td>sign组成说明</td>
                                <td colspan="3">var str=以上所有参数按ASCII进行排序,再通过'&'连接所有非空的参数再与&key=[key];
                                    <br />
                                    sign= str.ToMD5();
                                </td>

                            </tr>
                            <tr>
                                <td>订单返回状态：CurrentStatus</td>
                                <td colspan="3">SUCCESS:充值成功,WAITFORGPAY:等待充值,PAYING:充值中,PROBLEMORDER:问题订单,RECHARGEFAILED:充值失败,UNKNOWN:未知状态
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                    <h4><a id="2.5"></a>2.5、订单通知</h4>
                    成功收到通知后返回“SUCCESS” 则停止发送通知
                    <br />
                    param参数：
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>参数名</th>
                                <th>名称</th>
                                <th>是否为空</th>
                                <th>说明</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>branchcode</td>
                                <td>代理编码</td>
                                <td>否</td>
                                <td>由上游提供</td>
                            </tr>
                            <tr>
                                <td>branchorderno</td>
                                <td>商户订单号</td>
                                <td>否</td>
                                <td>自定义生成保证唯一</td>
                            </tr>

                            <tr>
                                <td>status</td>
                                <td>成功状态</td>
                                <td>否</td>
                                <td>成功返回1,失败返回0</td>
                            </tr>
                            <tr>
                                <td>extendparam</td>
                                <td>扩展参数</td>
                                <td>是</td>
                                <td>原样返回</td>
                            </tr>
                            <tr>
                                <td>sign</td>
                                <td>签名</td>
                                <td>否</td>
                                <td></td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td>sign组成说明</td>
                                <td colspan="3">var str=以上所有参数按ASCII进行排序,再通过'&'连接所有非空的参数再与&key=[key];
                                    <br />
                                    sign= str.ToMD5();
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                    <h4><a id="2.6"></a>2.6、返回码说明</h4>
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>编码</th>
                                <th>返回信息</th>

                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>000</td>
                                <td>请求成功</td>
                            </tr>
                            <tr>
                                <td>101</td>
                                <td>缺少参数</td>
                            </tr>
                            <tr>
                                <td>102</td>
                                <td>参数缺少值</td>
                            </tr>
                            <tr>
                                <td>103</td>
                                <td>代理不能正常使用</td>
                            </tr>
                            <tr>
                                <td>104</td>
                                <td>账户密钥信息错误</td>
                            </tr>
                            <tr>
                                <td>105</td>
                                <td>验证验证失败</td>
                            </tr>
                            <tr>
                                <td>106</td>
                                <td>充值号码未能查询到运营商信息</td>
                            </tr>
                            <tr>
                                <td>107</td>
                                <td>订单号不存在</td>
                            </tr>
                            <tr>
                                <td>108</td>
                                <td>号码格式错误</td>
                            </tr>
                            <tr>
                                <td>109</td>
                                <td>账户余额不足</td>
                            </tr>
                            <tr>
                                <td>110</td>
                                <td>暂不支付此商品</td>
                            </tr>
                            <tr>
                                <td>111</td>
                                <td>订单录入失败</td>
                            </tr>
                            <tr>
                                <td>112</td>
                                <td>订单号重复</td>
                            </tr>
                            <tr>
                                <td>113</td>
                                <td>请求时间格式错误</td>
                            </tr>
                            <tr>
                                <td>114</td>
                                <td>请求时间超过三分钟</td>
                            </tr>
                            <tr>
                                <td>115</td>
                                <td>充值金额只能是10的倍数元</td>
                            </tr>
                            <tr>
                                <td>116</td>
                                <td>充值金额只能是10的倍数元</td>
                            </tr>
                            <tr>
                                <td>117</td>
                                <td>暂不支持该区域充值</td>
                            </tr>
                            <tr>
                                <td>118</td>
                                <td>代理编码错误</td>
                            </tr>
                            <tr>
                                <td>119</td>
                                <td>请求参数Key数据错误</td>
                            </tr>
                            <tr>
                                <td>120</td>
                                <td>请求参数Param数据错误</td>
                            </tr>
                        </tbody>
                    </table>
                    <h4><a id="2.7"></a>2.7、可充省份</h4>
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>省份名称</th>

                            </tr>
                        </thead>
                        <tbody>

                            <tr>
                                <td>1000</td>
                                <td>全国</td>
                            </tr>
                            <tr>
                                <td>1001</td>
                                <td>北京</td>
                            </tr>
                            <tr>
                                <td>1002</td>
                                <td>上海</td>
                            </tr>
                            <tr>
                                <td>1003</td>
                                <td>天津</td>
                            </tr>
                            <tr>
                                <td>1004</td>
                                <td>重庆</td>
                            </tr>
                            <tr>
                                <td>1005</td>
                                <td>河北</td>
                            </tr>
                            <tr>
                                <td>1006</td>
                                <td>江苏</td>
                            </tr>
                            <tr>
                                <td>1007</td>
                                <td>浙江</td>
                            </tr>
                            <tr>
                                <td>1008</td>
                                <td>广东</td>
                            </tr>
                            <tr>
                                <td>1009</td>
                                <td>山西</td>
                            </tr>
                            <tr>
                                <td>1010</td>
                                <td>辽宁</td>
                            </tr>
                            <tr>
                                <td>1011</td>
                                <td>吉林</td>
                            </tr>
                            <tr>
                                <td>1012</td>
                                <td>黑龙江</td>
                            </tr>
                            <tr>
                                <td>1013</td>
                                <td>安徽</td>
                            </tr>
                            <tr>
                                <td>1014</td>
                                <td>福建</td>
                            </tr>
                            <tr>
                                <td>1015</td>
                                <td>江西</td>
                            </tr>
                            <tr>
                                <td>1016</td>
                                <td>山东</td>
                            </tr>
                            <tr>
                                <td>1017</td>
                                <td>河南</td>
                            </tr>
                            <tr>
                                <td>1018</td>
                                <td>湖北</td>
                            </tr>
                            <tr>
                                <td>1019</td>
                                <td>湖南</td>
                            </tr>
                            <tr>
                                <td>1020</td>
                                <td>四川</td>
                            </tr>
                            <tr>
                                <td>1021</td>
                                <td>贵州</td>
                            </tr>
                            <tr>
                                <td>1022</td>
                                <td>云南</td>
                            </tr>
                            <tr>
                                <td>1023</td>
                                <td>陕西</td>
                            </tr>
                            <tr>
                                <td>1024</td>
                                <td>甘肃</td>
                            </tr>
                            <tr>
                                <td>1025</td>
                                <td>青海</td>
                            </tr>
                            <tr>
                                <td>1026</td>
                                <td>内蒙古</td>
                            </tr>
                            <tr>
                                <td>1027</td>
                                <td>广西</td>
                            </tr>
                            <tr>
                                <td>1028</td>
                                <td>海南</td>
                            </tr>
                            <tr>
                                <td>1029</td>
                                <td>西藏</td>
                            </tr>
                            <tr>
                                <td>1030</td>
                                <td>宁夏</td>
                            </tr>
                            <tr>
                                <td>1031</td>
                                <td>新疆</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
