﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="frmBaseInfo.aspx.cs" Inherits="Recharge.Agent.Interface.frmBaseInfo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <script>
        $(function () {
            var divHeigh = $(window).height() - 50;
            if ($("#divHtml").height() > divHeigh) {
                $("#divHtml").css({ "overflow": "scroll", "height": (divHeigh + "px") });
            }
        })
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="content">
        <div class="row">
            
            <div class="col-sm-3">
                <h3>1、公共方法(.Net 为例)</h3>
                <h4><a href="#1.1" rel="nofollow" target="_self">1.1、 Key值加密方式</a></h4>
                <h4><a href="#1.2" rel="nofollow" target="_self">1.2、 参数排列方式</a></h4>
                <h4><a href="#1.3" rel="nofollow" target="_self">1.3、 拼接参数为URL方式</a></h4>
                <h4><a href="#1.4" rel="nofollow" target="_self">1.4、 MD5加密</a></h4>
            </div>
            <div class="col-sm-9">
                <div style="width: auto; " id="divHtml">
                    <h4> <a id="1.1"></a>1.1、 Key值加密方式</h4>
                    <xmp>
                        验证码(需全小写)，組成方式如下:key=A+B+C(验证码組合方式)
                        A= 无意义字串长度6位
                        B=MD5(Agent + KeyB + yyyyMMdd)
                        C=无意义字串长度8位
                        yyyyMMdd为北京时间(GMT+8)(20150320)
                        public static string EncodeBase64String(string input)
                        {
                        byte[] encbuff = System.Text.Encoding.UTF8.GetBytes(input);
                        return Convert.ToBase64String(encbuff);
                        }
                    </xmp>
                    <h4><a id="1.2"></a>1.2、 参数排列方式</h4>
                    <xmp>
                        public static Dictionary<string, string> SortDictionary(Dictionary<string, string> dic)
                        {
                            List<KeyValuePair<string, string>> myList = new List<KeyValuePair<string, string>>(dic);
                            myList.Sort(delegate (KeyValuePair<string, string> s1, KeyValuePair<string, string> s2)
                            {
                                return s1.Key.CompareTo(s2.Key);
                            });
                            dic.Clear();
                            foreach (KeyValuePair<string, string> pair in myList)
                            {
                                dic.Add(pair.Key, pair.Value);
                            }
                            return dic;
                        }

                    </xmp>
                    <h4><a id="1.3"></a>1.3、 拼接参数为URL方式</h4>
                    <xmp>
                        public static string CreateLinkString(Dictionary<string, string> dicArray, string splitSymbol = "&", bool isRemoveEmpty = false)
                        {
                            StringBuilder prestr = new StringBuilder();
                            foreach (KeyValuePair<string, string> temp in dicArray)
                            {
                                if (isRemoveEmpty)
                                {
                                    if (string.IsNullOrEmpty(temp.Value))
                                    {
                                        continue;
                                    }
                                }
                                prestr.Append($"{temp.Key}={temp.Value}{(string.IsNullOrEmpty(splitSymbol) ? "&" : splitSymbol)}");
                            }
                            int nLen = prestr.Length;
                            prestr.Remove(prestr.Length - 1, 1);
                            return prestr.ToString();
                        }
                    </xmp>
                    <h4><a id="1.4"></a>1.4、 MD5加密</h4>
                    <xmp>
                        public static string ToMD5(this string input)
                        {
                            MD5 md5Hasher = MD5.Create();
                            byte[] data = md5Hasher.ComputeHash(Encoding.UTF8.GetBytes(input));
                            StringBuilder sBuilder = new StringBuilder();
                            for (int i = 0; i < data.Length; i++)
                            {
                                sBuilder.Append(data[i].ToString("x2"));
                            }
                            return sBuilder.ToString();
                        }

                    </xmp>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
