﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="frmUpdateLog.aspx.cs" Inherits="Recharge.Agent.Interface.frmUpdateLog" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="content">
        <div class="row">
            &nbsp;

        </div>
        <div class="row">
            <div class="col-sm-1">
            </div>
            <div class="col-sm-10">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th colspan="3"><h2>充值接口更新日志</h2></th>
                    </tr>
                    <tr>
                        <th>序号</th>
                        <th>更新内容</th>
                        <th>更新时间</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>1</td>
                        <td>更新Key加密方式</td>
                        <td>2018-07-28</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>更新充值接口中参数名:rechrgemoney为：rechargemoney</td>
                        <td>2018-07-28</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>更新订单查询返回状态码说明</td>
                        <td>2018-07-28</td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>订单查询，余额查询，商品查询中增加参数:requestTime</td>
                        <td>2018-07-28</td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>修改接口中所有branchid为branchcode</td>
                        <td>2018-07-28</td>
                    </tr>
                    <tr>
                        <td>6</td>
                        <td>修改接口请求方式为"http://ip:port/api/agendcode/接口类型/接口方法"</td>
                        <td>2018-07-28</td>
                    </tr>
                    </tbody>
                </table>
            </div>


        </div>
    </section>
</asp:Content>
