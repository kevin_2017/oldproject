﻿using Common.DBUtility;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Recharge.Agent
{
    public partial class index : BasePageClass
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                litDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
                GetTotalInfo();
                GetBlance();
            }

        }
        private void GetBlance()
        {
            var model = new BLL.MS_BranchManage().GetModel(BranchLoginInfo.ID);
            litBlance.Text = model.Balance.ToString("f4");
            litFrozenMoney.Text = model.FrozenMoney.ToString("f4");
            litTrueBlance.Text = (model.Balance - model.FrozenMoney).ToString("f4");
        }

        private void GetTotalInfo()
        {
            var sqlInfo = $"select COUNT(case when IspType=1 then 1 else 0 end ) lianTongCount,sum(case when IspType=1 then TotalPrice else 0 end ) lianTongmoney ,COUNT(case when IspType=2 then 1 else 0 end ) yidongCount,sum(case when IspType=2 then TotalPrice else 0 end ) yidongmoney,COUNT(case when IspType=3 then 1 else 0 end ) dianxinCount,sum(case when IspType=3 then TotalPrice else 0 end ) dianxinmoney  from vw_orderManage  where CurentStatus=1  and (BranchCode='{BranchLoginInfo.BranchCode}' or BranchID={BranchLoginInfo.ID})  and (CreateTime  between '" + $"{DateTime.Now.ToString("yyyy-MM-dd")} 00:00:00" + "' and '" + $"{DateTime.Now.ToString("yyyy-MM-dd")} 23:59:59" + "')  ";
            DataTable dtInfo = DBHelper.Query(sqlInfo).Tables[0];

            if (dtInfo.Rows.Count == 1)
            {
                DataRow dr = dtInfo.Rows[0];
                litDianXinCount.Text = dr["dianxinCount"].ToString();
                litDianXinMoney.Text = string.IsNullOrEmpty(dr["dianxinmoney"].ToString()) ? "0" : dr["dianxinmoney"].ToString();
                litYiDongCount.Text = dr["yidongCount"].ToString();
                litYiDongMoney.Text = string.IsNullOrEmpty(dr["yidongmoney"].ToString()) ? "0" : dr["yidongmoney"].ToString();
                litLianTongCount.Text = dr["lianTongCount"].ToString();
                litLianTongMoney.Text = string.IsNullOrEmpty(dr["lianTongmoney"].ToString()) ? "0" : dr["lianTongmoney"].ToString();
            }

            var sqlStr =
                $"select sum(1) totalCounts, sum(case when  curentstatus=1 then 1 else 0 end ) SuccessCount,sum(case when  curentstatus=1 then (totalprice)else 0 end )SuccessMoney ,sum(case when  curentstatus=1 then (totalprice-ProductSalePrice)else 0 end ) RebateMoney,sum(case when  curentstatus=5 then 1 else 0  end ) ErrorCount,sum(case when  curentstatus=5 then totalprice else 0 end ) ErrorMoney,sum(case when  curentstatus=3 then 1 else 0 end ) RechargeingCount,sum(case when  curentstatus=3 then  totalprice else 0 end ) RechargeingMoney from vw_orderManage where (BranchCode='{BranchLoginInfo.BranchCode}' or BranchID={BranchLoginInfo.ID})  and (CreateTime  between '" + $"{DateTime.Now.ToString("yyyy-MM-dd")} 00:00:00" + "' and '" + $"{DateTime.Now.ToString("yyyy-MM-dd")} 23:59:59" + "') ";

            DataTable dt = DBHelper.Query(sqlStr).Tables[0];
            var html = "";
            if (dt.Rows.Count == 1)
            {
                DataRow dr = dt.Rows[0];

                html = $"总充值记录数：{(string.IsNullOrEmpty(dr["totalCounts"].ToString()) ? "0" : dr["totalCounts"].ToString())}，成功记录数：<span style=\"color:red\">{(string.IsNullOrEmpty(dr["SuccessCount"].ToString()) ? "0" : dr["SuccessCount"].ToString())}</span>，成功金额：<span style=\"color:red\">{(string.IsNullOrEmpty(dr["SuccessMoney"].ToString()) ? "0" : dr["SuccessMoney"].ToString())}</span>，返利金额：<span style=\"color:red\">{(string.IsNullOrEmpty(dr["RebateMoney"].ToString()) ? "0" : dr["RebateMoney"].ToString())}</span>，失败记录数：{(string.IsNullOrEmpty(dr["ErrorCount"].ToString()) ? "0" : dr["ErrorCount"].ToString())}，失败金额：{(string.IsNullOrEmpty(dr["ErrorMoney"].ToString()) ? "0" : dr["ErrorMoney"].ToString())}，正在充值数：{(string.IsNullOrEmpty(dr["RechargeingCount"].ToString()) ? "0" : dr["RechargeingCount"].ToString())}，正在充值金额：{(string.IsNullOrEmpty(dr["RechargeingMoney"].ToString()) ? "0" : dr["RechargeingMoney"].ToString())}";
            }

            divTotal.InnerHtml = html;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            GetBlance();
        }
    }
}