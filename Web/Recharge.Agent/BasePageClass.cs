﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Recharge.Agent
{
    public class BasePageClass : System.Web.UI.Page, IHttpHandler
    {

        /// <summary>
        /// 初始化的时候执行的操作
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreInit(EventArgs e)
        {
            if (!IsLogin || HttpContext.Current.Session["BranchLoginInfo"] == null)
            {
                Response.Write("<script>window.top.location.href='/frmlogin.aspx'</script>");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(EventArgs e)
        {
            if (BranchLoginInfo != null && IsLogin && HttpContext.Current.Session["BranchLoginInfo"] != null)
            {
                base.OnLoad(e);
            }
            else
            {
                //Response.Redirect("/frmLogin.aspx", false);
                Response.Write("<script>window.top.location.href='/frmlogin.aspx'</script>");
            }
        }

 
        
        public string url(string btnName, string ID, string DisplayName)
        {
            string url = "";
            switch (btnName)
            {
                case "Delete":
                    //url = "<a href='#' onclick='javascript:OpenURL(\"删除\",\"Delete.aspx?entityid=<%#Eval(" + ID + ") %>\")'>删除</a>";
                    url = "<a href='javascript:void(0);' onclick='javascript:DeleteURL(\"Delete.aspx?entityid=" + ID + "\")'>" + DisplayName + "</a>";
                    break;
                default:
                    url = "<a href='javascript:void(0);' onclick='javascript:OpenURL(\"" + DisplayName + "\",\"" + btnName + ".aspx?entityid=" + ID + "\")'>" + DisplayName + "</a>";
                    break;

            }
            return url;
        }
        public virtual string StrWhere
        {
            get
            {
                if (ViewState["strWhere"] == null)
                {
                    return "";
                }
                else
                    return ViewState["strWhere"].ToString();
            }
            set { ViewState["strWhere"] = value; }
        }
        /// <summary>
        /// true 登录， false 未登录 
        /// </summary>
        public bool IsLogin
        {
            get
            {
                if (HttpContext.Current.Session["BranchLoginInfo"] != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }


            }
        }
        /// <summary>
        /// 根据票证获取登录信息
        /// </summary>
        public static Model.MS_BranchManage BranchLoginInfo
        {
            get
            {
                Model.MS_BranchManage model = new Model.MS_BranchManage();
                if (HttpContext.Current.Session["BranchLoginInfo"] != null)
                    model = HttpContext.Current.Session["BranchLoginInfo"] as Model.MS_BranchManage;
                return model;
            }
        }

       


        #region  SetDropDownList or SetRadioButtonList

        public void SetDropDownListValue(System.Web.UI.WebControls.DropDownList ddl, string dropdownlistSelectValue)
        {
            ddl.SelectedIndex = 0;
            for (int i = 0; i < ddl.Items.Count; i++)
            {
                if (ddl.Items[i].Value == dropdownlistSelectValue)
                {
                    ddl.SelectedIndex = i;
                    break;
                }
            }
        }
        public void SetDropDownListValueByText(System.Web.UI.WebControls.DropDownList ddl, string dropdownlistSelectText)
        {
            ddl.SelectedIndex = 0;
            for (int i = 0; i < ddl.Items.Count; i++)
            {
                if (ddl.Items[i].Text == dropdownlistSelectText)
                {
                    ddl.SelectedIndex = i;
                    break;
                }
            }
        }
        public void SetRadioButtonList(System.Web.UI.WebControls.RadioButtonList ddl, string radioButtonListSelectValue)
        {
            ddl.SelectedIndex = 0;
            for (int i = 0; i < ddl.Items.Count; i++)
            {
                if (ddl.Items[i].Value == radioButtonListSelectValue)
                {
                    ddl.SelectedIndex = i;
                    break;
                }
            }
        }
        #endregion


       

        #region 截取成一定长度的文字

        /// <summary>
        ///截取成一定长度的文字
        /// </summary>
        /// <param name="Htmlstring"></param>
        /// <param name="intLen"></param>
        /// <returns></returns>
        public static string GetCut(string Htmlstring, int intLen)
        {
            //删除脚本
            Htmlstring = Regex.Replace(Htmlstring, @"<script[^>]*?>.*?</script>", "", RegexOptions.IgnoreCase);
            //删除HTML
            Htmlstring = Regex.Replace(Htmlstring, @"<(.[^>]*)>", "", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"([\r\n])[\s]+", "", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"-->", "", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"<!--.*", "", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(quot|#34);", "\"", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(amp|#38);", "&", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(lt|#60);", "<", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(gt|#62);", ">", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(nbsp|#160);", " ", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(iexcl|#161);", "\xa1", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(cent|#162);", "\xa2", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(pound|#163);", "\xa3", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(copy|#169);", "\xa9", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&#(\d+);", "", RegexOptions.IgnoreCase);
            Htmlstring.Replace("<", "");
            Htmlstring.Replace(">", "");
            Htmlstring.Replace("\r\n", "");
            Htmlstring = HttpContext.Current.Server.HtmlEncode(Htmlstring).Trim();
            if (Htmlstring.Length > intLen)
            {
                return Htmlstring = Htmlstring.Substring(0, intLen) + "……";
            }
            else
            {
                return Htmlstring;
            }

        }

        #endregion


        #region
        bool IHttpHandler.IsReusable
        {
            get { throw new NotImplementedException(); }
        }

        void IHttpHandler.ProcessRequest(HttpContext context)
        {
            throw new NotImplementedException();
        }
        #endregion

        protected void ResponseJs(string name, string News)
        {
            this.ClientScript.RegisterStartupScript(this.GetType(), name, "<script>" + News + "</script>");
        }
        protected void ErrorMessage(string News, string t = "")
        {
            this.ClientScript.RegisterStartupScript(this.GetType(), "", "<script>" + t + "layer.alert('" + News + "！', {icon: 2}); </script>");
        }
        protected void SuccessMessage(string News, string t = "")
        {
            this.ClientScript.RegisterStartupScript(this.GetType(), "", "<script>" + t + "layer.alert('" + News + "！', {icon: 1}); </script>");
        }
    }
}