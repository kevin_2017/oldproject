﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmUpdatePwd.aspx.cs" Inherits="Recharge.Agent.frmUpdatePwd" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <!-- Bootstrap 3.3.4 -->
    <link href="/Content/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="/Content/dist/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="/Content/dist/css/ionicons.min.css" rel="stylesheet" type="text/css" />

    <!-- Theme style -->
    <link href="/Content/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins 
    folder instead of downloading all of them to reduce the load. -->
    <link href="/Content/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
    <script src="/Content/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="/Content/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

    <!-- SlimScroll -->
    <script src="/Content/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src='/Content/plugins/fastclick/fastclick.min.js'></script>
    <!-- AdminLTE App -->
    <script src="/Content/dist/js/app.min.js" type="text/javascript"></script>
    <script src="/Content/dist/js/demo.js" type="text/javascript"></script>

    <script src="/Content/layer/layer.js"></script>
    <script src="/Content/jquery.json.min.js"></script>

</head>
<body class="skin-blue">
    <form runat="server" id="form1">
        <section class="content">
            <div class="row">
                <div class="box box-info">
                    <div class="box-header">
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered table-striped">
                            <tbody>
                                <tr>
                                    <td>原始密码：</td>
                                    <td>
                                        <label>
                                            <asp:TextBox ID="txtOldPwd" runat="server" ForeColor="#434C55" class="form-control"></asp:TextBox></label>
                                    </td>

                                </tr>
                                <tr>
                                    <td>新密码：</td>
                                    <td>
                                        <label>
                                            <asp:TextBox ID="txtNewPwd" runat="server" ForeColor="#434C55" class="form-control"></asp:TextBox></label>
                                    </td>
                                </tr>
                                
                            </tbody>
                        </table>
                    </div>
                    <div class="box-footer">
                        <input type="button" class="btn btn-primary" value="保存" onserverclick="btnSaveClick" runat="server" />
                    </div>
                </div>
            </div>


        </section>

    </form>
</body>
</html>
