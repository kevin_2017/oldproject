﻿using Common.DBUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Recharge.Agent
{
    public partial class frmUpdatePwd : BasePageClass
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

            }
        }
        protected void btnSaveClick(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.txtOldPwd.Text.Trim()))
            {
                ErrorMessage("请输入原始密码"); return;
            }
            if (string.IsNullOrEmpty(this.txtNewPwd.Text.Trim()))
            {
                ErrorMessage("请输入新密码"); return;
            }
            var model = new BLL.MS_BranchManage().GetModel(new Model.StrWhere() { IsWhereExist = true, strWhere = new System.Text.StringBuilder($"  LOWER(BranchCode)='{BranchLoginInfo.BranchCode.ToLower()}' and pwd='{ Common.Method.GetMD5(this.txtOldPwd.Text.Trim(), Common.Method.MD5EncryptBit.bit32)}'") });
            if (model == null)
            {
                ErrorMessage("原始密码错误");
                return;
            }
            var result = DBHelper.ExecuteSql($" update MS_BranchManage set pwd='{Common.Method.GetMD5(this.txtNewPwd.Text.Trim(), Common.Method.MD5EncryptBit.bit32)}' where id={BranchLoginInfo.ID}");
            if (result > 0)
            {
                // SuccessMessage("密码修改成功");
                HttpContext.Current.Session.Clear();
                this.ClientScript.RegisterStartupScript(this.GetType(), "", "<script>  layer.alert('密码修改成功！', {icon: 1}); </script>");

            }
        }
    }
}