﻿using Common.DBUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Recharge.Agent
{
    public partial class Main : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (HttpContext.Current.Session["BranchLoginInfo"] == null)
                {
                    Response.Redirect("/frmLogin.aspx");
                }
                else
                {
                    var model = (Model.MS_BranchManage)HttpContext.Current.Session["BranchLoginInfo"];

                    spanUserName.InnerHtml = model.BranchName;
                    spanUserName1.InnerHtml = model.BranchCode;
                    pUserName.InnerHtml = model.BranchCode;

                    GetMenu();

                    var dataset = DBHelper.Query($" select  IspType,count(1) counts from V8_OrderManage where BranchCode='{model.BranchCode}' or BranchID={model.ID} and CONVERT(varchar(10),CreateTime,120)=CONVERT(varchar(10),GETDATE(),120)  group by IspType");
                    if (dataset.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in dataset.Tables[0].Rows)
                        {
                            if (dr["IspType"].ToString() == "1")
                                ltLianTong.Text = dr["counts"].ToString();
                            if (dr["IspType"].ToString() == "2")
                                ltYiDong.Text = dr["counts"].ToString();
                            if (dr["IspType"].ToString() == "3")
                                ltDianXin.Text = dr["counts"].ToString();
                        }

                    }

                }
            }

        }

        private void GetMenu()
        {
            var menuHtml = "<ul class=\"sidebar-menu\">" +
           "<li class=\"header\">MAIN NAVIGATION</li>" +

            "<li class=\"active\"><a href=\"/index.aspx\"><i class=\"fa fa-circle-o\"></i>首页</a></li>" +
           "</li>" +
           "<li class=\"treeview\">" +
           "<a href=\"#\">" +
           "<i class=\"fa fa-files-o\"></i>" +
           "<span>账户管理</span>" +
            "<i class=\"fa fa-angle-left pull-right\"></i>" +
           "</a>" +
           "<ul class=\"treeview-menu\">" +
           "<li><a href=\"/AccoutManage/frmPrivateKeyManage.aspx\"><i class=\"fa fa-circle-o\"></i> 密钥管理</a></li>" +
           "<li><a href=\"/AccoutManage/frmBranchRechargeLog.aspx\"><i class=\"fa fa-circle-o\"></i> 余变动记录</a></li> " +
           "</ul>" +
           "</li>" +
           "<li class=\"treeview\">" +
           "<a href=\"#\">" +
           "<i class=\"fa fa-table\"></i>" +
           "<span>订单管理</span>" +
           "<i class=\"fa fa-angle-left pull-right\"></i>" +
           "</a>" +
           "<ul class=\"treeview-menu\">" +
           "<li><a href=\"/OrderManage/frmRechargeOrderManage.aspx\"><i class=\"fa fa-circle-o\"></i> 订单管理</a></li>" +
           "</ul>" +
           "</li>" +

            "<li class=\"treeview\">" +
           "<a href=\"#\">" +
           "<i class=\"fa fa-folder\"></i>" +
           "<span>接口文档</span>" +
           "<i class=\"fa fa-angle-left pull-right\"></i>" +
           "</a>" +
           "<ul class=\"treeview-menu\">" +
           "<li><a href=\"/Interface/frmUpdateLog.aspx\"><i class=\"fa fa-circle-o\"></i> 更新日志</a></li>" +
            "<li><a href=\"/Interface/frmBaseInfo.aspx\"><i class=\"fa fa-circle-o\"></i> 基础方法</a></li>" +
             "<li><a href=\"/Interface/frmInterfaceInfo.aspx\"><i class=\"fa fa-circle-o\"></i> 接口方法</a></li>" +
           "</ul>" +
           "</li>" +
           "</ul>";
            ltAuthorityInfo.Text = menuHtml;
        }



        protected void LoginOut(object sender, EventArgs e)
        {
            Session.Clear();
            Response.Redirect("/frmlogin.aspx#loginout");
        }
    }
}