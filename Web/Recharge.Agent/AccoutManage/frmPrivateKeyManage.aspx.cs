﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Recharge.Agent.AccoutManage
{
    public partial class frmPrivateKeyManage : BasePageClass
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var model = new BLL.MS_BranchManage().GetModel(BranchLoginInfo.ID);
            hfId.Value = model.ID.ToString();
            txtPassword.Value = model.PrivateKey;
            txtBranchCode.Value = model.BranchCode;
        }
    }
}