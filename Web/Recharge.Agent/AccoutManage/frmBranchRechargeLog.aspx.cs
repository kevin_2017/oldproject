﻿using Common;
using Common.DBUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Recharge.Agent.AccoutManage
{
    public partial class frmBranchRechargeLog : BasePageClass
    {

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                StrWhere = $" and BranchId={BranchLoginInfo.ID}";
                AspNetPager1.CurrentPageIndex = 1;
                InitPage();
            }
        }

        public void InitPage()
        {
            PageAttribute page = new PageAttribute
            {
                TableName = "vw_BranchRechargeLog",
                Columns =
                    @"ID, BranchName, Status, EmployeeCount, ClientCount, RechargeMOney, Balance, AccountType, ChangeType, ChangeMethod, BeforeValue, ChangeValue, AfterValue, Descriptions, CreateTime,BranchId",
                StrOrder = " ID desc",
                WhereCondition = (" and Status=1" + StrWhere),
                PageIndex = AspNetPager1.CurrentPageIndex - 1,
                PageSize = AspNetPager1.PageSize
            };
            DataTable dt = new BLL.V8_OrderManage().GetListByPage(page);
            AspNetPager1.RecordCount = page.TotalRowCount.Value;
            repList.DataSource = dt;
            repList.DataBind();
        }

        protected void AspNetPager1_PageChanging(object src, Wuqi.Webdiyer.PageChangingEventArgs e)
        {
            AspNetPager1.CurrentPageIndex = e.NewPageIndex;
            InitPage();
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            StrWhere = $" and BranchId={BranchLoginInfo.ID}";

            if (ddlChangeType.SelectedItem.Value.Trim() != "-1")
            {
                StrWhere += " and ChangeType='" + ddlChangeType.SelectedItem.Value.Trim() + "'";
            }
            if (txtStartTime.Text.Trim() != "")
            {
                StrWhere += " and   CreateTime  >= '" + txtStartTime.Text.Trim() + "' ";
            }
            if (txtEndTime.Text.Trim() != "")
            {
                StrWhere += " and  CreateTime  <= '" + txtEndTime.Text.Trim() + "' ";
            }
            AspNetPager1.CurrentPageIndex = 1;
            InitPage();
        }
        protected void btnExport_Click(object obj, EventArgs e)
        {
            string fileName = "";

            StrWhere = $" and BranchId={BranchLoginInfo.ID}";

            if (ddlChangeType.SelectedItem.Value.Trim() != "-1")
            {
                fileName += ddlChangeType.SelectedItem.Text + "_";
                StrWhere += " and ChangeType='" + ddlChangeType.SelectedItem.Value.Trim() + "'";
            }
            if (txtStartTime.Text.Trim() != "")
            {
                fileName += txtStartTime.Text.Trim() + "_";
                StrWhere += " and   CreateTime  >= '" + txtStartTime.Text.Trim() + "' ";
            }
            if (txtEndTime.Text.Trim() != "")
            {
                fileName += txtEndTime.Text.Trim() + "";
                StrWhere += " and  CreateTime  <= '" + txtEndTime.Text.Trim() + "' ";
            }
            var TableName = "vw_BranchRechargeLog";
            var Columns = @" ChangeType 变动类型,ChangeMethod 变化方式, BeforeValue 变化前,ChangeValue 变化值,AfterValue 变化后,CreateTime 操作时间,Descriptions 描述";

            var sqlStr = $" select {Columns} from {TableName} where 1=1 {StrWhere} order by CreateTime desc";
            DataSet ds = DBHelper.Query(sqlStr);
            fileName = string.IsNullOrEmpty(fileName)
                ? DateTime.Now.ToString("yyyyMMddHHmmss")
                : fileName.Substring(0, fileName.Length - 1);
            NPOIHelper.ExportExcel(ds.Tables[0], fileName);
        }
    }
}