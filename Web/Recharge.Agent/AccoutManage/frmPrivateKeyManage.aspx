﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="frmPrivateKeyManage.aspx.cs" Inherits="Recharge.Agent.AccoutManage.frmPrivateKeyManage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/Content/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="content">
        <div class="row">
            <div class="box box-info">
                <div class="box-header">
                    <span style="color: red">"代理编码"及"密钥信息"提供给下游，新接口已不使用代理ID</span>
                </div>
                <div class="box-body">
                    <asp:HiddenField runat="server" ID="hfId" />
                    <div class="input-group">
                        <span class="input-group-addon">代理编码</span>
                        <input type="text" class="form-control" placeholder="代理编码" id="txtBranchCode" readonly runat="server" />
                    </div>
                    <br />
                    <div class="input-group">
                        <span class="input-group-addon">密&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;钥</span>
                        <input type="text" class="form-control" placeholder="密钥" id="txtPassword" runat="server" />
                    </div>

                </div>

            </div>
        </div>
    </section>
</asp:Content>
