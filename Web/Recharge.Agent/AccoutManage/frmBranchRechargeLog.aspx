﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="frmBranchRechargeLog.aspx.cs" Inherits="Recharge.Agent.AccoutManage.frmBranchRechargeLog" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/Content/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
    <script src="/Content/My97DatePicker/lang/zh-cn.js"></script>
    <link href="/Content/My97DatePicker/skin/default/datepicker.css" rel="stylesheet" />
    <script src="/Content/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="dataTables_length" id="example1_length">
                            &nbsp; 
                            <label>变动类型：</label>
                            <label>
                                <asp:DropDownList runat="server" ID="ddlChangeType" class="form-control" placeholder="所有记录" aria-controls="datatable-default" Width="150px">
                                    <asp:ListItem Value="-1">--请选择--</asp:ListItem>
                                    <asp:ListItem Value="消费">消费</asp:ListItem>
                                    <asp:ListItem Value="充值">充值</asp:ListItem>
                                    <asp:ListItem Value="返利">返利</asp:ListItem>
                                </asp:DropDownList>
                            </label>
                            &nbsp; 
                            <label>开始时间： </label>
                            <label>
                                <asp:TextBox ID="txtStartTime" runat="server"
                                    onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'%y-%M-%d'})" Width="180px"
                                    ForeColor="#434C55" class="form-control"></asp:TextBox></label>
                            &nbsp; 
                                <label>结束时间：</label>
                            <label>
                                <asp:TextBox ID="txtEndTime" runat="server" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'%y-%M-%d'})"
                                    Width="180px" ForeColor="#434C55" class="form-control"></asp:TextBox></label>
                            &nbsp; 
                            <label>
                                <asp:Button runat="server" ID="btnSearch" Text="查询" CssClass="btn btn-success" OnClick="btnSearch_Click" /></label>
                              &nbsp;
                                    <label>
                                        <asp:Button runat="server" ID="btnExport" Text="导出数据" CssClass="btn btn-success" OnClick="btnExport_Click" /></label>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    
                                    <th>代理商</th>
                                    <th>变动类型</th>
                                    <th>变化方式</th>
                                    <th>变化前</th>
                                    <th>变化值</th>
                                    <th>变化后</th>
                                    <th>操作时间</th>
                                    <th>描述</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater runat="server" ID="repList">
                                    <ItemTemplate>
                                        <tr>
                                            
                                            <td>
                                                <%#Eval("BranchName")%>
                                            </td>
                                            <td style="text-align: center; vertical-align: middle">
                                                <%#Eval("ChangeType")%>
                                            </td>
                                            <td style="text-align: center; vertical-align: middle">
                                                <%#Eval("ChangeMethod")%>
                                            </td>
                                            <td style="text-align: center; vertical-align: middle">
                                                <%#Eval("BeforeValue")%>
                                            </td>
                                            <td style="text-align: center; vertical-align: middle">
                                                <%#Eval("ChangeValue")%>
                                            </td>
                                            <td style="text-align: center; vertical-align: middle">
                                                <%#Eval("AfterValue")%>
                                            </td>
                                            <td style="text-align: center; vertical-align: middle">
                                                <%#Eval("CreateTime")%>
                                            </td>
                                            <td>
                                                <%#Eval("Descriptions")%>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>

                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-5">
                        <div class="dataTables_info" id="example2_info1" role="status" aria-live="polite">总计 <%=AspNetPager1.RecordCount %>条记录，每页显示<%=AspNetPager1.PageSize %>条 </div>
                    </div>

                    <div class="col-sm-7">
                        <div class="dataTables_paginate" id="example1_paginate">
                            <webdiyer:AspNetPager ShowPageIndexBox="Never" ID="AspNetPager1" runat="server" Width="100%" PageSize="16" PrevPageText="上一页" NextPageText="下一页" AlwaysShow="true" OnPageChanging="AspNetPager1_PageChanging" ShowFirstLast="False" PagingButtonsClass="paginate_button" CssClass="pagination" LayoutType="Ul" PagingButtonLayoutType="UnorderedList" PagingButtonSpacing="0" CurrentPageButtonClass="active" PrevNextButtonsClass="paginate_button previous">
                            </webdiyer:AspNetPager>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</asp:Content>
