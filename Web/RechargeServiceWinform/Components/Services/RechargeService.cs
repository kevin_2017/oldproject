﻿using CE.Utility;
using Helper;
using Model;
using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Messaging;
using System.Text;
using Common.DBUtility;
using RechargeServiceWinform.Components.RechargeService;
using Newtonsoft.Json;

namespace RechargeServiceWinform.Components.Services
{
    public class RechargeService : BaseService
    {

        #region 获取待发送的第三方充值记录

        public bool GetRecordData()
        {
            var result = true;
            try
            {
                var rechargeQueuName = ConfigurationManager.AppSettings["RechargeOrderQueueName"];

                var msmq = MsmqHelper.GetInstance(rechargeQueuName);
                try
                {
                    MessageQueueTransaction trans = null;
                    if (msmq.Transactional)
                    {
                        trans = new MessageQueueTransaction();
                    }
                    msmq.ReceiveCompleted += new ReceiveCompletedEventHandler(DoRecharge);
                    trans?.Begin();
                    msmq.BeginReceive();
                    trans?.Commit();
                }
                catch (Exception ex)
                {
                    WriteWinFormLog.WriteError(DateTime.Now.ToString("yyyy-MM-dd"), "{GetRecordData},发生其它错误", ex);
                }
            }
            catch (Exception ex)
            {
                WriteWinFormLog.WriteError(DateTime.Now.ToString("yyyy-MM-dd"), "{GetRecordData},发生其它错误", ex);
                result = false;
            }
            return result;
        }

        #endregion


        /// <summary>
        /// 消息队列处理器
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void DoRecharge(object sender, ReceiveCompletedEventArgs e)
        {
            var rechargeQueue = sender as MessageQueue;
            if (rechargeQueue == null)
            {
                WriteWinFormLog.Write("充值服务", $"没有请求队列");
                return;
            }
            try
            {
                //获取当前Message
                var msg = rechargeQueue.EndReceive(e.AsyncResult);   //e.Message
                if (msg == null)
                {
                    WriteWinFormLog.Write("充值服务", $"没有数据");
                    return;
                }
                msg.Formatter = new XmlMessageFormatter(new Type[] { typeof(string) });
                var orderNo = msg.Body.ToString();
                var orderInfo = new BLL.V8_OrderManage().GetModel(orderNo);

                if (orderInfo == null)
                    return;

                DBHelper.ExecuteSql($"update V8_OrderManage set  CurentStatus=3  where OrderNo='{orderNo}'");
                //var orderNo = orderInfo.OrderNo;

                //orderInfo = new BLL.V8_OrderManage().GetModel(orderNo);


                #region 充值

                var branchModel = new BLL.MS_BranchManage().GetModel(orderInfo.BranchID);

                //Model.StrWhere str = new StrWhere();

                //str.IsWhereExist = true;

                //str.strWhere = branchModel.RechargeType == 0 ? new StringBuilder($" Status=1001 ") : new StringBuilder($" status=1001 and id in (select SupplierId from MS_Branch_Supplier where BranchId={orderInfo.BranchID})  order by sort");

                DataTable suppList = new DataTable();

                if (orderInfo.CityId > 0)
                {
                    suppList = DBHelper.Query($"select ProvinceId,BranchProductId,ProductID,BranchID,BranchCode,ISPType,ISPName,ProvinceName,suppCityId,suppCityName,SupplierId,SupplierName  from vw_Branch_Product_Supplier where BranchID={branchModel.ID} and ISPType={orderInfo.IspType} and ProductID={orderInfo.ProductID} and ProvinceId={orderInfo.ProvinceId} and suppCityId={orderInfo.CityId} order by sort").Tables[0];
                }
                if (suppList == null || suppList.Rows.Count == 0)
                {
                    suppList = DBHelper.Query($"select ProvinceId,BranchProductId,ProductID,BranchID,BranchCode,ISPType,ISPName,ProvinceName,suppCityId,suppCityName,SupplierId,SupplierName  from vw_Branch_Product_Supplier where BranchID={branchModel.ID} and ISPType={orderInfo.IspType} and ProductID={orderInfo.ProductID} and ProvinceId={orderInfo.ProvinceId} order by sort").Tables[0];
                }
                
                var orderRequestList = new BLL.V8_OrderRequestRecs().GetModelList(new StrWhere() { IsWhereExist = true, strWhere = new StringBuilder($" OrderNo='{orderInfo.OrderNo}' ") });

                bool isRequest = false;
                WriteWinFormLog.Write("充值服务", $"订单号:{orderInfo.OrderNo},查询可充值接口数：{suppList.Rows.Count}");

                if (suppList.Rows.Count > 0)
                {
                    foreach (DataRow dr in suppList.Rows)
                    {
                        var model = new BLL.SUP_SupplierManage().GetModel(Convert.ToInt64(dr["SupplierId"]));

                        if (model == null)
                            continue;

                        // var supplierInfo = new BLL.SUP_SupplierManage().GetModel(model.ApiCode);
                        if (model.Status != 1001 && model.Status != 1004)
                            continue;

                        //if (model.ApiCode.Equals("Jiaofei365") && orderInfo.TotalPrice % 10 != 0)//只能接收10的倍数元
                        //    continue;

                        if (orderRequestList.FirstOrDefault(x => x.SupplierId == model.ID && x.OrderNo.Equals(orderInfo.OrderNo)) != null)
                            continue;

                        var factory = new RechargeFactroy();
                        var recharge = factory.GetInstance(model.ApiCode);
                        WriteWinFormLog.Write("充值服务", $"订单号:{orderInfo.OrderNo},充值接口：{model.ApiCode}");
                        if (recharge != null)
                        {
                            recharge.PhoneType = orderInfo.PhoneType;
                            recharge.ApiCode = model.ApiCode;
                            recharge.FlowNoticeUrl = model.FlowNoticeUrl;
                            recharge.FlowPayDomain = model.FlowPayDomain;
                            recharge.FlowSearchUrl = model.FlowSearchUrl;
                            recharge.ProductId = orderInfo.ProductID;
                            recharge.OrderNo = orderInfo.OrderNo;
                            recharge.RechargeMoney = orderInfo.TotalPrice;
                            recharge.RechargeNoticeUrl = model.RechargeNoticeUrl;
                            recharge.RechargePayDomain = model.RechargePayDomain;
                            recharge.RechargeSearchUrl = model.RechargeSearchUrl;
                            recharge.TelePhone = orderInfo.RechargeNo;
                            recharge.Gateway = model.GateWay;
                            recharge.SupplierId = model.ID;

                            var result = recharge.Process();
                            WriteWinFormLog.Write("充值服务", $"订单号:{orderInfo.OrderNo},充值接口：{model.ApiCode}，充值结果；{JsonConvert.SerializeObject(result)}");
                            if (result != null && result.Success)
                            {
                                isRequest = true;
                                break;
                            }
                        }

                    }
                    if (!isRequest)
                    {
                        // rechargeQueue.Send(orderInfo.OrderNo);
                        WriteWinFormLog.Write("充值服务", $"订单号:{orderInfo.OrderNo},充值失败");
                        DBHelper.ExecuteSql($"update V8_OrderManage set CallBackTime=getdate(),CurentStatus=5,NotifyStatus=0 where OrderNo='{orderInfo.OrderNo}'");

                        var notifyQueueName = ConfigurationManager.AppSettings["RechargeNotifyQueueName"];

                        var notifyQueue = new MsmqHelper(notifyQueueName);
                        notifyQueue.Send<string>(orderInfo.OrderNo);
                    }
                }
                else
                {
                    DBHelper.ExecuteSql($"update V8_OrderManage set CallBackTime=getdate(),CurentStatus=5,NotifyStatus=0,Descriptions='没可充值接口' where OrderNo='{orderInfo.OrderNo}'");
                    WriteWinFormLog.Write("充值服务", $"订单号:{orderInfo.OrderNo},充值失败");
                    var notifyQueueName = ConfigurationManager.AppSettings["RechargeNotifyQueueName"];

                    var notifyQueue = new MsmqHelper(notifyQueueName);
                    notifyQueue.Send<string>(orderInfo.OrderNo);
                }

                #endregion
            }
            catch (Exception ex)
            {
                WriteWinFormLog.WriteError(DateTime.Now.ToString("yyyy-MM-dd"), $"充值服务", ex);
            }
            rechargeQueue.BeginReceive();
        }
    }
}
