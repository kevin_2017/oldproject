﻿using CE.Utility;
using CE.Utility.Text;
using CE.Utility.Web;
using Common.DBUtility;
using Helper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Messaging;
using System.Net;

namespace RechargeServiceWinform.Components.Services
{
    public class NotifyService : BaseService
    {
        string msmqName = ConfigurationManager.AppSettings["RechargeNotifyQueueName"];


        public bool GetNotifyData()
        {
            var result = true;
            try
            {
                var msmq = MsmqHelper.GetInstance(msmqName);
                try
                {
                    MessageQueueTransaction trans = null;
                    if (msmq.Transactional)
                    {
                        trans = new MessageQueueTransaction();
                    }
                    msmq.ReceiveCompleted += new ReceiveCompletedEventHandler(DoNotify);
                    trans?.Begin();
                    msmq.BeginReceive();
                    trans?.Commit();
                }
                catch (Exception ex)
                {
                    WriteWinFormLog.WriteError(DateTime.Now.ToString("yyyy-MM-dd"), "{通知服务.GetNotifyData},发生其它错误", ex);
                }
            }
            catch (Exception ex)
            {
                WriteWinFormLog.WriteError(DateTime.Now.ToString("yyyy-MM-dd"), "{通知服务.GetRecordData},发生其它错误", ex);
                result = false;
            }
            return result;
        }
        public void DoNotify(object sender, ReceiveCompletedEventArgs e)
        {

            var msmq = sender as MessageQueue;
            if (msmq == null)
            {
                WriteWinFormLog.Write("通知服务.DoNotify", $"没有队列请求");
                return;
            }
            try
            {
                //获取当前Message
                var msg = msmq.EndReceive(e.AsyncResult);   //e.Message
                if (msg == null)
                {
                    WriteWinFormLog.Write("通知服务.DoRecharge", "消息队列中没有数据");
                    return;
                }
                msg.Formatter = new XmlMessageFormatter(new Type[] { typeof(string) });
                var orderNo = msg.Body.ToString();
                var orderInfo = new BLL.V8_OrderManage().GetModel(orderNo);

                if (orderInfo == null)
                    return;

                if (orderInfo.CurentStatus != 1 && orderInfo.CurentStatus != 5)
                {
                    var notifyMsmq = new MsmqHelper(msmqName);
                    notifyMsmq.Send(orderNo);
                    return;
                }

                if (orderInfo.NoticeCount == 0 && orderInfo.NotifyStatus == 0)
                {
                    //修改代理的冻结金额
                    DBHelper.ExecuteSql($"update MS_BranchManage set FrozenMoney=FrozenMoney-{orderInfo.TotalPrice} where ID={orderInfo.BranchID}");
                }


                #region 通知
                DBHelper.ExecuteSql($"update V8_OrderManage set NotifyStatus=3 where OrderNo='{orderInfo.OrderNo}'");
                var branchModel = new BLL.MS_BranchManage().GetModel(orderInfo.BranchID);
                var status = orderInfo.CurentStatus == 1 ? "1" : "0";

                var branchid = branchModel.BranchCode.ToString();

                Dictionary<string, string> dic = new Dictionary<string, string>() {

                    {"branchorderno",orderInfo.BranchOrderNo},
                    {"status",status},
                    {"extendparam",orderInfo.ExtendParam}//自定义扩展参数，原样返回
                };
                var branchids = ConfigurationManager.AppSettings["BranchIds"].ToString();
                string[] branchidArray = branchids.Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                if (branchidArray.Contains(branchModel.ID.ToString()))
                {
                    branchid = branchModel.ID.ToString();
                    dic.Add("branchid", branchid);
                }
                else
                {
                    dic.Add("branchcode", branchid);
                }

                var signStr = $"{Tools.CreateLinkString(Tools.SortDictionary(dic), "&", true)}&key={branchModel.PrivateKey}";

                WriteWinFormLog.Write("通知服务.DoNotify", $"{orderInfo.OrderNo}  通知下游数据   {signStr}");

                dic.Add("sign", signStr.ToMD5().ToLower());

                var param = Tools.CreateLinkString(Tools.SortDictionary(dic)).EncodeBase64String();
                var key = branchModel.PrivateKey.EncodeBase64String();

                WriteWinFormLog.Write("通知服务.DoNotify", $"{orderInfo.OrderNo}  通知下游数据   NotifyUrl：{orderInfo.NotifyUrl},Param:{param},Key:{key}");

                for (int i = 0; i < 5; i++)
                {
                    var httpRequestUtil = new HttpRequestUtil($"{orderInfo.NotifyUrl}", HttpRequestUtil.Method.Post, $"Param={param}&Key={key}");
                    var httpResult = httpRequestUtil.Request();
                    var html = httpResult.Html;

                    var notifyResult = 0;

                    WriteWinFormLog.Write("通知服务.DoNotify", $"{orderInfo.OrderNo}  通知下游返回信息     httpResult.StatusCode={httpResult.StatusCode}");

                    WriteWinFormLog.Write("通知服务.DoNotify", $"{orderInfo.OrderNo}  通知下游返回数据     {html}");

                    if ((httpResult.StatusCode == HttpStatusCode.OK ||
        httpResult.StatusCode == HttpStatusCode.InternalServerError) && html.Trim().ToUpper().Equals("SUCCESS"))
                        notifyResult = 1;

                    var notifyId = new BLL.V8_OrderNotifyRecs().Add(new Model.V8_OrderNotifyRecs()
                    {
                        OrderNo = orderInfo.OrderNo,
                        BranchId = orderInfo.BranchID,
                        NotifyTime = DateTime.Now,
                        NotifyConent = $"{Tools.CreateLinkString(Tools.SortDictionary(dic))}",
                        NotifyResult = notifyResult,
                        NotifyResponse = $"订单充值状态信息[CurentStatus：{orderInfo.CurentStatus},{(orderInfo.CurentStatus == 1 ? "充值成功" : "充值失败")}],下游通知状态信息[status：{status}]"
                    });

                    //修改通知次数
                    DBHelper.ExecuteSql($"update V8_OrderManage set NoticeCount=NoticeCount+1 where OrderNo='{orderInfo.OrderNo}'");

                    if (notifyResult == 1)
                    {
                        //修改订单的通知状态
                        DBHelper.ExecuteSql($"update V8_OrderManage set NotifyStatus=1 where OrderNo='{orderInfo.OrderNo}'");
                        break;
                    }
                    i++;
                }
                #endregion

            }
            catch (Exception ex)
            {
                WriteWinFormLog.WriteError(DateTime.Now.ToString("yyyy-MM-dd"), $"通知服务.DoNotify", ex);
            }
            msmq.BeginReceive();
        }
    }
}
