﻿using CE.Utility.Text;
using CE.Utility.Web;
using Common.DBUtility;
using Helper;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Xml;
using RechargeServiceWinform.Entitys;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace RechargeServiceWinform.Components.RechargeService
{
    public class HunanwangdaService : RechargeBase
    {
        #region 基础数据 
        private string merCode = string.Empty;
        private string merKey = string.Empty;
        #endregion
        public override RechargeResult Process()
        {
            var resp = new RechargeResult();
            var reqCon = "";
            var respCon = "";
            try
            {
                var keys = Tools.GetKeys(this.ApiCode);
                merCode = keys[0].ConfigValue;
                merKey = keys[1].ConfigValue;

                var Action = "Pay";
                var User = merCode;
                var Mobile = TelePhone;
                var Money = RechargeMoney.ToString("f0");
                var BackUrl = $"{this.Gateway}{this.RechargeNoticeUrl}";
                var signStr = $"{User}{Mobile}{Money}{OrderNo}{merKey}";
                var Sign = signStr.ToMD5();

                var dic = new Dictionary<string, string>()
                {
                    { "Action",Action},
                    { "User",User},
                    { "Mobile",Mobile},
                    { "Money",Money},
                    { "OrderNo",OrderNo},
                    { "BackUrl",BackUrl},
                    { "Sign",Sign},
                };

                reqCon = GenGetParams(dic);
                WriteWinFormLog.Write($"{OrderNo} HunanwangdaService.Process", $"充值请求数据：{RechargePayDomain},{reqCon}    加密原始数据：{signStr}");

                // 从远程接口获取
                var httpRequestUtil = new HttpRequestUtil($"{RechargePayDomain}", HttpRequestUtil.Method.Post, reqCon);
                var httpResult = httpRequestUtil.Request();
                var html = httpResult.Html;
                WriteWinFormLog.Write($"{OrderNo} HunanwangdaService.Process", $"充值返回数据：{html}");
                respCon = html;
                if (httpResult.StatusCode == HttpStatusCode.OK || httpResult.StatusCode == HttpStatusCode.InternalServerError)
                {
                    if (!html.Contains("|"))
                    {
                        resp.Success = false;
                        resp.Message = $"访问第三方服务器出错:";
                    }
                    else
                    {
                        var result = html.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                        if (result.Length > 0)
                        {
                            if ((result[0].Equals("success") || result[0].Equals("wait")))
                            {
                                resp.Success = true;
                                resp.Message = $"请求成功,返回信息：{GetErrMsg(result[0])}";
                            }
                            else
                            {
                                resp.Success = false;
                                resp.Message = $"请求失败,返回信息：{GetErrMsg(result[1])}";
                            }
                        }
                        else
                        {
                            resp.Success = false;
                            resp.Message = $"请求失败,返回信息：{html}";
                        }
                    }
                }
                else
                {
                    resp.Success = false;
                    resp.Message = $"访问第三方服务器出错,状态:{httpResult.StatusCode},状态码:{(int)httpResult.StatusCode}";
                }
            }
            catch (Exception ex)
            {
                WriteWinFormLog.WriteError(DateTime.Now.ToString("yyyy-MM-dd"), $"{OrderNo} HunanwangdaService.Process", ex);
            }

            #region 增加充值请求及返回数据

            var orderResquestData = new Model.V8_OrderRequestRecs()
            {
                SupplierId = SupplierId,
                OrderNo = OrderNo,
                RequestTime = DateTime.Now,
                RequestContent = reqCon,
                ResponseContent = $"{respCon},{ resp.Message}",
            };

            OperInfo(orderResquestData, resp.Success);

            #endregion
            WriteWinFormLog.Write($"{OrderNo} HunanwangdaService.Process", $"充值请求结果：{JsonConvert.SerializeObject(resp)}");

            return resp;
        }



        private string GetErrMsg(string status)
        {
            var msg = "";
            switch (status)
            {
                case "wait":
                    msg = "订单缴费等待";
                    break;
                case "success":
                    msg = "缴费成功";
                    break;

                case "fail":
                    msg = "失败";
                    break;
                case "Doubt":
                    msg = "存疑订单";
                    break;
                case "Action Error or Null":
                    msg = "调用方式错误或空";
                    break;
                case "Unauthorized access":
                    msg = "用户未被授权";
                    break;
                case "Mobile Error or Null":
                    msg = "号码错误或空";
                    break;
                case "OrderNo Error or Null":
                    msg = "订单错误或空";
                    break;
                case "Mobile_APIS_NetError":
                    msg = "接口网络错误(移动)";
                    break;
                case "Mobile_APIS_closed":
                    msg = "接口关闭或暂停(移动)";
                    break;
                case "Unicom_APIS_NetError":
                    msg = "接口网络错误(联通)";
                    break;
                case "Unicom_APIS_closed":
                    msg = "接口关闭或暂停(联通)";
                    break;
                case "Telecom_APIS_NetError":
                    msg = "接口网络错误(电信)";
                    break;
                case "Telecom_APIS_closed":
                    msg = "接口关闭或暂停(电信) ";
                    break;
                case "No data or Error!":
                    msg = "数据为空或错误";
                    break;
                case "Mobile User Money Null or Error":
                    msg = "号码 用户或金额为空或错误";
                    break;
                case "Insufficient account balance":
                    msg = "用户余额不足";
                    break;
                case "The same Mobile, After X Seconds to re submit":
                    msg = "重号,请在X秒后重新提交";
                    break;
                case "Failure of the network or other reasons":
                    msg = "网络中断或其它原因失败";
                    break;
                case "Duplicate Order":
                    msg = "因订单号重复失败";
                    break;
            }
            return msg;
        }

    }
}
