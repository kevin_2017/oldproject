﻿using CE.Utility.Text;
using CE.Utility.Web;
using Common.DBUtility;
using Helper;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Xml;
using RechargeServiceWinform.Entitys;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace RechargeServiceWinform.Components.RechargeService
{
    public class LichuangService : RechargeBase
    {
        #region 基础数据 
        private string merCode = string.Empty;
        private string merKey = string.Empty;
        private string GetKeyB()
        {
            var newStr = $"{merCode}{merKey}{DateTime.Now.ToString("yyyyMMdd")}".ToMD5().ToLower();
            newStr = $"{StringUtil.GetRandomString(6)}{newStr}{StringUtil.GetRandomString(8)}";
            return newStr;
        }
        #endregion
        public override RechargeResult Process()
        {
            var resp = new RechargeResult();
            var reqCon = "";
            var respCon = "";
            try
            {
                var keys = Tools.GetKeys(this.ApiCode);
                merCode = keys[0].ConfigValue;
                merKey = keys[1].ConfigValue;


                var dic = new Dictionary<string, string>() {
                {"branchcode",merCode},
                {"branchorderno",OrderNo},
                {"requesttime",DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")},
                {"phonenum",TelePhone},

                {"rechargemoney",RechargeMoney.ToString("f0")},
                {"notifyurl",RechargeNoticeUrl},
                {"extendparam","test"},//自定义扩展参数，原样返回
            };

                var signStr = $"{Tools.CreateLinkString(Tools.SortDictionary(dic), "&", true)}&key={merKey}";
                dic.Add("sign", signStr.ToMD5().ToLower());

                var Param = Tools.CreateLinkString(Tools.SortDictionary(dic)).EncodeBase64String();
                var Key = GetKeyB().EncodeBase64String();
                reqCon = $"Param={Param}&Key={Key}";
                WriteWinFormLog.Write($"{OrderNo} LichuangService.Process", $"充值请求数据：{RechargePayDomain}/api/{merCode}/RechargeApi/PhoneRecharge,{reqCon}    加密原始数据：{signStr}");

                // 从远程接口获取
                var httpRequestUtil = new HttpRequestUtil($"{RechargePayDomain}/api/{merCode}/RechargeApi/PhoneRecharge", HttpRequestUtil.Method.Post, reqCon);
                var httpResult = httpRequestUtil.Request();
                var html = httpResult.Html;
                WriteWinFormLog.Write($"{OrderNo} LichuangService.Process", $"充值返回数据：{html}");
                respCon = html;
                if (httpResult.StatusCode == HttpStatusCode.OK || httpResult.StatusCode == HttpStatusCode.InternalServerError)
                {
                    var result = (rechargeResult)JsonConvert.DeserializeObject(JsonConvert.DeserializeObject(html).ToString(), typeof(rechargeResult));
                    if (result.Success && result.Code.Equals("000"))
                    {
                        if (result.CurrentStatus.Equals("PAYING"))
                        {
                            resp.Success = true;
                            resp.Message = $"请求成功,返回信息：{result.Message}";
                        }
                        else {
                            resp.Success = false;
                            resp.Message = $"请求成功,返回信息：{result.Message}";
                        }

                    }
                    else
                    {
                        resp.Success = false;
                        resp.Message = $"请求失败,返回信息：{result.Message}";
                    }
                    
                }
                else
                {
                    resp.Success = false;
                    resp.Message = $"访问第三方服务器出错,状态:{httpResult.StatusCode},状态码:{(int)httpResult.StatusCode}";
                }
            }
            catch (Exception ex)
            {
                WriteWinFormLog.WriteError(DateTime.Now.ToString("yyyy-MM-dd"), $"{OrderNo} LichuangService.Process", ex);
            }

            #region 增加充值请求及返回数据

            var orderResquestData = new Model.V8_OrderRequestRecs()
            {
                SupplierId = SupplierId,
                OrderNo = OrderNo,
                RequestTime = DateTime.Now,
                RequestContent = reqCon,
                ResponseContent = $"{respCon},{ resp.Message}",
            };

            OperInfo(orderResquestData, resp.Success);

            #endregion
            WriteWinFormLog.Write($"{OrderNo} LichuangService.Process", $"充值请求结果：{JsonConvert.SerializeObject(resp)}");

            return resp;
        }


    }

    public class rechargeResult
    {
        public bool Success { get; set; }

        public string Code { get; set; }

        public string Message { get; set; }

        public string CurrentStatus { get; set; }

        public decimal Data { get; set; }

        public string BranchOrderNo { get; set; }

        public string BranchId { get; set; }

    }

}
