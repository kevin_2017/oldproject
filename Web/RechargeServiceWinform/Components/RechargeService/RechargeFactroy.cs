﻿using System;

namespace RechargeServiceWinform.Components.RechargeService
{
    public class RechargeFactroy
    {
        public RechargeBase GetInstance(string code)
        {
            if (string.IsNullOrEmpty(code)) return null; 

            string typename = $"RechargeServiceWinform.Components.RechargeService.{code}Service";

            Type t = Type.GetType(typename);
            if (t == null) return null;

            object obj = Activator.CreateInstance(t);
            if (obj == null) return null;

            RechargeBase paymentBase = (RechargeBase)obj;
            if (paymentBase == null) return null;

            return paymentBase;
        }
    }
}
