﻿using CE.Utility.Text;
using CE.Utility.Web;
using Common.DBUtility;
using Helper;
using RechargeServiceWinform.Entitys;
using System;
using System.Collections.Generic;
using System.Net;
using Newtonsoft.Json;

namespace RechargeServiceWinform.Components.RechargeService
{
    public class Jiaofei100Service : RechargeBase
    {
        #region 基础数据
        string fileName = DateTime.Now.ToString("yyyy-MM-dd");
        private string merCode = string.Empty;
        private string merKey = string.Empty;
        #endregion

        public override RechargeResult Process()
        {
            var resp = new RechargeResult();
            var supplierInfo = new BLL.SUP_SupplierManage().GetModel(this.ApiCode);

            var reqCon = "";
            var respCon = "";

            var keys = Tools.GetKeys(this.ApiCode);
            merCode = keys[0].ConfigValue;
            merKey = keys[1].ConfigValue;

            var apiid = merCode;
            var tradeType = "10";//10、话费直充，11、话费慢充
            var orderId = OrderNo;
            var account = TelePhone;
            var unitPrice = RechargeMoney * 1000;
            var totalPrice = unitPrice;
            if (unitPrice % 10 != 0)
            {
                resp.Success = false;
                resp.Message = "充值金额只能为整数";
                return resp;
            }
            if (supplierInfo.Status != 1001 && supplierInfo.Status != 1004)
            {
                resp.Success = false;
                resp.Message = "该接口暂不能使用";
                return resp;
            }

            var signStr =
                $"APIID={apiid}&Account={account}&BuyNum=1&CreateTime={DateTime.Now:yyyyMMddHHmmss}&isCallBack=1&OrderID={orderId}&TotalPrice={totalPrice.ToString("f0")}&TradeType={tradeType}&UnitPrice={unitPrice.ToString("f0")}&APIKEY={merKey}";
            WriteWinFormLog.Write("Jiaofei100Service.Process", $"{OrderNo},充值请求加密原始数据：{signStr}");
            var sign = signStr.ToMD5().ToUpper();

            var dic = new Dictionary<string, string>()
            {
                { "APIID",apiid},
                { "TradeType",tradeType},
                { "Account",account},
                { "UnitPrice",unitPrice.ToString("f0")},
                { "BuyNum","1"},
                { "TotalPrice",totalPrice.ToString("f0")},
                { "OrderID",orderId},
                { "CreateTime",DateTime.Now.ToString("yyyyMMddHHmmss")},
                { "IsCallBack","1"},
                { "Sign",sign},
            };
            var paramData = GenGetParams(dic);
            reqCon = paramData;
            WriteWinFormLog.Write( "Jiaofei100Service.Process", $"{OrderNo},充值请求数据：{RechargePayDomain},{paramData}");
            var requestTime = DateTime.Now;
            // 从远程接口获取
            var httpRequestUtil = new HttpRequestUtil($"{RechargePayDomain}?{paramData}", HttpRequestUtil.Method.Get);
            var httpResult = httpRequestUtil.Request();
            var html = httpResult.Html;
            respCon = html;
            WriteWinFormLog.Write("Jiaofei100Service.Process", $"{OrderNo},充值返回数据：{html}");
            if (httpResult.StatusCode == HttpStatusCode.OK ||
        httpResult.StatusCode == HttpStatusCode.InternalServerError)
            {
                if (!html.Contains("Code"))
                {
                    resp.Success = false;
                    resp.Message = $"访问第三方服务器出错";
                }
                else
                {
                    var result = JsonConvert.DeserializeAnonymousType(html, new { Code = "", Msg = "", OrderID = "", Account = "", UnitPrice = "", ReturnOrderID = "", TradingID = "" });
                    if (result.Code.Equals("10018"))
                    {
                        resp.Success = true;
                        resp.Message = "请求成功";
                    }
                    else
                    {
                        var msg = ErrorMsg(result.Code);
                        resp.Success = false;
                        resp.Message = $"请求失败,返回信息：{result.Code}，{msg}";
                    }
                }
            }
            else
            {
                resp.Success = false;
                resp.Message = $"访问第三方服务器出错,状态:{httpResult.StatusCode},状态码:{(int)httpResult.StatusCode}";
            }

            #region 增加充值请求及返回数据


            var orderResquestData = new Model.V8_OrderRequestRecs()
            {
                SupplierId = SupplierId,
                OrderNo = OrderNo,
                RequestTime = DateTime.Now,
                RequestContent = reqCon,
                ResponseContent = $"{respCon},{ resp.Message}",
            };

            OperInfo(orderResquestData, resp.Success);

            #endregion
            WriteWinFormLog.Write("Jiaofei100Service.Process", $"{OrderNo},充值请求结果：{JsonConvert.SerializeObject(resp)}");

            return resp;
        }


        private string ErrorMsg(string errCode)
        {
            var msg = "";
            switch (errCode)
            {
                case "10007":
                    msg = "参数错误";
                    break;
                case "10008":
                    msg = "订单超时";
                    break;
                case "10009":
                    msg = "参数校验错误";
                    break;
                case "10010":
                    msg = "代理商 ID 不存在";
                    break;
                case "10011":
                    msg = "订单号长度大于36";
                    break;
                case "10012":
                    msg = "代理商状态错误";
                    break;
                case "10013":
                    msg = "账户余额不足";
                    break;
                case "10014":
                    msg = "IP 地址验证失败";
                    break;
                case "10015":
                    msg = "充值号码有误";
                    break;
                case "10016":
                    msg = "暂不支持该号码";
                    break;
                case "10017":
                    msg = "禁止采购该商品";
                    break;
                case "10018":
                    msg = "订单提交成功";
                    break;
                case "10020":
                    msg = "订单提交失败";
                    break;
                case "10021":
                    msg = "未知错误";
                    break;
                case "10022":
                    msg = "订单号重复";
                    break;
                case "10024":
                    msg = "暂不支持该面值";
                    break;
                case "10025":
                    msg = "订单处理中";
                    break;
                case "10026":
                    msg = "交易失败";
                    break;
                case "10027":
                    msg = "交易成功";
                    break;
                case "10029":
                    msg = "订单不存在";
                    break;
            }

            return msg;
        }
    }
}
