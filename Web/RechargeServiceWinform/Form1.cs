﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RechargeServiceWinform.Components.Services;

namespace RechargeServiceWinform
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Components.Services.RechargeService rechargeService = new Components.Services.RechargeService();
            rechargeService.GetRecordData();

            NotifyService notifyService = new NotifyService();
            notifyService.GetNotifyData();
        }
    }
}
