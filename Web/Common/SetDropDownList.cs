﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

namespace Common
{
    public static class SetDropDownList
    {
        public static void BindDropDownList(DropDownList control, IDictionary<string, string> dic)
        {
            control.DataSource = dic;
            control.DataTextField = "key";
            control.DataValueField = "value";
            control.DataBind();
        }

        public static void BindDropDownList(DropDownList control, IEnumerable dic, string TextField, string ValueField)
        {
            control.DataSource = dic;
            control.DataTextField = TextField;
            control.DataValueField = ValueField;
            control.DataBind();
            control.Items.Insert(0, "--请选择--");

        }

        public static void BindDropDownList(DropDownList control, DataTable table, string TextField, string ValueField)
        {
            control.DataSource = table;
            control.DataTextField = TextField;
            control.DataValueField = ValueField;
            control.DataBind();
            control.Items.Insert(0, new ListItem("--请选择--", "-1"));

        }

        public static void BindRadioButtonList(RadioButtonList control, DataTable table, string TextField, string ValueField)
        {
            control.DataSource = table;
            control.DataTextField = TextField;
            control.DataValueField = ValueField;
            control.DataBind();


        }

        //public static void BindDropListForAddress(DropDownList control, DataTable table)
        //{
        //    DataView drv = new DataView(table);
        //    ListItem items;
        //    foreach (DataRowView dr in drv)
        //    {
        //        items = new ListItem(string.Concat(dr["provincesName"].ToString(), dr["cityName"].ToString(), dr["areaName"].ToString(), dr["residentialName"].ToString()), dr["ResidentialID"].ToString());
        //        control.Items.Add(items);

        //    }
        //    control.Items.Insert(0, "--请选择--");

        //}

        //public static void BindDropListForAddress(DropDownList control, DataTable table, string firstItem)
        //{
        //    DataView drv = new DataView(table);
        //    ListItem items;
        //    foreach (DataRowView dr in drv)
        //    {
        //        items = new ListItem(string.Concat(dr["provincesName"].ToString(), dr["cityName"].ToString(), dr["areaName"].ToString(), dr["residentialName"].ToString()), dr["ResidentialID"].ToString());
        //        control.Items.Add(items);

        //    }
        //    control.Items.Insert(0, firstItem);

        //}

        public static void BindDropDownList(DropDownList control, DataTable table, string TextField, string ValueField, string ParentID)
        {
            //    control.DataSource = table;
            //    control.DataTextField = TextField;
            //    control.DataValueField = ValueField;
            //    control.DataBind();
            ListTreeShow(control, table, 0, "", "0", TextField, ValueField, ParentID);
            control.Items.Insert(0, "--请选择--");

        }

        public static void ListTreeShow(DropDownList control, DataTable dt, int nLevel, string Rn, string pid, string TextField, string ValueField, string dtparentID)
        {
            ListItem items;

            DataView drv = new DataView(dt);
            foreach (DataRowView dr in drv)
            {
                if (pid.Equals(dr[dtparentID].ToString()))
                {
                    Rn = "";
                    if (nLevel == 0)
                    {
                        Rn += "├";
                    }
                    else
                    {
                        string _f = "";
                        for (int i = 1; i <= nLevel; i++)
                        {
                            _f += "│ ";
                        }
                        Rn += _f + "├";
                    }
                    Rn += dr[TextField].ToString();
                    items = new ListItem(Rn, dr[ValueField].ToString());
                    control.Items.Add(items);
                    ListTreeShow(control, dt, nLevel + 1, Rn, dr[ValueField].ToString(), TextField, ValueField, dtparentID);
                }
            }
        }
    }
}
