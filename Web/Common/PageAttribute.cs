﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    public class PageAttribute
    {
        private int? m_PageIndex;
        /// <summary>
        /// 当前页，最小索引0（默认 0）
        /// </summary>
        public int? PageIndex
        {
            get { return m_PageIndex ?? 0; }
            set { m_PageIndex = value; }
        }

        private int? m_PageSize;
        /// <summary>
        /// 每页数据行数（默认 10）
        /// </summary>
        public int? PageSize
        {
            get { return m_PageSize ?? 15; }
            set { m_PageSize = value; }
        }

        private int? m_TotalRowCount;
        /// <summary>
        /// 表总数据行数
        /// </summary>
        public int? TotalRowCount
        {
            get { return m_TotalRowCount; }
            set { m_TotalRowCount = value; }
        }

        private string m_TableName;
        /// <summary>
        /// 表名
        /// </summary>
        public string TableName
        {
            get
            {
                if (string.IsNullOrEmpty(m_TableName))
                {
                    throw new Exception("表名不能为空");
                }
                return m_TableName;
            }
            set { m_TableName = value; }
        }

        private string m_WhereCondition;
        /// <summary>
        /// Where条件（默认 空字符）
        /// </summary>
        public string WhereCondition
        {
            get { return m_WhereCondition ?? ""; }
            set { m_WhereCondition = value; }
        }

        private string m_Columns;
        /// <summary>
        /// 查询字段,逗号分隔（默认 *）
        /// </summary>
        public string Columns
        {
            get { return m_Columns ?? TableName + ".*"; }
            set { m_Columns = value; }
        }

        private string m_StrOrder;
        /// <summary>
        /// 排序字段（默认 空字符）
        /// </summary>
        public string StrOrder
        {
            get { return m_StrOrder ?? ""; }
            set { m_StrOrder = value; }
        }

        private string m_LinkQuery;
        /// <summary>
        /// 多表联查语句（默认 空字符）
        /// eg: …… " inner join userrole on userrole.userid=userinfo.userid" ……
        /// </summary>
        public string LinkQuery
        {
            get { return m_LinkQuery ?? ""; }
            set { m_LinkQuery = value; }
        }

    }
}
