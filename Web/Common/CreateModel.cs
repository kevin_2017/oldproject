﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Reflection;

namespace Common
{
    public static class CreateModel
    {
        /// <summary>
        /// 传入一行数据,将实体类填充后传回实体类
        /// 返回值需强制转换
        /// </summary>
        /// <param name="dr">DataRow dr</param>
        /// <param name="classType">object classType</param>
        /// <returns>object</returns>
        public static object Create(DataRow dr, object classType)
        {
            Type t = classType.GetType();
            PropertyInfo[] pi = t.GetProperties();

            object[] os = new object[dr.Table.Columns.Count];

            for (int i = 0; i < dr.Table.Columns.Count; i++)
            {
                os[i] = dr[i].ToString();

                for (int j = 0; j < pi.Length; j++)
                {
                    if (dr.Table.Columns[i].ColumnName.ToLower() == pi[j].Name.ToLower())//根据表字段和实体类的属性
                    {

                        PropertyInfo p = pi[j];//得到属性基类
                        object obj = dr[i];//得到数据
                        if (p.PropertyType.IsEnum)//判断该数据是不是枚举型数据
                        {
                            //参数列表说明:被赋值对象,值,可为空的索引
                            p.SetValue(classType, Enum.ToObject(p.PropertyType, obj), null);//以枚举方式枚举出该属性的类型
                        }
                        else
                        {
                            if (obj != DBNull.Value)
                            {
                                p.SetValue(classType, obj, null);//否则直接赋予引用类型
                            }
                        }
                    }
                }
            }
            return classType;
        }


        public static IList<T> CreateEntity<T>(DataTable dt, Type t)
        where T : IComparable, new()
        {
            IList<T> ilist = new List<T>();

            foreach (DataRow dr in dt.Rows)
            {
                ilist.Add((T)Create(dr, new T()));
            }

            return ilist;
        }
    }
}
