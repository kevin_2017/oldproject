﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Web.Security;

using System.Data;
using System.Configuration;
using System.Web;

using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.IO;

namespace Common
{
    /// <summary>
    /// 数据加密解密
    /// </summary>
    public class DEncryptHelper
    {
        
        //锁定对象
        private static object obj = new object();
        /// <summary>
        /// 生成MD5摘要
        /// </summary>
        /// <param name="original"></param>
        /// <returns></returns>
        public static byte[] MakeMD5(byte[] original)
        {
            MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
            byte[] keyhash = hashmd5.ComputeHash(original);
            hashmd5 = null;
            return keyhash;
        }

        /// <summary>
        /// 数据加密
        /// </summary>
        /// <param name="original">数据源</param>
        /// <param name="key">密钥</param>
        /// <returns>密文</returns>
        public static byte[] Encrypt(byte[] original, byte[] key)
        {
            TripleDESCryptoServiceProvider des = new TripleDESCryptoServiceProvider();
            des.Key = MakeMD5(key);
            des.Mode = CipherMode.ECB;

            return des.CreateEncryptor().TransformFinalBlock(original, 0, original.Length);
        }

        /// <summary>
        /// 数据加密
        /// </summary>
        /// <param name="original">数据源</param>
        /// <param name="key">密钥</param>
        /// <returns>密文</returns>
        public static string Encrypt(string original, string key)
        {
            byte[] buff = System.Text.Encoding.Default.GetBytes(original);
            byte[] kb = System.Text.Encoding.Default.GetBytes(key);
            return Convert.ToBase64String(Encrypt(buff, kb));
        }

        /// <summary>
        /// 使用给定密钥解密数据
        /// </summary>
        /// <param name="encrypted">密文</param>
        /// <param name="key">密钥</param>
        /// <returns>明文</returns>
        public static byte[] Decrypt(byte[] encrypted, byte[] key)
        {
            TripleDESCryptoServiceProvider des = new TripleDESCryptoServiceProvider();
            des.Key = MakeMD5(key);
            des.Mode = CipherMode.ECB;

            return des.CreateDecryptor().TransformFinalBlock(encrypted, 0, encrypted.Length);
        }

        /// <summary>
        /// 使用给定密钥字符串解密string,返回指定编码方式明文
        /// </summary>
        /// <param name="encrypted">密文</param>
        /// <param name="key">密钥</param>
        /// <param name="encoding">字符编码方案</param>
        /// <returns>明文</returns>
        public static string Decrypt(string encrypted, string key, Encoding encoding)
        {
            byte[] buff = Convert.FromBase64String(encrypted);
            byte[] kb = System.Text.Encoding.Default.GetBytes(key);
            return encoding.GetString(Decrypt(buff, kb));
        }

        /// <summary>
        /// 使用给定密钥字符串解密string
        /// </summary>
        /// <param name="encrypted">密文</param>
        /// <param name="key">密钥</param>
        /// <returns>明文</returns>
        public static string Decrypt(string encrypted, string key)
        {
            return Decrypt(encrypted, key, Encoding.Default);
        }

        /// <summary>
        /// 数据加密
        /// </summary>
        /// <param name="original">数据源</param>
        /// <param name="encryptFormat">0:SHA1,1:MD5</param>
        /// <returns></returns>
        public static string Encrypt(string original, int encryptFormat)
        {
            string ciphertext = "";
            switch (encryptFormat)
            {
                case 0:
                    ciphertext = FormsAuthentication.HashPasswordForStoringInConfigFile(original, "SHA1");
                    break;
                case 1:
                    ciphertext = FormsAuthentication.HashPasswordForStoringInConfigFile(original, "MD5");
                    break;
            }
            return ciphertext;
        }

        ///// <summary>
        ///// 生成MD5码,生成的数据同Encrypt(original,1)
        ///// </summary>
        ///// <param name="original"></param>
        ///// <param name="encoding"></param>
        ///// <returns></returns>
        //public static string MakeMD5(string original, string encoding)
        //{
        //    MD5 hashmd5 = new MD5CryptoServiceProvider();
        //    byte[] byteOriginal = hashmd5.ComputeHash(Encoding.GetEncoding(encoding).GetBytes(original));
        //    StringBuilder ciphertext = new StringBuilder(32);
        //    for (int i = 0; i < byteOriginal.Length; i++)
        //    {
        //        ciphertext.Append(byteOriginal[i].ToString("x").PadLeft(2, '0'));
        //    }
        //    return ciphertext.ToString();
        //}

        /// <summary>
        /// 生成MD5码,生成的数据同Encrypt(original,1)
        /// </summary>
        /// <param name="original"></param>
        /// <param name="encoding"></param>
        /// <returns></returns>
        public static string MakeMD5(string original)
        {
            MD5 hashmd5 = new MD5CryptoServiceProvider();
            byte[] byteOriginal = hashmd5.ComputeHash(Encoding.UTF8.GetBytes(original));
            StringBuilder ciphertext = new StringBuilder(32);
            for (int i = 0; i < byteOriginal.Length; i++)
            {
                ciphertext.Append(byteOriginal[i].ToString("x").PadLeft(2, '0'));
            }
            return ciphertext.ToString();
        }

        /// <summary>
        /// 生成一个随机数
        /// </summary>
        /// <returns></returns>
        public static string GetRandomNumber()
        {
            string randomNumber = "";
            
          //  System.Threading.Thread.Sleep(100);

          //  randomNumber = System.DateTime.Now.ToString("yyyyMMddHHmmssfffffff");
            
            System.Threading.Thread.Sleep(100);

            Random rdm = new Random();

            randomNumber = rdm.Next(0, 100000).ToString() + rdm.Next(0, 100).ToString() + rdm.Next(0, 1000);
            rdm = null;
            return randomNumber;
        }

        /// <summary>
        /// 生成一个随机数
        /// </summary>
        /// <param name="length"></param>
        /// <param name="isSleep"></param>
        /// <returns></returns>
        public static string GetRandomNumber(int length, bool isSleep)
        {
            if (isSleep)
                System.Threading.Thread.Sleep(3);
            string result = "";
            System.Random random = new Random();
            for (int i = 0; i < length; i++)
            {
                result += random.Next(10).ToString();
            }
            return result;
        }

        /// <summary>
        /// 生成字母和数字的验证码
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string GetRandWord(int length)
        {
            int number;
            char code;
            string checkCode = String.Empty;

            System.Random random = new Random();
            for (int i = 0; i < length; i++)
            {
                number = random.Next();

                if (number % 2 == 0)
                {
                    code = (char)('0' + (char)(number % 10));
                }
                else
                {
                    code = (char)('A' + (char)(number % 26));
                }
                checkCode += code.ToString();
            }

            return checkCode;
        }

        /// <summary>
        /// 当前程序加密所使用的密钥

        /// </summary>
        public static readonly string myKey = "12345678";

        #region 加密方法
        /// <summary>
        /// 加密方法
        /// </summary>
        /// <param name="pToEncrypt">需要加密字符串</param>
        /// <param name="sKey">密钥</param>
        /// <returns>加密后的字符串</returns>
        public static string xwjEncrypt(string pToEncrypt, string sKey)
        {
            try
            {
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                //把字符串放到byte数组中


                //原来使用的UTF8编码，我改成Unicode编码了，不行
                byte[] inputByteArray = Encoding.Default.GetBytes(pToEncrypt);

                //建立加密对象的密钥和偏移量


                //使得输入密码必须输入英文文本
                des.Key = ASCIIEncoding.ASCII.GetBytes(sKey);
                des.IV = ASCIIEncoding.ASCII.GetBytes(sKey);
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(), CryptoStreamMode.Write);

                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                StringBuilder ret = new StringBuilder();
                foreach (byte b in ms.ToArray())
                {
                    ret.AppendFormat("{0:X2}", b);
                }
                ret.ToString();
                return ret.ToString();
            }
            catch (Exception ex)
            {
                 
                //throw new Exception(ex.Message);
                return "";
            }

             
        }
        #endregion

        #region 解密方法
        /// <summary>
        /// 解密方法
        /// </summary>
        /// <param name="pToDecrypt">需要解密的字符串</param>
        /// <param name="sKey">密匙</param>
        /// <returns>解密后的字符串</returns>
        public static string xwjDecrypt(string pToDecrypt, string sKey)
        {
            if (pToDecrypt == null || sKey == null)
                return null;

            try
            {
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                byte[] inputByteArray = new byte[pToDecrypt.Length / 2];
                for (int x = 0; x < pToDecrypt.Length / 2; x++)
                {
                    int i = (Convert.ToInt32(pToDecrypt.Substring(x * 2, 2), 16));
                    inputByteArray[x] = (byte)i;
                }

                //建立加密对象的密钥和偏移量，此值重要，不能修改
                des.Key = ASCIIEncoding.ASCII.GetBytes(sKey);
                des.IV = ASCIIEncoding.ASCII.GetBytes(sKey);
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                //建立StringBuild对象，CreateDecrypt使用的是流对象，必须把解密后的文本变成流对象
                StringBuilder ret = new StringBuilder();
                return System.Text.Encoding.Default.GetString(ms.ToArray());
            }
            catch (Exception ex)
            {
                 
               // throw new Exception(ex.Message);
                return "";
            }
             // return "";
        }
        #endregion

        #region king 加方法用于金额加密

        public static string Hash(string input)
        {
            System.Security.Cryptography.SHA1CryptoServiceProvider sha1 = new System.Security.Cryptography.SHA1CryptoServiceProvider();
            byte[] bytValue, bytHash;
            bytValue = System.Text.Encoding.UTF8.GetBytes(input);
            bytHash = sha1.ComputeHash(bytValue);

            return Convert.ToBase64String(bytHash);
        }
        public static string HashTwo(string input)
        {
            System.Security.Cryptography.SHA1 sha1 = new System.Security.Cryptography.SHA1CryptoServiceProvider();
            byte[] bytResult = sha1.ComputeHash(System.Text.Encoding.Default.GetBytes(input));
            string strResult = BitConverter.ToString(bytResult, 2, 10);
            //string strResult = BitConverter.ToString(bytResult);
            strResult = strResult.Replace("-", "");
            return strResult;

        }
        public static string HashThree(string input)
        {
            System.Security.Cryptography.SHA1 sha1 = System.Security.Cryptography.SHA1.Create();
            string strResult = "";
            byte[] bytResult = sha1.ComputeHash(System.Text.UnicodeEncoding.Unicode.GetBytes(input));
            for (int i = 0; i < bytResult.Length; i++)
            {
                strResult = strResult + bytResult[i].ToString("x");//x 十六进制
            }
            return strResult;
        }
        #endregion 

    }
}
