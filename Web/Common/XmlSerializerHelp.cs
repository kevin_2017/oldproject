﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;//引用XML名称空间
using System.Xml.Serialization;//引用Xml.Serialization名称空间
using System.IO;//输入输出
namespace Common
{
    public static class XmlSerializerHelp
    {
     
        /// <summary>
        /// 讲一个对象 用xml序列化成字符串
        /// </summary>
        /// <typeparam name="T">对象类型</typeparam>
        /// <param name="entity">实体对象</param>
        /// <returns>序列化结果的字符串</returns>
        public static string XMLSerialize<T>(T entity)
        {
            try
            {
                //字符流
                StringBuilder buffer = new StringBuilder();
                //序列化这个对象
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                using (TextWriter writer = new StringWriter(buffer))
                {
                    serializer.Serialize(writer, entity);
                }
                return buffer.ToString();
            }
            catch //(Exception ex)
            {
                //mLog.Error(ex);
                return null;
            }
        }

        /// <summary>
        /// 反序列化
        /// </summary>
        /// <typeparam name="T">对象类型</typeparam>
        /// <param name="xmlString">XML字符串</param>
        /// <returns>实体对象</returns>
        public static T DeXMLSerialize<T>(string xmlString)
        {
            T cloneObject = default(T);
            try
            {
                
                StringBuilder buffer = new StringBuilder();
                buffer.Append(xmlString);
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                using (TextReader reader = new StringReader(buffer.ToString()))
                {
                    Object obj = serializer.Deserialize(reader);
                    cloneObject = (T)obj;//强制转换为需要的对象类型
                }
                return cloneObject;
            }
            catch (Exception ex)
            {
                string a = ex.Message + ex.Source + ex.StackTrace + ex.TargetSite;
                return cloneObject;
            }
        }

        /// <summary>
        /// 根据文件路径和字符集编码 反序列化 
        /// </summary>
        /// <typeparam name="T"> 反序列化 对象</typeparam>
        /// <param name="xmlFilePath">XML文件路径（全路径）</param>
        /// <param name="encoding">字符集编码</param>
        /// <returns>反序列化 对象</returns>
        public static T XMLSerializeFilePath<T>(string xmlFilePath, Encoding encoding)
        {
            T cloneObject = default(T);
            try
            {  
                 Common.Helper.FileHelper fileHelper = new Common.Helper.FileHelper();
                cloneObject = DeXMLSerialize<T>(fileHelper.ReadFileContent(xmlFilePath, encoding));
                return cloneObject;
            }
            catch //(Exception ex)
            {
               // mLog.Error(ex);
                return cloneObject;
            }
        }

        /// <summary>
        /// 根据XML文件路径 反序列对象（默认编码集Encoding.UTF8）
        /// </summary>
        /// <typeparam name="T">反序列化 对象</typeparam>
        /// <param name="xmlFilePath">XML文件路径 </param>
        /// <returns>反序列化 对象</returns>
        public static T XMLSerializeFilePath<T>(string xmlFilePath)
        {
            T cloneObject = default(T);
            try
            {               
                cloneObject = XMLSerializeFilePath<T>(xmlFilePath, Encoding.UTF8);
                return cloneObject;
            }
            catch //(Exception ex)
            {
                //mLog.Error(ex);
                return cloneObject;
            }
        }
    }
}
