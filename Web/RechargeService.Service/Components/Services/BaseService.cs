﻿using CE.Utility.Text;
using DotNet4.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using RechargeService.Components.Common;

namespace RechargeService.Components.Services
{
    public class BaseService : BaseClass
    {
        #region 得到Http对象

        protected HttpItem GetHttpItem(string url)
        {
            var item = new HttpItem()
            {
                URL = url,//URL     必需项
                Encoding = System.Text.Encoding.UTF8,//编码格式（utf-8,gb2312,gbk）
                Method = "POST",//URL
                Timeout = 100000,//连接超时时间
                ReadWriteTimeout = 30000,//写入Post数据超时时间
                //IsToLower = true,//得到的HTML代码是否转成小写     可选项默认转小写
                ProtocolVersion = new Version(1, 0),
                KeepAlive = false,
                Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",//    可选项有默认值
                ContentType = "application/x-www-form-urlencoded;charset=UTF-8",//返回类型    可选项有默认值
                Referer = ""//来源URL     可选项
            };
            return item;
        }
        #endregion

        #region 生成Http参数列表
        /// <summary>
        /// 生成Http参数列表
        /// </summary>
        /// <param name="ht"></param>
        /// <returns></returns>
        protected string GetParams(NameValueCollection ht)
        {
            if (null == ht || ht.Count == 0)
            {
                return "";
            }
            var sb = new StringBuilder();
            foreach (var key in ht.Keys.Cast<string>().Where(key => null != key))
            {
                sb.AppendFormat("{0}={1}&", key, ht[key].ToUrlEncode());
            }
            return sb.ToString(0, sb.Length - 1);
        }
        #endregion

        #region 反序列化JSON字符串
        /// <summary>
        /// 反序列化JSON字符串
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="json"></param>
        /// <returns></returns>
        public T DeserializeJson<T>(string json)
        {
            var jSetting = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore };
            var info = JsonConvert.DeserializeObject<T>(json, jSetting);
            return info;
        }
        #endregion

        #region 时间戳
        protected static string Timespan
        {
            get
            {
                var dt = DateTime.Now;
                var dt1 = new DateTime(0x7b2, 1, 1, 0, 0, 0);
                var ts = (TimeSpan)(dt - dt1);
                var tt = ts.TotalMilliseconds;
                return tt.ToString("0");
            }
        }
        #endregion
    }
}
