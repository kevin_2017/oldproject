﻿using CE.Utility;
using CE.Utility.Text;
using CE.Utility.Web;
using Common.DBUtility;
using Helper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Messaging;
using System.Net;

namespace RechargeService.Components.Services
{
    public class NotifyService : BaseService
    {
        string msmqName = ConfigurationManager.AppSettings["RechargeNotifyQueueName"];


        public bool GetNotifyData()
        {
            var result = true;
            try
            {
                var msmq = MsmqHelper.GetInstance(msmqName);
                try
                {
                    MessageQueueTransaction trans = null;
                    if (msmq.Transactional)
                    {
                        trans = new MessageQueueTransaction();
                    }
                    msmq.ReceiveCompleted += new ReceiveCompletedEventHandler(DoNotify);
                    trans?.Begin();
                    msmq.BeginReceive();
                    trans?.Commit();
                }
                catch (Exception ex)
                {
                    WriteWinFormLog.WriteError(DateTime.Now.ToString("yyyy-MM-dd"), "{RechargeSystemService.GetRecordData},发生其它错误", ex);
                }
            }
            catch (Exception ex)
            {
                WriteWinFormLog.WriteError(DateTime.Now.ToString("yyyy-MM-dd"), "{RechargeSystemService.GetRecordData},发生其它错误", ex);
                result = false;
            }
            return result;
        }
        public void DoNotify(object sender, ReceiveCompletedEventArgs e)
        {
            var msmq = sender as MessageQueue;
            if (msmq == null)
            {
                WriteWinFormLog.Write(DateTime.Now.ToString("yyyy-MM-dd"), "RechargeSystemService.DoNotify", $"没有队列请求");
                return;
            }
            try
            {
                //获取当前Message
                var msg = msmq.EndReceive(e.AsyncResult);   //e.Message
                if (msg == null)
                {
                    WriteWinFormLog.Write(DateTime.Now.ToString("yyyy-MM-dd"), "NotifyService.DoRecharge", "消息队列中没有数据");
                    return;
                }
                msg.Formatter = new XmlMessageFormatter(new Type[] { typeof(Model.V8_OrderManage) });
                var orderInfo = (Model.V8_OrderManage)msg.Body;
                if (orderInfo == null)
                    return;

                #region 通知
                DBHelper.ExecuteSql($"update V8_OrderManage set NotifyStatus=3 where OrderNo='{orderInfo.OrderNo}'");
                var branchModel = new BLL.MS_BranchManage().GetModel(orderInfo.BranchID);
                Dictionary<string, string> dic = new Dictionary<string, string>() {
                    {"branchid",branchModel.ID.ToString()},
                    {"branchorderno",orderInfo.BranchOrderNo},
                    {"status","1"},
                    {"extendparam",orderInfo.ExtendParam}//自定义扩展参数，原样返回
                };
                var signStr = $"{Tools.CreateLinkString(Tools.SortDictionary(dic), "&", true)}&key={branchModel.PrivateKey}";
                WriteWinFormLog.Write(DateTime.Now.ToString("yyyy-MM-dd"), "RechargeSystemService.DoNotify", $"通知下游数据：{signStr}");
                dic.Add("sign", signStr.ToMD5().ToLower());
                var Param = Tools.CreateLinkString(Tools.SortDictionary(dic)).EncodeBase64String();
                var Key = branchModel.PrivateKey.EncodeBase64String();
                for (int i = 0; i < 5; i++)
                {
                    var httpRequestUtil = new HttpRequestUtil($"{orderInfo.NotifyUrl}", HttpRequestUtil.Method.Post, $"Param={Param}&Key={Key}");
                    var httpResult = httpRequestUtil.Request();
                    var html = httpResult.Html;

                    var notifyResult = 0;
                    WriteWinFormLog.Write(DateTime.Now.ToString("yyyy-MM-dd"), "RechargeSystemService.DoNotify", $"通知下游返回数据：{html}");

                    if ((httpResult.StatusCode == HttpStatusCode.OK ||
        httpResult.StatusCode == HttpStatusCode.InternalServerError) && html.Trim().ToUpper().Equals("SUCCESS"))
                        notifyResult = 1;

                    var notifyId = new BLL.V8_OrderNotifyRecs().Add(new Model.V8_OrderNotifyRecs()
                    {
                        OrderNo = orderInfo.OrderNo,
                        BranchId = orderInfo.BranchID,
                        NotifyTime = DateTime.Now,
                        NotifyConent = $"{signStr}",
                        NotifyResult = notifyResult
                    });
                    if (notifyResult == 1)
                    {
                        DBHelper.ExecuteSql($"udate V8_OrderManage set NoticeCount={i},NotifyStatus=1 where OrderNo='{orderInfo.OrderNo}'");
                        break;
                    }
                    i++;
                }
                #endregion

            }
            catch (Exception ex)
            {
                WriteWinFormLog.WriteError(DateTime.Now.ToString("yyyy-MM-dd"), $"NotifyService.DoNotify", ex);
            }
            msmq.BeginReceive();
        }
    }
}
