﻿using CE.Utility;
using Helper;
using Model;
using System;
using System.Configuration;
using System.Linq;
using System.Messaging;
using System.Text;
using Common.DBUtility;
using RechargeService.Components.RechargeService;

namespace RechargeService.Components.Services
{
    public class RechargeService : BaseService
    {
        string msmqName = ConfigurationManager.AppSettings["RechargeOrderQueueName"];


        #region 获取待发送的第三方充值记录

        public bool GetRecordData()
        {
            var result = true;
            try
            {
                var msmq = MsmqHelper.GetInstance(msmqName);
                try
                {
                    MessageQueueTransaction trans = null;
                    if (msmq.Transactional)
                    {
                        trans = new MessageQueueTransaction();
                    }
                    msmq.ReceiveCompleted += new ReceiveCompletedEventHandler(DoRecharge);
                    trans?.Begin();
                    msmq.BeginReceive();
                    trans?.Commit();
                }
                catch (Exception ex)
                {
                    WriteWinFormLog.WriteError(DateTime.Now.ToString("yyyy-MM-dd"), "{GetRecordData},发生其它错误", ex);
                }
            }
            catch (Exception ex)
            {
                WriteWinFormLog.WriteError(DateTime.Now.ToString("yyyy-MM-dd"), "{GetRecordData},发生其它错误", ex);
                result = false;
            }
            return result;
        }

        #endregion


        /// <summary>
        /// 消息队列处理器
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void DoRecharge(object sender, ReceiveCompletedEventArgs e)
        {
            var msmq = sender as MessageQueue;
            if (msmq == null)
            {
                WriteWinFormLog.Write(DateTime.Now.ToString("yyyy-MM-dd"), "DoRecharge", $"没有请求队列");
                return;
            }
            try
            {
                //获取当前Message
                var msg = msmq.EndReceive(e.AsyncResult);   //e.Message
                if (msg == null)
                {
                    WriteWinFormLog.Write(DateTime.Now.ToString("yyyy-MM-dd"), "DoRecharge", $"没有数据");
                    return;
                }
                msg.Formatter = new XmlMessageFormatter(new Type[] { typeof(Model.V8_OrderManage) });
                var orderInfo = (Model.V8_OrderManage)msg.Body;

                if (orderInfo == null)
                    return;
                #region 充值
                Model.StrWhere str = new StrWhere()
                {
                    IsWhereExist = true,
                    strWhere = new StringBuilder($"  id not in ( select SupplierId from V8_OrderRequestRecs where OrderNo='{orderInfo.OrderNo}'  and SupplierId in ( select SupplierID from V8_Product_Supplier where ProductID={orderInfo.ProductID})) and Status=1001  order by sort")
                };
                var suppList
                    = new BLL.SUP_SupplierManage().GetModelList(str);
                bool isRequest = false;
                if (suppList.Any())
                {
                    foreach (var model in suppList)
                    {
                        var factory = new RechargeFactroy();
                        var recharge = factory.GetInstance(model.ApiCode);
                        if (recharge != null)
                        {
                            recharge.PhoneType = orderInfo.PhoneType;
                            recharge.ApiCode = model.ApiCode;
                            recharge.FlowNoticeUrl = model.FlowNoticeUrl;
                            recharge.FlowPayDomain = model.FlowPayDomain;
                            recharge.FlowSearchUrl = model.FlowSearchUrl;
                            recharge.ProductId = orderInfo.ProductID;
                            recharge.OrderNo = orderInfo.OrderNo;
                            recharge.RechargeMoney = orderInfo.TotalPrice;
                            recharge.RechargeNoticeUrl = model.RechargeNoticeUrl;
                            recharge.RechargePayDomain = model.RechargePayDomain;
                            recharge.RechargeSearchUrl = model.RechargeSearchUrl;
                            recharge.TelePhone = orderInfo.RechargeNo;
                            recharge.Gateway = model.GateWay;
                            recharge.SupplierId = model.ID;
                            var result = recharge.Process();
                            if (result != null && result.Success)
                            {
                                isRequest = true;
                                break;
                            }
                        }
                        continue;
                    }

                    if (!isRequest)
                    {
                        DBHelper.ExecuteSql($"update V8_OrderManage set CallBackTime=getdate(),CurentStatus=5,NotifyStatus=0 where OrderNo='{orderInfo.OrderNo}'");
                    }
                }
                else
                {
                    msmq.Send(orderInfo);
                }

                #endregion

            }
            catch (Exception ex)
            {
                WriteWinFormLog.WriteError(DateTime.Now.ToString("yyyy-MM-dd"), $"RechargeSystemService.DoRecharge", ex);
            }
            msmq.BeginReceive();
        }
    }
}
