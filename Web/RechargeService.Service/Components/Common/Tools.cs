﻿using CE.Utility.Text;
using CE.Utility.Web;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace RechargeService.Components
{
    public class Tools
    {
        public static IList<Model.SUP_SupplierConfig> GetKeys(string apiCode)
        {
            Model.StrWhere str = new Model.StrWhere()
            {
                IsWhereExist = true,
                strWhere = new StringBuilder($" SupplierName='{apiCode}'")
            };
            var list = new BLL.SUP_SupplierConfig().GetModelList(str);
            foreach (var key in list)
            {
                key.ConfigValue = key.ConfigValue.Replace("\r", "").Replace("\n", "").Replace(" ", "");
            }
            return list;
        }

        public static Dictionary<string, string> SortDictionary(Dictionary<string, string> dic)
        {
            List<KeyValuePair<string, string>> myList = new List<KeyValuePair<string, string>>(dic);
            myList.Sort(delegate (KeyValuePair<string, string> s1, KeyValuePair<string, string> s2)
            {
                return s1.Key.CompareTo(s2.Key);
            });
            dic.Clear();
            foreach (KeyValuePair<string, string> pair in myList)
            {
                dic.Add(pair.Key, pair.Value);
            }
            return dic;
        }

        public static string CreateLinkString(Dictionary<string, string> dicArray, string splitSymbol = "&", bool isRemoveEmpty = false)
        {
            StringBuilder prestr = new StringBuilder();
            foreach (KeyValuePair<string, string> temp in dicArray)
            {
                if (isRemoveEmpty)
                {
                    if (string.IsNullOrEmpty(temp.Value))
                    {
                        continue;
                    }
                }
                prestr.Append($"{temp.Key}={temp.Value}{(string.IsNullOrEmpty(splitSymbol) ? "&" : splitSymbol)}");
            }

            //去掉最后一个&字符
            int nLen = prestr.Length;
            prestr.Remove(nLen - 1, 1);

            return prestr.ToString();
        }

        #region 基本函数

        private static string GetCurrDate()
        {
            return DateTime.Now.ToString("yyyyMMdd");
        }

        /// <summary>
        /// 转换时间
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        private static DateTime GetTimestamp(string date)
        {
            var time = date.ToDouble();
            var dt = new DateTime(0x7b2, 1, 1, 0, 0, 0).AddMilliseconds(time);
            return dt;
        }

        /// <summary>
        /// 将任意类型转换为日期型，当无法转换时返回 DateTime.MinValue 。
        /// </summary>
        /// <param name="dt">object 类型。</param>
        /// <returns>返回转换结果。</returns>
        private static DateTime GetDateTime(object dt)
        {
            DateTime newdt;
            DateTime.TryParse(GetString(dt), out newdt);
            return newdt;
        }
        /// <summary>
        /// 将任意类型转换为整型，当无法转换时返回 0 。
        /// </summary>
        /// <param name="id">object 类型。</param>
        /// <returns>返回转换结果。</returns>
        private static int GetInt(object id)
        {
            int newid;
            int.TryParse(GetString(id).Trim(), out newid);
            return newid;
        }
        /// <summary>
        /// 将任意类型转换为整型，当无法转换时返回 0 。
        /// </summary>
        /// <param name="id">object 类型。</param>
        /// <returns>返回转换结果。</returns>
        private static decimal GetDecimal(object id)
        {
            decimal newid;
            decimal.TryParse(GetString(id).Trim(), out newid);
            return newid;
        }
        /// <summary>
        /// 将任意类型转换为字符串型，当无法转换时返回空字符串。
        /// </summary>
        /// <param name="str">object 类型。</param>
        /// <returns>返回转换结果。</returns>
        private static string GetString(object str)
        {
            if (str == null)
                return string.Empty;
            else
                return str.ToString();
        }

        private static string GetDate(object dt)
        {
            if (string.IsNullOrEmpty(GetString(dt))) return string.Empty;
            DateTime newdt;
            DateTime.TryParse(GetString(dt), out newdt);
            return newdt.ToString("yyyy-MM-dd");
        }

        private static string GetTime(object dt)
        {
            if (string.IsNullOrEmpty(GetString(dt))) return string.Empty;
            DateTime newdt;
            DateTime.TryParse(GetString(dt), out newdt);
            return newdt.ToString("HH-mm-ss");
        }

        private static string GetDateTime(object dt, string format)
        {
            if (string.IsNullOrEmpty(GetString(dt))) return string.Empty;
            DateTime newdt;
            DateTime.TryParse(GetString(dt), out newdt);
            return newdt.ToString(format);
        }
        #endregion

        #region 生成支付订单号
        /// <summary>
        /// OrderNo
        /// </summary>
        /// <returns></returns>
        public static string OrderNo()
        {
            char[] constant = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
            System.Text.StringBuilder newRandom = new System.Text.StringBuilder(10);
            Random rd = new Random();
            for (int i = 0; i < 5; i++)
            {
                newRandom.Append(constant[rd.Next(5)]);
            }
            return $"{DateTime.Now.ToString("yyMMddHHmmssfff")}{newRandom}";
        }
        #endregion

        #region 获取调用链上的方法
        /// <summary>  
        /// 获取调用链上的方法  
        /// </summary>  
        /// <param name="depth">回朔深度</param>  
        public static MethodBase GetCurrentMethod(int depth)
        {
            try
            {
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace();
                return st.GetFrame(depth).GetMethod();
            }
            catch
            {
                return null;
            }
        }
        #endregion

        #region 解析Url代码的值
        public static string ParseCode(IDictionary<string, object> dic)
        {
            var code = string.Empty;
            if (dic != null && dic["code"] != null)
            {
                code = dic["code"].ToString();
            }
            return code;
        }
        #endregion



        public static Dictionary<string, string> GetParamArray(string param)
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            param = param.Replace("\"", "");
            var sArray = param.Split('&');
            foreach (var item in sArray)
            {
                if (item.IndexOf("=") > 0)
                {
                    var index = item.IndexOf("=");
                    var key = item.Substring(0, index).Trim();
                    var value = item.Substring(index + 1, item.Length - (index + 1)).Trim();
                    dic.Add(key.ToLower(), value);
                }
                //dic.Add(item.Split('=')[0].ToLower(), item.Split('=')[1]);
            }
            return dic;
        }



    }
}
