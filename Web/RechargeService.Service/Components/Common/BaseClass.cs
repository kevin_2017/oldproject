﻿using System;
using System.Text;
using System.Text.RegularExpressions;

namespace RechargeService.Components.Common
{
    public class BaseClass : IDisposable
    {
        /// <summary>
        /// 
        /// </summary>
        private bool _disposed = false; 
         

        

        #region 释放资源

        /// <summary>
        /// 释放资源
        /// </summary>
        /// <param name="disposing">若要释放托管资源和非托管资源，则为 true；若仅释放非托管资源，则为 false。</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                }
            }
            this._disposed = true;
        }

        /// <summary>
        /// 释放资源
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

        #region 基本函数

        protected bool IsToday(string date)
        {
            if (GetDate(date).Equals(GetDate(DateTime.Now.ToString())))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        protected string Now()
        {
            return DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
        }

        /// <summary>
        /// 将任意类型转换为日期型，当无法转换时返回 DateTime.MinValue 。
        /// </summary>
        /// <param name="dt">object 类型。</param>
        /// <returns>返回转换结果。</returns>
        public static DateTime GetDateTime(object dt)
        {
            DateTime newdt;
            DateTime.TryParse(GetString(dt), out newdt);
            return newdt;
        }
        /// <summary>
        /// 将任意类型转换为整型，当无法转换时返回 0 。
        /// </summary>
        /// <param name="id">object 类型。</param>
        /// <returns>返回转换结果。</returns>
        public static int GetInt(object id)
        {
            int newid;
            int.TryParse(GetString(id).Trim(), out newid);
            return newid;
        }
        /// <summary>
        /// 将任意类型转换为整型，当无法转换时返回 0 。
        /// </summary>
        /// <param name="id">object 类型。</param>
        /// <returns>返回转换结果。</returns>
        public static decimal GetDecimal(object id)
        {
            decimal newid;
            decimal.TryParse(GetString(id).Trim(), out newid);
            return newid;
        }
        /// <summary>
        /// 将任意类型转换为字符串型，当无法转换时返回空字符串。
        /// </summary>
        /// <param name="str">object 类型。</param>
        /// <returns>返回转换结果。</returns>
        public static string GetString(object str)
        {
            if (str == null)
                return String.Empty;
            else
                return str.ToString();
        }

        public static string GetDate(object dt)
        {
            if (String.IsNullOrEmpty(GetString(dt))) return string.Empty;
            DateTime newdt;
            DateTime.TryParse(GetString(dt), out newdt);
            return newdt.ToString("yyyy-MM-dd");
        }

        public static string GetDate(object dt, double minutes)
        {
            if (String.IsNullOrEmpty(GetString(dt))) return string.Empty;
            DateTime newdt;
            DateTime.TryParse(GetString(dt), out newdt);
            return newdt.AddMinutes(minutes).ToString("yyyy-MM-dd");
        }

        public static string GetTime(object dt)
        {
            if (String.IsNullOrEmpty(GetString(dt))) return string.Empty;
            DateTime newdt;
            DateTime.TryParse(GetString(dt), out newdt);
            return newdt.ToString("HH:mm:ss");
        }

        public static string GetTime(object dt, string format)
        {
            if (String.IsNullOrEmpty(GetString(dt))) return string.Empty;
            DateTime newdt;
            DateTime.TryParse(GetString(dt), out newdt);
            return newdt.ToString(format);
        }
        public static string GetTime(object dt, double min)
        {
            if (String.IsNullOrEmpty(GetString(dt))) return string.Empty;
            DateTime newdt;
            DateTime.TryParse(GetString(dt), out newdt);
            return newdt.AddMinutes(min).ToString("HH:mm:ss");
        }

        public static string GetDateTime(object dt, double minutes)
        {
            if (String.IsNullOrEmpty(GetString(dt))) return string.Empty;
            DateTime newdt;
            DateTime.TryParse(GetString(dt), out newdt);
            return newdt.AddMinutes(minutes).ToString("yyyy-MM-dd HH:mm:ss");
        }


        /// <summary>
        /// 将任意类型转换为日期型，当无法转换时返回 DateTime.MinValue 。
        /// </summary>
        /// <param name="dt">object 类型。</param>
        /// <returns>返回转换结果。</returns>
        public static DateTime GetDateTimeDT(object dt)
        {
            DateTime newdt;
            DateTime.TryParse(GetString(dt), out newdt);
            return newdt;
        }
        /// <summary>
        /// 将任意类型转换为日期型，当无法转换时返回 DateTime.MinValue 。
        /// </summary>
        /// <param name="dt">object 类型。</param>
        /// <returns>返回转换结果。</returns>
        public static DateTime GetDateTimeDT(object dt, double minutes)
        {
            DateTime newdt;
            DateTime.TryParse(GetString(dt), out newdt);
            return newdt.AddMinutes(minutes);
        }
        /// <summary>
        /// 将任意类型转换为整型，当无法转换时返回 0 。
        /// </summary>
        /// <param name="id">object 类型。</param>
        /// <returns>返回转换结果。</returns>

        public static string GetAbsoluteId(string CodeId)
        {
            if (CodeId == null) throw new ArgumentNullException("CodeId");
            if (CodeId.Length % 2 == 1) CodeId += "0";
            StringBuilder sb = new StringBuilder();
            string tmpstr;
            for (int i = 0; i < CodeId.Length; i += 2)
            {
                tmpstr = CodeId.Substring(i, 2);
                if (String.Compare(tmpstr, "00") != 0)
                {
                    sb.Append(tmpstr);
                }
            }
            return sb.ToString();
        }

        public static string GetImgUrl(string strhtml)
        {
            Regex reg = new Regex("<img [^>]+src=\"(.+)\"[^>]+>", RegexOptions.IgnoreCase);
            return reg.Match(strhtml).Groups[1].Value;
        }
        
        #endregion

    }
}
