﻿using CE.Utility.Text;
using DotNet4.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using RechargeService.Components.Common;
using RechargeService.Entitys;

namespace RechargeService.Components.RechargeService
{
    public abstract class RechargeBase : BaseClass
    {
        #region 属性

        /// <summary>
        /// 供应商ID
        /// </summary>
        public long SupplierId { get; set; }


        public string ApiCode { get; set; }

        /// <summary>
        /// 订单号
        /// </summary>
        public string OrderNo { get; set; }

        /// <summary>
        /// 电话号码
        /// </summary>
        public string TelePhone { get; set; }

        /// <summary>
        /// 充值金额
        /// </summary>
        public decimal RechargeMoney { get; set; }


        /// <summary>
        /// 号码类型1、座机，2、手机
        /// </summary>
        public int PhoneType { get; set; }

        /// <summary>
        /// 支付网关
        /// </summary>
        public string Gateway { get; set; }

        public string RechargePayDomain { get; set; }

        public string RechargeSearchUrl { get; set; }

        public string RechargeNoticeUrl { get; set; }

        public string FlowPayDomain { get; set; }

        public string FlowSearchUrl { get; set; }

        public string FlowNoticeUrl { get; set; }

        /// <summary>
        /// 商品ID
        /// </summary>
        public long ProductId { get; set; }


        public string ClientIp
        {
            get
            {
                System.Net.IPHostEntry myEntry = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName());
                string ipAddress = myEntry.AddressList[0].ToString();
                return ipAddress;
            }
        }

        #endregion


        #region 生成GET方法的参数字符串
        /// <summary>
        /// 生成GET方法的参数字符串
        /// </summary>
        /// <param name="formData"></param>
        /// <returns></returns>
        protected string GenGetParams(Dictionary<string, string> formData)
        {
            var sb = new StringBuilder();
            foreach (KeyValuePair<string, string> item in formData)
            {
                sb.AppendFormat("{0}={1}&", item.Key, item.Value);
            }

            return sb.ToString(0, sb.Length - 1);
        }
        #endregion

        #region 生成POST方法的表单并且提交到指定地址
        /// <summary>
        /// 生成POST方法的表单并且提交到指定地址
        /// </summary>
        /// <param name="formData"></param>
        /// <param name="formUrl"></param>
        /// <returns></returns>
        protected string GenFormHtml(Dictionary<string, string> formData, string formUrl)
        {
            var sb = new StringBuilder();
            sb.AppendFormat("<form name='frm1' id='frm1' method='POST' action='{0}'>", formUrl).Append(Environment.NewLine);
            foreach (KeyValuePair<string, string> item in formData)
            {
                sb.AppendFormat("<input type='hidden' id='{0}' name='{0}' value='{1}' />", item.Key, item.Value).Append(Environment.NewLine);
            }
            sb.Append("</form>").Append(Environment.NewLine);
            sb.Append("<script type ='text/javascript' language='javascript'>setTimeout('document.getElementById(\"frm1\").submit();',100);</script>").Append(Environment.NewLine);

            return sb.ToString();
        }
        #endregion

        #region 操作入口

        /// <summary>
        /// 充值
        /// </summary>
        /// <returns></returns>
        public virtual RechargeResult Process()
        {
            return null;
        }

        


        #endregion

        #region 得到Http对象

        protected HttpItem GetHttpItem(string url)
        {
            var item = new HttpItem()
            {
                URL = url,//URL     必需项
                Encoding = System.Text.Encoding.UTF8,//编码格式（utf-8,gb2312,gbk）
                Method = "POST",//URL
                Timeout = 100000,//连接超时时间
                ReadWriteTimeout = 30000,//写入Post数据超时时间
                //IsToLower = true,//得到的HTML代码是否转成小写     可选项默认转小写
                ProtocolVersion = new Version(1, 0),
                KeepAlive = false,
                Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",//    可选项有默认值
                ContentType = "application/x-www-form-urlencoded;charset=UTF-8",//返回类型    可选项有默认值
                Referer = ""//来源URL     可选项
            };
            return item;
        }
        #endregion

        #region 生成Http参数列表
        /// <summary>
        /// 生成Http参数列表
        /// </summary>
        /// <param name="ht"></param>
        /// <returns></returns>
        protected string GetParams(NameValueCollection ht)
        {
            if (null == ht || ht.Count == 0)
            {
                return "";
            }
            var sb = new StringBuilder();
            foreach (var key in ht.Keys.Cast<string>().Where(key => null != key))
            {
                sb.AppendFormat("{0}={1}&", key, ht[key].ToUrlEncode());
            }
            return sb.ToString(0, sb.Length - 1);
        }
        #endregion

        #region 反序列化JSON字符串
        /// <summary>
        /// 反序列化JSON字符串
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="json"></param>
        /// <returns></returns>
        public T DeserializeJson<T>(string json)
        {
            var jSetting = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore };
            var info = JsonConvert.DeserializeObject<T>(json, jSetting);
            return info;
        }
        #endregion

        #region 时间戳
        protected static string Timespan
        {
            get
            {
                var dt = DateTime.Now;
                var dt1 = new DateTime(0x7b2, 1, 1, 0, 0, 0);
                var ts = (TimeSpan)(dt - dt1);
                var tt = ts.TotalMilliseconds;
                return tt.ToString("0");
            }
        }
        #endregion
    }
}
