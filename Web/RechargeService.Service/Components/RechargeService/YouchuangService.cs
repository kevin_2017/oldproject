﻿using CE.Utility.Text;
using CE.Utility.Web;
using Common.DBUtility;
using Helper;
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Net;
using System.Xml;
using CE.Utility;
using Model;
using RechargeService.Entitys;

namespace RechargeService.Components.RechargeService
{
    public class YouchuangService : RechargeBase
    {
        #region 基础数据
        string fileName = DateTime.Now.ToString("yyyy-MM-dd");
        private string merCode = string.Empty;
        private string merKey = string.Empty;
        #endregion
        public override RechargeResult Process()
        {
            var resp = new RechargeResult();
            try
            {
                var keys = Tools.GetKeys(this.ApiCode);
                merCode = keys[0].ConfigValue;
                merKey = keys[1].ConfigValue;

                var uid = merCode;
                var od = OrderNo;
                var hm = TelePhone;
                var money = RechargeMoney;
                var bkurl = $"{this.Gateway}{this.RechargeNoticeUrl}";
                var key = $"uid={uid}&od={od}&hm={hm}&money={money}&sn={merKey}".ToMD5();

                var xmlStr = @"<items>" +
                  "<ob>pay</ob>" +
                  "<uid>" + uid + "</uid>" +
                  "<od>" + od + "</od>" +
                  "<hm>" + hm + "</hm>" +
                  "<bkurl>" + bkurl + "</bkurl>" +
                  "<money>" + money + "</money>" +
                  "<key>" + key + "</key>" +
                  "</items>";
                WriteWinFormLog.Write(fileName, "YouchuangService.Process", $"充值请求数据：{RechargePayDomain},{xmlStr}");
                var requestTime = DateTime.Now;
                // 从远程接口获取
                var httpRequestUtil = new HttpRequestUtil($"{RechargePayDomain}", HttpRequestUtil.Method.Post, HttpRequestUtil.ContentType.Xml, xmlStr);
                var httpResult = httpRequestUtil.Request();
                var html = httpResult.Html;
                WriteWinFormLog.Write(fileName, "YouchuangService.Process", $"充值返回数据：{html}");
                if (httpResult.StatusCode == HttpStatusCode.OK ||
            httpResult.StatusCode == HttpStatusCode.InternalServerError)
                {
                    if (!html.Contains("items"))
                    {
                        resp.Success = false;
                        resp.Message = $"访问第三方服务器出错:";
                    }
                    else
                    {
                        var xml = new XmlDocument();
                        xml.LoadXml(html);
                        var status = xml?.SelectSingleNode("items/state")?.InnerText;
                        if (status != null && status.Equals("8888"))
                        {
                            resp.Success = true;
                            resp.Message = "请求成功";
                        }
                        else
                        {
                            status = xml?.SelectSingleNode("items/err")?.InnerText;
                            var errMsg = "";

                            switch (status)
                            {
                                case "1000":
                                    errMsg = "接口关闭中";
                                    break;
                                case "1001":
                                    errMsg = "充值关闭中";
                                    break;
                                case "1002":
                                    errMsg = "签名错误";
                                    break;
                                case "1003":
                                    errMsg = "参数提交错误";
                                    break;
                                case "1004":
                                    errMsg = "地区维护或当日充值数超过五笔";
                                    break;
                                case "1005":
                                    errMsg = "余额不足";
                                    break;
                                case "1006":
                                    errMsg = "无此订单号";
                                    break;
                                case "1007":
                                    errMsg = "订单号重复";
                                    break;
                            }

                            resp.Success = false;
                            resp.Message = $"请求失败,返回信息：{status},{errMsg}";
                        }
                    }
                }
                else
                {
                    resp.Success = false;
                    resp.Message = $"访问第三方服务器出错,状态:{httpResult.StatusCode},状态码:{(int)httpResult.StatusCode}";
                }


                #region 增加充值请求及返回数据

                var orderResquestData = new Model.V8_OrderRequestRecs()
                {
                    SupplierId = SupplierId,
                    OrderNo = OrderNo,
                    RequestTime = requestTime,
                    RequestContent = xmlStr,
                    ResponseContent = $"{html},错误消息{ resp.Message }",
                };

                var requestId = new BLL.V8_OrderRequestRecs().Add(orderResquestData);

                //修改订单当前的请求ID
                DBHelper.ExecuteSql($"update V8_OrderManage set CurentStatus=3 ,CurrentRequestId={requestId}  where OrderNo='{OrderNo}'");

                #endregion
                WriteWinFormLog.Write(fileName, "YouchuangService.Process", $"充值请求结果：{JsonConvert.SerializeObject(resp)}");
            }
            catch (Exception ex)
            {
                WriteWinFormLog.WriteError(fileName, "YouchuangService.Process", ex);
            }
            return resp;
        }


    }
}
