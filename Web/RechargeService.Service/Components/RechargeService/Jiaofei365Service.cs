﻿using CE.Utility.Text;
using CE.Utility.Web;
using Common.DBUtility;
using Helper;
using RechargeService.Entitys;
using System;
using System.Collections.Generic;
using System.Net;
using Newtonsoft.Json;

namespace RechargeService.Components.RechargeService
{
    public class Jiaofei365Service : RechargeBase
    {
        #region 基础数据
        string fileName = DateTime.Now.ToString("yyyy-MM-dd");
        private string merCode = string.Empty;
        private string merKey = string.Empty;
        #endregion

        public override RechargeResult Process()
        {
            var resp = new RechargeResult();

            var keys = Tools.GetKeys(this.ApiCode);
            merCode = keys[0].ConfigValue;
            merKey = keys[1].ConfigValue;

            var account = merCode;
            var orderid = OrderNo;
            var phone = TelePhone;
            var amount = RechargeMoney;
            //if (amount % 10 != 0)
            //{
            //    resp.Success = false;
            //    resp.Message = "充值金额只能为10的倍数";
            //    return resp;
            //}
            var supplierInfo = new BLL.SUP_SupplierManage().GetModel(this.ApiCode);
            if (supplierInfo.Status != 1001 && supplierInfo.Status != 1004)
            {
                resp.Success = false;
                resp.Message = "该接口暂不能使用";
                return resp;
            }

            var callback = $"{this.Gateway}{this.RechargeNoticeUrl}";
            var sign = $"{phone}{account}{amount}{orderid}&{merKey}".ToMD5().ToUpper();

            var dic = new Dictionary<string, string>()
            {
                { "account",account},
                { "amount",amount.ToString("f0")},
                { "phone",phone},
                { "callback",callback},
                { "orderid",orderid},
                { "sign",sign},
            };
            var paramData = GenGetParams(dic);

            WriteWinFormLog.Write(fileName, "Jiaofei365Service.Process", $"充值请求数据：{RechargePayDomain},{paramData}");
            var requestTime = DateTime.Now;
            // 从远程接口获取
            var httpRequestUtil = new HttpRequestUtil($"{RechargePayDomain}?{paramData}", HttpRequestUtil.Method.Get);
            var httpResult = httpRequestUtil.Request();
            var html = httpResult.Html;
            WriteWinFormLog.Write(fileName, "Jiaofei365Service.Process", $"充值返回数据：{html}");
            if (httpResult.StatusCode == HttpStatusCode.OK || httpResult.StatusCode == HttpStatusCode.InternalServerError)
            {
                if (!html.Contains("account"))
                {
                    resp.Success = false;
                    resp.Message = $"访问第三方服务器出错";

                }
                else
                {
                    var result = JsonConvert.DeserializeAnonymousType(html, new { account = "", phone = "", amount = "", billid = "", message = "", orderid = "", state = "", cut = "", sign = "" });
                    if (result.state.Equals("1") || result.state.Equals("2"))
                    {
                        resp.Success = true;
                        resp.Message = "请求成功";
                    }
                    else
                    {
                        var msg = "";
                        switch (result.state)
                        {
                            case "0":
                                msg = "订单提交失败";
                                break;
                            case "1":
                                msg = "缴费成功";
                                break;

                            case "2":
                                msg = "缴费中";
                                break;
                            case "-1":
                                msg = "未找到该订单";
                                break;
                            case "-7":
                                msg = "业务关闭";
                                break;

                            case "-8":
                                msg = "系统扎帐,暂停提交";
                                break;
                        }

                        resp.Success = false;
                        resp.Message = $"请求失败,返回信息：{result.state}，{msg}";
                    }
                }
            }
            else
            {
                resp.Success = false;
                resp.Message = $"访问第三方服务器出错,状态:{httpResult.StatusCode},状态码:{(int)httpResult.StatusCode}";
            }
            #region 增加充值请求及返回数据

            var orderResquestData = new Model.V8_OrderRequestRecs()
            {
                SupplierId = SupplierId,
                OrderNo = OrderNo,
                RequestTime = requestTime,
                RequestContent = paramData,
                ResponseContent = $"{html},{resp.Message}"
            };

            var requestId = new BLL.V8_OrderRequestRecs().Add(orderResquestData);
            //修改订单当前的请求ID
            DBHelper.ExecuteSql($"update V8_OrderManage set CurentStatus=3,CurrentRequestId={requestId}   where OrderNo='{OrderNo}'");

            #endregion
            WriteWinFormLog.Write(fileName, "Jiaofei365Service.Process", $"充值请求结果：{JsonConvert.SerializeObject(resp)}");

            return resp;
        }

    }
}
