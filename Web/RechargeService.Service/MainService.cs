﻿using RechargeService.Components.Services;
using System.ServiceProcess;

namespace RechargeService.Service
{
    public partial class MainService : ServiceBase
    {
        public MainService()
        {
            InitializeComponent(); 
        }

        protected override void OnStart(string[] args)
        {
            Start();
        }

        public void Start()
        {
            Components.Services.RechargeService rechargeService = new Components.Services.RechargeService();
            rechargeService.GetRecordData();

            NotifyService notifyService = new NotifyService();
            notifyService.GetNotifyData();
        }

        protected override void OnStop()
        {
        }
    }
}
