﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RechargeService.Entitys
{
    public class RechargeResult
    {
        public bool Success
        {
            get;
            set;
        }
        public int OrderNo
        {
            get;
            set;
        }
        public string Message
        {
            get;
            set;
        }

        
    }
}
