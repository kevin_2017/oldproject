﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.DBUtility
{
   public class CommonActive
    {
       public string GetRndStr(int num)
       {
           string Vchar = "1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,a,b,d,e,f,h,q,r,t,u,y";
           string[] vcArray = Vchar.Split(',');
           string checkCode = string.Empty;
           Random rand = new Random();
           for (int i = 0; i < num; i++)
           {
               int t = rand.Next(vcArray.Length);
               checkCode += vcArray[t];
           }
           return checkCode;
       }
       public string GetRandomCode(int CodeCount)
       {
           string allChar = "0,1,2,3,4,5,6,7,8,9";
           string[] allCharArray = allChar.Split(',');
           string RandomCode = "";
           int temp = -1;

           Random rand = new Random();
           for (int i = 0; i < CodeCount; i++)
           {
               if (temp != -1)
               {
                   rand = new Random(temp * i * ((int)DateTime.Now.Ticks));
               }

               int t = rand.Next(10);

               while (temp == t)
               {
                   t = rand.Next(10);
               }

               temp = t;
               RandomCode += allCharArray[t];
           }

           return RandomCode;
       }
    }
}
