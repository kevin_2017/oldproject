﻿using System;//
using System.Collections.Generic;//
using System.Linq;//
using System.Text;//

namespace Common.DBUtility
{
    public class Enumeration
    {
        public const int OrderStatus_PAYMENTSDUE = 1;           //		订单生成，等待买家付款
        public const int OrderStatus_WAITINGFORDELIVERY = 2;    //		买家已付款，等待卖家发货
        public const int OrderStatus_WAITINGTORECEIVE = 3;      //		卖家已发货，等待买家收货
        public const int OrderStatus_SIGNED = 4;                //		买家已签收，
        public const int OrderStatus_CLOSEDNORMAL = 5;          //		交易关闭，正常结束
        public const int OrderStatus_CLOSEDCUSTOMERCANCEL = 6;  //		交易关闭，买家取消订单
        public const int OrderStatus_CLOSEDTIMEOUT = 7;         //		交易关闭，买家超时未支付，系统自动关闭

        public const decimal FreePostageThresholds = 20;        //      免邮费阈值
        public const string EXPRESS = "EXPRESS";                //      快递代码
        public const string FREEPOSTAGE = "FREEPOSTAGE";        //      免邮代码
    }
}
