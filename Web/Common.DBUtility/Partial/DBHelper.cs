﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
 
using System.Data.SqlClient; 

namespace Common.DBUtility
{
    public partial class DBHelper
    {
        public static DataTable QuerySQLByPaging(PageAttribute pager)
        {
            SqlParameter tablename = new SqlParameter("@TABLENAME", SqlDbType.NVarChar, 4000) {Value = pager.TableName};

            SqlParameter pageindex = new SqlParameter("@PAGEINDEX", SqlDbType.Int, 4) {Value = pager.PageIndex};

            SqlParameter pagesize = new SqlParameter("@PAGESIZE", SqlDbType.Int, 4) {Value = pager.PageSize};

            SqlParameter where = new SqlParameter("@WHERE", SqlDbType.NVarChar, 4000)
            {
                Value = pager.WhereCondition
            };

            SqlParameter columns = new SqlParameter("@COLUMNS", SqlDbType.NVarChar, 4000) {Value = pager.Columns};

            SqlParameter sortcolumn =
                new SqlParameter("@SORTCOLUMN", SqlDbType.NVarChar, 4000) {Value = pager.StrOrder};

            SqlParameter linkquery = new SqlParameter("@LINKQUERY", SqlDbType.NVarChar, 4000) {Value = pager.LinkQuery};


            SqlParameter totalRowsCount =
                new SqlParameter("@TOTALROWSCOUNT", SqlDbType.Int, 4) {Direction = ParameterDirection.Output};


            List<SqlParameter> list = new List<SqlParameter>
            {
                tablename,
                pageindex,
                pagesize,
                @where,
                columns,
                sortcolumn,
                linkquery,
                totalRowsCount
            };

            DataTable table = ExecuteProcedureOutputTable("Proc_QueryTable", list);
            pager.TotalRowCount = (int)totalRowsCount.Value;
            return table;
        }
        public static DataTable ExecuteProcedureOutputTable(string proc_Name, List<SqlParameter> list)
        {
            DataTable dt = new DataTable();

            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    DataSet ds = new DataSet();
                    connection.Open();
                    SqlDataAdapter sqlDA = new SqlDataAdapter();
                    SqlCommand cmd = new SqlCommand();
                    sqlDA.SelectCommand = cmd;

                    foreach (SqlParameter sp in list)
                    {
                        cmd.Parameters.Add(sp);
                    }

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = proc_Name;
                    cmd.Connection = connection;

                    sqlDA.Fill(ds);
                    dt = ds.Tables.Count > 0 ? ds.Tables[0] : new DataTable();

                    connection.Close();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }


            return dt;
        }
        public static DataSet ExecuteProcedureOutputTable(string proc_Name, SqlParameter[] list)
        {
            DataSet ds = new DataSet();

            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    SqlDataAdapter sqlDA = new SqlDataAdapter();
                    SqlCommand cmd = new SqlCommand();
                    sqlDA.SelectCommand = cmd;

                    foreach (SqlParameter sp in list)
                    {
                        cmd.Parameters.Add(sp);
                    }

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = proc_Name;
                    cmd.Connection = connection;

                    sqlDA.Fill(ds);

                    connection.Close();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }


            return ds;
        }
    }
}


/*

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROC [dbo].[Proc_QueryTable]
@TABLENAME NVARCHAR(4000),
@PAGEINDEX INT,
@PAGESIZE INT,
@WHERE NVARCHAR(4000),
@COLUMNS NVARCHAR(4000),
@SORTCOLUMN NVARCHAR(255),
@LINKQUERY NVARCHAR(4000),
@TOTALROWSCOUNT INT OUTPUT
AS
DECLARE @SQL NVARCHAR(4000)
DECLARE @STARTROWNUMBER INT
DECLARE @ENDROWNUMBER INT
DECLARE @ENDINDEX INT
SET @SQL='SELECT @A=COUNT(1) FROM '+@TABLENAME +' AS A WHERE 1=1' + @WHERE
EXEC SP_EXECUTESQL @SQL,N'@A INT OUTPUT',@TOTALROWSCOUNT OUTPUT
IF @PAGESIZE IS NULL OR @PAGESIZE = 0
BEGIN
	SET @STARTROWNUMBER = 1  
	SET @ENDROWNUMBER = @TOTALROWSCOUNT
END
ELSE
BEGIN 
	SET @STARTROWNUMBER = @PAGESIZE * @PAGEINDEX + 1  
	SET @ENDROWNUMBER  = @STARTROWNUMBER + @PAGESIZE - 1; 
END
SET @SQL=
'SELECT '
+@COLUMNS+
' FROM (
	SELECT '
		+@COLUMNS+',
		ROW_NUMBER() OVER(ORDER BY '+@SORTCOLUMN+') ROWNUMBER
		FROM '+@TABLENAME +' AS TABLE1 
			WHERE 1=1'+
			@WHERE+') '+@TABLENAME+' '+ 
				CASE  WHEN @LINKQUERY='' THEN '' WHEN @LINKQUERY<>'' THEN @LINKQUERY END +
				' AND ROWNUMBER >= @STARTROWNUMBER AND
				ROWNUMBER <= @ENDROWNUMBER'

EXEC SP_EXECUTESQL @SQL,N'@STARTROWNUMBER INT,@ENDROWNUMBER INT',@STARTROWNUMBER,@ENDROWNUMBER";
        internal string PagerStringModel = @"";
 */