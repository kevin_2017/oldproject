﻿using System;
using System.Configuration;
namespace Common.DBUtility
{

    public class PubConstant
    {
        /// <summary>
        /// 获取连接字符串
        /// server=218.70.16.202;database=V8_TechnologyLtd;User ID=sa;Password=la_crv_365;
        /// </summary>
        public static string V8RechargeSystem
        {
            get
            {
                string _connectionString = "";
                string connectionName = "ConnString";
                string Base64key = "qwert1!83asd7~5qwe96@6ws#x96@@@6$";

                if (ConfigurationManager.ConnectionStrings[connectionName] == null)
                {
                    var paramName = ("数据库连接失败，找不到配置信息：" + connectionName);

                    throw new ArgumentNullException(paramName);
                }
                else
                {
                    string _providerName = ConfigurationManager.ConnectionStrings[connectionName].ProviderName;
                    if (ConfigurationManager.ConnectionStrings["ConnStringEncrypt"] == null || !bool.Parse(ConfigurationManager.ConnectionStrings["ConnStringEncrypt"].ConnectionString))
                        _connectionString = ConfigurationManager.ConnectionStrings[connectionName].ConnectionString;
                    else
                        _connectionString = DESEncrypt.Decrypt(ConfigurationManager.ConnectionStrings[connectionName].ConnectionString, Base64key);

                    return _connectionString;
                }
                 
            }
        }

    }
}
