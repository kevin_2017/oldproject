﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Reflection;
namespace Helper
{
    /// <summary>
    /// 反射 
    /// </summary>
    public class ReflectionHelper
    {
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="row"></param>
        /// <returns></returns>
        public static T DataRowToModel<T>(DataRow row) where T : class,new()
        {
            Type type = typeof(T);
            T entity = new T();
            PropertyInfo[] pArray = type.GetProperties();

            foreach (PropertyInfo p in pArray)
            {
                try
                {
                    if (row[p.Name] is Int64)
                    {


                        p.SetValue(entity, Convert.ToInt32(row[p.Name]), null);

                        continue;
                    }
                    p.SetValue(entity, row[p.Name], null);
                }
                catch (Exception)
                {
                    //异常
                }
            }
            return entity;
        }
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="row"></param>
        /// <returns></returns>
        public static T DataRowToModel<T>(DataSet ds) where T : class,new()
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                Type type = typeof(T);
                T entity = new T();
                PropertyInfo[] pArray = type.GetProperties();
                DataRow row = ds.Tables[0].Rows[0];
                foreach (PropertyInfo p in pArray)
                {

                    try
                    {
                        if (row[p.Name] is Int64)
                        {


                            p.SetValue(entity, Convert.ToInt32(row[p.Name]), null);

                            continue;
                        }
                        p.SetValue(entity, row[p.Name], null);
                    }
                    catch (Exception)
                    {
                        //异常
                    }
                } return entity;
            }
            else
            {
                return null;
            }

        }
        /// <summary>
        /// Datatable 转 list<对象>
        /// </summary>
        /// <typeparam name="T">对象</typeparam>
        /// <param name="dt">Datatable</param>
        /// <returns>list<对象></returns>
        public static List<T> DataTableToList<T>(DataTable dt) where T : class,new()
        {
            Type type = typeof(T);
            List<T> list = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                PropertyInfo[] pArray = type.GetProperties();
                T entity = new T();
                foreach (PropertyInfo p in pArray)
                {
                    if (row[p.Name] is Int64)
                    {
                        try
                        {
                            p.SetValue(entity, Convert.ToInt32(row[p.Name]), null);
                        }
                        catch (Exception)
                        {
                            //异常
                        }

                        continue;
                    }
                    p.SetValue(entity, row[p.Name], null);
                }
                list.Add(entity);
            }
            return list;
        }


        public static List<T> DataTableToList<T>(DataSet ds) where T : class,new()
        {
            Type type = typeof(T);
            List<T> list = new List<T>();
            if (ds != null)
            {
                if (ds.Tables != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        DataTable dt = ds.Tables[0];

                        foreach (DataRow row in dt.Rows)
                        {
                            PropertyInfo[] pArray = type.GetProperties();
                            T entity = new T();
                            foreach (PropertyInfo p in pArray)
                            {

                                if (!(row[p.Name] is System.DBNull))//是否为空
                                {
                                    if (row[p.Name] is Int64)
                                    {
                                        try
                                        {
                                            p.SetValue(entity, Convert.ToInt32(row[p.Name]), null);
                                        }
                                        catch (Exception)
                                        {
                                            //异常
                                        }

                                        continue;
                                    }
                                    //if (row[p.Name] is )
                                    //{
                                    //    try
                                    //    {
                                    //        p.SetValue(entity, Convert.ToInt32(row[p.Name]), null);
                                    //    }
                                    //    catch (Exception)
                                    //    {
                                    //        //异常
                                    //    }

                                    //    continue;
                                    //}
                                    // if (row[p.Name])
                                    p.SetValue(entity, row[p.Name], null);
                                }

                            }
                            list.Add(entity);
                        }
                        // return list;
                    }
                }
            }

            //        else
            //        {
            //            return null;
            //        }
            //    }
            //    else
            //    {
            //        return null;
            //    }
            //}
            //else
            //{
            //    return null;
            //}

            //}
            return list;
        }
    }
}
