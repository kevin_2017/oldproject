﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Helper
{
    public class TmallErrorCode
    {
        public string GetErrorDes(string errorCode)
        {
            string result = "";

            switch (errorCode)
            {

                case "0101": result = "参数缺失"; break;
                case "0102": result = "验签不正确"; break;
                case "0103": result = "参数格式非法，例如期望是整数的参数传递过来一串字符"; break;
                case "0104": result = "时间戳相差过大"; break;
                case "0201": result = "被充值账户与充值产品不匹配，例如移动的产品填写了一个联通的手机号之类"; break;
                case "0202": result = "被充值账户存在问题，导致无法充值成功，例如账户已经被注销之类"; break;
                case "0203": result = "购买数量非法，例如接口只支持单笔充值，传过来的购买数量却不等于1之类"; break;
                case "0204": result = "请求中的spuId对应不到充值产品"; break;
                case "0402": result = "商品库存不足"; break;
                case "0404": result = "运营商例行维护"; break;
                case "0405": result = "供应商自身例行维护"; break;
                case "0410": result = "当前是出账日期"; break;
                case "0411": result = "客户非实名制客户"; break;
                case "0412": result = "该客户运营商的黑名单客户"; break;
                case "0413": result = "该号码不存在"; break;
                case "0414": result = "该号码当前状态【%】异常"; break;
                case "0415": result = "4G用户不能订购3G流量"; break;
                case "0416": result = "该号码已欠费"; break;
                case "0417": result = "该用户服务密码是初始密码"; break;
                case "0418": result = "该号码存在在途单"; break;
                case "0419": result = "该号码已经超过运营商的充值次数限制"; break;
                case "0420": result = "2G号码不支持订购"; break;
                case "9999": result = "未知错误"; break;
            }
            return result;
        }
    }
}
